/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server.execution.platform;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import org.jdom.Document;
import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.XMLConstants;
import eu.telecom_bretagne.praxis.common.Utile.ResourceAvailability;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.Process.ProcessContext;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.server.Server;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine.ExecutionEngineConfiguration;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine.ExecutionEngineConfigurationFactory;




/**
 * @author Sébastien Bigaret
 */
public class PlatformDescription
{
	public static class CannotExecuteException
	    extends Exception
	{
		private static final long serialVersionUID = 8488070778532798704L;

		public CannotExecuteException(String msg)
		{
			super(msg);
		}

		public CannotExecuteException(String msg, Throwable cause)
		{
			super(msg, cause);
		}
	}
	
	/**
	 * Builds and return a new PlatformDescription based on the provided informations
	 * @param server
	 * @param doc
	 * @param availabilityMap
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static PlatformDescription buildPlatformDescription(Server server,
	                                                           Document doc,
	                                                           Map<ResourceAvailability, List<String>> availabilityMap)
	throws IllegalArgumentException
	{
		PlatformDescription platform = new PlatformDescription(server, doc, availabilityMap);
		ExecutionEngineConfigurationFactory factory = ExecutionEngine.getConfigurationFactory(platform.getType());
		if (factory==null)
			throw new IllegalArgumentException("Unknown execution engine: "+platform.getType());
		platform.executionEngineConfiguration = factory.build(platform); // throws IllegalArgumentExc. as well
		return platform;
	}
	
	public static void executeActivity(Activity activity) throws CannotExecuteException
	{
		PlatformDescription platform = activity.getPlatform();

		if (platform != null)
		{
			platform.execute(activity);
		}
		else
		{
			throw new CannotExecuteException("Aucune ressource n'est actuellement disponible pour exécuter le programme\n"
			                                 +"There is currently no resources able to execute the program"); //i18n ce genre de message pourrait p.ex. être taggué I18N et transformé du côté client (pour le moment pas possible, on écrit direct dans un fichier, que ce soit ici ou du côté plateforme) 
		}
	}

	/** The type of a platform is used to determine its execution engine. Example: shell, SGE */
	private String type;

	/** The name of the operating system on which the platform runs */
	private String os;

	protected ExecutionEngineConfiguration        executionEngineConfiguration;
	
	/**
	 * Connection between the server and the platform
	 */
	private Server                                server;
	
	private TreeMap<String, ResourceAvailability> resources_availability = new TreeMap<String, ResourceAvailability>();
	
	/**
	 * Constructor.
	 * @param connection
	 *            connection between the serveur and the platform
	 * @param platformDescription
	 *            xml description sent by the platform
	 */
	public PlatformDescription(Server server, Document platformDescription,
	                           Map<ResourceAvailability, List<String>> availabilityMap)
	{
		// platformDescription is ignored here
		this.server = server;
		
		parseDescription(platformDescription);
		
		for (Map.Entry<ResourceAvailability, List<String>> avail_prg: availabilityMap.entrySet())
			for (String prgID: avail_prg.getValue())
			{
				if (!RemoteComponents.resourceRepository().isDeclared(prgID))
					server.sendMessage("WARNING: program " + prgID + " is unknown on the resource repository: ignored");
				else
					resources_availability.put(prgID, avail_prg.getKey());
			}
	}
	
	private Map <String,String> configuration = new LinkedHashMap<String, String>();
	
	/**
	 * Parses the description and initializes {@link #os}, {@link #type} and {@link #configuration}.
	 * @param description
	 *            the description of a platform
	 * @throws IllegalArgumentException
	 *             if the description is invalid.
	 */
	private void parseDescription(Document description)
	throws IllegalArgumentException
	{
		Element xml = description.getRootElement();

		type = xml.getAttributeValue("type");
		if (type==null || "".equals(type.trim()))
			throw new IllegalArgumentException("Platform's description has an invalid null or empty type");
		type = type.trim();
		
		os = xml.getAttributeValue(XMLConstants.PLATFORM_OS_ATTR_TAG);

		Element config = xml.getChild("configuration");
		if (config!=null)
		{
			for (Object oitem: config.getChildren("item"))
			{
				final Element item = (Element) oitem;
				final String name = item.getAttributeValue("name");
				final String value = item.getAttributeValue("value");
				final String text  = item.getText();
				if ("".equals(name))
				{
					Log.log.warning(()->"Platform type:"+type+" declares a config. item with an empty name. IGNORING");
					continue;
				}
				if (value==null && "".equals(text))
				{
					Log.log.warning(()->"Platform type:"+type+" declares a config. item "+name
					                +" with no attribute 'value' and no character data. IGNORING");
					continue;
				}
				if (value!=null && !"".equals(text))
					Log.log.warning(()->"Platform type:"+type+" declares a config. item "+name
					                +" with an attribute value AND character data. Attribue value is IGNORED");
				if (configuration.containsKey(name))
				{
					Log.log.warning(()->"Platform type:"+type+" redefines the config. item "+name+". IGNORING");
					continue;
				}
				if (!"".equals(text))
					configuration.put(name, text);
				else
					configuration.put(name, value); //value cannot be null, cf. tests above.
				}
		}
		// programs are ignored for the moment being, they are also present in the availability map
	}

	public String getType()
	{
		return this.type;
	}
	
	public String getOS()
	{
		return this.os;
	}
	
	/**
	 * Returns the names of the configuration items.
	 * @return names of the configuration items.
	 * @see #getConfiguration(String)
	 */
	public String[] getConfigurationKeys()
	{
		return this.configuration.keySet().toArray(new String[]{});
	}
	
	/**
	 * Returns the value stored in the configuration under this key.
	 * @param key
	 *            the name of a configuration item
	 * @return the corresponding value. A {@code null} value indicates that the key was not found.
	 */
	public String getConfiguration(String key)
	{
		return configuration.get(key);
	}
	
	/**
	 * Returns the value stored in the configuration under this key.
	 * @param key
	 *            the name of a configuration item
	 * @param defaultValue
	 *            the default value to be returned if key is not found.
	 * @return the corresponding value, or the default value if the key was not found.
	 */
	public String getConfiguration(String key, String defaultValue)
	{
		final String value = configuration.get(key);
		return ( value!=null ) ? value: defaultValue;
	}

	/**
	 * Tells whether a given program is declared by the platform.
	 * @param id
	 *            the id of the program
	 * @return true if the program is declared by the platform
	 */
	public boolean isDeclared(String id)
	{
		return resources_availability.containsKey(id);
	}
	
	/**
	 * Checks whether a given program is available on this platform.
	 * @param prgID
	 *            the program's ID. It should not be null.
	 * @return
	 */
	public boolean isAvailable(String prgID)
	{
		return ResourceAvailability.AVAILABLE.equals(resources_availability.get(prgID));
	}
	
	public void updateResourcesAvailability(Map<ResourceAvailability, List<String>> resources_avail)
	{
		List<String> prgs_ids;
		prgs_ids = resources_avail.get(ResourceAvailability.AVAILABLE);
		if (prgs_ids != null)
			for (String prg_id: prgs_ids)
			{
				if (!RemoteComponents.resourceRepository().isDeclared(prg_id))
					server.sendMessage("WARNING: program " + prg_id + " is unknown on the resource repository: ignored");
				else
					resources_availability.put(prg_id, ResourceAvailability.AVAILABLE);
			}
		prgs_ids = resources_avail.get(ResourceAvailability.NOT_AVAILABLE);
		if (prgs_ids != null)
			for (String prg_id: prgs_ids)
			{
				if (!RemoteComponents.resourceRepository().isDeclared(prg_id))
					server.sendMessage("WARNING: program " + prg_id + " is unknown on the resource repository: ignored");
				else
					resources_availability.put(prg_id, ResourceAvailability.NOT_AVAILABLE);
			}
	}

	/**
	 * Prepares and sends to the corresponding platform the necessary material for executing the activity.
	 * @param activity
	 *            the activity to execute
	 * @throws CannotExecuteException
	 *             if the resource cannot be executed
	 */
	public void execute(Activity activity) throws CannotExecuteException
	{
		ProcessContext processContext = activity.getContainer().getContext();
		ExecutionEngine executionEngine = executionEngineConfiguration.getEngine(processContext.workflowID,
		                                                                         processContext.executionID);
		
		executionEngine.prepareExecution(activity, executionEngineConfiguration);
		
		Map<String, File> filesNeededForExec = new HashMap<String, File>();
		//List<String> prg_inputFiles = activity.getProgram().getInputFiles();
		// TODO remplacer ça: getInputFiles() renvoie des infos qu'il n'a pas à avoir
		// en particulier le nom des fichiers de sortie des progs précédents!

		final ProcessContext pContext = activity.getContainer().getContext();
		for (Program program: activity.getExecutionSet().programsSortedForExecution())
		{
		final String prgID = program.getProgramID();
		final ArrayList<File> missingFiles = new ArrayList<File>();
		final ArrayList<String> missingOutputs = new ArrayList<String>();
		final Result currentResult = activity.getContainer().getCurrentExecutionSummary();
		for (Parameter input: program.getInputParameters())
		{
			if (!input.isActivated())
				continue;
			// at this point we know they are all connected, this has been checked in prepareForExecution(), above
			if (input.getPrgInput()!=null)
			{
				if (activity.getExecutionSet().contains(input.getPrgInput().getProgram()))
					continue; 
				final Parameter predecessorOutputParam = input.getPrgInput();
				// search the corresponding file generated by the preceding program
				String prgInputId = predecessorOutputParam.getDescription().getId();
				File expectedFile = currentResult.getProgramInfo(predecessorOutputParam.getProgram().getProgramID()).getOutput(prgInputId);
				if (expectedFile!=null)
				{
					final String relativePath = expectedFile.getPath();
					expectedFile = new File(pContext.resultsDirectory(), relativePath);
					if (!expectedFile.exists())
						missingFiles.add(new File(relativePath));
					else if (!filesNeededForExec.containsKey(relativePath))
						filesNeededForExec.put(relativePath, expectedFile);
				}
				else
				{ 
					// there is no entry in result but still, execute() was called
					// TODO voir si ça peut arriver quand ERROR à cause de missing files, mais qu'on essaie quand même
					// de continuer ce qui peut l'être (check exec.loop, pour le moment c'est probablement impossible
					// mais ce serait pas mal de savoir le faire)
					missingOutputs.add(input.getPrgInput().output_xml_id());
				}
			}
			else // search for a file supplied along with the workflow
			{
				String[] filepaths = input.getInput().filepaths();
				for (String filepath: filepaths)
				{
					final File inputFile = new File(pContext.workingDirectory(), filepath);
					if (!inputFile.exists())
						missingFiles.add(inputFile);
					else if (!filesNeededForExec.containsKey(filepath))
						filesNeededForExec.put(filepath, inputFile);
				}
			}
		}

		if (!missingFiles.isEmpty() || !missingOutputs.isEmpty())
		{
			StringBuilder msg = new StringBuilder();
			msg.append("The following elements:");
			if (!missingFiles.isEmpty())
				msg.append("\n- File").append(missingFiles.size()>1?"s:":":");
			for (File missingFile: missingFiles)
				msg.append(missingFile.getPath()).append(", ");
			if (!missingOutputs.isEmpty())
				msg.append("\n- Programs' output").append(missingOutputs.size()>1?"s:":":");
			for (String missingOutput: missingOutputs)
				msg.append(missingOutput).append(", ");
			msg.append("\nneeded to execute the workflow could not be found -- aborting execution ");
			msg.append("\n(debug info: prg's ID is: ").append(prgID).append(")");
			Log.log.severe(() -> msg.toString());
			throw new CannotExecuteException(msg.toString());
		}
		}
		// compress...
		File zipFile = null;
		if (!filesNeededForExec.isEmpty())
		{
			zipFile = new File(pContext.workingDirectory(), "script-" + activity.getKey() + ".zip");
			try
			{
				Utile.zipFiles(filesNeededForExec, zipFile);
			}
			catch (IOException e)
			{
				final String err="Got an exception while trying to send the necessary files for execution";
				Log.log.log(Level.SEVERE, err, e);
				throw new CannotExecuteException(err, e);
			}
		}
		executionEngine.setKey(activity.getKey());
		server.requestExecution(executionEngine, zipFile);
	}
	
	@Override
	public String toString()
	{
		return server.toString();
	}
	
	/*  PlatformToServerEventListener methods */
	public void platformLogout(Event event)
	{
	// TODO Auto-generated method stub
	
	}
	
	public void platformSendsResults(Event event)
	{
	// TODO Auto-generated method stub
	
	}
	
}
