/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.server.Server;


public class DirectCommunicationFacade extends CommunicationFacade
{
    /** The queue of events waiting to be handled by the main loop */
	private ArrayList<Event> events_queue = new ArrayList<Event>();
    /** The number of permits the semaphore holds is exactly the size of {@link #events_queue}.**/
    private Semaphore events_queue_semaphore = new Semaphore(0, true);
    
    protected DirectCommunicationFacade other_end;
    protected boolean disconnected = false;
    
    /**
     * Static initializer is empty: client and platform connects to the server
     * using the accept() method
     */
    static { /* left intentionally empty */ }
    
    public static void accept(DirectCommunicationFacade platformFacade)
    {
        DirectCommunicationFacade serveur_dcf = new DirectCommunicationFacade("Serveur (direct)");
        new Server(serveur_dcf);
        serveur_dcf.connect(platformFacade);
        platformFacade.connect(serveur_dcf);
        serveur_dcf.start();
    }
    
    public DirectCommunicationFacade(String name)
    {
        super(name);
    }
    
    public synchronized void connect(DirectCommunicationFacade platformFacade)
    {
        other_end = platformFacade;
        notify();
    }

    @Override
    public boolean isConnected()
    {
        return ( other_end != null );
    }
    
    @Override
    public void disconnect(Exception exc)
    {
		DirectCommunicationFacade the_other_end = null;
    	synchronized(this)
    	{
    			super.disconnect(exc);
    			the_other_end = other_end;
    			other_end = null;
    			disconnected = true;
    	}
    	if (the_other_end!=null)
    		the_other_end.disconnect();
    }
    
    /** This is the method one should use to enqueue new events.
     * The running thread is responsible for calling {@link #handle_event(Event)},
     * which should NOT be called by hand.
     */
    public void addEvent(Event event)
    {
    	synchronized(events_queue)
    	{
    		events_queue.add(event);
    		events_queue_semaphore.release();
    	}
    }

    @Override
    public void send(Event event)
    {
    	Event clone = null;
    	clone = event.clone();
        if (clone.file_conveyor!=null) {
            File tmp = null;
            try
            {
                tmp = File.createTempFile("praxis_", ".dcf");//direct comm facade
                Utile.copyFile(clone.file_conveyor, tmp);
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            clone.file_conveyor = tmp;
        }
        other_end.addEvent(clone);
        eventSent(event);
    }
    
    /** The corresponding thread first waits for connect() being called, 
     * then handles every queued event. The thread can be started before it
     * is actually connected, and events can be queued before the effective
     * connection as well.
     */
    @Override
    public void run()
    {
        while (other_end == null && !disconnected)
            synchronized(this)
            {
                try{ wait(1); } catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
            }
        final ArrayList <Event> events_copy = new ArrayList<Event>();
        while (other_end != null && !disconnected) {
        	try {
        		// acquire just one: if more events are available, we will acquire the number of the handled
        		// events minus one at the end of this loop
        		events_queue_semaphore.acquire();
        		synchronized(events_queue)
        		{
        			if (events_queue.size()!=0)
        			{
        				events_copy.addAll(events_queue);
        			}
        		}
        	}
        	catch(InterruptedException e)
        	{
        		break;
        	}
        	int events_copy_size = events_copy.size();
        	Log.log.log(Level.FINEST, ()->"events_copy length:"+events_copy_size);
        	for (Event event: events_copy)
        	{
        		try
        		{
        			handle_event(event);
        		}
        		catch (Exception e) // TODO may be this should be specified at the CommunicationFacade level
        		{
        			Log.log.log(Level.SEVERE, "Unhandled exception in loop", e);
        		}
        	}
        	// now remove those which have been handled
        	synchronized(events_queue)
        	{
        		for (int i=0; i<events_copy_size; i++)
        		{
        			events_queue.remove(0);
        		}
        		if (events_copy_size>1)
        			events_queue_semaphore.acquireUninterruptibly(events_copy_size-1); // -1: an acquire() has already been done earlier
				events_copy.clear();
        	}
        }
    }
    
}
