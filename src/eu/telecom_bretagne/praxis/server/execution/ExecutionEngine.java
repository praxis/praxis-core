/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server.execution;

import java.io.File;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult.ProgramStatus;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;
import eu.telecom_bretagne.praxis.server.execution.platform.ProgramExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.platform.ShellExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.platform.WorkflowExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;



public abstract class ExecutionEngine
    implements Serializable
{
	private static final long serialVersionUID = -4817345004845418591L;

	public static abstract class ExecutionEngineConfiguration
	    implements Serializable
	{
        private static final long serialVersionUID = -4885632643969386155L;

        /** cet objet n'est disponible que sur le serveur, pas sur la plateforme */
		protected transient PlatformDescription platform;
		
		public ExecutionEngineConfiguration(PlatformDescription platform)
		{
			this.platform = platform;
		}
		
		public abstract ExecutionEngine getEngine(WorkflowID workflowID, ExecutionID executionID);
	}
	
	public static interface ExecutionProgressMonitor
	{
		/**
		 * Sets the progress of an execution.
		 * @param result
		 *            the result object holding information on the progress of
		 *            the execution
		 */
		public abstract void setProgress(final Result result);

		/**
		 * {@link #setProgress(Result) Sets progress} and treats {@code result} as if its status is
		 * {@link Result.Status#RUNNING RUNNING}.  A caller uses this when it builds a Result object that will be
		 * ultimately sent as the final one (i.e. merging partial results with OK, WARNING and ERROR status during
		 * execution), and that it wishes to communicate as execution progress.
		 * @param result the result to set.
		 */
		public default void setRunningExecutionProgress(final Result result)
		{
			Result running = new Result(result);
			running.setStatus(Result.Status.RUNNING);
			setProgress(running);
		}
	}

	/**
	 * Should be implemented by any concrete factory for {@link PlatformDescription platform visitors}. Concrete
	 * PlatformVisitors should register themselves in the factories' repositories using
	 * {@link PlatformDescription#registerConfigurationFactory(ExecutionEngineFactory)}; this is usually done either
	 * w/ a static inner class defined in the concrete <code>PlatformVisitor</code> class, or with an anonymous
	 * class based on this interface.
	 */
	public static interface ExecutionEngineConfigurationFactory
	{
		/**
		 * The key under which the concrete ExecutionEngineConfigurationFactory should be registered
		 */
		String key();
		
		/**
		 * Builds and returns a concrete ExecutionEngineConfiguration based on the supplied parameters.
		 * @param server
		 *            the connection between the platform and the server
		 * @param platform
		 *
		 * @return a concrete ExecutionEngineConfiguration
		 * @exception IllegalArgumentException
		 *                when for any reason it cannot be built because the information supplied in the XML document
		 *                is insufficient or incorrect
		 */
		ExecutionEngineConfiguration build(PlatformDescription platform);
	}
	
	/**
	 * Stores {@link ExecutionEngine.ExecutionEngineFactory platform visitor factories} and their respective keys.
	 * Note: this is a {@link HashTable} so that keys and values cannot be <code>null</code>
	 */
	private static Map<String, ExecutionEngineConfigurationFactory> factories = null;

	/** An execution progress monitor doing nothing whose operations are no-op */
	public static final ExecutionProgressMonitor noopProgressMonitor = new ExecutionProgressMonitor()
	{
		@Override public void setProgress(Result result) { /* no-op */ }
	};

	/**
	 * Registers the default factories shipped with praxis, namely:
	 * <ul>
	 * <li> {@link ProgramExecutionEngine.ProgramExecutionEngineConfigurationFactory},
	 * <li> {@link ShellExecutionEngine.ShellExecutionEngineConfigurationFactory},
	 * <li> {@link WorkflowExecutionEngine.WorkflowExecutionEngineConfigurationFactory}.
	 * </ul>
	 */
	static
	{
		factories = new Hashtable<String, ExecutionEngineConfigurationFactory>();
		registerConfigurationFactory(new ShellExecutionEngine.ShellExecutionEngineConfigurationFactory());
		registerConfigurationFactory(new ProgramExecutionEngine.ProgramExecutionEngineConfigurationFactory());
		registerConfigurationFactory(new WorkflowExecutionEngine.WorkflowExecutionEngineConfigurationFactory());
	}
	
	/**
	 * Registers the platform visitor factory under the key returned by {@link ExecutionEngineFactory#key()}. A
	 * concrete factory generally registers itself in its static initializer.
	 * @exception IllegalArgumentException
	 *                if the concrete factory's {@link ExecutionEngineFactory#key()} has already been registered
	 * @exception NullPointerException
	 *                if the concreteFactory or its key is <code>null</code>
	 */
	public static void registerConfigurationFactory(ExecutionEngineConfigurationFactory concreteFactory)
	        throws NullPointerException, IllegalArgumentException
	{
		String key = concreteFactory.key();
		if (factories.containsKey(key))
			throw new IllegalArgumentException("Error: a factory with key" + key + "has already been registered");
		factories.put(key, concreteFactory);
	}
	
	/**
	 * Returns the platform visitor factory corresponding to the supplied key
	 * @param key
	 *            a factory's key
	 * @return the corresponding factory, or <code>null</code> if none corresponds
	 * @exception NullPointerException
	 *                if key is <code>null</code>
	 */
	public static ExecutionEngineConfigurationFactory getConfigurationFactory(String key) throws NullPointerException
	{
		return factories.get(key);
	}
	
	/**
	 * Returns an iterator on the set of factories' keys
	 * @return iterator
	 */
	public static Iterator<String> getFactoriesKeys()
	{
		return factories.keySet().iterator();
	}
	
	protected Result                       result;
	
	protected ExecutionEngineConfiguration configuration;
	
	private ExecutionProgressMonitor       progressMonitor;
	
	protected ExecutionEngine(ExecutionEngineConfiguration configuration, WorkflowID workflowID, ExecutionID executionID)
	{
		this.configuration = configuration;
		this.result = new Result(workflowID, executionID);
	}
	
	/**
	 * Returns the default target to use when the target is not explicitly set within the description.<br>
	 * The returned value should be one of the targets returned by {@link #getTargets()}.
	 * @return the default target to be used when no target is specified.
	 */
	public abstract Enum<? extends Enum<?>> defaultTarget();
	
	/**
	 * Returns the available (valid) targets for this ExecutionEngine.<br>
	 * <b>Important note:</b> The framework expects that the names of the enumerations returned by this method are
	 * made of upper-case characters (for example: <code>STDIN</code> is correct but <code>stdin</code> or
	 * <code>Stdin</code> are not). Please refer to {@link #getTargetForName(String)} for details.
	 * @return the enumeration class holding the valid targets for the execution engine.
	 */
	@SuppressWarnings("rawtypes")
	public abstract Class<? extends Enum> getTargets();
	
	/**
	 * Searchs the target with the supplied name within the targets of this execution engine, as returned by
	 * {@link #getTargets()}.<br>
	 * <b>Important note:</b> The framework expects the names of the enumerations returned by {@link #getTargets()} to
	 * be made of upper-case characters (for example: <code>STDIN</code> is correct but <code>stdin</code> or
	 * <code>Stdin</code> are not). If not, the default implementation for this method will not be able to find any
	 * targets, as the target's name is converted to upper-case before being searched. Subclasses willing to propose
	 * an alternative search method (to support enumeration items such as <code>Stdin</code> e.g.) should overwrite
	 * this method; in this case, a call to super's implementation is possible, but it is not required.
	 * @param targetName
	 *            the name of the target to search within this execution engine. If the supplied name is
	 *            <code>null</code> or the empty string, the returned target is the {@link #defaultTarget()}.
	 *            Subclasses overwriting this method are expected to behave likewise.
	 * @return The corresponding target, or {@link #defaultTarget()} is the name is <code>null</code> or the empty
	 *         string..
	 * @throws IllegalArgumentException
	 *             if the target could not be found.
	 */
	@SuppressWarnings("unchecked") // for Enum.valueof, below
    public Enum<?> getTargetForName(String targetName)
	{
		if (targetName==null)
			targetName="";
		if ("".equals(targetName))
			return defaultTarget();
		
		targetName = targetName.trim().toUpperCase(Locale.ENGLISH);
		return Enum.valueOf(getTargets(), targetName);
		//return Target.valueOf(targetName);
	}
	
	/**
	 * Called by {@link PlatformDescription#execute(Activity)} on the server-side, before the necessary elements are
	 * zipped and sent to the platform for execution.<br>
	 * This method's implementation checks that all input parameters of the activity's program are connected.
	 * @param activity
	 *            the activity about to be executed
	 * @param aConfiguration
	 *            the configuration object associated to the execution engine.
	 * @throws CannotExecuteException
	 *             when the execution should be aborted.
	 */
	public void prepareExecution(Activity activity, ExecutionEngineConfiguration aConfiguration)
	throws CannotExecuteException
	{
		if (activity.hasDisconnectedInputFiles())
			throw new CannotExecuteException("Program could not be executed: a program's input is not connected\n"+//i18n
			                                 "Impossible d'exécuter le programme: une entrée de programme n'est pas connectée");
		if (activity.hasInvalidInputFiles())
			throw new CannotExecuteException("Program could not be executed: a program's input is invalid (no file)\n"+//i18n
                    "Impossible d'exécuter le programme: une entrée de programme n'est pas valide (aucun fichier associé)");
		result.setStatus(Result.Status.RUNNING);
		for (Program prg: activity.getExecutionSet())
			result.setExecStatus(prg.getProgramID(), ProgramStatus.WAITING);
	}
	
	/**
	 * @param executionDirectory
	 * @param zippedInputFiles
	 *            the zipped file containing all necessary input files for the execution. It can be null if no input
	 *            files are necessary.
	 * @return the result of the execution
	 * @throws Exception
	 */
	public abstract Result execute(File executionDirectory, File zippedInputFiles) throws Exception;
	
	public abstract void cancel();
	
	/**
	 * Returns the execution directory that should be used on the platform side. By default, this method builds a new
	 * directory within the system's temporary directory (java system property: <code>java.io.tmpdir</code>).
	 * Subclasses will override this method if for some reason, the default is not suitable; for example, when the
	 * script is to be executed within a grid, the execution directory must accessible whenever the script is executed:
	 * the system's tmp directory is not suitable in this case, while the user's home directory is ok.
	 * @return the execution directory on the platform side.
	 */
	public File getExecutionDirectory()
	{
		return Utile.createTempDirectory("exec", ".bsx", new File(System.getProperty("java.io.tmpdir")));
	}

	/**
	 * Returns the progress monitor. If it is unset, or {@link #setProgressMonitor(ExecutionProgressMonitor) set} to
	 * {@code null}, the methods returns {@link #noopProgressMonitor the no-op progress monitor}.
	 * @return the current progress monitor.
	 */
	public synchronized ExecutionProgressMonitor getProgressMonitor()
	{
		return progressMonitor != null ? progressMonitor : noopProgressMonitor;
	}

	public void setKey(String key)
	{
		result.key = key;
	}

	public Result getResult()
	{
		return result;
	}

	public synchronized void setProgressMonitor(ExecutionProgressMonitor progressMonitor)
	{
		this.progressMonitor = progressMonitor;
	}
}
