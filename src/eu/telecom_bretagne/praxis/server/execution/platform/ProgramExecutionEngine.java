/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server.execution.platform;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.SystemExecution;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.SimpleFormatterPlatform;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;


/**
 * Handles portions of workflows directly executed as programs
 * @author SébastienBigaret
 */
public class ProgramExecutionEngine
    extends SimpleFormatterPlatform
{
	private static final long serialVersionUID = 8691388145919088210L;

	public static class ProgramExecutionEngineConfiguration
	    extends ExecutionEngineConfiguration
	{
		private static final long serialVersionUID = 4236422832692692038L;

		/**
		 * @param platform
		 * @param xml
		 *            the xml description of the platform.
		 */
		public ProgramExecutionEngineConfiguration(PlatformDescription platform)
		{
			super(platform);
		}
		
		@Override
		public ExecutionEngine getEngine(WorkflowID workflowID, ExecutionID executionID)
		{
			return new ProgramExecutionEngine(this, workflowID, executionID);
		}
	}
	
	public static final class ProgramExecutionEngineConfigurationFactory
	    implements ExecutionEngine.ExecutionEngineConfigurationFactory
	{
		/** Returns <code>"program"</code> */
		public String key()
		{
			return "program";
		}
		
		/**
		 * Builds and returns a new Platform for executing programs
		 * @exception IllegalArgumentException
		 *                if the supplied document's root element has no attribute <code>os</code> or if its value is
		 *                not a valid argument for {@link ProgramExecutionEngine#set_OS(String)}.
		 */
		public ExecutionEngineConfiguration build(PlatformDescription platform)
		{
			return new ProgramExecutionEngineConfiguration(platform);
		}
	}
	
	public static enum TARGETS
	{
		/**
		 * This target indicates the code it is attached to is part of the command-line.
		 */
		CMDARG,
		/**
		 * This target indicates that the code attached to it should be interpreted as a command-line. After being
		 * split, the different parts are added as if they were declared as {@link #CMDARG}.
		 * @see Utile#splitCommandLine(String)
		 */
		CMDLINE,

		/**
		 * This targets indicates that the code it is attached to should be sent to the standard input of the running
		 * process.
		 */
		STDIN,
		/**
		 * This targets indicates that the (output) parameter it is bound to corresponds to the standard output.
		 */
		STDOUT,
	}
	
	/**
	 * 
	 */
	protected SystemExecution process;
	
	@Override
	public Enum< ? extends Enum< ? >> defaultTarget()
	{
		return TARGETS.CMDARG;
	}
	
	@Override
	public Class< ? extends Enum< ? >> getTargets()
	{
		return TARGETS.class;
	}
	
	@Override
	public void prepareExecution(Activity activity, ExecutionEngineConfiguration aConfiguration)
	        throws CannotExecuteException
	{
		super.prepareExecution(activity, aConfiguration);
		for (String prgID: scripts_for_prg.keySet())
			if (scripts_for_prg.get(prgID).get(TARGETS.CMDARG)==null)
				throw new CannotExecuteException("Cannot execute: nothing to execute. Reason: null content for target CMDARG in prgID: "+prgID);
	}
	
	protected ProgramExecutionEngine(ExecutionEngineConfiguration configuration, WorkflowID workflowID,
	        ExecutionID executionID)
	{
		super(configuration, workflowID, executionID);
	}
	
	public ProgramExecutionEngineConfiguration configuration()
	{
		return (ProgramExecutionEngineConfiguration) configuration;
	}
	
	@Override
	protected void addCodeToTarget(String prgID, Enum< ? > target, String code, int position)
	{
		if (target == null)
			target = defaultTarget();
		if (target.equals(TARGETS.CMDLINE))
		{
			for (String arg: Utile.splitCommandLine(code))
				super.addCodeToTarget(prgID, TARGETS.CMDARG, arg, position);
			return;
		}
		if (target.equals(TARGETS.CMDARG) && (code == null || "".equals(code)))
			return;
		super.addCodeToTarget(prgID, target, code, position);
	}
	
	@Override
	protected String getCode_output(Parameter param)
	{
		if (TARGETS.STDOUT.name().equals(param.getDescription().getValueTemplateTarget()))
			return param.getDescription().getVdef();
		return super.getCode_output(param);
	}
	
	@Override
	public Result execute(File executionDirectory, File zippedInputFiles) throws InterruptedException,
	        InterruptedIOException
	{
		// this is only used to identify the current exec. within log messages
		String log_exec_id = executionDirectory.toString();
		
		// uncompress the script and the related files into the appropriate dir
		if (zippedInputFiles != null) // do not remove it, we need to restore it before every execution
			Utile.unzip(zippedInputFiles, executionDirectory, false, false);
		
		List<String> sentFiles = new ArrayList<String>(Arrays.asList(executionDirectory.list()));
		
		// time to execute the script
		// TODO: treat IOException + InterruptedException individually AND return a useful status to the server!!
		Log.log.info(()->"Executing workflow: " + log_exec_id);
		
		for (String prgID: scripts_for_prg.keySet())
		{
			result.setExecStatus(prgID, ProgramResult.ProgramStatus.RUNNING);
			getProgressMonitor().setRunningExecutionProgress(result);

			Result prgResult = new Result(result.workflowID(), null);
			
			/* move input files */
			File[] createdFiles = prepareInputFiles(prgID, executionDirectory, prgResult);
			// TODO check prg_result.status
			
			/* the execution itself */
			List<String> stdin_content = scripts_for_prg.get(prgID).get(TARGETS.STDIN);
			process = new SystemExecution(scripts_for_prg.get(prgID).get(TARGETS.CMDARG).toArray(new String[] {}),
			                              stdin_content != null ? stdin_content.toArray(new String[] {}) : null,
			                              executionDirectory);
			// handle stdout
			List<String> stdout_target = scripts_for_prg.get(prgID).get(TARGETS.STDOUT);
			Writer logs = null;
			if (stdout_target!=null && stdout_target.size() != 0)
			{
				String stdout = stdout_target.get(0);
				try
				{
					logs = new FileWriter(new File(executionDirectory, stdout));
				}
				catch (IOException e)
				{
					// TODO
					e.printStackTrace();
				}
			}
			if (logs == null)
				logs = new StringWriter();
			
			StringWriter errs = new StringWriter();
			
			int exitValue = process.execute(logs, 51200, errs, 51200); /* 50 Ko */
			Log.log.fine(()->"shell process finished. Exit value for prg_id: " + prgID + ": " + exitValue);
			
			/* Put into result the relevant informations */
			// IllegalStateException will be raised by stdout()/stderr() if the process couldn't be executed
			try
			{
				logs.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (logs instanceof StringWriter)
				prgResult.setResultForPrg(prgID, "stdout", logs.toString());
			prgResult.setResultForPrg(prgID, "stderr", errs.toString());
			// prgResult.setResultForPrg(prgID, "script", scriptForID(prgID));
			prgResult.setResultForPrg(prgID, "exit_value", "" + exitValue);
			prgResult.setResultForPrg(prgID, "files_not_found", "");
			
			if (exitValue == 0)
			{
				prgResult.setStatus(Result.Status.OK);
				prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.OK);
			}
			else
			{
				prgResult.setStatus(Result.Status.ERROR);
				prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.ERROR);
				prgResult.setResultForPrg(prgID, "failure_reason",
				                           "Voir les sorties stderr/stdout pour plus de détails sur l'échec\n"+
				                           "Refer to sections stderr & stdout for further details on the failure"); // i18n devrait être fait côté client, cf. note similaire dans Resource
			}
			
			/* time to move the files */
			renameOutputFiles(prgID, executionDirectory, prgResult);
			for (File createdFile: createdFiles)
				Utile.deleteRecursively(createdFile); // may be a directory

			/* move all EXTRA files into the prgID/ directory (i.e. that were not identified as output files) */
			// TODO quick fix here, renameOutputFiles() should probably take care of this, e.g. being cleanup/handleOutputFiles() instead
			// TODO voir aussi ShellExecEngine... et mettre tout ça ensemble plutôt que de répliquer le execute()...
			for (File f: executionDirectory.listFiles())
				if (f.isFile() && ( ! "script.txt".equals(f.getName()) ) && (!sentFiles.contains(f.getName())))
					Utile.renameFile(f, new File(new File(executionDirectory, prgID), f.getName())); // TODO set WARNING state if it returns false?
			result.mergeWith(prgResult);
			getProgressMonitor().setRunningExecutionProgress(result);
			prgResult.dumpContent(executionDirectory);

			if (zippedInputFiles!=null) // Restore the input files before the next program begins
				Utile.unzip(zippedInputFiles, executionDirectory, false, false);
		}
		

		Log.log.info(()->"End of execution: " + log_exec_id + "status: " + result.getStatus());
		if (zippedInputFiles!=null)
			zippedInputFiles.delete();
		// TODO if false, this means that the execution has been stopped
		// if(platform.executeWorkflow(executionID))
		{
			File zippedResults = new File(executionDirectory, "resultat.zip");
			Log.log.info(()->"Sending results to the server: " + zippedResults);
			
			// add to 'sentFiles' any file you do NOT want to send back
			sentFiles.add("script.txt");
			Utile.zipDirectory(executionDirectory, zippedResults, sentFiles);
			result.setZipFile(zippedResults);
			return result;
		}
		// else return null;
	}
	
	/**
	 * Waits until the current process is cancelled
	 */
	@Override
	public void cancel()
	{
		while (process == null)
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{ /* ignore */}
		process.cancel();
	}
}
