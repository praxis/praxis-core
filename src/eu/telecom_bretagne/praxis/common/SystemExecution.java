/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;

/**
 * Allows the execution of an external process, the specification of its standard input and the retrieval of the
 * standard output and standard error streams.<br>
 * This is essentially a wrapper around a Process object. It works along with a {@link StreamGobbler} so that
 * processes do not hang when they come to an end: under some OSes, such as Windows (at least some of Win.OSes),
 * processes hang and do not finish if some bytes remain unread in their stdout/stderr outputs. <br>
 * Example of use:
 * 
 * <pre>
 * @code
 * 	final String[] reg_query = new String[] { "ls", "-als", "/" };
 * 	SystemExecution exec = new SystemExecution(reg_query, null, null));
 * 	try {
 * 		exec.execute(new StringWriter(), -1, new StringWriter(), -1);
 * 		String stdout = exec.getStdout().toString();
 * 		String stderr = exec.getStderr().toString();
 * 		...
 * 	} catch (InterruptedException e) { whatever() }
 * }
 * </pre>
 * @author Sébastien Bigaret
 */
public class SystemExecution
{
	public static class Delegate
	{
		/**
		 * Invoked by {@link SystemExecution#execute(Writer, long, Writer, long)} before the process is launched. A
		 * delegate use this to adjust:
		 * <ul>
		 * <li>the environment variables, as needed,
		 * <li>the command and its arguments.
		 * </ul>
		 * To determine the changes that should be made, delegates may use helper methods defined in
		 * {@link Environment}, such as {@link Environment#standaloneExecution()} and
		 * {@link Environment#getPlatformOS()}. <br>
		 * <br>
		 * Subclasses are not required to call the superclass' implementation. <br>
		 * <br>
		 * The default implementation simply returns the supplied command, leaving the environment map untouched.
		 * @param command_array
		 *            the string array containing the command and its arguments, as computed by the framework.
		 * @param environment
		 *            a map containing the system's environment. A delegate can add or remove specific keys, it change
		 *            whatever value and it can clear() the map. If the map is cleared, the executed process will
		 *            inherit the environment of the current process.
		 * @return the string array with the command that should be executed along with its arguments.
		 */
		public String[] prepareForExecution(String[] command_array, final Map<String, String> environment)
		{
			return command_array;
		}
	}
	
	protected static final Delegate defaultDelegate = new Delegate();
	
	protected static Delegate       delegate        = defaultDelegate;
	
	/**
	 * This object can have three different states:
	 * <ul>
	 * <li>UNSET when it has not been executed, nor cancelled yet,
	 * <li>RUNNING when execute() has already been called, prior to cancel()
	 * <li>CANCELLED if cancel() has been called prior to execute().
	 * </ul>
	 * This allows both methods execute() and cancel() to interact with each other, as expected. WARNING: when the
	 * {@link SystemExecution#execute_status object's status} is RUNNING, it JUST mean that execute() was called prior
	 * to cancel(), NOT that execution is still running, or that it has not been cancelled afterwards.
	 */
	protected enum STATUS
	{
		UNSET, STARTED, CANCELLED
	}
	
	/**
	 * This object's execution status, refer to {@link STATUS} for details.
	 */
	protected STATUS   execute_status = STATUS.UNSET;
	
	/** The command to execute */
	protected String[] cmd_array;
	
	/** The strings to be written to the standard input. It may be null. */
	protected String[] stdin;
	
	/** the process created by {@link #execute()} */
	protected Process  process;
	
	/** Used to store the writer for stdout supplied to {@link #execute(Writer, long, Writer, long, boolean)} */
	protected Writer   stdout;
	
	/** Used to store the writer for stderr supplied to {@link #execute(Writer, long, Writer, long, boolean)} */
	protected Writer   stderr;
	
	/**
	 * The so-called "current working directory" for the process
	 */
	protected File     working_directory;
	
	/**
	 * Builds a new SystemExecution, ready to be {@link #execute() executed}.<br>
	 * See {@link Runtime#exec(String, String[], File)} for details on how the {@code command} string is interpreted.
	 * @param command_line
	 *            the command to execute. It must not be null nor empty.
	 * @param working_directory
	 *            the working directory for the process. If null, it inherits the working directory of the process
	 *            <b>which calls {@link #execute()}<b> on the object.
	 * @throws NullPointerException
	 *             if command_line is null
	 * @throws IllegalArgumentException
	 *             if command_line is empty
	 */
	public SystemExecution(String command_line, File working_directory)
	{
		if (command_line == null)
			throw new NullPointerException();
		if (command_line.equals(""))
			throw new IllegalArgumentException();
		
		// do exactly what is done in Runtime.exec() when taking a String instead of a String[]
		StringTokenizer st = new StringTokenizer(command_line);
		this.cmd_array = new String[st.countTokens()];
		for (int i = 0; st.hasMoreTokens(); i++)
			this.cmd_array[i] = st.nextToken();
		
		this.working_directory = working_directory;
	}
	
	/**
	 * Builds a new SystemExecution, ready to be {@link #execute() executed}.
	 * @param command_array
	 *            the command to execute. It must not be null nor empty.
	 * @param stdin
	 *            this string array will be piped into the process' stdin. It may be null or empty.
	 * @param working_directory
	 *            the working directory for the process. If null, it inherits the working directory of the process
	 *            <b>which calls {@link #execute()}<b> on the object (see parameter '{@code dir}' in
	 *            {@link Runtime#exec(String[], String[], File)}
	 * @throws NullPointerException
	 *             if command_line is null
	 * @throws IllegalArgumentException
	 *             if command_line is empty
	 */
	public SystemExecution(String[] command_array, String[] stdin, File working_directory)
	{
		if (command_array == null)
			throw new NullPointerException("Parameter command_array cannot be null");
		if (command_array.length == 0)
			throw new IllegalArgumentException("Parameter command_array cannot be null");
		this.cmd_array = command_array;
		this.stdin = stdin;
		this.working_directory = working_directory;
	}
	
	/**
	 * Sets the current delegate for this class.
	 * @param delegate
	 *            the new delegate. Note that <code>setDelegate(null)</code> restores the default delegate (an
	 *            instance of {@link Delegate}) --{@link #getDelegate()} never returns a {@code null} value.
	 */
	public static void setDelegate(Delegate delegate)
	{
		if (delegate == null)
			SystemExecution.delegate = SystemExecution.defaultDelegate;
		else
			SystemExecution.delegate = delegate;
	}
	
	/**
	 * Returns the delegate currently used.
	 * @return the delegate used by this object. It is never null.
	 */
	public static Delegate getDelegate()
	{
		return SystemExecution.delegate;
	}
	
	/**
	 * Executes the supplied command. This command also {@link StreamGobbler gobbles} the content of the so-called
	 * standard output and standard error streams, no matter whether you want their content or not; the reason is that
	 * within some environment a process won't be terminated unless the streams are read up to the end-of-file (for
	 * example, this behaviour can be observed with a standard Windows XP installation). Content of both streams can
	 * be retrieved with {@link #stdout()} and {@link #stderr()}. <br>
	 * <br>
	 * This is equivalent to: {@link #execute(Writer, long, Writer, long, boolean)
	 * <code>execute(stdout, stdout_limit, stderr, stderr_limit, SystemExecution.getDelegate());</code>}
	 * @param stdoutWriter
	 *            a writer in which the content of the standard output is written. It may be null: in this case, the
	 *            standard output is still read, but it is not stored. For convenience, the supplied writer is made
	 *            available through {@link #getStdout()}.
	 * @param stdout_limit
	 *            the maximum number of bytes to read from the standard output. If 0 or negative, no limitation
	 *            applies.
	 * @param stderrWriter
	 *            a writer in which the content of the standard error is written. It may be null: in this case, the
	 *            standard output is still read, but it is not stored. For convenience, the supplied writer is made
	 *            available through {@link #getStderr()}.
	 * @param stderr_limit
	 *            the maximum number of bytes to read from the standard error. If 0 or negative, no limitation
	 *            applies.
	 * @return the exit status of the process
	 * @throws InterruptedException
	 * @throws IllegalStateException
	 *             if executed more than once.
	 */
	public int execute(Writer stdoutWriter, long stdout_limit, Writer stderrWriter, long stderr_limit)
	        throws InterruptedException
	{
		// use getDelegate(), in case it is overridden
		return this.execute(stdoutWriter, stdout_limit, stderrWriter, stderr_limit, SystemExecution.getDelegate());
	}
	
	/**
	 * Executes the supplied command. This command also {@link StreamGobbler gobbles} the content of the so-called
	 * standard output and standard error streams, no matter whether you want their content or not; the reason is that
	 * within some environment a process won't be terminated unless the streams are read up to the end-of-file (for
	 * example, this behaviour can be observed with a standard Windows XP installation). Content of both streams can
	 * be retrieved with {@link #stdout()} and {@link #stderr()}.
	 * @param stdoutWriter
	 *            a writer in which the content of the standard output is written. It may be null: in this case, the
	 *            standard output is still read, but it is not stored. For convenience, the supplied writer is made
	 *            available through {@link #getStdout()}.
	 * @param stdout_limit
	 *            the maximum number of bytes to read from the standard output. If 0 or negative, no limitation
	 *            applies.
	 * @param stderrWriter
	 *            a writer in which the content of the standard error is written. It may be null: in this case, the
	 *            standard output is still read, but it is not stored. For convenience, the supplied writer is made
	 *            available through {@link #getStderr()}.
	 * @param stderr_limit
	 *            the maximum number of bytes to read from the standard error. If 0 or negative, no limitation
	 *            applies.
	 * @param aDelegate
	 *            Uses a different delegate than the {@link #setDelegate(Delegate) global one}: in some particular
	 *            situations this is sometimes desirable to use a different delegate than the one used almost
	 *            everywhere.
	 * @return the exit status of the process
	 * @throws InterruptedException
	 *             see {@link Process#waitFor()} for details
	 * @throws IllegalStateException
	 *             if executed more than once.
	 */
	public int execute(Writer stdoutWriter, long stdout_limit, Writer stderrWriter, long stderr_limit, Delegate aDelegate)
	        throws InterruptedException, IllegalStateException
	{
		// System.getenv() is unmodifiable: we need a copy
		Map<String, String> environment = new HashMap<String, String>(System.getenv());
		
		if (aDelegate != null)
			cmd_array = aDelegate.prepareForExecution(cmd_array, environment);
		
		/* Create String[] from Map */
		// NB: do NOT initialize it here with a new String[] array, or, if the following condition are not satisfied,
		// the exec() will be supplied with a 'envp' filled with 'null', resulting in a NullPointerException.
		String[] envp = null;
		
		if (environment.size()>0)
		{
			envp = new String[environment.size()];
			int idx=0;
			for (String key: environment.keySet())
			{
				envp[idx] = key + "=" + environment.get(key);
				idx++;
			}
		}
		
		/* Now it's time to make sure we were not already cancelled */
		synchronized(this)
		{
			if (execute_status == STATUS.CANCELLED)
				return -1;
			
			if (execute_status == STATUS.STARTED) // SHOULD NOT HAPPEN
				throw new IllegalStateException("Cannot execute() twice");
			
			execute_status = STATUS.STARTED;
		}
		this.stdout = stdoutWriter;
		this.stderr = stderrWriter;
		try
		{
			process = Runtime.getRuntime().exec(cmd_array, envp, working_directory);
		}
		catch (IOException e)
		{
			Log.log.log(Level.WARNING, "exec() throw IOException", e);
			return -1;
		}
		Log.log.finest("Process started, now process.waitFor()");
		
		StreamGobbler sg_stdout = new StreamGobbler(process.getInputStream(), stdoutWriter);
		StreamGobbler sg_stderr = new StreamGobbler(process.getErrorStream(), stderrWriter);
		
		sg_stdout.setLimit(stdout_limit);
		sg_stderr.setLimit(stderr_limit);
		
		/*
		 * Be sure to launch the threads reading the standard output/error stream or the call to waitFor() will wait
		 * indefinitely on windows platforms: AFAIK windows keeps the process alive until the standard output has been
		 * read to its very end (== until the End Of File has been reached)
		 */
		sg_stdout.start();
		sg_stderr.start();
		int exitValue;
		try
		{
			if (stdin!=null)
			{
				for (String str: stdin)
				{
					process.getOutputStream().write(str.getBytes("UTF-8"));
				}
			}
			// close stdin, or the process will block while trying to read the pipe
			process.getOutputStream().close();
			exitValue = process.waitFor();
		}
		catch (InterruptedException exc)
		{
			process.destroy();
			throw exc;
		}
		catch (IOException exc)
		{
			process.destroy();
			throw new RuntimeException(exc);
		}
		finally
		{
			try { sg_stdout.join(); } catch (InterruptedException ie) { /* ignore */ }
			try { sg_stderr.join(); } catch (InterruptedException ie) { /* ignore */ }
			try { process.getInputStream().close(); } catch (IOException ioe) { /* ignore */ }
			try { process.getErrorStream().close(); } catch (IOException ioe) { /* ignore */ }
		}
		Log.log.finest(()->"Process finished. Exit value: " + exitValue);
		return exitValue;
	}
	
	/**
	 * Kills the corresponding subprocess
	 * @throws IllegalStateException
	 *             if the process has not been {@link #execute() launched} yet
	 * @see Process#destroy()
	 */
	public void destroy()
	{
		if (process == null)
			throw new IllegalStateException("Cannot destroy a process that has not been executed yet!");
		process.destroy();
	}
	
	/** Waits until the process is launched, then cancels the execution */
	public void cancel()
	{
		synchronized(this)
		{
			if (execute_status == STATUS.CANCELLED) // already cancelled
				return;
			if (execute_status == STATUS.UNSET)
			{
				execute_status = STATUS.CANCELLED;
				return;
			}
		}
		// status was STARTED: let's wait for the process
		while (process == null)
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{ /* ignore */}
		process.destroy();
	}
	
	/**
	 * Returns the Writer objet collecting the standard output's content, as supplied to
	 * {@link #execute(Writer, long, Writer, long, boolean)}.<br>
	 * @return the Writer object supplied at execution time. It is {@code null} before the execution, and may be null
	 *         afterwards if no writer for stdout was supplied.
	 */
	public Writer getStdout()
	{
		return this.stdout;
	}
	
	/**
	 * Returns the Writer objet collecting the standard error's content, as supplied to
	 * {@link #execute(Writer, long, Writer, long, boolean)}.<br>
	 * @return the Writer object supplied at execution time. It is {@code null} before the execution, and may be null
	 *         afterwards if no writer for stdout was supplied.
	 */
	public Writer getStderr()
	{
		return this.stderr;
	}
	
}
