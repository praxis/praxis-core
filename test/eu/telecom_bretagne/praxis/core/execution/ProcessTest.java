/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import org.junit.Test;

import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.EventAudit;
import eu.telecom_bretagne.praxis.core.execution.Process;
import static org.junit.Assert.*;


public class ProcessTest
    extends Process
{   
    private static final long serialVersionUID = 3382762830171752019L;

	public static class TestRequester implements Requester
    {
    	public EventAudit lastEvent;
    	@Override
    	public void handleEvent(EventAudit event)
    	{
    		lastEvent=event;
    		synchronized(this) { this.notify(); }
    	}
    }

	public ProcessTest(){
		super(null,null);
	}
	
	@Test
	public void activityHasaDefaultContext()
	{
		assertNotNull(new Process(null, "test").getContext());
	}
	
	@Test
	public void addContainer()
	{
		Process p = new Process(null, "p");
		Activity a = new Activity(null, "a");

		p.addActivity(a);
		assertEquals(p, a.getContainer());
	}

	@Test
	public void getActivityWithID()
	{
		Process p = new Process(null, "test");
		Activity a1 = new Activity("key a1", "a1");
		p.addActivity(a1);
		assertEquals(a1, p.getActivity("key a1"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void activitiesHaveUniqueKeys()
	{
		Process p = new Process(null, "test");
		Activity a1 = new Activity("same key", "a1");
		Activity a2 = new Activity("same key", "a2");
		p.addActivity(a1);
		p.addActivity(a2);
	}
	
/*
	//@Test
	public void activateRunnableActivitiesTest()
	{
		Process p = new Process(null, "test");
		p.setRequester(new TestRequester());
		Activity a1 = new Activity("key1", "a1");
		Activity a2 = new Activity("key2", "a2");
		p.addActivity(a1).addActivity(a2);
		
		ActivityContext c2 = a2.getContext();
		ActivityContext.addPredecessorTo(a1, a2);
		//c2.predecessors.add(a1);
		a2.setContext(c2);
 
		java.util.List <Activity> activatedActivities = p.updateActivitiesStatus();
		assertEquals(Status.NOT_STARTED, a1.getState());
		assertTrue(activatedActivities.contains(a1));
		assertEquals(1, activatedActivities.size());
		assertEquals(Status.NOT_STARTED, a2.getState());
		
		a1.setState(Status.CANCELLED);
		activatedActivities = p.updateActivitiesStatus();
		assertTrue(activatedActivities.isEmpty());
		assertEquals(Status.CANCELLED, a2.getState());
		assertNotNull(a2.getResult());
		assertEquals(ProgramStatus.NOT_EXECUTED, a2.getResult().getProgramInfo("key2").status);
		a2.setState(Status.NOT_STARTED);

		a1.setState(Status.ERROR);
		activatedActivities = p.updateActivitiesStatus();
		assertTrue(activatedActivities.isEmpty());
		assertEquals(Status.ERROR, a2.getState());
		assertNotNull(a2.getResult());
		assertEquals(ProgramStatus.NOT_EXECUTED, a2.getResult().getProgramInfo("key2").status);
		a2.setState(Status.NOT_STARTED);

		a1.setState(Status.OK);
		activatedActivities = p.updateActivitiesStatus();
		assertEquals(Status.NOT_STARTED, a2.getState());
		assertTrue(activatedActivities.contains(a2));
		a2.setState(Status.NOT_STARTED);

		a1.setState(Status.RUNNING);
		activatedActivities = p.updateActivitiesStatus();
		assertEquals(Status.NOT_STARTED, a2.getState());
		assertTrue(activatedActivities.isEmpty());
		
		a1.setState(Status.SUSPENDED);
		p.updateActivitiesStatus();
		assertEquals(Status.NOT_STARTED, a2.getState());

		// Testing for a special bug: under these specific circumstances, we had a bug where an activity
		// (here, this was 'a2') was returned twice in the list of activated activities
		Activity a1b = new Activity("key1b", "a1b");
		p.addActivity(a1b);
		//c2.predecessors.add(a1b);
		ActivityContext.addPredecessorTo(a1b, a2); // CHECK conformité avec précédent test AVT DE COMMIT
		
		Activity b1 = new Activity("keyb1", "b1");
		Activity b2 = new Activity("keyb2", "b2");
		p.addActivity(b1).addActivity(b2);
		ActivityContext cb2 = b2.getContext();
//		cb2.predecessors.add(b1);
//		b1.setContext(cb2);
		ActivityContext.addPredecessorTo(b1, b2);  // CHECK conformité avec préalable AVT DE COMMIT

		a1.setState(Status.OK);
		a1b.setState(Status.OK);
		b1.setState(Status.ERROR);
		activatedActivities = p.updateActivitiesStatus();
		assertEquals(1, activatedActivities.size());
		
	}
*/
}
