/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

/**
 * This interface holds the constants relative to XML files used in praxis. <br>
 * The following items can be found:
 * <ul>
 * <li>their DTD Public ID,</li>
 * <li>their DTD System ID,</li>
 * <li>the paths pointing to the local version of the DTDs,</li>
 * <li>and at last and for some DTDs, some constants containing the (main) tags' names.</li>
 * </ul>
 * Note: every DTDs that were once used to serialize/export workflows into XML are kept here. This is because a
 * {@link eu.telecom_bretagne.praxis.core.workflow.Workflow} object needs it to determine which version of the DTD was
 * used when a workflow was serialized into xml, in order to restore it. <br>
 * <br>
 * The latest version, which is the one that will always be used to serialize workflows into XML, is identified by the
 * following IDs and path:
 * <ul>
 * <li>{@link #WORKFLOW_DTD_PUBID}</li>
 * <li>{@link #WORKFLOW_DTD_SYSID}</li>
 * <li>{@link #WORKFLOW_DTD_FILE}</li>
 * </ul>
 * @author Luiz Fernando Rodrigues
 * @author Sébastien Bigaret
 */
/* XML DTD public id are Formal Public Identifier (FPI), cf. http://www.ietf.org/rfc/rfc3151.txt */
public interface XMLConstants
{
	// Platform Description
	/**
     * 
     */
    public static final String
    PLATFORM_DTD_PUBID = "-//Telecom Bretagne/DTD XML Praxis Platform 1.0//EN";
    
    /**
     * 
     */
    public static final String PLATFORM_DTD_SYSID = "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/platform.dtd";
    
    /**
     * Path of the local file where the DTD specified by
     * {@link #PLATFORM_DTD_PUBID} can be found
     */
    public static final String PLATFORM_DTD_FILE = "data/dtd/platform.dtd";
    
    /**
     * Name of the attribute in the root element indicating the platform's os.
     */
    public static final String PLATFORM_OS_ATTR_TAG = "os";
    
    /**
     * Name of the tag in a platform description containing the programs
     */
    public static final String PLAT_PROGRAMS_TAG = "programs";
    
    /**
     * Name of the tag in a platform description pointing to a program
     */
    public static final String PLAT_PROGRAM_TAG = "program"; 
    
    // Hierarchy of resources
    /**
     * 
     */
    public static final String
    HRCHY_DTD_PUBID = "-//Telecom Bretagne/DTD XML Praxis Resources Hierarchy 1.0//EN";
    
    /**
     * 
     */
    public static final String HRCHY_DTD_SYSID = "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/hierarchy.dtd";

    /**
     * 
     */
    public static final String HRCHY_DTD_FILE = "data/dtd/hierarchy.dtd";
    
    /**
     * Balise de hierarchy
     */
    public static final String HRCHY_HIERARCHY_TAG = "hierarchy";
    
    /**
     * The root tag for the xml file describing the program's hierarchy
     */
    public static final String HRCHY_ROOT_TAG = "resources_hierarchy";

    /**
     * Balise de service
     */
    public static final String HRCHY_RESOURCE_TAG = "resource";
    
    /**
     * Balise pour le nom du service
     */
    public static final String HRCHY_RESOURCE_ID_TAG = "id";
    
    /**
     * Balise qui indique le service defaut d'un service generique
     */
    public static final String HRCHY_SERVICE_DEFAULT_TAG = "default";
    
    // Workflows    
    /**
     * The public ID for the version 3 of the DTD used to serialize workflows into xml.
     */
    public static final String
    WORKFLOW_DTD_PUBID_v3 = "-//Telecom Bretagne//DTD XML Praxis Workflow 3.0//EN";

    /**
     * The system ID for the version 3 of the DTD used to serialize workflows into xml.
     */
    public static final String WORKFLOW_DTD_SYSID_v3 = "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/workflow_3.0.dtd";

    /**
     * Path pointing to the file containing the DTD used to serialize workflows into XML, version 3.
     */
    public static final String WORKFLOW_DTD_FILE_v3 = "data/dtd/workflow_3.0.dtd";

    /**
     * The public id of the latest DTD currently used to serialize workflows into XML.
     */
    public static final String WORKFLOW_DTD_PUBID = WORKFLOW_DTD_PUBID_v3;

    /**
     * The system id of the latest DTD currently used to serialized workflow into XML.
     */
    public static final String WORKFLOW_DTD_SYSID = WORKFLOW_DTD_SYSID_v3;

    /**
     * Path pointing to the file containing the latest DTD currently used to serialize workflows into XML.
     */
    public static final String WORKFLOW_DTD_FILE = WORKFLOW_DTD_FILE_v3;

    // Older DTD, needed for conversion
	/**
	 * This is the 1st DTD (version 2) that has appeared in a publicly-released praxis project.
	 */
    public static final String
    WORKFLOW_DTD_PUBID_v2 = "-//ENSTBr//DTD Bioside Scenario 2.0//EN";
	
	/**
     * Path pointing to the file containing the DTD used to serialize workflows into XML, version 2.
	 */
    public static final String WORKFLOW_DTD_FILE_v2 = "data/dtd/workflow_2.0.dtd";

    
    
    // Program Descriptions
    /**
     * 
     */
    public static final String
    PROGRAM_DESCRIPTION_DTD_PUBID_v3_0 = "-//Telecom Bretagne/DTD XML Praxis Program Description 3.0//EN";
    public static final String
    PROGRAM_DESCRIPTION_DTD_PUBID_v3_1 = "-//Telecom Bretagne/DTD XML Praxis Program Description 3.1//EN";

    /**
     * 
     */
    public static final String
    PROGRAM_DESCRIPTION_DTD_SYSID = "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/program_description.dtd";
    
    public static final String PROGRAM_DESCRIPTION_DTD_FILE = "data/dtd/program_description.dtd";

    // External Viewers
    /**
     * 
     */
    public static final String
    EXT_VIEWERS_DTD_PUBID = "-//Telecom Bretagne/DTD XML Praxis External Viewers 1.0//EN";
    
    /**
     * 
     */
    public static final String
    EXT_VIEWERS_DTD_SYSID = "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/external_viewers.dtd";
    
    /**
     * Path of the local file where the DTD specified by
     * {@link #EXT_VIEWERS_DTD_PUBID} can be found.
     */
    public static final String
    EXT_VIEWERS_DTD_FILE = "data/dtd/external_viewers.dtd";
    
}
