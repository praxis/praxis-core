package eu.telecom_bretagne.praxis.common;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.dgc.VMID;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Supplier;
import java.util.prefs.Preferences;

import org.apache.commons.lang.SystemUtils;

import com.github.markusbernhardt.proxy.ProxySearch;
import com.github.markusbernhardt.proxy.ProxySearch.Strategy;

// https://www.developer.com/lang/jscript/top-7-open-source-json-binding-providers-available-today.html
import flexjson.JSONSerializer;

public class SystemInformations
{
	static final String CHECK_HTTP_URI = "http://server-telecom-bretagne.diviz.org/cgi-bin/java-rmi.cgi?check";
	static final String CHECK_HTTPS_URI = "https://server-telecom-bretagne.diviz.org/cgi-bin/java-rmi.cgi?check";

	public static class Data
	    extends LinkedHashMap<String, Object>
	{
		private static final long serialVersionUID = 1L;

		/**
		 * Associates the specified value with the specified key in this map. If the map previously contained a
		 * mapping for the key, the old value is replaced.
		 *
		 * @return this object
		 */
		@Override
		public Data put(final String key, final Object value)
		{
			super.put(key, value);
			return this;
		}

		/**
		 * Associates the value returned by the specified Supplier with the specified key in this map. If the map
		 * previously contained a mapping for the key, the old value is replaced. If an exception is raised when
		 * asking the supplier for a result, the exception is silently ignored and it does nothing except returning
		 * this object.
		 *
		 * @param f
		 *            the Supplier from which the value to be associated with the supplied key is
		 *            {@link Supplier#get() computed}.
		 * @return this object
		 */
		public <T> Data put(final String key, final Supplier<T> f)
		{
			try
			{
				super.put(key, f.get());
			}
			catch (final Exception exception)
			{ /* ignore */ }
			return this;
		}

		public Data add(final String key)
		{
			final Data child = new Data();
			put(key, child);
			return child;
		}

		public String json()
		{
			return new JSONSerializer().prettyPrint(true).serialize(this);
		}
	}

	static private String id;
	static
	{
		Preferences preferences = Preferences.userNodeForPackage(SystemInformations.class);
		id = preferences.get("id", null);
		if ( id == null )
		{
			id = new VMID().toString();
			preferences.put("id", id);
		}
	}

	static void screen(final Data data)
	{
		final Data screens = data.add("screens");
		if ( GraphicsEnvironment.isHeadless() )
			return;
		final GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final GraphicsDevice defaultScreen = graphicsEnvironment.getDefaultScreenDevice();

		screens.add("default")
		        .put("id", defaultScreen.getIDstring())
		        .put("width", defaultScreen.getDefaultConfiguration().getBounds().getWidth())
		        .put("height", defaultScreen.getDefaultConfiguration().getBounds().getHeight());

		for (final GraphicsDevice graphicsDevice: graphicsEnvironment.getScreenDevices())
		{
			screens.add(graphicsDevice.getIDstring())
			        .put("default", graphicsDevice.equals(defaultScreen))
			        .put("width", graphicsDevice.getDisplayMode().getWidth())
			        .put("height", graphicsDevice.getDisplayMode().getHeight())
			        .put("dpi", rethrowSupplier(() -> Toolkit.getDefaultToolkit().getScreenResolution()));
		}
	}

	static void rootDirectories(final Data data)
	{
		final Data rootDirs = data.add("root directories");
		for (final Path root: FileSystems.getDefault().getRootDirectories())
		{
			final Data rootDir = rootDirs.add(root.toString());
			try
			{
				final FileStore store = Files.getFileStore(root);
				rootDir.put("available", rethrowSupplier(() -> store.getUsableSpace()));
				rootDir.put("total", rethrowSupplier(() -> store.getTotalSpace()));
			}
			catch (final IOException e)
			{
				rootDir.put("error", e.getMessage());
			}
		}
	}

	static void fileStores(final Data data)
	{
		final Iterable<FileStore> stores;
		try
		{
			stores = FileSystems.getDefault().getFileStores();
		}
		catch (final Exception e)
		{
			return;
		}
		final Data filestores = data.add("filestores");

		for (final FileStore store: stores)
			filestores.add(store.name())
			        .put("type", rethrowSupplier(() -> store.type()))
			        .put("toString", rethrowSupplier(() -> store.toString()))
			        .put("total", rethrowSupplier(() -> store.getTotalSpace()))
			        .put("used", rethrowSupplier(() -> store.getTotalSpace() - store.getUnallocatedSpace()))
			        .put("available", rethrowSupplier(() -> store.getUsableSpace()));
	}

	/**
	 * 
	 * @param data
	 * @param name
	 * @param file
	 */
	public static void filesystem(final File file, final String name, final Data data)
	{
		data.add(name)
		        .put("available", rethrowSupplier(() -> file.getUsableSpace()))
		        .put("free", rethrowSupplier(() -> file.getFreeSpace()))
		        .put("total", rethrowSupplier(() -> file.getTotalSpace()));

	}

	public static void network(final Data data)
	{
		final Data network = data.add("network");
		iterate_on_proxies(network);
		detectProxy(network);
		externalIp(network);
		networkProxies(network);
	}

	public static void iterate_on_proxies(Data data)
	{
		System.err.println("iterate_on_proxies() start");
		Data proxiesData = data.add("proxy selectors");
		final ProxySelector defaultProxySelector = ProxySelector.getDefault();
		try
		{
			final URI home = URI.create(CHECK_HTTP_URI);
			for (Strategy strategy: Strategy.values())
			{
				final ProxySearch proxySearch = new ProxySearch();
				proxySearch.addStrategy(strategy);
				testProxySearch(proxySearch, home, proxiesData.add(strategy.name()));
			}
		}
		finally
		{
			// Restore the default proxy selector
			ProxySelector.setDefault(defaultProxySelector);
		}
		System.err.println("iterate_on_proxies() end");
	}

	private static void testProxySearch(ProxySearch proxySearch, URI uri, Data dataNode)
	{
		System.err.println("testProxySearch()");
		final ProxySelector defaultProxySelector = ProxySelector.getDefault();
		final ProxySelector proxySelector;
		try
		{
			proxySelector = proxySearch.getProxySelector();
		}
		catch (Throwable t)
		{
			dataNode.put("getProxySelector", t);
			return;
		}
		ProxySelector.setDefault(proxySelector);
		test_diviz(dataNode);
		ProxySelector.setDefault(defaultProxySelector);

		if ( proxySelector == null )
		{
			dataNode.put("getProxySelector", (Object) null); // cast needed to use put(String, Object)
			return;
		}
		final List<Proxy> proxyList = proxySelector.select(uri);
		if ( proxyList == null )
		{
			dataNode.put("proxySelectorList", (Object) null); // cast needed to use put(String, Object)
			return;
		}
		if ( proxyList.isEmpty() )
		{
			dataNode.put("proxySelectorList", "empty");
			return;
		}

		Data proxiesForStrategy = dataNode.add("proxySelectorList");
		Integer idx = 0;
		for (final Proxy proxy: proxyList)
		{
			idx += 1;
			Data proxyDetail = proxiesForStrategy.add(idx.toString());
			proxyDetail.put("type", proxy.type().name());
			final SocketAddress address = proxy.address();
			proxyDetail.put("socket address", address == null ? null : address.toString());
			if ( address instanceof InetSocketAddress )
			{
				final String host = ( (InetSocketAddress) address ).getHostName();
				final String port = Integer.toString(( (InetSocketAddress) address ).getPort());
				proxyDetail.put("host", host);
				proxyDetail.put("port", port);
			}
		}

	}

	/**
	 * Detects proxy settings for HTTP and HTTPS connections, and set related system properties appropriately. The
	 * related system properties are: {@code http.host}, {@code http.port}, {@code http.nonProxyHosts},
	 * {@code https.host} and {@code https.port}.<br>
	 * The {@link ProxySelector#getDefault() default system-wide proxy selector} is preserved.
	 * 
	 * @see <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/net/proxies.html">Java Networking and
	 *      Proxies</a>
	 * @param network
	 *            The data object that the method puts informations into.
	 */
	public static void detectProxy(final Data network)
	{
		final ProxySelector defaultProxySelector = ProxySelector.getDefault();
		try
		{
			_detectProxy(network);
		}
		finally
		{
			ProxySelector.setDefault(defaultProxySelector);
		}
	}


	/**
	 * Implements the {@link #detectProxy(Data)} method.
	 */
	private static void _detectProxy(final Data network)
	{
		System.err.println("_detectProxy() start");
		final Data proxyDetectionData = network.add("proxy detection");

		ProxySearch proxySearch = ProxyAutoDetect.getDefaultProxySearch();
		
		Proxy proxy;
		// http
		proxy = _detectProxyForURL(proxySearch.getProxySelector(), CHECK_HTTP_URI, proxyDetectionData.add("http"));
		SocketAddress address = null;
		if ( proxy != null )
		{
			address = proxy.address();
			if ( address instanceof InetSocketAddress )
			{
				System.setProperty("http.proxyHost", ((InetSocketAddress) address).getHostString());
				System.setProperty("http.proxyPort", Integer.toString(((InetSocketAddress) address).getPort()));
				System.setProperty("http.nonProxyHosts", "localhost|127.0.0.1");
			}
		}
		// https
		proxy = _detectProxyForURL(proxySearch.getProxySelector(),CHECK_HTTPS_URI, proxyDetectionData.add("https"));
		if ( proxy != null )
		{
			address = proxy.address();
			if ( proxy.address() instanceof InetSocketAddress )
			{
				System.setProperty("https.proxyHost", ((InetSocketAddress) address).getHostString());
				System.setProperty("https.proxyPort", Integer.toString(((InetSocketAddress) address).getPort()));
				// be sure to set it even if http proxy is DIRECT
				System.setProperty("http.nonProxyHosts", "localhost|127.0.0.1");
			}
		}
		System.err.println("_detectProxy() end");
	}

	/**
	 * Given a {@link ProxySelector}, returns a suitable proxy to access the {@code url}. When an
	 * {@link java.net.Proxy.Type#HTTP HTTP proxy} is suitable, it is preferred to a {@link java.net.Proxy.Type#DIRECT
	 * direct proxy}.<br>
	 * The {@link ProxySelector#getDefault() default system-wide proxy selector} is preserved.
	 * 
	 * @param data
	 *            The Data object that the informations should be dropped into
	 * @param proxySelector
	 *            The proxy selector to use
	 * @param url
	 *            The url that a connection is required to
	 * @param network
	 *            The data object that the method puts informations into.
	 * @throws IllegalArgumentException
	 *             if the url is incorrect
	 */
	public static Proxy detectProxyForURL(final ProxySelector proxySelector, final String url, final Data data)
	{
		final ProxySelector defaultProxySelector = ProxySelector.getDefault();
		try
		{
			return _detectProxyForURL(defaultProxySelector, url, data);
		}
		finally
		{
			ProxySelector.setDefault(defaultProxySelector);
		}
	}

	/**
	 * Implements the {@link #detectProxyForURL(ProxySelector, String, Data)} method except that it does NOT preserve
	 * the {@link ProxySelector#getDefault() default system-wide proxy selector}.
	 */
	private static Proxy _detectProxyForURL(final ProxySelector proxySelector, final String url, final Data data)
	{
		System.err.println("_detectProxyForURL() start: " + url);
		data.put("URL", url);
		final URI uri = URI.create(url);
		ProxySelector.setDefault(proxySelector);
		if ( proxySelector == null )
			return null;
		data.put("ProxySelector", proxySelector.getClass());

		final List<Proxy> proxies = proxySelector.select(uri);

		// Prefer HTTP, use DIRECT only if no HTTP proxy could be found
		// Iterate on all proxies, even if an HTTP proxy was found, so that all proxies in the list are logged
		Proxy detected_proxy = null;
		Data detected_proxy_data = null;
		if ( proxies != null && !proxies.isEmpty() )
		{
			Integer idx = 0;
			for (final Proxy proxy: proxies)
			{
				idx += 1;
				final Data currentProxyData = data.add(idx.toString());
				//currentProxyData.put("proxy", proxy);
				if ( proxy == null )
				{
					currentProxyData.put("proxy", proxy);
					continue;
				}
				currentProxyData.put("proxy class", proxy.getClass());
				currentProxyData.put("proxy.address", proxy.address());

				switch (proxy.type())
				{
				case HTTP:
					if ( detected_proxy == null || detected_proxy.type() == Type.DIRECT )
					{
						detected_proxy = proxy;
						detected_proxy_data = currentProxyData;
					}
					break;
				case DIRECT:
					if ( detected_proxy == null )
					{
						detected_proxy = proxy;
						detected_proxy_data = currentProxyData;
					}
					break;
				case SOCKS:
				default:
					continue;
				}
			}
		}
		if ( detected_proxy_data != null )
			detected_proxy_data.put("detected", "true");
		System.err.println("_detectProxy() end");
		return detected_proxy;
	}

	public static Object[] getURL(final URL url) throws IOException
	{
		System.err.println("getURL() start: " + url);
		final StringBuilder sb = new StringBuilder();
		final long startTimeMillis = System.currentTimeMillis();

		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(1000);
		conn.setReadTimeout(1000);
		String s;
		boolean firstElement = true;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream())))
		{
			while ( (s=in.readLine()) != null )
			{
				sb.append(s);
				if (firstElement)
					continue;
				firstElement = false;
				sb.append("\n");
			}
		}
		System.err.println("getURL() end" + (System.currentTimeMillis() - startTimeMillis));
		return new Object[] { sb.toString(), System.currentTimeMillis() - startTimeMillis };
	}
	
	public static void externalIp(final Data data)
	{
		System.err.println("");
		try
		{
			java.net.URL ip = new java.net.URL("http://webservices.decision-deck.org/cgi-bin/ip");
			Object[] results = getURL(ip);
			data.put("ip", results[0]);
			data.put("duration", results[1]);
		}
		catch (Throwable t)
		{
			data.put("ip", (Object) null);
			data.put("exception", t);
		}
	}

	public static void test_diviz(final Data data)
	{
		System.err.println("test_diviz");
		try
		{
			URL ip = new URL(CHECK_HTTP_URI);
			Object[] results = getURL(ip);
			data.put("connection to diviz server",results[0]);
			data.put("connection to diviz server - duration", results[1]);
		}
		catch (Throwable t)
		{
			data.put("connection to diviz server", "Failed, reason: "+t.toString());
		}
	}

	public static void networkProxies(final Data data)
	{
		System.err.println("network_proxies");
		final Data proxySettings = data.add("proxy settings");
		proxySettings.put("java.net.useSystemProxies", System.getProperty("java.net.useSystemProxies"));
		//System.setProperty("java.net.useSystemProxies", "true");
		final ProxySelector defaultProxySelector = ProxySelector.getDefault();
		proxySettings.put("default proxy selector",
		                  defaultProxySelector == null ? null : defaultProxySelector.getClass());

		final Data proxies = proxySettings.add("proxies");
		for (String type: new String[] { "http", "https" })
		{
			System.err.println("network_proxies: type=" + type);
			List<Proxy> l = null;
			try
			{
				l = defaultProxySelector.select(new URI(type + "://www.diviz.org"));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			if ( l != null )
			{
				for (Iterator<Proxy> iter = l.iterator(); iter.hasNext();)
				{
					java.net.Proxy proxy = iter.next();
					proxies.put(type, proxy.toString());
				}
			}
		}
		test_diviz(proxySettings);
	}

	public static void systemProperties(final Data data, final String key, final String[] properties)
	{
		final Data d = data.add(key);
		for (final String property: properties)
			d.put(property, rethrowSupplier(() -> System.getProperty(property)));
	}

	public static void systemProperties(final Data data)
	{
		final String[] propertiesNames = System.getProperties().stringPropertyNames().toArray(new String[0]);
		Arrays.sort(propertiesNames);
		systemProperties(data, "system properties", propertiesNames);
	}

	public static void system(final Data data)
	{
		final Runtime rt = Runtime.getRuntime();
		final Data runtime = data.add("runtime");
		runtime.put("cpus", rt.availableProcessors());
		runtime.put("total memory", rt.totalMemory());
		runtime.put("free memory", rt.freeMemory());
	}

	public static Data fullInfo()
	{
		final Data data = new Data();
		data.put("id", id);
		data.put("timestamp_ms", System.currentTimeMillis());
		systemProperties(data, "os", new String[] { "os.arch", "os.name", "os.version" });
		system(data);
		screen(data);
		// fileStores(data);
		rootDirectories(data);
		filesystem(new File(System.getProperty("user.home")), "home", data);
		filesystem(new File(System.getProperty("user.dir")), "cwd", data);
		filesystem(new File(System.getProperty("java.io.tmpdir")), "tmp", data);

		systemProperties(data, "java",
		                 new String[] { "java.home", "java.vm.specification.version", "java.vm.specification.vendor",
		                                "java.vm.specification.name", "java.vm.version", "java.vm.vendor",
		                                "java.vm.name", "java.specification.version", "java.specification.vendor",
		                                "java.specification.name", "java.class.version", "java.class.path",
		                                "java.library.path", "java.io.tmpdir", "java.compiler", "java.ext.dirs" });
		systemProperties(data);
		network(data);
		return data;
	}

	public static void main(final String[] args)
	{
		System.err.println(fullInfo().json());
	}

	/* https://stackoverflow.com/questions/27644361/how-can-i-throw-checked-exceptions-from-inside-java-8-streams-
	 * without-wrapping */
	@FunctionalInterface
	private static interface Supplier_WithExceptions <T, E extends Exception>
	{
		T get() throws E;
	}

	private static <T, E extends Exception> Supplier<T> rethrowSupplier(final Supplier_WithExceptions<T, E> function)
	{
		return () -> {
			try
			{
				return function.get();
			}
			catch (final Exception exception)
			{
				throwAsUnchecked(exception);
				return null;
			}
		};
	}

	@SuppressWarnings("unchecked")
	private static <E extends Throwable> void throwAsUnchecked(final Exception exception) throws E
	{
		throw (E) exception;
	}

}
