/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.InputStream;

import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;


/**
 * This class contains various methods for accessing the system's properties used in the project.
 * 
 * The use of {@link System#getProperty(String)} is discouraged in the project; instead, a method should be defined
 * here testing the property or returning its value.
 * 
 * @author Sébastien Bigaret
 *
 */
public abstract class Environment
{
	/**
	 * 
	 */
	public static final String DEFAULT_CONFIGURATION_FILE = "configurations/configuration.defaults";

	protected static final String DEFAULT_CONFIGURATION_KEY = "PRAXIS_DEFAULT_CONFIGURATION";
	/**
	 * 
	 */
	public static final String IGNORE_CONFIGURATION_DEFAULTS = "PRAXIS_IGNORE_CONFIGURATION_DEFAULTS";
	/**
	 * Tells whether we run in "development mode" (see project's doc for details).
	 * @return true if property DEVELOPMENT_MODE is set
	 */
	public static boolean development_mode()
	{
		return isPropertySet("DEVELOPMENT_MODE");
	}
	
	/**
	 * Tells whether we run in "ciphered mode" (see project's doc for details).
	 * @return true if property CIPHERED_MODE is set
	 */
	public static boolean ciphered_mode()
	{
		return isPropertySet("CIPHERED_MODE");
	}
	
	/**
	 * 
	 * @return
	 * @throws ResourceNotFoundException if the corresponding file could not be found
	 */
	public static InputStream getConfigurationDefaults() throws ResourceNotFoundException
	{
		String configurationDefaultsFile = System.getProperty(DEFAULT_CONFIGURATION_KEY);
		final InputStream defaultsStream;
		if (configurationDefaultsFile==null)
			defaultsStream = FileResources.inputStreamForResource(DEFAULT_CONFIGURATION_FILE);
		else if (Environment.IGNORE_CONFIGURATION_DEFAULTS.equals(configurationDefaultsFile))
			defaultsStream = null;
		else
			defaultsStream = FileResources.inputStreamForResource(configurationDefaultsFile);

		return defaultsStream;
	}
	
	public static String getConfigurationFile()
	{
		String conf = System.getProperty("CONFIGURATION");
		if (conf==null || "".equals(conf))
			conf = "configuration/configuration.cfg";
		return conf;
	}
	
	/**
	 * Returns the current working directory.  This method is just used as a marker for easily identifying where
	 * this is needed in the code.
	 * @return the value of the system's property <code>user.dir</code>.
	 */
	public static String getCurrentWorkingDirectory()
	{
		return System.getProperty("user.dir");
	}
	
	/**
	 * When in "loose credentials" mode, the client stores its unique id using a system's property named after the
	 * configuration's name.
	 * @return the prefix value as an non-empty string, or null if {@link #useLooseCredentials()} is false.
	 * @see #useLooseCredentials()
	 */
	public static String getLooseCredentialsConfigurationName()
	{
		String configuration = System.getProperty("CSLC");
		return configuration != null && !"".equals(configuration) ? "cred_"+configuration : null;
	}
	
	/**
	 * Indicates to all components (client, server, platform) on which platform it runs.
	 * @return the value of the property PLATFORM_OS (should be either WINDOWS or UNIX, but this is not checked yet TODO)
	 */
	public static String getPlatformOS()
	{
		if (org.apache.commons.lang.SystemUtils.IS_OS_WINDOWS)
			return "WINDOWS";
		else if (org.apache.commons.lang.SystemUtils.IS_OS_UNIX)
			return "UNIX";
		else
			return "unknown";
	}
	
	/**
	 * Returns the user's home directory.  This method is just used as a marker for easily identifying where
	 * this is needed in the code.
	 * @return the value of the system's property <code>user.home</code>.
	 */
	public static String getUserHome()
	{
		return System.getProperty("user.home");
	}
	
	/**
	 * Returns the user's language.  This method is just used as a marker for easily identifying where
	 * this is needed in the code.
	 * @return the value of the system's property <code>user.language</code>.
	 */
	public static String getUserLanguage()
	{
		return System.getProperty("user.language");
	}
	
	/**
	 * Return true if the property is set (even if it's empty), false otherwise
	 * @param propertyName
	 *            the system's property to test
	 * @return true if the system's property is set
	 */
	public static boolean isPropertySet(String propertyName)
	{
		return System.getProperty(propertyName) != null;
	}

	/**
	 * Indicates to platforms whether they should leave the temporary execution directory untouched after execution
	 * is finished, instead of deleting it.
	 * @return true if property LEAVE_EXEC_DIR_ON_PLATFORM is set
	 */
	public static boolean leaveExecutionDirectoryOnPlatform()
	{
		return isPropertySet("LEAVE_EXEC_DIR_ON_PLATFORM");
	}
	
	/**
	 * Indicates where the file containing the defaults for praxis' {@link Configuration configuration} can be found.
	 * <ul>
	 * <li>A null value (the default) is equivalent to {@link #DEFAULT_CONFIGURATION_FILE};</li>
	 * <li>the special value {@link #IGNORE_CONFIGURATION_DEFAULTS} instructs the framework to ignore the defaults, as
	 * expected,</li>
	 * <li>any other value is used as the path pointing to the default configuration file.</li>
	 * </ul>
	 * Please note that the empty string (<code>""<code>) is not a special value: it will be considered as a path, so
	 * the framework will fail to initialize its configuration; this is intended to avoid confusion, since some people
	 * would think that the empty string means "ignore configuration" while others would consider it as a way to say
	 * "use the default value".
	 * @param defaultsFilename the path pointing to the file containing the configuration defaults
	 */
	public static void setConfigurationDefaults(String defaultsFilename)
	{
		if (defaultsFilename==null)
			System.clearProperty(DEFAULT_CONFIGURATION_KEY);
		else
			System.setProperty(DEFAULT_CONFIGURATION_KEY, defaultsFilename);
	}

	/**
	 * Indicates to the client that it runs in the "simple command" mode (see project's doc. for details).
	 * @return true if property SIMPLE_COMMAND_LINE has been set
	 */
	public static boolean simpleCommandLine()
	{
		return isPropertySet("SIMPLE_COMMAND_LINE");
	}
	
	/**
	 * Indicates to the 3 elements: client, server and platforms that they all run inside the same process.
	 * @return true if property STANDALONE_EXEC is set
	 */
	public static boolean standaloneExecution()
	{
		return isPropertySet("STANDALONE_EXEC");
	}
	
	/**
	 * On the client-side, tells if the client uses so-called "loose" credentials; on the server-side, it tells
	 * whether the server accepts them.<br>
	 * "Loose credentials" mode is a mode where the client makes up a unique login the 1st time and re-uses it
	 * afterwards. On the server-side, the server will accept such logins without checking the users'DB. This is used
	 * in environments where one do not want to manage a users'DB: everyone is allowed to connect and use the service,
	 * so all what we need is that each client uses a unique login.
	 * @return true if the property's value is a non-empty string.
	 * @see #getLooseCredentialsConfigurationName()
	 */
	public static boolean useLooseCredentials()
	{
		String prefix = System.getProperty("CSLC");
		return prefix != null && !"".equals(prefix);
	}
	
	
}
