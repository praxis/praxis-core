/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.protocol.connection;

/**
 * Interface responsable pour ecouter le progress de la transference de donnees sur une connexion. Sa utilisation peut
 * être utilise, par example, pour notifier l'usager du progress de la transference d'un fichier.
 * Cet interface suvre le pattern Observer.
 *
 * @author Luiz Fernando Rodrigues
 * @version $Revision: 1.2 $
 */
public interface TransferProgressListener
{
	/**
	 * Indiquer au ecouter la quantite d'octets seront envoies
	 * @param max - La quantite de octets qui seront envoies
	 */
	public void setMaxTaile(int max);
	
	/***
	 * Indique le progress de la transference au ecouteur
	 * @param increment - La quantite qui sera ajouté au total de octets déjà transmis
	 */	
	public void majProgres(int increment);	
}
