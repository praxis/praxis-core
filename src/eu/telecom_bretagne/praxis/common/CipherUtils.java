/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class CipherUtils
{
    private static String algo = "Blowfish";
    
    /**
     * Copies the specified array, truncating or padding with zeros
     * (if necessary) so the copy has the specified length.
     * (copy/pasted from javadoc 1.6, Array.copyOf())
     */
    public static byte[] copyOf(byte[] original, int newLength)
    {
        byte[] copy = new byte[newLength];
        Arrays.fill(copy, (byte)0);
        for (int i=0; i<Math.min(original.length, newLength); i++)
            copy[i] = original[i];
        return copy;
    }
    
    public static void encrypt(String password, File input, File output)
    throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        byte[] buf = copyOf(password.getBytes("ISO-8859-1"), Cipher.getMaxAllowedKeyLength(algo)/8);
        Key key = new SecretKeySpec(buf, algo);
        Cipher cipher = Cipher.getInstance(algo);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        try (CipherInputStream cis = new CipherInputStream(new FileInputStream(input), cipher);
             FileOutputStream fos = new FileOutputStream(output))
        {
        	int numRead = 0;
        	while ( ( numRead = cis.read(buf) ) >= 0 )
        	{
        		fos.write(buf, 0, numRead);
        	}
        }
	}

    public static String encrypt(File inputFile, String input, String charset)
    throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
     {
        byte[] buf = copyOf((inputFile.getName()+"DESede/ECB/ISO10126Padding").getBytes("ISO-8859-1"), Cipher.getMaxAllowedKeyLength(algo)/8);
        Key key = new SecretKeySpec(buf, algo);
        Cipher cipher = Cipher.getInstance(algo);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return new String(cipher.doFinal(input.getBytes(charset)));
    }

    public static InputStream decryptedStream(InputStream stream, String filename)
    throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        return decryptedStream(filename+"DESede/ECB/ISO10126Padding", stream);
    }

    public static InputStream decryptedStream(String password, File input)
    throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
    	try (InputStream is = new FileInputStream(input)) {
    		return decryptedStream(password, is);
    	}
    }
    
    protected static InputStream decryptedStream(String password, InputStream stream)
    throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
    {
        byte[] buf = copyOf(password.getBytes("ISO-8859-1"), Cipher.getMaxAllowedKeyLength(algo)/8);
        Key key = new SecretKeySpec(buf, algo);
        Cipher cipher = Cipher.getInstance(algo);
        cipher.init(Cipher.DECRYPT_MODE, key);

        return new CipherInputStream(stream, cipher);
    }
    
    public static String decrypt(String password, File input)
    throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {

        InputStream is = decryptedStream(password, input);

        byte[] buf = new byte[1024];
        int numRead = 0;
        String result = "";
        while ((numRead = is.read(buf)) >= 0) {
            result += new String(buf, 0, numRead);
        }
        is.close();
        return result;
    }

    public static void main(String[] args) throws Exception
    {
        if ( args.length == 4 && args[0].equals("encrypt") )
            encrypt(args[1], new File(args[2]), new File(args[3]));
        else if ( args.length == 3 && args[0].equals("decrypt") )
            System.out.println(decrypt(args[1], new File(args[2])));
        else
            System.err.println("Usage: encrypt pass src dst / decrypt pass src\n");

    }
}
