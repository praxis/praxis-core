/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

public interface EventListener
    extends java.util.EventListener
{
	/**
	 * Called when the source of the events is not "connected" anymore. The exact meaning of "connected" depends on
	 * the source and the way it is connected to the receiver; however, in any cases, this methods gets called
	 * whenever it is sure that the source of the events will not be able to send messages anymore
	 * @param exception
	 *            the exception supplied, if any, is the raised exception from which the decision was made that the
	 *            source of the events is (or has been) disconnected. This is for information only, and it may be
	 *            null.
	 */
	public abstract void disconnected(Exception exception);
}
