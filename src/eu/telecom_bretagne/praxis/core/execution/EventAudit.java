/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public abstract class EventAudit implements Serializable
{   
	private static final long serialVersionUID = 5111730534767676711L;
	public static enum EventType
	{
		Created,
		StateChanged,
		ContextChanged,
		ActivityResultChanged,
		ActivityAssignmentChanged,
	}
	public static class CreateEventAudit extends EventAudit
	{
		private static final long serialVersionUID = 8293291639955925926L;

		public CreateEventAudit(ExecutionObject execObject)
		{
			super(EventType.Created, execObject);
		}
	}
	public static class StateEventAudit extends EventAudit
	{
		private static final long serialVersionUID = -2771756808004174329L;

		public final Result.Status newState;
		/**
		 * 
		 * @param execObject
		 * @param newState the new state (non null)
		 * @throws IllegalArgumentException if newState is null
		 */
		public StateEventAudit(ExecutionObject execObject, Result.Status newState)
		{
			super(EventType.StateChanged, execObject);
			this.newState = newState;
		}
	}
	public static class DataEventAudit extends EventAudit
	{
		private static final long serialVersionUID = 3119369264430857388L;

		/**
		 * 
		 * @param type either ContextChanged or ActivityResultChanged
		 * @param execObject
		 */
		public DataEventAudit(EventType type, ExecutionObject execObject)
		{
			super(type, execObject);
		}
	}
	public static class AssignmentEventAudit extends EventAudit
	{
		private static final long serialVersionUID = -8608688491870520697L;

		/**
		 * 
		 * @param execObject
		 */
		public AssignmentEventAudit(ExecutionObject execObject)
		{
			super(EventType.ActivityAssignmentChanged, execObject);
		}
	}
	/** the moment when the event happens */
	protected final Date timestamp = new Date();
	
	protected final EventType eventType;
	
	/**
	 * Either an activity or a process
	 */
	protected ExecutionObject executionObject;

	public EventAudit(EventType eventType, ExecutionObject executionObject)
	{
		this.eventType = eventType;
		this.executionObject = executionObject;
	}
	
	public EventType eventType()
	{
		return eventType;
	}
	
	public Date timestamp()
	{
		return timestamp;
	}
	
	public ExecutionObject object()
	{
		return executionObject;
	}
	@Override
	public String toString()
	{
	    return "["+this.eventType.name()+"] "+this.executionObject;
	}
}
