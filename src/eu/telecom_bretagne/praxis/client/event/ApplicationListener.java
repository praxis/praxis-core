/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client.event;

import java.util.EventListener;

/**
 * 
 * @author Sébastien Bigaret
 */
public interface ApplicationListener extends EventListener
{   
	/**
	 * Invoked when the application exits.  At this point there is no way to interrupt the process.
	 * Code needing to know whether this event has already been fired will check
	 * {@link eu.telecom_bretagne.praxis.common.Application#exiting()}.
	 */
	public void applicationExiting();
}
