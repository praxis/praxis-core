#! /bin/bash
# Extracts the keys from the properties files,
# and check that both files define the same subset

sed -E '/^[^=]*$/ d;s/^([^ =]+).*$/\1/' praxis_en.properties \
    | sort -o praxis_en.keys

sed -E '/^[^=]*$/ d;s/^([^ =]+).*$/\1/' praxis_fr.properties \
    | sort -o > praxis_fr.keys

diff -s praxis_en.keys praxis_fr.keys
