/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import static eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.Type.AUTHORIZATION_RESULT;
import static eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.Type.EXECUTION_RESULTS;
import static eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.Type.EXECUTION_STATUS;
import static eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.Type.LIST_OF_AVAILABLE_RESULTS;
import static eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.Type.MESSAGE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.ReleaseInfo;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.EventAudit;
import eu.telecom_bretagne.praxis.core.execution.EventAudit.StateEventAudit;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ExecutionObject.Requester;
import eu.telecom_bretagne.praxis.core.execution.Process;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.execution.ResultStore;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;


/**
 * An instance of Client is part of the connection between a client and the server, on the server-side. It is
 * responsible for sending to the client any messages on the server's behalf.
 * @see eu.telecom_bretagne.praxis.server.Server which is the other part of the client/server connection, receiving and processing the
 *      messages that the client sends to the server.
 * @author Sébastien Bigaret
 */
public class Client implements Requester
{
    /**
     * Sends to a client a message indicating that its authentification
     * failed.  Please note that when the method returns,
     * <b>the cnx is disconnected</b>.
     * @param cnx the connection object to use
     * @param reject_msg 
     */
    public static void notifyAuthentificationFailureAndDisconnect(CommunicationFacade cnx, String reject_msg)
    {
        // see comments in Server.clientSendsCredentials() for details on why we have this
        notifyAuthentificationFailureAndDisconnect(cnx, (Serializable)reject_msg);
    }

    /**
     * Sends to a client a message indicating that its authentification
     * failed.  Please note that when the method returns,
     * <b>the cnx is disconnected</b>.
     * @param cnx the connection object to use
     * @param reject_msg
     */
    public static void notifyAuthentificationFailureAndDisconnect(CommunicationFacade cnx, String[] reject_msg)
    {
        // see comments in Server.clientSendsCredentials() for details on why we have this
        notifyAuthentificationFailureAndDisconnect(cnx, (Serializable)reject_msg);
    }

    /**
     * Sends to a client a message indicating that its authentification
     * failed.  Please note that when the method returns,
     * <b>the cnx is disconnected</b>.
     * @param cnx the connection object to use
     * @param reject_msg 
     */
    private static void notifyAuthentificationFailureAndDisconnect(CommunicationFacade cnx, Serializable reject_msg)
    {
        ServerToClientEvent answer = new ServerToClientEvent(AUTHORIZATION_RESULT);

        answer.data = reject_msg;
        cnx.send(answer);
        try
        {
        	// just wait a little so that the message is delivered to the other side
        	// this is esp. important w/ direct communication since in this case, disconnection
        	// can happen before the message is delivered TODO should be fixed
        	Thread.sleep(500);
        }
        catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
        cnx.disconnect();
    }

    /**
     * Login sent by the client. It defaults to null and should only be changed
     * after the login has been confirmed.
     */
    private String login = null;

    /** */
    protected CommunicationFacade cnx;
        
    /**
     * Creates a new Client; note that a Client object is created as soon as
     * a client connects to the server.
     * At creation time, 
     * <br>
     * @param cnx connection established between the client and the
     * server
     * @param login the login of the user
     * @exception IllegalArgumentException if login is null
     */
    public Client(CommunicationFacade cnx, String login)
    {
        if (login == null)
            throw new IllegalArgumentException("login cannot be null");
        this.cnx = cnx;
        this.login = login;
        //notifyAuthentificationSuccess();
    }

    /**
     * Sends to the client a message indicating that its authentification
     * was successful
     */
    protected void notifyAuthentificationSuccess()
    {
        ServerToClientEvent answer = new ServerToClientEvent(AUTHORIZATION_RESULT);
        answer.data = ReleaseInfo.application_revision;
        cnx.send(answer);
    }


    /**
     * Returns the login for the client. Note that the login equals to
     * null until {@link #notifyAuthentificationSuccess()} is called.
     * @return client login
     */
    public String getLogin()
    {
        return login;
    }

    /**
     * Indicates whether the client is still connected.
     */
    public boolean isConnectionAlive()
    {
        return cnx.isConnected();
    }

	/**
	 * Sends to the client the list of the available results for the requested workflow. The results are sent in the
	 * form of a "summary", see {@link ResultStore#getResultsSummaryForWorkflow(WorkflowID, String)}.
	 * @param workflowID
	 *            id of the workflow whose results should be sent
	 */
	public void sendResultsSummary(WorkflowID workflowID)
	{
		ServerToClientEvent event = new ServerToClientEvent(LIST_OF_AVAILABLE_RESULTS);
		event.workflowID = workflowID;
		ArrayList<Result> results = Serveur.resultStore().getResultsSummaryForWorkflow(workflowID, login);
		if (results == null)
		    results = new ArrayList<>();
		// method getResultsSummaryForWorkflow() returns results w/ null zipFile, we want to make it explicit that
		// zipFile should not be sent.
		for (Result result: results)
			result.setZipFile(null);
		event.data = results;
		cnx.send(event);
	}

	public void sendResultsSummary()
	{
		ServerToClientEvent event = new ServerToClientEvent(LIST_OF_AVAILABLE_RESULTS);
		event.workflowID = null;
		ArrayList<Result> results = Serveur.resultStore().getResultsSummaryForUser(login);
		if (results==null || results.isEmpty())
			return;
		// no matter that the previous methods returns results w/ null zipFile, we want to make it explicit that
		// zipFile should not be sent.
		for (Result result: results)
			result.setZipFile(null);
		event.data = results;
		cnx.send(event);
	}

    /**
     * 
	 * @param workflowID
	 * @param index
	 */
    public void sendResult(WorkflowID workflowID, Result result_summary)
    {
    	Result full_result = Serveur.resultStore().getResult(result_summary.workflowID(),
    	                                                     result_summary.executionID(),
    	                                                     login);
        ServerToClientEvent event = new ServerToClientEvent(EXECUTION_RESULTS);
        event.workflowID = workflowID;
        event.data = full_result;
        if (full_result != null)
        {
        	event.file_conveyor = full_result.zipFile();
        	full_result.setZipFile(null);
        	full_result.readyToSerialize();
        }
        cnx.send(event);
    }

	/**
	 * 
	 * @param workflowID
	 * @param index
	 */
	public void deleteResults(WorkflowID workflowID, java.util.List<ExecutionID> results)
	{
    	if ( results == null )
    		Serveur.resultStore().deleteAllResults(workflowID, login);
    	else
    		Serveur.resultStore().deleteResults(workflowID, results, login);
    }
    
    /**
     * 
	 * @param message
	 * @param workflowID
	 */
    protected void sendMessage(String message, boolean forward, WorkflowID workflowID)
    {
        ServerToClientEvent event = new ServerToClientEvent(MESSAGE);
        event.workflowID = workflowID;
        event.data = new Object[] { message, null };
        event.forward=forward;
        cnx.send(event);
    }

    public void sendMessage(String message, WorkflowID workflowID)
    {
    	sendMessage(message, false, workflowID);
    }

    public void sendAndForwardMessage(String message, WorkflowID workflowID)
    {
    	sendMessage(message, true, workflowID);
    }
   /**
     * 
     * @param message
     * @param error
     * @param workflowID
     */
    public void sendMessage(String message, Exception error, WorkflowID workflowID)
    {
        ServerToClientEvent event = new ServerToClientEvent(MESSAGE);
        event.workflowID = workflowID;
        event.data = new Object[] { message, error };
        cnx.send(event);
	}
	
	/**
	 * Sends the status of an execution, embedded in a result object.
	 * @param result
	 *            the result object storing an execution status.
	 * @throws IllegalArgumentException
	 *             if {@code result.zipFile} is not {@code null}.
	 */
    public void sendExecutionStatus(Result result)
    {
    	if (result.zipFile()!=null)
    		throw new IllegalArgumentException("Result.zipFile must be null");
    	ServerToClientEvent event = new ServerToClientEvent(EXECUTION_STATUS);
    	event.workflowID = result.workflowID();
    	event.data = result;
    	cnx.send(event);
    }
    
    /**
	 * Two clients are said to be equal iff they share the same login
	 */
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Client)
        {
            return ((Client)obj).getLogin().equals(login);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
    	return login.hashCode();
    }

    @Override
    public String toString()
    {
        return "Client: "+getLogin()+" ("+super.toString()+")";
    }
    
    public void handleEvent(EventAudit event)
    {
    	Log.log.finest("Handling event:  " + event.toString());
    	if ( ! isConnectionAlive() )
    		return;
    	if (event.eventType().equals(EventAudit.EventType.StateChanged))
    	{
    		StateEventAudit sea = (EventAudit.StateEventAudit) event;
    		
    		final Process currentProcess;
    		if (sea.object() instanceof Activity)
    			currentProcess = ( (Activity) event.object() ).getContainer();
    		else if (sea.object() instanceof Process && sea.newState!=Status.RUNNING)
    			currentProcess = (Process) event.object();
    		else
    			currentProcess = null;

        	Log.log.finest(()->"current process" + (currentProcess==null?"<null>":currentProcess));
        	if (currentProcess==null)
    			return;

        	final Result updatedResult = currentProcess.getCurrentExecutionSummary();
        	Log.log.finest(()->"updatedResult: " + (updatedResult==null?"<null>":updatedResult));
    		if (updatedResult==null)
    			return;
    		updatedResult.setZipFile(null);
    		updatedResult.setStatus(currentProcess.getState());
    		sendExecutionStatus(updatedResult);
    	}
    }
}
