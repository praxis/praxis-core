/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.core.execution;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;

import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;

/**
 * A set of activities ready to be executed on a given platform.
 * @author Sébastien Bigaret
 */
public class ExecutionSet extends AbstractSet<Program> implements java.io.Serializable
{
	private static final long serialVersionUID = -9199892622056742816L;

	public static class ProgramInfo implements java.io.Serializable
	{
		private static final long serialVersionUID = -6947059938375147209L;
		/**
		 * Set by execution planners to indicate in which order program of an execution set should be executed.
		 * Programs having the same sequence number will be runned independently or one after another, in a
		 * non-specified order.
		 * @see ExecutionSet#programsSortedForExecution()
		 */
		int sequence = 0;
	}
	
	private HashMap<Program, ProgramInfo> map = new HashMap<Program, ProgramInfo>();
	transient private PlatformDescription platform;

	/**
	 * A generic container that can be used by e.g. planning algorithms
	 */
	private Object tag = null;
	
	public ExecutionSet()
	{
		super();
	}
	
	/**
	 * Adds a program to the set. After a successful addition, the program's {@link Program#getExecutionSet()
	 * execution set} equals to <code>this</code>.
	 */
	@Override
	public boolean add(Program p)
	{
		if (map.containsKey(p))
			return false;

		ProgramInfo info = null;
		final ExecutionSet originalSet = p.getExecutionSet();
		if (originalSet != null)
		{
			info = originalSet.getInfoForProgram(p);
			p.getExecutionSet().remove(p);
		}
		else
			info = new ProgramInfo();
		p.setExecutionSet(this);
		map.put(p, info);
		return true;
 	}
	
	/**
	 * Merges the supplied set into this one.  Callers should normally call {@link Process#merge(Activity, Activity)}
	 * instead.
	 * @param aSet
	 *            the set to merge into this set. After the function returns, it is empty.
	 */
	void mergeWith(ExecutionSet aSet)
	{
		if (platform != aSet.platform)
			throw new IllegalArgumentException("Sets should be assigned to the same platform");
		for (Program p: aSet.toArray(new Program[aSet.size()])) // copy: avoid ConcurrentModificationExc.
		{
			this.add(p);
		}
	}
	
	/**
	 * Removes a program from the set. After a successful removal, the program's {@link Program#getExecutionSet()
	 * execution set} is <code>null</code>.
	 */
	@Override
	public boolean remove(Object program)
	{
		if (!(program instanceof Program))
			return false;
		boolean b = map.remove(program) != null;
		if (b)
		{
			((Program) program).setExecutionSet(null);
		}
		return b;
	}
	
	@Override
	public boolean removeAll(java.util.Collection< ? > c)
	{
		/*
		 * Extracted from java.util.AbstractSet
		 * 
		 * The problem in AbstractSet.removeAll(): if c.size() >= this.size(), remove() is not called and the
		 * removed activities' executionSets are not nullified.
		 */
		boolean modified = false;
		for (java.util.Iterator<?> i = c.iterator(); i.hasNext(); )
			modified |= remove(i.next());
		return modified;
	}
	
	@Override
	public void clear()
	{
		for (Program p: this)
			p.setExecutionSet(null);
		map.clear();
	}
	
	@Override
	public int size()
	{
		return map.size();
	}
	
	/**
	 * Returns the ProgramInfo object attached to a program. Modifying the returned object actually modifies the
	 * information attached to the supplied program within this execution set.
	 * @param program
	 * @return the ProgramInfo object attached to the {@code program}, or {@code null} if the set has no such program.
	 */
	public ProgramInfo getInfoForProgram(Program program)
	{
		return map.get(program);
	}

	public PlatformDescription getPlatform()
	{
		return platform;
	}
	
	/**
	 * Returns the tag attached to this object.  It is used by planning algorithms.
	 * @return the receiver's tag.
	 */
	public Object getTag()
	{
		return tag;
	}
	
	public void setPlatform(PlatformDescription aPlatform)
	{
		this.platform = aPlatform;
	}

	/**
	 * Sets the tag attached to this object.
	 * @param aTag the object to assign to this.tag
	 * @see #getTag()
	 */
	public void setTag(Object aTag)
	{
		tag = aTag;
	}
	
	/**
	 * An iterator where remove() is not allowed
	 */
	@Override
    public Iterator<Program> iterator()
    {
		return new Iterator<Program>()
		{
			private final Iterator <Program> provider = ExecutionSet.this.map.keySet().iterator();
			@Override
            public boolean hasNext() { return provider.hasNext(); }

			@Override
            public Program next() { return provider.next(); }

			@Override
            public void remove() { throw new UnsupportedOperationException(); }
		};
    }
	
	public Program[] programsSortedForExecution()
	{
		final ExecutionSet this_set = this;
		Program[] sortedPrograms = map.keySet().toArray(new Program[map.size()]);
		java.util.Arrays.sort(sortedPrograms,
		                      new java.util.Comparator<Program>() {
			@Override
			public int compare(Program o1, Program o2)
			{
				final int seq1 = this_set.getInfoForProgram(o1).sequence;
				final int seq2 = this_set.getInfoForProgram(o2).sequence;
				return seq1 > seq2 ? 1 : (seq1 == seq2 ? 0 : -1);
			}
		});
		return sortedPrograms;
	}
	
	@Override
	public String toString()
	{
		return super.toString() + " tag: " + ( tag == null ? "<null>" : tag.toString() );
	}
}
