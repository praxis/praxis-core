#! /bin/bash
set -e
VERSION=$(cat ${0%/*}/VERSION)
DATE=$(cat ${0%/*}/DATE)
GUI="../praxis-swing-gui"
cd "${0%/*}"/..
BUILD_DATE=$(date --iso-8601=seconds)

# NB: Changed files hereafter should be ignored by the version control,
# e.g. git update-index --assume-unchanged <file>
# or the commits retrieved here won't be the right ones
CORE_COMMIT=$(git describe --tags --always --broken)
GUI_COMMIT=$(cd "${GUI}" && git describe --tags --always --broken)

sed -i -e 's#<property name="praxis.version" value="[^"]*"/>#<property name="praxis.version" value="'${VERSION}'"/>#' build.xml
sed -e '/public static final String release / s/"[^"]*"/"'${VERSION}'"/' \
    -i src/eu/telecom_bretagne/praxis/common/ReleaseInfo.java
sed -e '/public static final String release_date / s/"[^"]*"/"'${DATE}'"/' \
    -i src/eu/telecom_bretagne/praxis/common/ReleaseInfo.java
sed -e "/DATE/ s/=.*;$/= \"${BUILD_DATE}\";/" \
    -i src/eu/telecom_bretagne/praxis/common/BuildInfo.java
sed -e "/COMMIT/ s/=.*;$/= \"${CORE_COMMIT}\";/" \
    -i src/eu/telecom_bretagne/praxis/common/BuildInfo.java

sed -e "/DATE/ s/=.*;$/= \"${BUILD_DATE}\";/" \
    -i ${GUI}/src/eu/telecom_bretagne/praxis/client/ui/BuildInfo.java
sed -e "/COMMIT/ s/=.*;$/= \"${GUI_COMMIT}\";/" \
    -i ${GUI}/src/eu/telecom_bretagne/praxis/client/ui/BuildInfo.java

./data/i18n/native_to_ascii.sh
\rm -f target/jars/praxis.jar target/jars/praxis_server_platform.jar

# even if the file is marked "assumed-unchanged", git describe marks it as
# dirty: refresh the index
( set +e; git update-index --refresh > /dev/null 2>&1 ; exit 0 )
( set +e; cd "${GUI}" && git update-index --refresh  > /dev/null 2>&1 ; exit 0)

ant praxis.build.standalone.jar  praxis.build.server_platform.jar

NEW_CORE_COMMIT=$(git describe --tags --always --broken)
if [ "${CORE_COMMIT}" != "${NEW_CORE_COMMIT}" ]; then
    echo "Warning: description of core commit changed!"
    echo "    ${CORE_COMMIT} -> ${NEW_CORE_COMMIT}"
fi
NEW_GUI_COMMIT=$(cd "${GUI}" && git describe --tags --always --broken)
if [ "${GUI_COMMIT}" != "${NEW_GUI_COMMIT}" ]; then
    echo "Warning: description of GUI commit changed!"
    echo "    ${GUI_COMMIT} -> ${NEW_GUI_COMMIT}"
fi


echo Built ${CORE_COMMIT} / ${GUI_COMMIT}
#
exit 0
