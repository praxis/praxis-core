package eu.telecom_bretagne.praxis.core.workflow;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.dgc.VMID;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;


public class WorkflowID
    implements Serializable, Comparable<WorkflowID>
{
	public static final long serialVersionUID = 9188607919017170485L;

	protected VMID id;
    protected String name;
    
    /** Builds a brand new WorkflowID */
    public WorkflowID()
    {
    	this.name = "<unknown>";
        id = new VMID();        
    }
 
    /** Rebuilds the object represented by <code>id_str</code>
     * @param id_str the string representation of the WorkflowID to build, as returned by
     * {@link #dumpID()}
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public WorkflowID(String id_str) throws IOException, ClassNotFoundException
    {
    	Object obj = Utile.deserializeObject(id_str);
    	try
    	{
    		Object[] id_name = (Object[]) obj;
        	id = (VMID) id_name[0];
        	name = (String) id_name[1];
    	}
    	catch (ClassCastException e) // COMPATIBILITY
    	{
    		// old format
    		id = (VMID) obj;
    		name = "<unknown>";
    	}
    }

    /** 
     * Builds the string representation of this object.
     * @return the id transformed to a String
     */
    public String dumpID()
	{
		try
		{
			return Utile.serializeObject(new Object[]{id,name});
		}
		catch (IOException e)
		{
			Log.log.log(Level.WARNING, "Unable to save the id", e); // TODO and then?
			return "";
		}
	}

    /** Returns a 20 bytes long hashcode for the object. */
    public String hash20()
    {
        MessageDigest md = null;
        try
        {
            md = MessageDigest.getInstance("SHA");
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.log.severe("NoSuchAlgorithmException for SHA, exiting");
            System.exit(-1);
        }
        Log.log.finest(dumpID());
		try
		{
	        md.update( Utile.serializeObject(id).getBytes() );
		}
		catch (IOException e)
		{
			Log.log.log(Level.WARNING, "Unable to get the hash code", e); // TODO and then?
			return null;
		}
        return Utile.byteArrayToHex(md.digest());
    }
    
    /**
     * Returns the name of the corresponding workflow.
     * @return the name
     */
    public String name() {
        return name;
    }

    /**
     * Sets the name of the corresponding workflow.
     * @parameter name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        // hashCode() and equals() should be kept in sync. Meaning, just relying on the internal id
        return id.hashCode();
    }
    
    @Override
    public boolean equals(Object anID)
    {
        if (anID==null) return false;
        if ( !(anID instanceof WorkflowID) ) return false; // handles null as well
        
        return id.equals(((WorkflowID)anID).id);
    }
    
    @Override
    public String toString() { return name(); }

    public int compareTo(WorkflowID anID) {
        return name().compareTo(anID.name());
    }
    
}
