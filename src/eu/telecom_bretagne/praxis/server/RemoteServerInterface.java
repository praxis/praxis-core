/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import org.jdom.Document;

public interface RemoteServerInterface extends Remote
{
	public interface ServerInterface extends RemoteServerInterface
	{
		/**
		 * @see RemoteServerInterface#isAvailable(String) for details
		 */
		public boolean isAvailable(String resourceID);
		/**
		 * @see RemoteServerInterface#sequence() for details
		 */
		public long sequence();
		public Document availableResources();
	}
	/**
	 * Tests whether a resource is available
	 * @param resourceID
	 *            the id for the resource to be tested
	 * @return true if the resource is available.
	 * @see #sequence()
	 */
	public boolean isAvailable(String resourceID) throws RemoteException;
	
	/**
	 * A sequence number that is incremented each time the underlying information changes. Callers are strongly
	 * encouraged to use this method to determine whether something has changed: when two successive calls to this
	 * method return the same integer, it is guaranteed that every other methods of this interface return the same
	 * value in the meantime; relying on this behaviour helps reducing the load on the server and on the network as
	 * well.
	 * @return the sequence number
	 */
	public long sequence() throws RemoteException;
	
	/**
	 * 
	 */
	public Document availableResources() throws RemoteException;
}
