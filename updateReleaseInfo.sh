#! /bin/bash

# mise à jour de ReleaseInfo.java avec les nums. de release Praxis core+GUI
# et date courante

# git log --pretty=format:'%H %ai' HEAD -1
# git log --pretty=format:'%h, %ad' --date=short -1

#core=$(cd ~/Projets/praxis-git/praxis-core; git rev-parse --verify HEAD | cut -c -10)
#swing=$(cd ~/Projets/praxis-git/praxis-swing-gui; git rev-parse --verify HEAD | cut -c -10)
core=$(cd ~/Projets/praxis-git/praxis-core; git log --pretty=format:'%H %ad' --date=short HEAD -1)
swing=$(cd ~/Projets/praxis-git/praxis-swing-gui; git log --pretty=format:'%H %ad --date=short' HEAD -1)

core_htag=${core%% *}
swing_htag=${swing%% *}

echo core:${core_htag:0:10} GUI:${swing_htag:0:10} 1>&2

cd ~/Projets/praxis-git/praxis-core/src/eu/telecom_bretagne/praxis/common
sed -i -e 's|\(public *static *final *String *release *= *"\)[^"]*";|\1'"core:${core_htag:0:10} GUI:${swing_htag:0:10}\";|" ReleaseInfo.java

today=$(date +"%Y-%m-%d")
sed -i -e 's|\(public *static *final *String *package_date *= *"\)[^"]*";|\1'"$today\";|" ReleaseInfo.java


