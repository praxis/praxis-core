package eu.telecom_bretagne.praxis.client;

import java.awt.HeadlessException;

import javax.swing.JOptionPane;

import eu.telecom_bretagne.praxis.common.Log;

public interface ErrorDialog
{

	/* No need to check Configuration.getBoolean("client.no_gui"): it loads to much classes (incl. sun.rmi.* classes)
	 * and we want to be able to display something when called with a non-supported jre/jdk, so we'd better load as
	 * little classes as possible. */

	/**
	 * Shows an error dialog message to the user. This includes:
	 * <ul>
	 * <li>opening a graphical dialog window displaying the error,
	 * <li>logging the error message at the {@link java.util.logging.Level#SEVERE <i>severe</i> level},
	 * <li>printing the error message on the {@link System#err standard error}.
	 * </ul>
	 * 
	 * @param title
	 *            the title of the message, or header of the dialog window shown
	 * @param message
	 *            the error message
	 */
	public static void showErrorMessage(String title, String message)
	{
		// print it & log it in any cases
		System.err.println("Error: " + title + " - "+message);
		Log.log.severe(()->title+" - "+message);
		try { JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE); }
		catch (HeadlessException e) { /* ignore */ }
	}

	/**
	 * Show a warning message to the user. This includes:
	 * <ul>
	 * <li>opening a graphical dialog window displaying the warning,
	 * <li>logging the warning message at the {@link java.util.logging.Level#WARNING <i>warning</i> level},
	 * <li>printing the warning message on the {@link System#err standard error}.
	 * </ul>
	 * 
	 * @param title
	 *            the title of the message, or header of the dialog window shown
	 * @param message
	 *            the error message
	 */
	public static void showWarningMessage(String title, String message)
	{
		// print it & log it in any cases
		System.err.println("Warning: " + title + " - "+message);
		Log.log.warning(()->title+" - "+message);
		try { JOptionPane.showMessageDialog(null, message, title, JOptionPane.WARNING_MESSAGE); }
		catch (HeadlessException e) { /* ignore */ }
	}

}
