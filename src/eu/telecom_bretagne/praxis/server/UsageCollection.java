/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.server;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Log;

/**
 * @author Sebastien Bigaret
 *
 */
class UsageCollection
{   
	static final java.io.File usage_directory;
	static
	{
		usage_directory = initDirectory();
	}

	static private java.io.File initDirectory()
	{
		String path = Configuration.get("collect_usage.in_directory");
		if ( path == null || "".equals(path.trim()) )
		{
			Log.log.info("collect_usage.in_directory unset: feature is disabled");
			return null;
		}

		java.io.File dir = new java.io.File(path.trim());
		if ( dir.isFile() )
		{
			Log.log.warning("Cannot initialize UsageCollection: file exists and is not a dir: "+dir.getPath());
			return null;
		}
		if ( ! dir.exists() )
		{
			if ( ! dir.mkdirs() )
			{
				Log.log.warning("Cannot initialize UsageCollection: cannot create directory: "+dir.getPath());
				return null;
			}
		}
		Log.log.info(()->"Collecting usage in directory "+dir);
		return dir;
	}

	static void workflowExecuted(byte[] workflow)
	{
		if (usage_directory == null)
			return;
		try
		{
		    final String date = new java.text.SimpleDateFormat("yyyyMMdd-HHmmssSSS-").format(new java.util.Date());
			java.io.File wfFile = java.io.File.createTempFile("wf_"+date, ".xml.gz", usage_directory);
			java.io.OutputStream os = new java.util.zip.GZIPOutputStream(new java.io.FileOutputStream(wfFile));
			os.write(workflow);
			os.close();
		}
		catch (Throwable t)
		{
			Log.log.log(java.util.logging.Level.WARNING, "Could not save workflow", t);
		}
	}
}
