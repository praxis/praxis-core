/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.workflow;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;


public class WorkflowInput implements java.io.Serializable
{
	private static final long serialVersionUID = -2207948454133764169L;

	/* the name of the root element in the parsed xml */
    protected static final String INPUT_NODE_NAME = "input";

    protected PropertyChangeSupport propChgSupport;

    public enum INPUT_TYPE { FILE, DATABASE, DIRECTORY }

    /** The type of the input
     * @see INPUT_TYPE
     */
    protected INPUT_TYPE type;
    
    protected List <String> filepaths = new ArrayList<>();
    
    /** Name given to the input element */
    protected String name;
    /** X-coordinate of the input element */
    protected int x;
    /** Y-coordinate of the input element */
    protected int y;    

    protected transient ArrayList<Parameter> parameters = new ArrayList<>();
    
    /**
     * This field is only used to mark the files copied from a result into the clipboard.  When pasting such an
     * input into a workflow, the file itself should be copied into an other location, so that the original file,
     * belonging to a result, is not subject to changes.
     */
    public transient boolean copiedFromResults = false;
    
    void addOutput(Parameter p)
    {
    	parameters.add(p);
        propChgSupport.firePropertyChange("outputs", null, outputs());
    }
    void removeOutput(Parameter p)
    {
    	parameters.remove(p);
        propChgSupport.firePropertyChange("outputs", null, outputs());
    }
    public Parameter[] outputs()
    {
    	return parameters.toArray(new Parameter[parameters.size()]);
    }
    
    protected WorkflowInput()
    {
    	this.propChgSupport = new PropertyChangeSupport(this);
    }
    public WorkflowInput(INPUT_TYPE type)
    {
    	this();
    	this.type = type;
    }
    public WorkflowInput(INPUT_TYPE type, String name, int x, int y)
    {
    	this();
        this.type = type;
        this.name = name;
        this.x = x;
        this.y = y;
    }
    
    public boolean isOfType(INPUT_TYPE aType) { return aType==type; }
    
    /**
	 * Tells whether the workflow input has a valid value. For files and directories, this is the case when
	 * {@link #filepaths()} is not empty, and all its elements correspond to existing files.
	 * 
	 * @return {@code true} if the workflow input has a valid value, {@code false} otherwise.
	 * @see Program#isReadyForExecution()
	 */
    public boolean hasaValidValue()
    {
    	switch (type)
    	{
    		case FILE:
    		case DIRECTORY:
    			if (filepaths().length == 0)
    				return false;
    			for (String filepath: filepaths())
    				if (!new java.io.File(filepath).exists())
    					return false;
    			return true;
    		case DATABASE:
    		default:
            	eu.telecom_bretagne.praxis.common.Utile.unimplemented("Unimplemented method for type:"+type);
            	return false;
    	}
    }

    /** Return the registered filepath
     * @return path, may be <tt>null</tt>
     * @throws IllegalStateException if the receiver has not the correct type,
     * or if two or more filepaths were registered.
     */
    public String filepath() {
        if ( isOfType(INPUT_TYPE.DATABASE) )
            throw new IllegalStateException("Not a file or a directory");
        if ( filepaths.size() > 1 )
            throw new IllegalStateException("More than one filepath registered");
        
        return filepaths.size()==0 ? null : filepaths.get(0);
    }

    /**
     * Returns the array of the registered file paths
     * @return an empty array when the receiver's type is not appropriate
     * ({@link INPUT_TYPE#DATABASE})
     */
    public String[] filepaths() {
        if ( isOfType(INPUT_TYPE.FILE) || isOfType(INPUT_TYPE.DIRECTORY))
            return filepaths.toArray(new String[filepaths.size()]);
        return new String[] {};
        
    }

    /** Unimplemented yet, see {@link #setContent(List)} also */
    public String[] banks() {
        eu.telecom_bretagne.praxis.common.Utile.unimplemented();
        return null;
    }
    
    /* XML related methods */

    public static WorkflowInput buildFromXML(Element xml) throws InvalidXMLException {
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW_INPUT);
        WorkflowInput input;

        checkRootNode(xml, xml_exc);
        try {
            input = new WorkflowInput(INPUT_TYPE.valueOf(xml.getAttributeValue("type").toUpperCase(Locale.ENGLISH)));
        }
        catch (IllegalArgumentException e) {
            throw xml_exc.setMsg("Invalid input type ("+xml.getAttributeValue("type")+"): "+e.toString());
        }
        input.fromXML(xml);
        return input;
   }

    public void fromXML(Element xml) throws InvalidXMLException {
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW_INPUT);
        checkRootNode(xml, xml_exc);
        if ( type == INPUT_TYPE.FILE )
            fileFromXML(xml, xml_exc);
        else if ( type == INPUT_TYPE.DIRECTORY )
            fileFromXML(xml, xml_exc);
        else
        	eu.telecom_bretagne.praxis.common.Utile.unimplemented("Unimplemented method for type:"+type);
    }

    
    /* protected methods */
    
    /** Checks that the supplied xml element's root has the correct name,
     * namely {@link #INPUT_NODE_NAME}.
     * @param xml the element to check
     * @param xml_exc the exception to raise if the element is incorrect, after
     * having set the appropriate
     * {@link InvalidXMLException#setMsg(String) message}.
     * @throws InvalidXMLException in case the xml element is incorrect
     */
    protected static void checkRootNode(Element xml, InvalidXMLException xml_exc)
    throws InvalidXMLException
    {
        if ( !xml.getName().equalsIgnoreCase(INPUT_NODE_NAME) )
            throw xml_exc.setMsg("root element should be <input/>");
    }
    
    /** Reads all <input/> attributes/element common to files, databases and
     * directories.
     * This method assumes without checking that the element is <input/>. 
     * @param xml the xml node to parse
     * @param xml_exc the exception to raise if the element is incorrect, after
     * having set the appropriate
     * {@link InvalidXMLException#setMsg(String) message}.
     * @throws InvalidXMLException 
     */
    protected void commonFromXML(Element xml, InvalidXMLException xml_exc)
    throws InvalidXMLException
    {
        name = xml.getAttributeValue("name");
        xml_exc.setName(name);

        try {
            x = Integer.valueOf(xml.getAttributeValue("x"));
            y = Integer.valueOf(xml.getAttributeValue("y"));
        }
        catch (NumberFormatException e) {
            throw xml_exc.setMsg("Attribute x and/or y: not an integer");
        }
    }

    /** Reads an <input/> element corresponding to a file.
     * This method assumes without checking that the element is <input/>. 
     * @param xml the xml node to parse
     * @param xml_exc the exception to raise if the element is incorrect, after
     * having set the appropriate
     * {@link InvalidXMLException#setMsg(String) message}.
     * @throws InvalidXMLException
     * @throws IllegalStateException if the receiver's {@link #type} is not
     * {@link INPUT_TYPE#FILE}.
     */
    protected void fileFromXML(Element xml, InvalidXMLException xml_exc)
    throws InvalidXMLException
    {
        XPath xpath = null;
        List <Element> xml_infiles;
        
        if (type != INPUT_TYPE.FILE && type != INPUT_TYPE.DIRECTORY)
            throw new IllegalStateException("Receiver's type is "+type);
        
        commonFromXML(xml, xml_exc);

        /* handle infiles */
        try { 
            xpath = XPath.newInstance("infile");
            xml_infiles = xpath.selectNodes(xml);
        }
        catch (JDOMException e) {
            String msg = "unable to select <infile/> nodes";
            if ( e.getMessage() != null )
            	msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        filepaths = new ArrayList<>();
        for (Element xml_infile: xml_infiles)
            filepaths.add( xml_infile.getText() );
        
    }
	
	/**
	 * Returns the object's name if it's not {@code null} or empty, or the (1st) file name in {@link #filepaths()} if
	 * it exists. Otherwise, returns {@code "unknown"}.
	 * @return the display name that the component wishes to be used for graphical representation.
	 */
    public String getDisplayName()
    {
    	if (name!=null && !"".equals(name))
    		return name;
    	if (filepaths.size()==0)
    		return "unknown";
    	return new java.io.File(filepaths.get(0)).getName();
    }
    
    public Element toXML() {
        Element xml = new Element("input");

        xml.setAttribute("type", type.name().toLowerCase())
        .setAttribute("x", ""+x).setAttribute("y", ""+y)
        .setAttribute("name", name);

        for (String file: filepaths) {
            xml.addContent(new Element("infile").setText(file));
        }

        return xml;
    }

    /* Getters and setters */

    public String getName() { return this.name; }

    public void setName(String name) {
    	String oldName = this.name;
    	this.name = name;
    	propChgSupport.firePropertyChange("name", oldName, name);
    }
	
	/**
	 * Sets the content of this object.
	 * @param content
	 *            the content to set. A {@code null} value is equivalent to the empty array.
	 */
	public void setContent(List <String> content) {
		if ( type == INPUT_TYPE.FILE || type == INPUT_TYPE.DIRECTORY)
		{
			List<String> _filepaths = this.filepaths;
			if (content == null)
				filepaths = new ArrayList<>();
			else
				filepaths = new ArrayList<>(content);
			propChgSupport.firePropertyChange("filepaths", _filepaths, filepaths);
		}
		else
			eu.telecom_bretagne.praxis.common.Utile.unimplemented();
	}
    
    public void setX(int x) {
    	int _x = this.x;
        this.x = x;
        propChgSupport.firePropertyChange("x", _x, x);
    }
    
    public void setY(int y) {
    	int _y = this.y;
        this.y = y;
        propChgSupport.firePropertyChange("y", _y, y);
    }
    
    public void invalidate() {
        //does nothing
    }

    /**
     * Returns the input's type
     */
    public INPUT_TYPE getType()
    {
    	return this.type;
    }
    
    /** Return the x-coordinate of the element */
    public int getX() {
        return x;
    }

    /** Return the y-coordinate of the element */
    public int getY() {
        return y;
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    	propChgSupport.addPropertyChangeListener(listener);
    }
    
    public PropertyChangeSupport getListeners() { 
    	return propChgSupport;
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    	propChgSupport.removePropertyChangeListener(listener);
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		parameters = new ArrayList<>();
	}
}