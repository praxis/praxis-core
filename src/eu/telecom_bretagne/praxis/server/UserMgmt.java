/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import java.io.File;
import java.util.logging.Level;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Environment;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;


/**
 * This class is dedicated to the management of users and their credentials.
 * 
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.fr>
 *
 */
public class UserMgmt
{
	protected static final String USERS_DB = Configuration.get("server.users");
	protected static final String USER_by_LOGIN_XPATH = "/users/user/login[text()='%s']";
	protected static final String PASS_TAG = "pass";

	protected static long xml_lastModificationTime=-1;
	protected static Document usersDB;
	
	/**
	 * Returns the users' credentials database. This method checks that the corresponding xml file has not been
	 * changed since the last time it was called; if it has been modified, it is reloaded. If an error occurs when
	 * rereading the xml file, the credentials are left untouched.
	 * @return the users' credential DB
	 */
	static protected synchronized Document getUsersDB()
	{
		String errMsg = "Couldn't read "+UserMgmt.USERS_DB+", users'DB unchanged";
		File usersDBFile = new File(UserMgmt.USERS_DB);
		if (usersDBFile.lastModified() > xml_lastModificationTime)
		{
			Document _usersDB = null;
	        try
	        {
	        	_usersDB = Facade_xml.read(FileResources.inputStreamForResource(UserMgmt.USERS_DB/*TODO, false*/),false); // USERS_DB remains in clear text
	        }
	        catch (InvalidXMLException | ResourceNotFoundException e) { Log.log.log(Level.SEVERE, errMsg, e); }

	        if (_usersDB!=null)
	        {
				xml_lastModificationTime = usersDBFile.lastModified();
	        	usersDB=_usersDB;
	        }
		}
		return usersDB;
	}
	
	/**
     * Checks that the supplied credentials are valid.
     * @param login a client login
     * @param pass a client password
     * @return true if login/password is valid
     */
    static synchronized boolean checkCredentials(String login, String pass)
    {
    	if ( Environment.useLooseCredentials() )
    		return login!=null && login.equals(pass);
    	
    	Document doc = getUsersDB();
        
        if ( doc == null )
        {
            Log.log.severe("No credentials ever registered?! check errors above");
            return false;
        }
    
		XPath xpa;
		try
		{
			xpa = XPath.newInstance(String.format(USER_by_LOGIN_XPATH, login));

			if (xpa.selectNodes(doc.getRootElement()).isEmpty())
				return false;
			Element user_xml = (Element) xpa.selectSingleNode(doc.getRootElement());
			String user_passwd = ((Element)user_xml.getParent()).getChildText(PASS_TAG);
			
			return ( user_passwd!=null && user_passwd.equals(pass) ); 
		}
		catch (JDOMException e)
		{
            Log.log.log(Level.SEVERE, "Unable to access credentials?!", e);
            return false;
		}
    }

}
