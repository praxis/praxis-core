/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class ExecutionID
    implements Serializable
{
	static final long serialVersionUID = 1L;
	
	/**
	 * Internal sequence number incremented each time the constructor {@link ExecutionID#ExecutionID()} is called, to
	 * avoid collision between two ids.
	 */
	static int        seq              = 0;
	
	String            id;
	
	/** The name assigned to the corresponding execution */
	protected String  name;
	
	/** A comment attached to this execution */
	protected String  comment;
	
	/**
	 * Builds a new ExecutionID object/
	 */
	public ExecutionID()
	{
		id = new SimpleDateFormat("yyyy-MM-dd'T'HH_mm_ss.SSSZ").format(new Date())+(seq++);
	}

	/**
	 * Builds an ExecutionID with the supplied id
	 * @param dumpedID the ID of an ExecutionID object as returned by {@link #dumpID()}
	 */
	public ExecutionID(String dumpedID)
	{
		this.id = dumpedID;
	}
	
	/**
	 * Returns the object's internal id as a String, in a form that is suitable for
	 * {@link ExecutionID#ExecutionID(String)}
	 * @return the internal ID transformed to a String. No assertion can be made as far as its form is concerned, and
	 *         while it looks like a formatted date for me moment being, it is subject to change in the next versions.
	 */
	public String dumpID()
	{
		return id;
	}
	
	/**
     * @return the name
     */
    public String getName()
    {
    	return name;
    }

	/**
     * @param name the name to set
     */
    public void setName(String name)
    {
    	this.name = name;
    }

	/**
     * @return the comment
     */
    public String getComment()
    {
    	return comment;
    }

	/**
     * @param comment the comment to set
     */
    public void setComment(String comment)
    {
    	this.comment = comment;
    }

	@Override
	public boolean equals(Object anExecutionID)
	{
		if (anExecutionID==null) return false;
		if ( !(anExecutionID instanceof ExecutionID) ) return false; // handles null as well
		
		return id.equals(((ExecutionID)anExecutionID).id);
	}
	
	@Override
	public int hashCode()
    {
        // hashCode() and equals() should be kept in sync. Meaning, just relying on the internal id
        return id.hashCode();
    }
    
	@Override
	public String toString()
	{
		return id;
	}
}
