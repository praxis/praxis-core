/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;

/**
 * @author Sébastien Bigaret
 *
 */
// NB: java.io.Console System.console() does not exist in many cases where we may want it anyway (such as within eclipse)
public class Console implements Runnable
{   
	public static interface ConsoleCommand
	{
		public abstract String description();
		public abstract String[] commands();
		/**
		 * Takes whatever action is needed for {@code command}.
		 * @param command
		 */
		public abstract void execute(String command, String argument, PrintStream outputStream);
	}
	
	private final Scanner input;
	private final PrintStream output;
	private final HashMap<String, ArrayList<ConsoleCommand>> commands = new HashMap<>();

	public void run()
	{
		try
		{
			loop();
		}
		catch (Throwable t)
		{
			Log.log.log(java.util.logging.Level.WARNING, "Console exiting", t);
			return;
		}
		Log.log.log(java.util.logging.Level.INFO, "Console exiting");
	}
	
	public void addCommand(ConsoleCommand command)
	{
		if (command.commands() == null || command.commands().length==0)
			throw new IllegalArgumentException("Invalid null or empty commands()");
		for (String keyword: command.commands())
			if (keyword==null || "".equals(keyword))
				throw new IllegalArgumentException("Invalid null or empty command");

		synchronized(commands)
		{
			for (String keyword: command.commands())
			{
				ArrayList<ConsoleCommand> kw_commands = commands.get(keyword);
				if (kw_commands == null)
				{
					kw_commands = new ArrayList<ConsoleCommand>();
					commands.put(keyword, kw_commands);
				}
				kw_commands.add(command);
			}
		}
	}
	
	private String help()
	{
		StringBuilder sb = new StringBuilder("\n");
		synchronized(commands)
		{
			for (String kw: commands.keySet())
			{
				sb.append(kw).append(":");
				for (ConsoleCommand c: commands.get(kw))
					sb.append("\t").append(c.description()).append("\n");
				sb.append("\n");
			}
		}
		return sb.toString();
	}
	
	private void loop()
	{
		String command = "", args;
		while (!"console.quit".equals(command))
		{
			try
			{
				Log.log.log(Level.FINEST, "Waiting for next()");
				String [] line = input.nextLine().split(" ", 2);
				command=line[0];
				args="";
				if ( line.length > 1 )
					args = line[1];
			}
			catch (NoSuchElementException | IllegalStateException e) { return; }
			ArrayList<ConsoleCommand> kw_cmds;
			synchronized(command)
			{
				kw_cmds = commands.get(command);
			}
			if (kw_cmds != null)
				for (ConsoleCommand cmd: kw_cmds)
				{
					try
                    {
	                    cmd.execute(command, args, output);
                    }
                    catch (Exception e)
                    {
	                    e.printStackTrace(output);
                    }
				}
		}
	}
	
	/**
	 * Launches a new thread, starting the console
	 */
	public void start()
	{
		new Thread(this).start();
	}

	/**
	 * Builds a new Console
	 * @param input
	 * @param output
	 */
	public Console(InputStream input, PrintStream output)
	{
		this.input = new Scanner(input);
		this.output = output;
		this.addCommand(new ConsoleCommand() {
			@Override
			public final String[] commands() { return new String[]{ "help" }; }
			@Override
			public String description() { return "print this help"; }
			@Override
			public void execute(String command, String argument, PrintStream s) { s.print(Console.this.help()); }
		});
	}
	
	public static void main(String[] args)
	{
		Console console = new Console(System.in, System.out);
		console.addCommand(new ConsoleCommand() {
			@Override
			public final String description() { return "A simple test command"; }
			@Override
			public final String[] commands() { return new String[]{"test"}; }
			@Override
			public void execute(String command, String argument, PrintStream printStream)
			{
				printStream.println("got: [" + (argument == null ? "<null>" : argument)+"] for cmd: "+command);
			}
		});
		console.start();
	}
}
