/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.platform.execution;

import java.io.File;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.platform.Platform;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;



public class PlatformExecution
    extends Thread
{
	protected final Platform platform;
	protected File infiles;
	protected File executionDirectory;
	protected ExecutionEngine executionEngine;
	
	/** 
	 * 
	 * @param platform
	 * @param executionEngine
	 * @param zippedInputFiles it cannot be null, but it can refer to a non-existent file if there is no input file
	 * (when non-existent, it is used nevertheless to determine, through getParentFile(), the execution directory.
	 */
	public PlatformExecution(Platform platform, ExecutionEngine executionEngine, File zippedInputFiles)
	{
		this.platform = platform;
		this.executionDirectory = zippedInputFiles.getParentFile();
		if (zippedInputFiles.exists())
			this.infiles = zippedInputFiles;
		else
			this.infiles = null;
		this.executionEngine = executionEngine;
	}
	
	@Override
	public void run()
	{
		Result result = null;
		executionEngine.setProgressMonitor(new ExecutionEngine.ExecutionProgressMonitor()
		{
			@Override
			public void setProgress(Result aResult)
			{
				platform.sendProgress(aResult);
			}
		});

		try
		{
			result = executionEngine.execute(executionDirectory, infiles);
		}

        catch(Exception e)
        {
            if(platform.executesWorkflow(executionDirectory))
            {
                // TODO !!!
            }
            Log.log.log(Level.WARNING,
                        "Error while executing the workflow: "+executionDirectory,
                        e);
            e.printStackTrace();
        }
        finally
        {
        	executionEngine.setProgressMonitor(null);
        }
        // if not interrupted AND results!=null ... TODO
        if (platform.executesWorkflow(executionDirectory) && result != null)        
        	platform.sendResults(result);
        
        if ( ! eu.telecom_bretagne.praxis.common.Environment.leaveExecutionDirectoryOnPlatform() ) {
            Utile.deleteRecursively(executionDirectory);
            Log.log.info(()->"Execution directory "+executionDirectory+" deleted");
        } else {
            Log.log.info(()->"Leaving execution directory "+executionDirectory+" untouched after execution is finished");
        }

	}
	public void cancel()
	{
		executionEngine.cancel();
		
	}
}
