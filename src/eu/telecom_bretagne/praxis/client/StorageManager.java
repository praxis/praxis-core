/* License: please refer to the file README.license located at the root directory of the project */

package eu.telecom_bretagne.praxis.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;


/**
 * Responsible for managing workflows on the client-side, along with the results of their executions. The component
 * caches informations about workflows and results stored in the workspace (i.e. on the file system), so that it is
 * easily available to other components.
 * @author Sébastien Bigaret
 * @implementation paths==null means that init() has NOT been called. You should never directly access to paths
 *                 directly here, always use paths() instead. The same happens to results: use results() instead
 */
public class StorageManager
    extends ServerToClientEvent.ServerToClientEventAdapter
{
	private static final String CURRENT = "current";

	public interface StorageListener
	{
		/**
		 * Called when a workflow has been added to the repository
		 * @param workflowID
		 *            id of the new workflow
		 */
		public void workflowAdded(WorkflowID workflowID);
		
		/**
		 * Called when a workflow object itself has changed (e;g. its name has changed)
		 * @param workflowID
		 *            id of the modified workflow
		 */
		public void workflowModified(WorkflowID workflowID);
		
		/**
		 * Called when a workflow has been deleted from the repository
		 * @param workflowID
		 *            id of the deleted workflow
		 */
		public void workflowDeleted(WorkflowID workflowID);
		/**
		 * Called when a result of a workflow has been added
		 * @param workflowID
		 *            the workflow on which changes happened
		 * @param result
		 *            The result which has been added.
		 */
		public void resultAdded(WorkflowID workflowID, Result result);
		
		/**
		 * Called when some results of a workflow have been updated. If the result's status is
		 * {@link Result.Status#isClosed() closed}, the supplied result's {@link Result#zipFile()} is non-null if and
		 * only if all results have already been received and stored by the StorageManager.
		 * @param workflowID
		 *            the workflow on which changes happened
		 * @param result
		 *            the result which has changed. A null value indicates that more than one change has occured, and
		 *            the receivers should consider that the whole set of results has changed.
		 */
		public void resultModified(WorkflowID workflowID, Result result);
		
		/**
		 * Called when a result of a workflow has been deleted
		 * @param workflowID
		 *            the workflow on which changes happened
		 * @param result
		 *            the result which has been deleted.
		 */
		public void resultDeleted(WorkflowID workflowID, Result result, int index);
	
	}
	
	/**
	 * An abstract adapter class for receiving storage update events. The methods in this class are empty. This class
	 * exists only as a convenience for creating listener objects.
	 * @author Sébastien Bigaret
	 */
	static public abstract class StorageListenerAdapter
	    implements StorageListener
	{
		@Override
		public void workflowAdded(WorkflowID workflowID)
		{}
		
		@Override
		public void workflowModified(WorkflowID workflowID)
		{}
		
		@Override
		public void workflowDeleted(WorkflowID workflowID)
		{}

		@Override
        public void resultAdded(WorkflowID workflowID, Result result)
        {
        }

		@Override
        public void resultModified(WorkflowID workflowID, Result result)
        {
        }

		@Override
        public void resultDeleted(WorkflowID workflowID, Result result, int index)
        {
        }
	}

	/**
	 * Interface for the {@link StorageManager}'s delegate.
	 * @author Sébastien Bigaret
	 */
	static public interface StorageManagerDelegateInterface
	{
		/**
		 * Called by the storage manager when it is receives update requests concerning unknown results, i.e. results
		 * whose worklow ID and execution ID do not {@link StorageManager#getResult(WorkflowID, ExecutionID) correspond}
		 * to any result it holds.
		 * 
		 * @param result 
		 * @return {@code true} if the result should be accepted by the storage manager.
		 */
		public boolean acceptUnknownResult(Result result);
	}

	/**
	 * The default delegate when none is supplied, or when the delegate is reset (set to {@code null}).
	 * @author Sébastien Bigaret
	 */
	static public class DefaultStorageManagerDelegate implements StorageManagerDelegateInterface
	{
		/**
		 * Rejects all requests
		 * 
		 * @return {@code false}
		 */
		public boolean acceptUnknownResult(Result result)
		{
			return false;
		}
	}

	static public final String SERIALIZED_RESULT_FILENAME = "praxis_serialized_result.xml";
	
	/** Paths of workflows */
	static private List<File>                 paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP;

	/** Paths for workflows that are created but not saved yet */
	static private List<File>                 reservedNames = new ArrayList<File>();

	/** Maps each workflow with its executions */
	static private HashMap<File, ArrayList<Result>> results_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP = new HashMap<File, ArrayList<Result>>();
	
	/** Contains the objects listening for changes in the repository */
	static private ArrayList<StorageListener> listeners     = new ArrayList<StorageListener>();
	
	/** A singleton object used to listen for {@link ServerToClientEvent} */
	static public final StorageManager singleton = new StorageManager();
	
	/** Whether the component should request the execution results when it is is notified that an execution is finished
	 * @see #receivedExecutionStatus(ServerToClientEvent)
	 */
	static public boolean requestFullResultAtEndOfExecution = true;

	/** Whether the component should store the results when receiving them.
	 * @see #receivedExecutionResults(ServerToClientEvent)
	 * */
	static public boolean storeReceivedExecutionResults = true;
	
	static private StorageManagerDelegateInterface delegate = new DefaultStorageManagerDelegate();

	static synchronized private void init()
	{
		if (paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP!=null)
			return;
		File[] dirs = PraxisPreferences.workspace().listFiles();
		paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP = new ArrayList<File>();
		
		for (File dir: dirs)
		{
			if (!dir.isDirectory())
				continue;
			
			String filename = dir.getName();
			
			if (filename.equals("tmp")) // still used on the server-side TODO change this name
				continue;
			
			// now try to find a valid workflow.<ext> somewhere with the directory
			String[] dirs_to_examine = dir.list();
			if ( dirs_to_examine == null )
				continue;
			
			for (String dir_to_examine: dirs_to_examine)
			{
				if (!new File(dir, dir_to_examine).isDirectory())
					continue;
				try
				{
					// We do not want to construct the workflow here, just check that it is valid
					checkWorkflowInDir(new File(dir, dir_to_examine));
				}
				catch (InvalidXMLException | IllegalArgumentException e)
				{
					Log.log.warning("Invalid workflow in directory: " + new File(dir, dir_to_examine).getName()
					                + ": " + (e.getMessage() != null ? e.getMessage() : "<unknown reason>\n"));
					continue;
				}
				if (CURRENT.equals(dir_to_examine))
				{
					if (!paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.contains(dir))
						paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.add(dir);
					continue;
				}
				// if not, maybe we just have results in there. If this is the case, that's ok, too
				Result result = resultInDir(new File(dir, dir_to_examine));
				if (result != null)
				{
					if (!paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.contains(dir))
						paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.add(dir);
					ArrayList<Result> workflow_results = results_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.get(dir);
					if (workflow_results==null)
						workflow_results = new ArrayList<Result>();
					workflow_results.add(result);
					results_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP.put(dir, workflow_results);
				}
			}
		}
	}
	
	private static List<File> paths()
	{
		if (paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP==null)
			init();
		return paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP;
	}

	private static HashMap<File, ArrayList<Result>> results()
	{
		if (paths_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP==null)
			init();
		return results_DO_NOT_USE_DIRECTLY_SEE_IMPLEMENTATION_COMMENT_ON_TOP;
	}

	public static void setDelegate(StorageManagerDelegateInterface delegate)
	{
		if (delegate==null)
			delegate=new DefaultStorageManagerDelegate();
		StorageManager.delegate = delegate;
	}

	/**
	 * @param dir
	 * @return
	 * @throws IllegalArgumentException if the directory has no file or more than one file with the appropriate
	 *             extension ( {@link Configuration configuration}'s key: file_extension)
	 */
	static private File workflowFileInDir(File dir) throws IllegalArgumentException
	{
		String[] bscFiles = dir.list(new FilenameFilter() {
			public boolean accept(File directory, String name)
			{
				return name.endsWith(Configuration.get("file_extension")) && new File(directory, name).isFile();
			}
		});
		
		if (bscFiles==null || bscFiles.length == 0 || bscFiles.length > 1)
			throw new IllegalArgumentException();
		
		return new File(dir, bscFiles[0]);
	}
	
	/**
	 * Checks that the supplied directory contains one valid workflow. A workflow is here said to be valid in the sense
	 * of {@link Workflow#checkDTD(org.jdom.Document)}.
	 * @param dir
	 *            the directory in which a workflow is searched.
	 * @throws IllegalArgumentException
	 *             if the directory has more than one file with the appropriate extension ( {@link Configuration
	 *             configuration}'s key: file_extension)
	 * @throws InvalidXMLException
	 *             in case the file is not valid.
	 */
	static private void checkWorkflowInDir(File dir) throws IllegalArgumentException, InvalidXMLException
    {
		final File workflowFile = workflowFileInDir(dir);
		Workflow.readWorkflowFile(workflowFile);
    }
	
	/**
	 * Searches the supplied directory for a valid workflow.
	 * @param dir
	 *            the directory in which a workflow is searched.
	 * @param warnings
	 *            may be null
	 * @return the workflow.
	 * @throws IllegalArgumentException
	 *             if the directory has more than one file with the appropriate extension ( {@link Configuration
	 *             configuration}'s key: file_extension)
	 * @throws InvalidXMLException
	 *             in case the workflow could not be built.
	 */
	static private Workflow workflowInDir(File dir, Workflow.XMLWarnings warnings, boolean executed)
	throws IllegalArgumentException, InvalidXMLException
	{
		File workflowFile = workflowFileInDir(dir);
		return new Workflow(workflowFile, warnings, executed);
	}
	
	static private Result resultInDir(File dir)
	{
		File psrFile = new File(dir, SERIALIZED_RESULT_FILENAME);
		
		try
		{
			if ( ! psrFile.isFile() )
				return null;
			return new Result(psrFile);
		}
		catch (Exception e) // incl. FileNotFoundExc
		{
			Log.log.warning("Invalid result in directory: " + dir.getName() + ": "
			                + (e.getMessage() != null ? e.getMessage() : "<unknown reason>"));
		}
		return null;
	}
	
	/**
	 * Returns the path to the directory containing the workflow
	 * @param id
	 *            id of the workflow
	 * @return path
	 */
	static public String rootWorkflowDirectoryPath(String name)
	{
		return PraxisPreferences.workspace().getPath() + File.separator + name;
	}

	/**
	 * Returns the directory containing the current version of a workflow
	 * 
	 * @param name
	 *            name of the workflow
	 * @return the directory containing the current version of a workflow
	 */
	// TODO private API
	static private File currentWorkflowDirectory(String name)
	{
		return new File(rootWorkflowDirectoryPath(name), CURRENT);
	}

	/**
	 * Returns the path to the directory containing the results of an execution
	 * @param id
	 *            id of the workflow
	 * @return path
	 */
	static public String resultDirectory(String workflowName, ExecutionID executionID)
	{
		return PraxisPreferences.workspace().getPath() + File.separator + workflowName + File.separator + executionID.dumpID();
	}
	
	/**
	 * Returns the names of all registered workflows. It include {@link #getWorkflowsNames() persistent workflows} and
	 * those whose name were marked as {@link #reserveName(String) reserved}.
	 * @return the names of all registered workflows.
	 */
	synchronized public static String[] getAllWorkflowsNames()
	{
		String[] all = new String[paths().size() + reservedNames.size()];
		for (int i = 0; i < paths().size(); i++)
			all[i] = paths().get(i).getName();
		for (int i = 0; i < reservedNames.size(); i++)
			all[i + paths().size()] = reservedNames.get(i).getName();
		return all;
	}
	
	/**
	 * Returns the list of the registered and persistent workflows
	 * @return the sorted list of the registered, persistent workflows' names
	 */
	synchronized public static String[] getWorkflowsNames()
	{
		String[] names = new String[paths().size()];
		for (int i = 0; i < paths().size(); i++)
			names[i] = paths().get(i).getName();
		Arrays.sort(names);
		return names;
	}
	
	/**
	 * Marks a workflow's name as reserved: when the user requests the creation of a new workflow, it is not saved on
	 * the filesystem yet, hence it is not part of the {@link #getWorkflowsNames() registered persistent workflows}.
	 * Reserving the name informs the StorageManager that the workflow exists but is not persistent yet.
	 * @param name
	 *            the name to be reserved
	 * @return {@code true} if the name was successfully reserved, {@code false} if an other registered workflow
	 *         already uses this name.
	 */
	synchronized public static boolean reserveName(String name)
	{
		if (Arrays.asList(getAllWorkflowsNames()).contains(name))
			return false;
		File reserved = new File(rootWorkflowDirectoryPath(name));
		reservedNames.add(reserved);
		return true;
	}
	
	/**
	 * Tells the component that this name is no longer reserved
	 * @param name
	 *            the name that to unregister
	 * @return true if the name was reserved
	 * @see #reserveName(String)
	 */
	synchronized public static boolean forgetName(String name)
	{
		return reservedNames.remove(new File(rootWorkflowDirectoryPath(name)));
	}
	
	synchronized public static void storeWorkflow(Workflow workflow) throws IOException
	{
		File workflow_dir = new File(rootWorkflowDirectoryPath(workflow.getName()));
		File current_workflow = currentWorkflowDirectory(workflow.getName());
		if (!current_workflow.exists() && !current_workflow.mkdirs())
			throw new IOException("Unable to create dir " + current_workflow);
		workflow.save(new File(current_workflow, workflow.getName() + Configuration.get("file_extension")), true);
		reservedNames.remove(workflow_dir);
		
		if (!paths().contains(workflow_dir))
		{
			paths().add(workflow_dir);
			notifyListenersWorkflowAdded(workflow.id());
		}
		else
		{
			notifyListenersWorkflowModified(workflow.id());
		}
	}
	
	/**
	 * Returns the workflow identified by the supplied name.
	 * @param workflowName name of the workflow
	 * @param warnings 
	 * @return the requested workflow itself
	 * @throws InvalidXMLException
	 * @throws IllegalArgumentException
	 */
	synchronized public static Workflow getWorkflow(String workflowName, XMLWarnings warnings)
	        throws InvalidXMLException, IllegalArgumentException
	{
		return workflowInDir(currentWorkflowDirectory(workflowName), warnings, false);
	}
	
	/**
	 * Deletes a workflow
	 * @param name
	 *            the name of the workflow to be deleted
	 */
	synchronized public static void deleteWorkflow(WorkflowID workflowID, String name)
	{
		File workflow_directory = new File(rootWorkflowDirectoryPath(name));
		paths().remove(workflow_directory);
		forgetName(name);
		Utile.deleteRecursively(workflow_directory);
		notifyListenersWorkflowDeleted(workflowID);
	}
	
	/**
	 * Delete a result
	 * @param name the name of the workflow to be deleted
	 */
	synchronized public static void deleteResult(String workflowName, Result resultToDelete)
	{
		String resultDirectory = resultDirectory(workflowName, resultToDelete.executionID());
		Utile.deleteRecursively(new File(resultDirectory));

		// On supprime la référence du résultat
		ArrayList<Result> results = results().get(new File(rootWorkflowDirectoryPath(workflowName)));
		// TODO the index that should be calculated here is not the index of the result within the internally
		// kept results() arrays, but the one in the returned array (#getResults(WorkflowID)).
		// In the future this list is subject to change, in particular regarding the way it is sorted
		// (plus, it filters out invalid results --CHECK why). For the moment being, we take the easy way
		// and send the notification with a null Result, meaning that undetermined number of changes in the results
		// have happened.
		//int index = results().indexOf(resultToDelete); 
		if (results.remove(resultToDelete))
		{
			notifyListenersResultDeleted(resultToDelete.workflowID(), null, -1);
		}
	}

	/**
	 * Delete all results for a given workflow.
	 * @param name the name of the workflow to be deleted
	 */
	synchronized public static void deleteAllResults(WorkflowID workflowID)
	{
		ArrayList<Result> results = results().get(new File(rootWorkflowDirectoryPath(workflowID.name())));
		for(Result resultToDelete : results)
		{
			String resultDirectory = resultDirectory(workflowID.name(), resultToDelete.executionID());
			Utile.deleteRecursively(new File(resultDirectory));
		}
		// Il n'y a plus de résultat du tout, on peut supprimer comlètement le référencement
		// du workflow
		results().remove(new File(rootWorkflowDirectoryPath(workflowID.name())));
		notifyListenersResultDeleted(workflowID, null, -1);
	}
	
	/**
	 * Has the given workflow results?
	 * @param name the name of the workflow to be deleted
	 */
	synchronized public static boolean hasResults(String workflowName)
	{
		ArrayList<Result> results = results().get(new File(rootWorkflowDirectoryPath(workflowName)));
		return results != null;
	}
	
	/**
	 * Renames a workflow.<br>
	 * <b>IMPORTANT NOTE:</b> the workflow is written back to the filesystem.
	 * @param workflow the workflow changing its name
	 * @param newName the new name for the workflow
	 * @return true if the renaming was successful, false otherwise
	 */
	synchronized public static boolean renameWorkflow(Workflow workflow, String newName)
	{
		String oldName = workflow.getName();
		File oldWFDir = new File(rootWorkflowDirectoryPath(oldName));
		File newWFDir = new File(rootWorkflowDirectoryPath(newName));
		if ( !oldWFDir.isDirectory() )
		{
			// it does not exist: this is a brand new workflow
			if (!reserveName(newName))
				return false;
			forgetName(oldName);
			workflow.setName(newName);
			return true;
		}
		/* try to move the current workflow's file */
		File oldCurrentDir = new File(oldWFDir, CURRENT);
		final String file_ext = Configuration.get("file_extension");
		if ( ! workflowFileInDir(oldCurrentDir).renameTo(new File(oldCurrentDir, newName+file_ext)) )
			return false;
		if ( ! oldWFDir.renameTo(newWFDir))
			return false;
		workflow.setName(newName);
		try {
			storeWorkflow(workflow); // adds it to paths()
		}
		catch (IOException e)
		{
			/* rollback, and stop */
			paths().remove(newWFDir);
			newWFDir.renameTo(oldWFDir);
			new File(oldCurrentDir, newName+file_ext).renameTo(new File(oldCurrentDir, oldName+file_ext));
			return false;
		}
		paths().remove(oldWFDir);
		// paths().add(newWFDir); // NO!! this is already done when storeWorkflow() was called, above
		forgetName(oldName);
		/* last, move executions and update their names */
		ArrayList <Result> _results = results().remove(oldWFDir);
		results().put(newWFDir, _results);
		if (_results!=null && _results.size()>0)
			for (Result result: _results)
				result.workflowID().setName(newName);
		notifyListenersWorkflowModified(workflow.id());
		return true;
	}

	/* **** Handling executions **** */
	/**
	 * Update the result cached at runtime (see {@link #results()}). If the result already exists, the supplied
	 * one is copied into the stored one, otherwise the result is simply added.
	 * @param workflowDirectory
	 *            the directory where a workflow stores its results.
	 * @param result
	 *            the result that should be added, or updated if a equivalent result was already cached (a result is
	 *            said to be equivalent to another if and only if they have equals workflowID and executionID).
	 * @return the stored, updated result. Be aware that the returned result is the one that is cached, so
	 *         modifications on the returned object will be reflected in the cached result.
	 */
	synchronized private static Result updateResultsMap(File workflowDirectory, Result result)
	{
		ArrayList<Result> workflow_results = results().get(workflowDirectory);
		if (workflow_results==null)
			workflow_results = new ArrayList<Result>();

		Result originalResult = null;
		for (Result r:workflow_results)
		{
			if (r.executionID().equals(result.executionID()) && r.workflowID().equals(result.workflowID()))
			{
				originalResult = r;
				break;
			}
		}
		if (originalResult!=null)
			originalResult.shallowCopy(result);
		else
		{
			originalResult = result;
			workflow_results.add(originalResult);
		}
		java.util.Collections.sort(workflow_results, Result.DateComparator);
		results().put(workflowDirectory, workflow_results);
		return originalResult;
	}
	
	public static Result createExecution(Workflow workflow) throws IOException
	{
		WorkflowID workflowID = workflow.id();
		ExecutionID executionID = new ExecutionID();

		/*
		 * create the result object that will be sent to the server: contains the ExecutionID as well as the
		 * workflow to execute
		 */
		Result result = new Result(workflowID, executionID);
		result.setStatus(Result.Status.RUNNING);

		File executionDirectory = StorageManager.directoryForExecution(workflow.id(), result.executionID());
		File zip = File.createTempFile("praxis_", ".pxz");
		Log.log.finest("preparing workflow for execution");
		Workflow executedWorkflow = workflow.prepareWorkflowForExecution(zip, executionDirectory);
		Log.log.finest("Workflow ready for execution, zip size:"+zip.length());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		executedWorkflow.save(baos, true);
		result.setWorkflowXML(baos.toByteArray());

		result = updateResultsMap(new File(rootWorkflowDirectoryPath(workflow.getName())), result);
		result.setZipFile(zip);
		notifyListenersResultAdded(workflowID, result);
		
		return result;
	}

	/**
	 * Asks the delegate whether an unknown result should be accepted. A result is considered as unknown if
	 * {@link #getResult(WorkflowID, ExecutionID)} returns {@code null} for its execution- and workflow-ids.
	 * When the result is already known by the storage manager, this methods return {@code true}.
	 * 
	 * @param result
	 *            the result to test
	 * @return {@code true} if the result is accepted.
	 */
	protected static boolean acceptResult(Result result)
	{
		if (getResult(result.workflowID(), result.executionID()) == null)
			return delegate.acceptUnknownResult(result);
		return true;
	}

	/**
	 * Stores the supplied result in the persistent store.
	 * @param result the stored result
	 * @param notify_listeners if true, a notification is sent to indicate that the supplied result has changed.
	 * @return the stored result
	 * @throws IOException if the result could not be written in the persistent store.
	 */
	private static Result updateResult(Result result, boolean notify_listeners) throws IOException
	{
		// TODO rename old dir., so that we can do this canonically
		File executionDir = directoryForExecution(result.workflowID(), result.executionID());
		
		Facade_xml.write(result.toXMLDocument(), new File(executionDir, SERIALIZED_RESULT_FILENAME));
		result = updateResultsMap(executionDir.getParentFile(), result);
		if (notify_listeners)
			notifyListenersResultModified(result.workflowID(), result);
		return result;
	}
	
	/**
	 * Updates the stored results and notify listeners. <br>
	 * This is equivalent to {@link #updateResult(Result, boolean) updateResult(result, true)}.
	 * @param result
	 *            the result that should be updated.
	 * @throws IOException
	 *             in case the result could not be stored.
	 */
	public static void updateResult(Result result) throws IOException
	{
		updateResult(result, true);
	}
	
	public static Result updateResult(Result result, File zippedResults) throws IOException
	{
		File executionDir = directoryForExecution(result.workflowID(), result.executionID());
		File wsFile = null;
		try {
			wsFile = workflowFileInDir(executionDir);
		}
		catch (IllegalArgumentException iae)
		{
			/* on est dans le cas où on récupère les résultats d'une ancienne version de praxis: on n'a pas
			 * le workflow initial dans l'objet Result, et il n'était pas non plus préalablement stocké dans le
			 * répertoire des résultats (puisque celui-ci n'existait pas). La seule solution ets de copier celui qui
			 * est dans 'current': pas de soucis, puisque dans ces anciennes version de praxis la modification
			 * d'un workflow entrainait la suppression des résultats antérieurs.
			 */
			final String file_ext = Configuration.get("file_extension");
			if (result.getWorkflowXML()==null)
			{
				try
				{
					Utile.copyFile(workflowFileInDir(new File(executionDir.getParentFile(), CURRENT)),
					               new File(executionDir,result.workflowID().name()+file_ext));
				}
				catch (IOException e)
				{
					e.printStackTrace(); // and ignore
				}
			}
			else
			{
				try
				{
					FileOutputStream fos = new FileOutputStream(new File(executionDir,
					                                                     result.workflowID().name()+file_ext));
					fos.write(result.getWorkflowXML());
					fos.flush();
					fos.close();
				}
				catch (IOException e)
				{
					e.printStackTrace(); // and ignore
				}
			}
		}
		result = updateResult(result, false);
		if (zippedResults != null)
			Utile.unzip(zippedResults, executionDir);
		if (result.getStatus().isClosed())
		{
			File resultsZip = result.zipFile();
			result.setZipFile(zippedResults);
			notifyListenersResultModified(result.workflowID(), result);
			result.setZipFile(resultsZip);
		}
		else
			notifyListenersResultModified(result.workflowID(), result);
		return result;
	}
	
	/**
	 * Returns the path of the directory where results for this execution are (or will be) stored
	 * @param workflowID
	 *            the workflow ID
	 * @param executionID
	 *            the execution id
	 * @return the directory where results for this execution are (or will be) stored
	 * @throws IOException
	 *             if the corresponding directory does not exist, or if a file with the same name exists but is not a
	 *             directory.
	 */
	static public File directoryForExecution(WorkflowID workflowID, ExecutionID executionID) throws IOException
	{
		File resultDir = new File(rootWorkflowDirectoryPath(workflowID.name()), executionID.dumpID());
		if (resultDir.exists() && !resultDir.isDirectory())
			throw new IOException("File " + resultDir + " exists and is not a directory");
		if (!resultDir.exists() && !resultDir.mkdirs())
			throw new IOException("Unable to create directory " + resultDir);
		return resultDir;
	}
	
	/**
	 * Returns the workflow that was executed, as stored in the filesystem.<br>
	 * This is used by {@link #getWorkflowForResult(Result, XMLWarnings)} when the stored (serialized) result returns
	 * null for {@link Result#getWorkflowXML()} (this happens for Result object stored before rev.737)
	 * @param result
	 *            the Result object, for a given execution
	 * @return the corresponding workflow, or <code>null</code> if it could not be retrieved
	 */
	static private Workflow workflowForExecution(Result result, XMLWarnings warnings)
	{
		try
		{
			return workflowInDir(directoryForExecution(result.workflowID(), result.executionID()),
			                     warnings, true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	synchronized public static Result getResult(WorkflowID workflowID, ExecutionID executionID)
	{
		for (Result result: getResults(workflowID))
			if (result.executionID().equals(executionID))
				return result;
		return null;
	}
	
	/**
	 * Returns all the results attached to this workflow, sorted by date.
	 * @param workflowID
	 *            id of a workflow
	 * @return an array containing all available executions' results, sorted by date
	 */
	synchronized static public Result[] getResults(WorkflowID workflowID)
	{
		ArrayList<Result> r = results().get(new File(rootWorkflowDirectoryPath(workflowID.name())));
		if (r==null)
			return new Result[0];
		/* */
		ArrayList <Result> invalidResults = new ArrayList<Result>();
		for (Result result: r)
		{
			if (result.workflowID()==null)
				invalidResults.add(result);
			else
				result.workflowID().setName(workflowID.name());
		}
		r.removeAll(invalidResults);
		Result [] res = r.toArray(new Result[r.size()]);
		Arrays.sort(res, Result.DateComparator);
		return res;
	}
	
	/**
	 * Retrieves the executed workflow corresponding to the supplied execution result.
	 * @param result the Result object representing an execution
	 * @param warnings see {@link Workflow#Workflow(File, XMLWarnings)}
	 * @return the requested workflow
	 */
	synchronized static public Workflow getWorkflowForResult(Result result, XMLWarnings warnings)
	{
		Workflow executedWorkflow = null;
		if (result.getWorkflowXML()==null)
		{
			// An old version of result did not store the corresponding workflow
			// In this configuration, we need to get back the original one in the filesystem
			executedWorkflow = workflowForExecution(result, warnings);
		}
		else
		{
			try
			{
				executedWorkflow = new Workflow(new ByteArrayInputStream(result.getWorkflowXML()),warnings, true);
			}
			catch (InvalidXMLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		executedWorkflow.setName(result.workflowID().name());
		try
        {
	        executedWorkflow.currentDirectory = directoryForExecution(result.workflowID(), result.executionID());
        }
        catch (IOException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		return executedWorkflow;
	}

	/* **** ServerToClientEventAdapter **** */
	@Override
	public void receivedExecutionStatus(ServerToClientEvent event)
	{
		final Result result = (Result) event.data;
		if (result==null)
		{
			Log.log.severe("Received a null result!?!");
			return;
		}
		if (!acceptResult(result))
		{
			Log.log.info(()->"Rejecting unknown result "+result);
			return;
		}
		try
        {
	        updateResult(result, event.file_conveyor);
        }
        catch (IOException e)
        {
	        // TODO
	        e.printStackTrace();
	        return;
        }
        if (requestFullResultAtEndOfExecution && result.getStatus().isClosed())
        	Client.uiClient.requestResult(result);

	}
	
	@Override
	public void receivedAvailableResults(ServerToClientEvent event)
	{
		@SuppressWarnings("unchecked")
		ArrayList <Result> results = (ArrayList<Result>) event.data;
		ArrayList <Result> finished_exec = new ArrayList<Result>();
		
		for (Result result: results)
		{
			if (!acceptResult(result))
			{
				Log.log.info(()->"Rejecting unknown result "+result);
				continue;
			}
			switch (result.getStatus())
			{
				case CANCELLED:
				case ERROR:
				case OK:
				case WARNING:
					finished_exec.add(result);
					break;
				case RUNNING:
				case SUSPENDED:
					try
					{
						updateResult(result);
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case NOT_STARTED:
					break;
			}
		}
		if (requestFullResultAtEndOfExecution && finished_exec.size() > 0)
			Client.uiClient.requestResults(finished_exec);
	}

	@Override
	public void receivedExecutionResults(ServerToClientEvent event)
	{
		if (!storeReceivedExecutionResults)
			return;
		final Result result = (Result) event.data;
		if (result==null)
		{
			Log.log.severe("Received a null result!?!");
			return;
		}
		if (!acceptResult(result))
		{
			Log.log.info(()->"Rejecting unknown result "+result);
			return;
		}
		try
        {
	        updateResult(result, event.file_conveyor);
			ArrayList<ExecutionID> results = new ArrayList<ExecutionID>();
			results.add(result.executionID());
	        Client.uiClient.requestsDeletionOfResults(event.workflowID, results);
        }
        catch (IOException e)
        {
	        // TODO
	        e.printStackTrace();
	        return;
        }
	}
	
	/* **** Handling listeners **** */
	static public synchronized void addListener(StorageListener listener)
	{
		listeners.add(listener);
	}
	
	static public synchronized void removeListener(StorageListener listener)
	{
		listeners.remove(listener);
	}
	
	static private void notifyListenersWorkflowAdded(WorkflowID workflowID)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.workflowAdded(workflowID);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}
	
	static private void notifyListenersWorkflowModified(WorkflowID workflowID)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.workflowModified(workflowID);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}
	static private void notifyListenersWorkflowDeleted(WorkflowID workflowID)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.workflowDeleted(workflowID);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}

	static private synchronized void notifyListenersResultAdded(WorkflowID workflowID, Result result)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.resultAdded(workflowID, result);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}
	
	static private synchronized void notifyListenersResultModified(WorkflowID workflowID, Result result)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.resultModified(workflowID, result);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}
	
	static private synchronized void notifyListenersResultDeleted(WorkflowID workflowID, Result result, int index)
	{
		ArrayList<StorageListener> _listeners = new ArrayList<StorageListener>(listeners);
		for (StorageListener listener: _listeners)
		{
			try
			{
				listener.resultDeleted(workflowID, result, index);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "listener raised", e);
			}
		}
	}
	
    @Override
    public String toString()
    {
    	return "Workflows";
    }
}
