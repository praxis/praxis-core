/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.jdom.Document;

import eu.telecom_bretagne.praxis.common.Application;
import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.ReleaseInfo;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.Utile.ResourceAvailability;
import eu.telecom_bretagne.praxis.common.events.ClientToServerEvent;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.common.events.ServerToPlatformEvent;
import eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.ClientToServerEventListener;
import eu.telecom_bretagne.praxis.common.events.PlatformToServerEvent.PlatformToServerEventListener;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ExecutionLoop;
import eu.telecom_bretagne.praxis.core.execution.Process;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.ExecutionLoop.NotEnabledException;
import eu.telecom_bretagne.praxis.server.Serveur.TooManyClientsException;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;


import static eu.telecom_bretagne.praxis.common.events.ServerToPlatformEvent.Type.*;

/**
 */
public class Server 
implements PlatformToServerEventListener, ClientToServerEventListener
{
    /**
     * The platform connected to this
     */
    protected PlatformDescription platform;

    /** The client connected to this */
    protected Client client;

    protected CommunicationFacade cnx;

    public Server(CommunicationFacade cnx)
    {
        this.cnx = cnx;
        cnx.addListener(this);
    }

    /**
     * Returns the connected client, if this object represents a client.  If it does not represent a client, it
     * represents a {@link #getPlatform() platform}
     * @return the connected client, or null
     */
    public Client getClient() {
        return client;
    }

    /**
     * Returns the connected platform, if this object represents a platform.  If it does not represent a platform, it
     * represents a {@link #getClient() client}
     * @return the connected platform, or null
     */
    public PlatformDescription getPlatform()
    {
        return platform;
    }

    /* **** Communicating w/ the platform **** */
	/**
	 * Request an execution. Note that the zipped file is deleted after the event has been sent.
	 * 
	 * @param execEngine
	 *            the execution engine prepared for executing the task
	 * @param zip
	 *            A zip file containing the necessary input files. If no file is needed for the execution, it equals to
	 *            <code>null</code>. <br>
	 *            <b>Please note that the zip file is deleted after the event has been sent</b>.
	 */
    public void requestExecution(ExecutionEngine execEngine, File zip)
    {
        ServerToPlatformEvent event = new ServerToPlatformEvent(this, START_EXECUTION);
        event.data=execEngine;
        event.file_conveyor=zip;
        event.callback = new CommunicationFacade.EventHandlingCallback() {
			@Override
			public void eventSent(Event evt)
			{
				if (evt.file_conveyor!=null)
					evt.file_conveyor.delete();
			}
		};
        cnx.send(event);
    }

    public void requestCancellation(String id)
    {
        ServerToPlatformEvent event = new ServerToPlatformEvent(this, CANCEL_EXECUTION);
        event.data=id;
        cnx.send(event);
    }

    public void requestTermination(String reason)
    {
        ServerToPlatformEvent event = new ServerToPlatformEvent(this, TERMINATION);
        event.data=reason;
        cnx.send(event);
    }

    public void sendMessage(String message)
    {
        ServerToPlatformEvent event = new ServerToPlatformEvent(this, MESSAGE);
        event.data=message;
        cnx.send(event);
    }

    /* **** PlatformToServerEventListener methods **** */
    @Override
	@SuppressWarnings("unchecked")
	public void platformSendsConfiguration(Event event) {
        Document doc = null;
        Map<ResourceAvailability, List<String>>  map;
        try {
        	Object[] configuration_n_availability = (Object[]) event.data;
            doc = (Document) configuration_n_availability[0];
            map = (Map <ResourceAvailability, List<String>>) configuration_n_availability[1];
        }
        catch (ClassCastException exc)
        {
        	if (doc == null)
				Log.log.severe("Platform REJECTED: no config. available" + (exc.getMessage() == null ? "" : exc.getMessage()));
        	else
				Log.log.severe("Platform REJECTED: " + (exc.getMessage() == null ? "" : exc.getMessage()));
            // simply disconnects -- we intentionally do not answer anything
            cnx.disconnect();
            return;
        }

        platform = null;
        try {
            platform = PlatformDescription.buildPlatformDescription(this, doc, map);
        }
        catch (IllegalArgumentException e) {
            Log.log.log(Level.SEVERE, "Couldn't build the appropriate platform description", e);
            String msgToPlatform = "Invalid description, disconnecting";
            if (e.getMessage()!=null)
            	msgToPlatform += ". Reason: "+e.getMessage();
            this.sendMessage(msgToPlatform);
            cnx.disconnect();
            return;
        }

        Serveur.registerPlatform(this);
        Log.log.info("platform " + cnx.toString() + " logged");
        sendMessage("LOGIN_ACK");
    }

    @Override
	@SuppressWarnings("unchecked")
    public void platformSendsResourcesAvailabilityUpdates(Event event)
    {
    	Map<ResourceAvailability, List<String>> map;
    	try {
        	map = (Map<ResourceAvailability, List<String>>) event.data;
        }
        catch (ClassCastException e) {
            Log.log.severe("Platform sent a loosy map for available resources: ignoring");
            return; // and ignore
        }
        platform.updateResourcesAvailability(map);
    }

    @Override
    public void platformSendsExecutionProgress(Event event)
    {
    	platformSendsResults(event);
    }

    @Override
    public void platformSendsResults(Event event)
    {
    	final Result result = (Result) event.data;
    	if (result==null)
    	{
    		Log.log.severe("Received a null result");
    		return;
    	}
    	Log.log.info(()->"Received result: "+result);
    	final Process process = ExecutionLoop.executionLoop.getProcess(result.workflowID(), result.executionID());
    	if (process==null)
    	{
    		Log.log.severe(()->"Received result is not known to the execution loop, dropping it: "+result);
    		return;
    	}
    	if (event.file_conveyor != null)
    	{
    		// if null, the platform is just updating the status of the execution
    		Utile.unzip(event.file_conveyor, process.getContext().resultsDirectory());
    		event.file_conveyor.delete();
    	}
        ExecutionLoop.executionLoop.add(result);
    }

    /** UNTESTED, simplement copié/collé ici, à tester absolument */
    @Override
    public void platformLogout(Event event) // TODO complètement refaire
    {
        Serveur.removePlatform(this);
        cnx.disconnect();
        if (!Application.getApplication().exiting())
            eu.telecom_bretagne.praxis.common.Utile.unimplemented("The running jobs should be rescheduled");
    }

    /* **** ClientToServerEventListener methods **** */

    @Override
    public void clientSendsCredentials(Event event) {
        String[] reject_msg = null;

        ClientToServerEvent cs_event = (ClientToServerEvent) event;
        Object[] msg_content = (Object[])cs_event.data;
        
        String login = null;
        String passwd = null;
        Integer clientRevision = null;

        if (msg_content==null || msg_content.length!=3)
        {
        	/* malformed message, may come from an older client with a different protocol:
        	   clientRevision==null will be the marker for such situations */
        }
        else {
        	try
        	{
        		login = (String) ((Object[])cs_event.data)[0];
        		passwd = (String) ((Object[])cs_event.data)[1];
        		clientRevision = (Integer) ((Object[])cs_event.data)[2];
        	}
        	catch (ClassCastException e)
        	{
        		/* malformed message: see above */
        	}
        }
        // TODO move this to a dedicated class
        String disabled = Serveur.getClientLoginDisabled();
        
        if (clientRevision == null || clientRevision < ReleaseInfo.application_revision)
        	reject_msg = new String[] { "common.client_out_of_date" };
        else if ( disabled != null )
        {
        	reject_msg = new String[] { "common.cnx_rejected.client_login_disabled" };
        	if ( ! "".equals(disabled) )
        		reject_msg = new String[] { "common.cnx_rejected.client_login_disabled_w_reason", disabled }; 
        }
        else if ( UserMgmt.checkCredentials(login, passwd) )
        {
        	if ( Serveur.isClientRegistered(login) )
        	{
            	if ( Configuration.getBoolean("server.reject_already_connected_client") )
            		reject_msg = new String[] { "common.cnx_rejected.client_already_connected" };
            	else
            	{
            		Serveur.getRegisteredClient(login).cnx.disconnect();
            		client = new Client(cnx, login);
            	}
        	}
        	else
                client = new Client(cnx, login);
        }
        else
            reject_msg = new String[] { "common.cnx_rejected.invalid_credentials" };


        if (reject_msg!=null) {
            Log.log.info(reject_msg[0]);
            /*
             * We cannot simply send a String[]: older versions of praxis' clients (i.e. before v1.6) expect a String
             * Those clients were receiving client_out_of_date or reject_already_connected_client only.
             */
            if ( reject_msg.length == 1 )
                Client.notifyAuthentificationFailureAndDisconnect(cnx, reject_msg[0]);
            else
                Client.notifyAuthentificationFailureAndDisconnect(cnx, reject_msg);
            return;
        }

        try
        {
	        Serveur.registerClient(client); // TODO: may fail if connected several times
	        Log.log.fine(()->"Client accepted: "+client);
        }
        catch (TooManyClientsException e)
        {
            Client.notifyAuthentificationFailureAndDisconnect(cnx, e.getMessage());
        }
    }

    @Override
    public void clientRequestsExecution(ClientToServerEvent event)
    {
        Log.log.finest("Execution requested, event.file_conveyor's size: "+(event.file_conveyor!=null?event.file_conveyor.length():"<null>"));
        // an execution has been requested: get an ID  // TODO this should be generated by the processMgr
    	Result r = (Result) event.data;
    	ExecutionID executionID = r.executionID();
    	
    	// immediately determine where it will be stored
    	File executionDirectory = // TODO CHECK null value
    		Utile.createTempDirectory(executionID.toString(), ".bse", Serveur.executionDirectory(event.workflowID));

    	if (event.file_conveyor!=null)
    	{
    		File zippedScript = new File(executionDirectory,"script.zip");
    		Utile.renameFile(event.file_conveyor, zippedScript); // CHECK needed?
    		File infiles = zippedScript.getParentFile(); //new File(zippedScript.getParentFile(), "infiles"); // TODO "infiles/"
    		infiles.mkdir();
    		Utile.unzip(zippedScript, infiles);
    	}
    	Process.ProcessContext wf_context = ExecutionLoop.executionLoop.getContextTemplate();
        wf_context.setWorkingDirectory(executionDirectory);
        wf_context.resultsDirectory().mkdir();// TODO remove this...
        wf_context.workflowID = event.workflowID;
        wf_context.executionID = executionID;

    	r.setZipFile(new File(executionDirectory, "results.zip"));
    	r.setStatus(Result.Status.RUNNING);

    	UsageCollection.workflowExecuted(r.getWorkflowXML());
    	Serveur.resultStore().insertResult(r, client.getLogin());

        eu.telecom_bretagne.praxis.core.execution.Process process = ExecutionLoop.executionLoop.createProcess(event.workflowID.name(),
                                                                                          wf_context,
                                                                                          getClient(), r);
        Log.log.finest("Adding the process consequently to the execution request");
        try
        {
	        ExecutionLoop.executionLoop.add(process);
        }
        catch (NotEnabledException e)
        {
	        // TODO generate Result w/ error
	        e.printStackTrace();
        }
    }

    @Override
    public void clientRequestsCancellation(ClientToServerEvent event) // TODO implement it!
    {
    	//ExecutionID executionID = (ExecutionID) event.data;
    	eu.telecom_bretagne.praxis.common.Utile.unimplemented();
    }

    @Override
    public void clientRequestsAvailableResults(ClientToServerEvent event)
    {
        client.sendResultsSummary(event.workflowID);
    }

    @Override
    public void clientRequestsResult(ClientToServerEvent event)
    {
        client.sendResult(event.workflowID, (Result) event.data);
    }

    @Override
	@SuppressWarnings("unchecked") // ignore warnings for the cast on event.data
    public void clientRequestsDeletionOfResults(ClientToServerEvent event)
    {
        client.deleteResults(event.workflowID, (List<ExecutionID>) event.data);
    }

    @Override
    public void disconnected(Exception exception)
    {
    	if (client!=null)
    	{
    		Log.log.info("Client disconnected ("+client+")"+ (exception==null ? "" : ": "+exception.toString()));
    		this.clientLoggedOut(null);
    	}
    	if (platform!=null)
    	{
    		Log.log.info("Platform disconnected ("+platform+")"+ (exception==null ? "" : ": "+exception.toString()));    	
    		this.platformLogout(null);
    	}
    }
    
    @Override
    public void clientLoggedOut(Event event) {
        Serveur.removeClient(client);
        cnx.disconnect();
        cnx = null;
    }

}
