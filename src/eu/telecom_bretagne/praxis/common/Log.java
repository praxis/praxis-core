/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.util.logging.Logger;

public class Log
{
	/*
	 * NB: Log levels are always localized
	 * https://stackoverflow.com/questions/3920930/java-util-logging-how-to-set-level-by-logger-package-or-prefix
	 */
	public static final Logger log       = Logger.getLogger("praxis", null);

	public static final Logger migration = Logger.getLogger("praxis.migration");

	public static final Logger exception = Logger.getLogger("exception.ignored", null);

}
