v3.1
----

- Auto-detection of proxy settings is now available.

- Fix a bug which prevented executions when
  simpleWorkflowPlanner_aggregate_activities is set.  In particular,
  this variable is set in the bridge so that the bridge sends the
  largest possible workflow to the server.


v3.0.2
------

- Fix a bug causing an application to crash if no logging.properties
  could be found (*sigh*)


v3.0.1
------

- When an application does not request a specific language, use the
  default one (or English)

- Fix: applications failed to start when the system's default locale
  was unsupported.
  Many thanks to Andrea Profili (Italy) and Miłosz Kadziński (Poland)
  for the bug reports, and for their precious help to diagnose the
  problem.

- Now properly detects when launched with unsupported JVM (currently
  v9 and v10), and informs the user before exiting.


v3.0.0
------

- New translations!
  - Greek, by Giota Digkoglou
  - Spanish, by Maria de Vicente y Oliva and Jaime Manera Bassa

- It is possible to set, server-side, a maximum number of clients.

- Possible boolean parameters' values and default values in the
  descriptions of boolean parameters are restricted to four values,
  namely: "0", "1", "true" and "false".

- Splasher now displays the splash screen centered on the main
  monitor.  On dual monitor configuration e.g., the splash screen
  straddled the two monitors.

- The launcher now sets the title of the application's window GUI.  On
  some linux systems, an application bundled with one-jar showed
  "com-simontuffs-onejar-Boot" (corresponding to the X11 property
  WM_CLASS, observed on Ubuntu 16.04 w/ Gnome 3.24 & 3.26.1).  The new
  configuration property application.name is used to set this X11
  property.

v2.2
----

- Praxis was really slow on machines where 'localhost' takes seconds
  to resolve, due to some DNS (mis)configuration.  This has been
  observed on Mac OS X "Sierra".
  Thanks to Patrick Gasser (Singapore-ETH Centre) for reporting the
  issue, and for his help to diagnose than fix the problem.

v2.1
----

- Fix a problem on Mac OS X where the server's internal database could
  not be read (it failed with java.sql.SQLException: [SQLITE_NOTADB]
  File opened that is not a database file (file is encrypted or is not
  a database).

v2.0
----

- The library is now released under the European Union Public Licence
  (EUPL) v1.1 (see details in LICENSE/)

- eu.telecom_bretagne.praxis.common.ReleaseInfo.package_date
  deprecated: it is of no use, was not used, and the release date is
  all what we want.

- Out-of-date execution status updates are now invalidated on the
  server side: when a client retrieves the execution status of a
  workflow, it only gets the latest.  Slow clients, or clients with a
  low-bandwidth connection to the server do not pay the extra penalty
  of having to receive and process out-of-date informations.

- Restore the parallel execution of jobs within workflows.  This
  functionality was not any more the default one after the "bridge"
  was introduced to allow mixing local executions with remote one
  (1.4b1)

- Accelerate the display of a program's configuration window.

v1.11
-----

- When using the platform-to-client bridge configuration, the initial
  client now correctly receives the notification when it is
  out-of-date and needs to be updated, and exits after having having
  notified the user, as expected.  Same goes for the notification sent
  by the server when it does not accept new clients.

- Fix the main loop which was observed continously looping instead of
  waiting for events.

- Fix the console which was eating some CPU when it should be simply
  waiting for input (observed at least on a Linux OpenVZ VM)

v1.10
-----
(changes in GUI only)

v1.9.1
------

- Mostly code cleanup & minor internal changes

v1.9
----

- By default, client.StorageManager now ignores update requests for unknown
  results.  A delegate can be provided to examine each of those requests and
  decide whether they should be accepted --for details, see
  StorageManager.StorageManagerDelegateInterface.

  This also fixes the situation where "ghost" execution-in-progress items
  where coming back each time the application was relaunched, no matter how
  many time you delete them (these items are processes in the RUNNING state
  whose processes is not executed anymore but which remain in the server
  database after a crash e.g.).

- Enhanced logging:

  - the number of connected users is now logged when a user successfully
    connects (it was only logged when a user disconnects).

  - the list of programs'ids in every executed workflow

v1.8.1
------

- Fixed: on Windows 7 64 bits, a praxis application could not start because
  the SQLite JDBC driver we used (made by David Crawshaw) fails with an out of
  memory error when building the connection to hte database.

  Replaced with Xerial's JDBC driver written by Taro L. Saito.
  The driver is an extension of David Crawshaw's SQLite JDBC driver.

v1.8
----

- The SGE execution engine now adds information about the SGE job, and
  specifically about its termination status (aborted, canceled...)

- PraxisPreferences.init() is now called in the class' static initializer to
  eliminate the cases where it was not called.

- Eliminated the possibility for the JVM to randomly crash on Linux at startup
  because of a nasty interaction between Java and GConf/DBus
  See https://bugs.launchpad.net/ubuntu/+source/openjdk-6/+bug/885195
  for details

- Fixed: the server could fail sending the results when an unhandled exception
  was raised while gathering information on a failure.

- Improved internationalization

v1.7.3
------

- Application language in now set through the Praxis pref. client/language

v1.7.2
------

- Fixed: StorageManager.reserveName() was not detecting anymore (since
  6f64d9e845, i.e. since v1.6) that a workflow's name was already used.

v1.7.1
------

(changes in GUI only)

v1.7
----

- New configuration key client.gui.param_dialog_accepts_invalid_values,
  defaults to true (this changes the default behaviour of the GUI)

- Workflow, Program: new isValidForExecution() method

v1.6.1
------

- Fixed: older clients did not understand the server anymore when it was
  informing them that they are out of date.  This was due to changes in the
  messages exchanged between the server and the client made when the new
  console command 'server.clients_login' was introduced.


v1.6
----

- new console command on the server: server.clients_login on/off allows to
  control whether clients are allowed to login or not.  This may be used to
  avoid new clients to log in before a maintenance takes place.

  **IMPORTANT** this functionality will NOT work with clients using an older
  version of Praxis.  This implies that projects willing to use this new
  console command should mark older releases as out of date and force the users
  to update their clients.

- new: eu.telecom_bretagne.praxis.client.Client.ClientDelegate can be used to
  add actions to run when the client connects to/disconnects from the server.

- ExecutionLoop: more robust handling of exceptions happening while executing
  activities.

API changes:
- StorageManager: the API is more consistent: workflows are now always
  identified by strings, and not by File objects anymore (rev.6f64d9e845)

- common.Launcher.InitialisationDelegate: initialize() now takes three
  arguments indicating whether client-, server- or platform-side specific
  initialization should be performed.

v1.5.3
------

- PlatformToClientBridge: when submitted by client on Windows, paths need to
  be normalized before being sent to a unix server.

v1.5.2
------

- Added an attribute 'name' in Result so that a user can supply its own short
  description.

v1.5.1
------

- Fixed: the server could fail with "Too many opened files" exception when it
  was significantly sollicited.

v1.5
----

- The bridge now asks the server to delete a result after receiving it.

- Added a consistency check to the ResultStore on the server side.  It removes
  results with invalid statuses, and the results which are closed and point to
  a non-existent file.

v1.4.1
------

- Fixed: if "localhost" was not resolved by an IP in the network 127/32, a
  connection to local components could fail because of attempts to connect
  using RMI over HTTP.

  Many thanks to Sascha Graßhoff for reporting the problem, and for working
  with us to diagnose the problem.

- java 1.7 from Oracle Corporation is not reported anymore as a JVM that can
  create problems.

v1.4
----

- Added server.UsageCollection, just collecting the executed workflows for
  now (without their input files).

- RMI connection to localhost are now always direct, even if the configuration
  says that in general RMI connection should be tunnelled in HTTP
  (configuration item 'rmi.useHTTP')

- Added a console to the application

- The "bridge mode" is now compatible with an offline execution:

  * The execution engine is capable to serialize its state onto the disk and
    to restore it when the application restarts.

  * The execution planner makes the biggest possible set of programs to be
    executed on a platform: this allows programs to be executed distantly to
    be sent in the same set --hence the client can then disconnect while the
    distant server handle the executions.

v1.4b1
------

- Added a "bridge" mode: a platform that behaves like a client and
  connects to a remote server.  This makes it possible to declare a
  local platform executing local scripts and programs, handled by a
  local server which delegates any non-local execution to a distant
  server through this local "bridge" platform.

  Important note: when in "bridge" mode, the ability to continue an
  execution when the client disconnects is NOT available anymore.
  We are working on this issue.

- Added a new execution engine: ProgramExecutionEngine.

v1.3
----

- Added an additional way of specifying the current workspace directory.

   Workspace directory is searched (in that order):
   - in the system property 'praxis.workspace',
   - in the user's preferences, under key client/last_workspace,
   - in the configuration under key 'workspace_directory'.

   NB: the 2nd option is not used for the moment being.

- The current execution engines now store the files produced by every program
  in their own directory --one directory per program.

  This allow us not to rename the files to cryptic names anymore (using
  Parameter.output_xml_id() namely); in particular, they keep their file
  extensions, meaning that it is simpler to associate a (default) viewer to
  them.

- Refactored the way execution engines manage result files: all result files
  are now registered in Result objects, using the new ProgramResult class.
  They also record the execution start and end times.

- Bug fix: a deleted workflow was not correctly unregistered from the
  StorageManager.  After deleting a workflow, it was still being displayed in
  the list of workflows that could be opened, until the application was
  restarted.

v1.2.1
------

(None)

v1.2
----

- The server now stores a Process' intermediary result when one of its
  Activities is updated.  This avoids situations where the client could get a
  Result without any individual prgs' status set when reconnecting to the
  server (this happened when the Process was finished but results were not
  sent back to the client yet).

- command-line refactored: eu.telecom_bretagne.praxis.client.SimpleCommandLine
  For details:
  java \
   -Done-jar.main.class=eu.telecom_bretagne.praxis.client.SimpleCommandLine \
   -jar ... -h

- universal types (formerly only one type: ALL) should now be explicitly
  declared in types.cfg, using the key: universal_types (comma-separated
  values)

v1.1
----

- Fixed: client was not completely wiped-out of the server-side when
  using RMI.

- SGE: new configuration item 'job-category'

- SGE: fixed: two jobs starting at the same time may fail to concurrently
  initialize the DRMAA session.

- Platform's DTD has changed: tag <initialisation/> renamed to
  <configuration/>, with sub-tags '<item/>'.

  Old <configuration/> tags should be migrated to sub-tag '<item
  name="script-init"/>'
