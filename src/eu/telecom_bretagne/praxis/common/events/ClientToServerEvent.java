/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;

/**
 * The data stored in this object depends on its type:
 * <ul>
 * <li>AUTHENTIFICATION:
 *     <ul>
 *     <li>{@link Event#data}: an Object[3] containing the login, password and the client's application_revision
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>REQUEST_EXECUTION: sets workflow<br>
 *   <b>WARNING:</b> the workflow id should be the same as the one stored within the workflow enclosed in the zip file (below)
 *     <ul>
 *     <li>{@link Event#data}: a Result object 
 *     <li>{@link Event#file_conveyor}: a zipped File, containing the necessary materials to execute the workflow.
 *     The workflow itself is included within the Result object.
 *     </ul>
 * <li>REQUEST_CANCELLATION: sets workspace & workflow
 *     <ul>
 *     <li>{@link Event#data}: an ExecutionID
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>REQUEST_AVAILABLE_RESULTS: sets workspace & workflow
 *     <ul>
 *     <li>{@link Event#data}: not used
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>REQUEST_RESULT: sets workspace & workflow
 *     <ul>
 *     <li>{@link Event#data}: id of the result to retrieve (String)
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>REQUEST_DELETION_OF_RESULTS: sets workspace & workflow
 *     <ul>
 *     <li>{@link Event#data}: a list of {@link ExecutionID results' ids}, or null when all results should be deleted
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>LOGOUT
 *     <ul>
 *     <li>{@link Event#data}: not used
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * </ul>
 * @author Sébastien Bigaret
 *
 */
public class ClientToServerEvent extends Event
{
	static final long serialVersionUID = 3080642337146652438L;
	
	public interface ClientToServerEventListener
	extends EventListener
	{
		public void clientSendsCredentials(Event event);
		public void clientRequestsExecution(ClientToServerEvent event);
		public void clientRequestsCancellation(ClientToServerEvent event);
		public void clientRequestsAvailableResults(ClientToServerEvent event);
		public void clientRequestsResult(ClientToServerEvent event);
		public void clientRequestsDeletionOfResults(ClientToServerEvent event);
		public void clientLoggedOut(Event event);
	}
	
	public static enum Type {
		AUTHENTIFICATION,
		REQUEST_EXECUTION,
		REQUEST_CANCELLATION,
		REQUEST_AVAILABLE_RESULTS,
		REQUEST_RESULT,
		REQUEST_DELETION_OF_RESULTS,
		LOGOUT
	}
	
	Type type;
	
	public WorkflowID workflowID;
	
	public ClientToServerEvent(Type type)
	{
		super("no source");
		this.type = type;
	}
	
	@Override
	public void dispatch_to(EventListener listener)
	throws IncompatibleListenerException
	{
		ClientToServerEventListener l;
		try {
			l = (ClientToServerEventListener) listener;
		}
		catch (ClassCastException e) {
			throw new IncompatibleListenerException(listener, this);
		}
		switch (type)
		{
			case AUTHENTIFICATION:              l.clientSendsCredentials(this);            break;
			case REQUEST_EXECUTION:             l.clientRequestsExecution(this);           break;
			case REQUEST_CANCELLATION:          l.clientRequestsCancellation(this);        break;
			case REQUEST_AVAILABLE_RESULTS:     l.clientRequestsAvailableResults(this);    break;
			case REQUEST_RESULT:                l.clientRequestsResult(this);              break;
			case REQUEST_DELETION_OF_RESULTS:   l.clientRequestsDeletionOfResults(this);   break;
			case LOGOUT:                        l.clientLoggedOut(this);                   break;
		}
	}
	
	@Override
	public String toString()
	{
		return "<ClientToServer type:"+type.toString()+" wfID:"+workflowID+">";
	}
}
