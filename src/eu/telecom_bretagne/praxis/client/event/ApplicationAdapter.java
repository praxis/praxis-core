/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client.event;

/**
 * @author Sébastien Bigaret
 *
 */
public class ApplicationAdapter
    implements ApplicationListener
{
	
	/* (non-Javadoc)
	 * @see eu.telecom_bretagne.praxis.client.ui.event.ApplicationListener#applicationClosing()
	 */
	@Override
	public void applicationExiting() { /* does nothing */ }
	
}
