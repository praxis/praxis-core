/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.preprocess;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;


import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.jdom.Document;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Environment;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;


/**
 * 
 * @author Sébastien Bigaret
 *
 */
public final class CoreDescriptionsGeneration
{
	static protected boolean verbose=true;
	
	/**
	 * Returns the class name corresponding to this description. It is quite similar to
	 * {@link ProgramDescription#fullName()}, in that it is essentially made of the concatenation of the program's
	 * provider, name and version, prefixed by "RD_" (RD stands for Resource Description); additionally, the
	 * characters minus and point are replaced by underscores, and any other characters that do not match {@code
	 * [A-Za-z0-9_]} are simply omitted.
	 * @return Returns the full name.
	 */
	public static String classNameForID(String id)
	{
		return "RD_"+id.replaceAll("[-\\.]","_").replaceAll("[^A-Za-z0-9_]", "");
	}
	
	/**
	 * Splits a description into small chunks. This is used in the velocity template generating the classes from the
	 * XML description: because the description may be longer than ~65k it cannot be represented by a single constant
	 * string value.
	 * @param description the description to split
	 * @return the description splitted into chunks of length 120.
	 */
	private static String[] description_to_chunks(String description)
	{
		final int chunk_length=120;
		if (description==null || "".equals(description))
			return new String[0];
		String [] chunks = new String[(description.length()-1)/chunk_length + 1];
		for	(int i=0; i<chunks.length; i++)
		{
			chunks[i] = description.substring(i*chunk_length, Math.min((i+1)*chunk_length, description.length()));
			chunks[i] = chunks[i].replaceAll("\\\\","\\\\\\\\")
			.replaceAll("\"","\\\\\"")
			.replaceAll("\n","\\\\n")
			.replaceAll("\r","\\\\r");
		}
		return chunks;
	}
	
	private static void generateJava(ProgramDescription prg, String packageName, File destination_directory)
	throws Exception
	{
		String className = classNameForID(prg.id());
		File java = new File(destination_directory, className+ ".java");
		if (verbose) {
			System.err.print("Generating "+java+"... ");
			System.err.flush();
		}
		PrintWriter out = new PrintWriter(new FileOutputStream(java));
		
		VelocityContext context = new VelocityContext();

		context.put("prg", prg);
		context.put("packageName", packageName);
		context.put("className", className);
		context.put("prg_description_chunks", description_to_chunks(prg.getDescription()));
		
		Template template = Velocity.getTemplate("templates/b.c.d.RD_prgID.vm");

		template.merge(context, out);
		out.flush(); out.close();
		if (verbose)
			System.err.println("done");
	}
	
	private static void generateAllDescriptions(ArrayList<String> classNames, String packageName, File destination_directory)
	throws Exception
	{
		File java = new File(destination_directory, "AllDescriptions.java");
		PrintWriter out = new PrintWriter(new FileOutputStream(java));
		
		VelocityContext context = new VelocityContext();

		Collections.sort(classNames);
		context.put("classNames", classNames);
		context.put("packageName", packageName);

		Template template = Velocity.getTemplate("templates/b.c.d.AllDescriptions.vm");

		template.merge(context, out);
		out.flush(); out.close();
	}
	
	public static ArrayList<String> handleDirectory(String packageName, String xml_directory_path, String destination_directory_path)
	throws Exception
	{
		return handleDirectory(packageName, xml_directory_path, destination_directory_path, null);
	}
	
	public static ArrayList<String> handleDirectory(String packageName, String xml_directory_path,
	                                                String destination_directory_path,
	                                                ArrayList<String> prgs_to_regenerate)
	throws Exception
	{
		ArrayList <String> classNames = new ArrayList<String>();
		ArrayList <String> failed = new ArrayList<String>();

		File xml_directory = new File(xml_directory_path);
		//TODO test if dir exists

		File destination = new File(destination_directory_path);
		for (String parts: packageName.split("\\."))
		{
			destination = new File(destination, parts);
		}
		if (!destination.exists())
			if ( !destination.mkdirs() )
				throw new java.io.IOException("Directory "+destination+" does not exist and couldn't be created");
		
	    String[] prgs_filenames = xml_directory.list(new FilenameFilter() {
			public boolean accept(File f, String s)
			{
				return s.endsWith(".xml");
			}
		});
		if (prgs_filenames != null)
		{
			for (int i = 0; i < prgs_filenames.length; i++)
			{
				String filename = prgs_filenames[i];
				ProgramDescription prg = null;
				try
				{
					prg = load(new File(xml_directory, filename));
				}
				catch (InvalidXMLException e)
				{
					Log.log.severe("Unable to load " + filename + ": invalid format: " + e.toString());
				}
				catch (ResourceNotFoundException e)
				{
					/* should not happen: we come from a File.list(), above! */
					Log.log.severe("Unable to load " + filename + ": " + e.toString());
				}
				finally
				{
					if (prg == null)
						failed.add(prgs_filenames[i]);
				}
				if (prg != null)
				{
					classNames.add(classNameForID(prg.id()));
					if (prgs_to_regenerate==null || prgs_to_regenerate.contains(prg.id()))
						generateJava(prg, packageName, destination);
				}
			}
			generateAllDescriptions(classNames, packageName, destination);
		}
		return failed;
	}
	
	static synchronized public ProgramDescription load(File file) throws InvalidXMLException,
	        ResourceNotFoundException
	{
		Document prog_doc;
		InputStream resourceInputStream = FileResources.inputStreamForResource(file);
		prog_doc = Facade_xml.read(resourceInputStream, true);
		try { resourceInputStream.close(); } catch (IOException e) { /* ignore */ }
		if (prog_doc == null)
			return null;
		
		ProgramDescription prog = new ProgramDescription(prog_doc.getRootElement());
		
		// DynamicCodeFactory.handle_dependencies(prog);
		return prog;
	}
	
	/**
	 * Initializes the velocity engine ("singleton" mode): we use here the classpath resource loader
	 */
	protected static void initVelocity() throws Exception
	{
		Properties p = new Properties();
	    Velocity.setProperty(Velocity.RESOURCE_LOADER, "classpathloader");
	    Velocity.setProperty("classpathloader." + VelocityEngine.RESOURCE_LOADER + ".class", ClasspathResourceLoader.class.getName());
	    // Impl. note: once init() had been called it's no use trying to change the resource loader and call init()
	    // again, this won't change anything.
	    Velocity.init(p);
	}
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception
	{
		// very 1st thing to do!
		Environment.setConfigurationDefaults(Environment.IGNORE_CONFIGURATION_DEFAULTS);

		String descriptionsDirectory = args[0];
		String sourceDirectory = args[1];		

		String packageName = Configuration.get("descriptions.package");

		initVelocity();
		
		ArrayList <String> failed = handleDirectory(packageName, descriptionsDirectory, sourceDirectory);
		if (failed.size()>0)
		{
			System.err.println("WARNING: generation failed for the following files --check the logs above for details");
			for (String name: failed)
				System.err.println(" - "+name);
			System.exit(-1);
		}
	}
	
}
