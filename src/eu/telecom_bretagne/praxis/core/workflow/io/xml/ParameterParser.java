package eu.telecom_bretagne.praxis.core.workflow.io.xml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.Item;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class ParameterParser
{
    private static final String PARAMETER = "parameter";
    private static final String INFO = "info";
    private static final String DATA = "data";
    private static final String DATA_INPUT_ID = "input_id";

    public static Parameter fromXML(Program parent_prg, StartElement startElement, XMLEventReader eventReader,
                                    Workflow.XMLWarnings warnings)
    throws InvalidXMLException//, XMLStreamException
    {
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.PARAMETER);

        // attribute: idRef (ref. param description)
        final String description_id = startElement.getAttributeByName(new QName("idref")).getValue();
        if (description_id == null)
            throw xml_exc;
        xml_exc.setName(description_id);

        // Important:
        // we do not construct a new Parameter, but we get the existing one in the supplied program
        // that has been initialized from the appropriate program description. This ensures that we only deal with
        // existing parameters, more importantly, that the order of the parameters remains the same, i.e. it reflects
        // the order in the program's description.
        final Parameter parameter = parent_prg.getParameterWithID(description_id);

        // even if parameter is null, collect the information from the xml file to be able to emit as much
        // informations as possible in the warnings that will be emitted in that precise case, below
        //
        // get:
        // - element info? (attributes: name, value),
        // - element data (attribute: input_id, PCDATA)
        StartElement info = null;
        String data = null;
        String data_input_id = null;

        while ( eventReader.hasNext() )
        {
            XMLEvent event = null;
            try
            {
                event = eventReader.nextEvent();
            }
            catch (XMLStreamException e)
            {
                throw (InvalidXMLException)xml_exc.setMsg("Unable to read <parameter/> child nodes").initCause(e);
            }
            if ( event.isEndElement() )
                if ( PARAMETER.equals(event.asEndElement().getName().getLocalPart()) )
                    break;

            if ( !event.isStartElement() )
                continue;

            final StartElement element = event.asStartElement();
            switch (element.getName().getLocalPart())
            {
            case INFO:
                info = element;
                break;
            case DATA:
                try
                {
                    data = Utils.getPCDATA(element, eventReader);
                }
                catch (XMLStreamException e)
                {
                    throw (InvalidXMLException)xml_exc.setMsg("Unable to read <parameter/> 'data' nodes").initCause(e);
                }

                final Attribute data_input_id_attr = element.getAttributeByName(new QName(DATA_INPUT_ID));
                if ( data_input_id_attr != null )
                    data_input_id = data_input_id_attr.getValue();
                break;
            }
        }

        if ( parameter == null )
        {
            if (info!=null) /* if null, the parameter was not activated, so it is not use to report it */
            {
                // info attributes name & value are required, let's however be lax if they are absent, a NullPtrExc
                // being raised when adding a warning is a non-sense
                final Attribute info_name = info.getAttributeByName(new QName("name"));
                final Attribute info_value = info.getAttributeByName(new QName("value"));
                warnings.add(Workflow.XMLWarnings.PRG_DECLARES_INVALID_PARAM,
                             new String[] { parent_prg.getDescription().fullName(),
                                            info_name != null ? info_name.getValue() : "(attribute 'name' absent)",
                                            info_value != null ? info_value.getValue() : "(attribute 'value' absent)" });
            }
            throw xml_exc.setMsg("Unable to find a valid description for parameter").setIsFatal(false);
        }
        
        switch (parameter.getDescription().getType())
        {
        case FLOAT:
        case INT:
        case STRING:
        case TEXT:
            parameter.setData(data);
            // TODO: check FLOAT, INT, peut-être pour tout le monde d'ailleurs (boolean, etc.)
            break;
        case BOOLEAN:
            parameter.setData(data);
            if ( !data.equals("0") && !data.equals("1") )
                throw xml_exc.setMsg("Invalid value for boolean parameter: "+data);
            break;
        case ENUM:
            Item item = parameter.getDescription().getItemWithID(data);
            if ( item == null )
            {
                if (info!=null) /* if null, the param. was not activated, no need to report */
                {
                    // info attributes name & value are required, let's however be lax if they are absent, a NullPtrExc
                    // being raised when adding a warning is a non-sense
                    final Attribute info_name = info.getAttributeByName(new QName("name"));
                    final Attribute info_value = info.getAttributeByName(new QName("value"));
                    warnings.add(Workflow.XMLWarnings.PRG_PARAM_HAS_INVALID_VALUE,
                                 new String[] { parent_prg.getDescription().getPrgName(),
                                                info_name != null ? info_name.getValue() : "(attribute 'name' absent)",
                                                info_value != null ? info_value.getValue() : "(attribute 'value' absent)",
                                                parameter.getDescription().defaultItem().getDescription()});
                throw xml_exc.setMsg("default selection not found in vlist").setIsFatal(false);
                }
            }
            break;
        case OUTPUT:
        case OUTPUT_DIRECTORY:
            parameter.setData(data);
            break;
        case INPUT:
        case INPUT_DIRECTORY:
            if ( data_input_id != null ) {
            	final WorkflowInput workflowInput =
            			parent_prg.getWorkflow().getInputWithId(data_input_id);
            	if ( workflowInput == null )
            		throw xml_exc.setMsg("Invalid input_id: "+data_input_id+": not registered within the workflow ");

            	parameter.setInput(workflowInput);
            }
            else
                parameter.setData(data);
            break;
        case CODE:
        case COMMAND:
            break;
        }
        return parameter;
    }
}
