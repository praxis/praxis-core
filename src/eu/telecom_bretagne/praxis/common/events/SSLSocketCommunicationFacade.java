/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;

import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.server.Server;


public class SSLSocketCommunicationFacade
    extends SocketCommunicationFacade
{   	
	static private class SSLSocketServer extends Thread
	{
		protected InetAddress bindAddress;
		protected int port ;
		
		/**
		 * Create a SSLSocketServer with the specified port and address to bind to. If bindAddress is null, the server
		 * will accept connections on all local addresses (see also
		 * {@link ServerSocket#ServerSocket(int, int, InetAddress)}
		 * @param bindAddress
		 *            the local address the server will bind to.
		 * @param port
		 *            the local TCP port
		 */
		public SSLSocketServer(InetAddress bindAddress, int port)
		{
			this.bindAddress = bindAddress;
			this.port = port;
		}
		
		@Override
		public void run() {
			try
			{
				ServerSocket serverSocket = SSLServerSocketFactory.getDefault().createServerSocket(port,
				                                                                                   BACKLOG,
				                                                                                   bindAddress);
				Log.log.info(()->"SSL Socket server started on port "+port);
				while (true)
				{
					@SuppressWarnings("resource") // socket is intentionally not closed here!
					Socket socket = serverSocket.accept();
					SocketCommunicationFacade cnx = new SocketCommunicationFacade("Server (socket SSL)", socket);
					new Server(cnx);
					cnx.start();
				}
			}
			catch (Exception execp)
			{ // TODO and then?
				Log.log.log(Level.WARNING, "Error", execp);
			}
			
		}
	}
	public static void launchServer(InetAddress bindAddress, int port)
	{
		new SSLSocketServer(bindAddress, port).start();
	}
	
	protected SSLSocketCommunicationFacade(String name, Socket socket)
	throws IOException
	{
		super(name, socket);
	}
	
	/** Used on client & platform-side */
	public SSLSocketCommunicationFacade(String name, InetAddress addr, int port)
	throws IOException
	{
		super(name, SSLSocketFactory.getDefault().createSocket(addr, port));
	}
	
}
