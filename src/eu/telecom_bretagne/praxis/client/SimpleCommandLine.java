/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.stream.Stream;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

import eu.telecom_bretagne.praxis.client.StorageManager.StorageListenerAdapter;
import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Launcher;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.ServerToClientEventListener;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;


/**
 * A simple command-line for executing a workflow and retrieving the results.<br/>
 * It gives the options of replacing one or more input file in the workflow before execution.<br/>
 * Launch with {@code -h} or {@code --help} for details; exit status in document in {@link #main(String[])}.
 * @implementation The main() in this class is a very simple command-line executing a workflow and retrieving the
 *                 results as a zip file. All other methods are here temporarily; they should be moved in a "façade",
 *                 client-side, encapsulating the communication level (using class {@link Client} and interface
 *                 {@link ServerToClientEvent}).
 * @author Sébastien Bigaret
 */
public class SimpleCommandLine extends StorageListenerAdapter
    implements ServerToClientEventListener
{
	public static final int INFO    = 0;
	
	public static final int DEBUG   = 1;

	static final String CLASS = SimpleCommandLine.class.getName();

	public static class DirectoryOptionHandler
	    extends OptionHandler<File>
	{
		public DirectoryOptionHandler(CmdLineParser parser, OptionDef option, Setter< ? super File> setter)
		{
			super(parser, option, setter);
		}
		
		@Override
		public int parseArguments(Parameters params) throws CmdLineException
		{
			File file = new File(params.getParameter(0));
			if (!file.exists() && !file.mkdirs())
				throw new CmdLineException(this.owner, "Unable to create directory: " + params.getParameter(0));
			if (!file.isDirectory())
				throw new CmdLineException(this.owner, params.getParameter(0) + " is not a directory");
			setter.addValue(file);
			return 1;
		}
		
		@Override
		public String getDefaultMetaVariable()
		{
			return "DIRECTORY";
		}
	}
	
	/**
	 * The class declaring the options accepted by the command-line.
	 * @author Sébastien Bigaret
	 */
	public static class CommandLineOptions
	{
		@Option(name = "-h", aliases = { "--help" }, usage = "Show this help")
		public boolean           help;
		
		public int           verbose = 0;
		@Option(name = "-v", aliases = { "--verbose" }, usage = "Verbose mode")
		protected void setVerboseLevel(boolean verbose) { this.verbose+=1; }
		
		/**
		 * Contains the workspace to be used. If null, the regular user's workspace will be used, as determined
		 * by configuration key "workspace_directory".
		 */
		public File temporaryWorkspace;
		
		@Option(name = "-w", aliases = { "--workspace" }, usage = "", handler = DirectoryOptionHandler.class)
		public File workspaceDir;

		//@Option(name = "-r", aliases = { "---regular-workspace" }, usage="Use the regular user's workspace")
		public boolean useRegularWorkspaceFlag = false;
		
		@Option(name = "-d", aliases = { "--results-directory" }, handler = DirectoryOptionHandler.class, usage = "Puts all results in the specified directory")
		public File              outputDirectory;
		
		@Option(name = "-o", aliases = { "--results" }, usage = "Puts the results as a zip in the specified file")
		public File              zipFile;
		
		@Option(name = "-z", aliases = { "--zip-in-directory" }, handler = DirectoryOptionHandler.class, usage = "Puts the results as a zip within the specified directory")
		public File              zipInDirectory;
		
		@Option(name = "-l", aliases = { "--list-file-ids" }, usage = "lists file ids for the supplied workflow (and immediately exit)")
		public boolean           listInputIDs;
		
		public Map<String, String> inputMap = new HashMap<>();
		
		@Option(name = "-i", metaVar = "id=file", usage = "Uses the supplied file for a given file ID")
		public void addInputMap(String $id_path) throws CmdLineException
		{
			String[] id_path = $id_path.split("=", 2);
			if (id_path.length < 2 || "".equals(id_path[1]))
				throw new CmdLineException(null, "Invalid value \"" + $id_path + "\": expecting id=file");
			try
			{
				// a workflow interprets a relative path in an input as relative to the file it was loaded from,
				// but the paths passed to option -i are relative to the current directory: we must store the
				// absolute paths in the mapping instead.
				id_path[1]=new File(id_path[1]).getAbsolutePath();
				if (options.verbose > DEBUG)
					System.err.printf(String.format("mapping input %s to: %s\n", id_path));
				inputMap.put(id_path[0], id_path[1]);
			}
			catch (NumberFormatException e)
			{
				throw new CmdLineException(null, "Invalid value \"" + $id_path
				                                 + "\": the 1st part should be an integer");
			}
		}
		
		@Argument
		public List<String> argument;
	}
	
	private static SimpleCommandLine  singleton      = new SimpleCommandLine();
	
	private ServerToClientEvent       event;
	
	private Result                    result;
	
	private static AtomicInteger      endOfExecution = new AtomicInteger(0);
	
	/**
	 * Whether we're waiting for available results
	 * @see #getAvailableResults(Workflow)
	 */
	private boolean                   waitForAvailableResults = false;

	
	private static CommandLineOptions options        = new CommandLineOptions();
	
	private SimpleCommandLine()
	{ /* empty constructor */}
	
	
	public static void usage(CmdLineParser parser, Writer writer) throws IOException
	{
		writer.write("Usage: [options] <workflow>\n");
		parser.printUsage(writer, null);
		writer.write("\nA single line is printed on the standard output: the name of the result file (or the directory in which results were dropped when option '-d' is active).\n");
		writer.flush();
	}
	
	public static void printWorkflowInputIds(Workflow workflow, Writer writer) throws IOException
	{
		Map <String, ArrayList<String>> id_to_prg_params = new HashMap<>();
		for (Program prg: workflow.getPrograms())
		{
			for (Parameter p: prg.getInputParameters())
			{
				if (p.getInput() == null)
					continue;
				String id = workflow.getIdForInput(p.getInput());
				if (!id_to_prg_params.containsKey(id))
					id_to_prg_params.put(id, new ArrayList<String>());
					
				id_to_prg_params.get(id).add(prg.getProgramID()+"\t"+p.getDescription().getDisplayName());
			}
		}
		String[] ids = id_to_prg_params.keySet().toArray(new String[id_to_prg_params.size()]);
		Arrays.sort(ids);
		for (String id: ids)
		{
			if (id_to_prg_params.get(id).size()>1)
				writer.write("File input ID: " + id + " connected to the following programs' parameters:\n");
			else
				writer.write("File input ID: " + id + " connected to program's parameter:\n");

			for (String p: id_to_prg_params.get(id))
				writer.write("  - "+p+"\n");
			writer.write("\n");
		}
		writer.write("\n");
		writer.flush();
	}
	
	private static void exitOnError(int exitStatus, String message, Throwable e)
	{
		System.err.println(message);
		if (e != null)
		{
			if (options.verbose>0)
				e.printStackTrace();
			else
				System.err.println(e.getMessage());
		}
		System.exit(exitStatus);
	}
	
	private static Workflow parseOptionsAndInit(String[] args)
	throws Exception
	{
		CmdLineParser parser = new CmdLineParser(options);
		try
		{
			parser.parseArgument(args);
			// Utilisation des arguments
		}
		catch (CmdLineException e)
		{
			System.err.println(e.getMessage());
			usage(parser, new OutputStreamWriter(System.err));
			System.exit(-1);
		}
		if (options.help)
		{
			usage(parser, new OutputStreamWriter(System.err));
			if (options.argument==null || options.argument.size() == 0)
				System.exit(0);
		}
		if (options.argument==null || options.argument.size() != 1)
		{
			exitOnError(-1, "Error: exactly one argument expected after options", null);
		}
		
		/* At most one of the options -Z, -z or -d can be active at a time */
		int output_options = 0;
		if (options.zipInDirectory!=null) output_options += 1;
		if (options.zipFile!=null) output_options += 1;
		if (options.outputDirectory!=null) output_options += 1;
		if (output_options > 1)
		{
			exitOnError(-1, "Error: at most one of the options -o, -z or -d can be active at a time", null);
		}
		if (output_options == 0)
		{
			try {
			options.zipFile = File.createTempFile("results_", ".zip");
			}
			catch (IOException e)
			{
				exitOnError(32, "Error: Could not create the results' file", e);
			}
		}
		if (options.zipInDirectory != null)
		{
			try
			{
				// set it and handle it like options.zipFile
				options.zipFile = File.createTempFile("results_", ".zip", options.zipInDirectory);
			}
			catch (IOException e)
			{
				exitOnError(32, "Could not create a zip file in directory"+options.zipInDirectory.getAbsolutePath(), e);
			}
		}	

		/* workspace */
		if (options.workspaceDir != null && options.useRegularWorkspaceFlag)
			exitOnError(-1, "Error: options -w and -r and mutually exclusive", null);

		if (options.workspaceDir != null)
			options.temporaryWorkspace = options.workspaceDir;
		else if (!options.useRegularWorkspaceFlag)
		{
			options.temporaryWorkspace = Utile.createTempDirectory("praxis_cmdline_", ".ws",
			                                                       new File(System.getProperty("java.io.tmpdir")));
			if (options.temporaryWorkspace==null)
			{
				exitOnError(-1, "Couldn't create a temporary workspace, try option '-w' instead", null);
			}
			if (options.verbose > INFO)
				System.err.println("Using workspace: " + options.temporaryWorkspace);
		}

		/* launch everything */
		if (options.verbose > INFO)
			System.err.println("Initializing (1/2)...");
		initStandalone();
		if (options.verbose > INFO)
			System.err.println("Initializing (2/2)...");
		Workflow workflow = null;
		try
		{
			workflow = new Workflow(new File(options.argument.get(0)), new Workflow.XMLWarnings(), false);
		}
		catch (InvalidXMLException e)
		{
			exitOnError(16, "Error: workflow could not be read", e);
		}
		return workflow;
	}
	
	/**
	 * Exit status: <ul>
	 * <li> 0: execution is successful
	 * <li> 1: execution is successful, with some warnings
	 * <li> 2: some errors were encountered while executing the workflow
	 * <li> 4: the workflow did not complete (if you see this, please contact us, this is a bug)
	 * <li> 8: an exception was raised during execution (same as above: this is a bug)
	 * <li> 16: the workflow could not be read
	 * <li> 32: the workflow is invalid
	 * <li> -1: incorrect command-line, due to unknown, incorrect or missing options/arguments.
	 * </ul>
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		Workflow workflow = parseOptionsAndInit(args);

		if (options.listInputIDs)
		{
			printWorkflowInputIds(workflow, new OutputStreamWriter(System.out));
			System.exit(0);
		}
		
		if (!options.inputMap.isEmpty())
		{
			for (String inputID: options.inputMap.keySet())
			{
				WorkflowInput wfInput = workflow.getInputWithId(inputID);
				if (wfInput == null)
				{
					exitOnError(16, "Error: workflow has no input for id: " + inputID, null);
				}
				wfInput.setContent(Arrays.asList(options.inputMap.get(inputID)));
			}
		}
		System.setProperty("SIMPLE_COMMAND_LINE", "Y");
		
		if (workflow != null)
		{
			boolean invalidWorkflow = false;
			final StringBuilder err = new StringBuilder();
			for ( Program prg: workflow.getPrograms()) 
			{
				List<Parameter> disconnectedInputs = prg.getDisconnectedInputFiles();
				if ( disconnectedInputs.isEmpty() )
					continue;
				invalidWorkflow = true;
				err.append("Program '"+prg.getProgramID()+"':\n");
				for (Parameter disconnectedInput: disconnectedInputs)
				{
					final ParameterDescription parameterDescription = disconnectedInput.getDescription();
					err.append("  disconnected input: id=").append(parameterDescription.getId());
					err.append(" (").append(parameterDescription.getDisplayName()).append(")\n");
				}
			}
			if ( invalidWorkflow )
			{
				exitOnError(32, err.toString(), null);
			}
			int ret = 0;
			try
			{
				Result result;
				result = executeAndGetResult(workflow);

				File resultDirectory = new File(StorageManager.resultDirectory(workflow.getName(), result.executionID()));
				if (options.verbose > DEBUG)
					System.err.printf("Result's directory: %s\n", resultDirectory.getAbsolutePath());

				// where the results are put after the program exist: either a directory or a zip file
				File finalResultFile = null;
				
				// options.zipInDirectory is handled in parseOptionsAndInit(), by setting options.zipFile
				
				if (options.zipFile != null)
				{
					finalResultFile = options.zipFile;
					if (options.verbose >= DEBUG)
						System.err.println("Creating: "+options.zipFile);
					if (!Utile.zipDirectory(resultDirectory, options.zipFile, null))
					{
						System.err.printf("Warning: couldn't create zip file %s\n", options.zipFile);
						ret = 32;
					}
				}
				if (options.outputDirectory != null)
				{
					finalResultFile = options.outputDirectory;
					if (options.verbose > DEBUG)
						System.err.printf("Moving: %s to: %s\n", resultDirectory, options.outputDirectory);
					Utile.deepCopyFile(resultDirectory, options.outputDirectory);
				}
				if (options.verbose > INFO)
					System.err.printf("Results are available here: %s\n", finalResultFile);
				System.out.printf("%s\n", finalResultFile);
				switch (result.getStatus())
				{
					case OK:
						ret |= 0;
						break;
					case WARNING:
						ret |= 1;
						break;
					case ERROR:
						ret |= 2;
						break;
					default: // it should not happen, the returned status isClosed()
						ret |= 4;
						break;
				}
			}
			catch (Throwable e)
			{
				e.printStackTrace();
				ret |= 8;
			}
			finally {
				if (options.verbose > INFO)
					System.err.println("Deleting temporary workspace");
				Utile.deleteRecursively(options.temporaryWorkspace);
			}
			disconnectAndExit(ret);
		}
		
		// disabled for the moment being
/*		if (false && args.length == 2)
		{
			n = Integer.parseInt(args[1]);
			for (int i = 0; i < n; i++)
			{
				executeWorkflow(workflow, false);
			}
			System.out.printf("Submitted the workflow %d times\n", n);
			while (endOfExecution < n)
				Thread.sleep(100);
		}
		disconnectAndExit(0);
*/
	}
	
	/**
	 * Launches all the threads necessary to have a working execution environment on a single workstation. After the
	 * method is called once, all subsequent calls simply return, doing nothing.
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws UnknownHostException
	 * @see #disconnectAndExit(int)
	 */
	private static void initStandalone() throws Exception
	{
		// disables the GUI
		Configuration.setBoolean("client.no_gui", true);
		// use a temporary workspace, so that the real one (if any) is not subject to change
		// Plus: this allows the command-line to be called concurrently (the app. is not ready to share the workspace
		// between two processes)
		if (options.temporaryWorkspace != null)
			Configuration.set("workspace_directory", options.temporaryWorkspace.getAbsolutePath());

		// Don't set Configuration.RMI_REGISTRY_PORT to 0 to let startRMIRegistry() find an available port:
		// if needed, an alternate configuration file should be provided.  Setting it to zero is handy in a
		// standalone configuration, but this should not be done in the general case, esp. when the client is
		// separated from the server & platforms.

		Launcher.main(new String[] {});
		Client.uiClient.getConnection().addListener(singleton);
		StorageManager.addListener(singleton);
	}
	
	public static Result executeAndGetResult(Workflow workflow) throws InvalidXMLException, IOException,
	        InterruptedException
	{
		Log.log.entering(CLASS, "executeAndGetResult", workflow);
		// execute: wait() on singleton, triggered by resultModified()
		executeWorkflow(workflow);
		
		Result result = singleton.result;

		/*
		 * make sure it *is* deleted: if we exit too soon, the server may not have the time to delete the result, and
		 * as a consequence the result remains registered.
		 * NB: it is deleted by a request made by the StorageManager
		 */
		Stream <Result> results = getAvailableResults(workflow).stream();
		while (results.anyMatch((aResult)->aResult.executionID().equals(result.executionID())))
		{
			Log.log.finest("The result has not been deleted server-side yet");
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e)
			{ Log.exception.log(Level.INFO, "Interrupted", e); }
			results = getAvailableResults(workflow).stream();
		}
		Log.log.exiting(CLASS, "executeAndGetResult", result);
		return result;
		
	}
	
	/**
	 * Clean up the standalone execution environment and exit.
	 * @param status
	 *            the process' return status
	 * @see #initStandalone()
	 */
	public static void disconnectAndExit(int status)
	{
		Client.uiClient.disconnect();
		System.exit(status); // FIXME si une tâche est en cours, p.ex. l'effacement d'un résultat du côté serveur, on
							 // court le risque qu'elle ne soit jamais faite.
	}
	
	/**
	 * Execute a workflow and waits for the execution to complete.
	 * @param workflow
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void executeWorkflow(Workflow workflow) throws IOException, InterruptedException
	{
		executeWorkflow(workflow, true);
	}
	
	public static void executeWorkflow(Workflow workflow, boolean waitEndOfExecution) throws IOException,
	        InterruptedException
	{
		File zip = File.createTempFile("praxis_cmdline_workflow_", ".zip");
		zip.deleteOnExit();
		Result result = StorageManager.createExecution(workflow);
		workflow.prepareWorkflowForExecution(zip, StorageManager.directoryForExecution(workflow.id(), result
		        .executionID()));
		Log.log.fine("Requesting execution of " + workflow.getName() + "...");
		synchronized(singleton)
		{
			Client.uiClient.requestExecution(result);
			if (waitEndOfExecution)
			{
				singleton.wait();
				Log.log.fine("Execution of " + workflow.getName() + " is finished");
			}
		}
	}
	
	/**
	 * Retrieves the most recent results
	 * @param workflow
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	public static List<Result> getAvailableResults(Workflow workflow) throws InterruptedException
	{
		Log.log.fine("Retrieving results for " + workflow.getName() + "...");
		List<Result> results = null;

		synchronized(singleton)
		{
			singleton.waitForAvailableResults = true;
			Client.uiClient.requestsAvailableResults(workflow);
			singleton.wait();
			// at this point we received the answer, and event is the answer
			// TODO note: we should check that the answer is the one we expected,
			// i.e.: correct type, correct workflow.
			Log.log.info(singleton.event.toString());
			results = (List<Result>) singleton.event.data;
		}
		Log.log.fine("Got " + results.size() + " results for " + workflow.getName());
		return results;
	}
	
	/* Implementation of ServerMessageListener */
	/**
	 * Called when available results are received. When the message is in response to the request made by
	 * {@link #getAvailableResults(Workflow)}, that method is unblocked and returns the received results.
	 * 
	 * @param anEvent the received event
	 */
	@Override
	public void receivedAvailableResults(ServerToClientEvent anEvent)
	{
		synchronized(this)
		{
			if (!waitForAvailableResults)
				return;
			waitForAvailableResults = false;
			this.event = anEvent;
			notify();
		}
	}
	
	@Override
	public void receivedExecutionStatus(ServerToClientEvent anEvent)
	{
		final Result aResult = (Result) anEvent.data;
		if (aResult == null)
		{
			Log.log.severe("Received a null result!?!");
			return;
		}
		if (aResult.getStatus().isClosed())
		{
			int _endOfExecution = endOfExecution.incrementAndGet();
			/*
			 * Log.log.info("Got end of exec. for " + this.event.username + "::" + this.event.workspace + ":" +
			 * this.event.workflowID + " status: "+((Result)this.event.data).getStatus().toString());
			 */
			Log.log.info(()->"End of exec. " + _endOfExecution);
			synchronized(this)
			{
				this.event = anEvent;
				//notify();
			}
		}
		else
		{
			/*
			 * Do NOT notify(): we're not interested in this event here, and notify()'ing here makes executeWorkflow()
			 * thinks that execution is finished
			 */
			/*
			this.event = event;
			Log.log.info("Got end of prg.exec. for " + this.event.username + "::" + this.event.workspace + ":"
			             + this.event.workflowID + " prgs'ids: "+((java.util.ArrayList<String>)this.event.data).toString());
			 */
		}
	}
	
	@Override
	public void receivedExecutionResults(ServerToClientEvent anEvent)
	{ /* ignore */ }
	
	@Override
	public void receivedMessage(ServerToClientEvent anEvent)
	{ /* ignore */ }
	
	public void receivedWorkflowStatus(ServerToClientEvent anEvent)
	{ /* ignore */ }
	
	public void receivedAvailableResources(ServerToClientEvent anEvent)
	{ /* ignore */ }
	
	public void receivedNewProgramsDescriptions(ServerToClientEvent anEvent)
	{ /* ignore */ }
	
	public void receivedAuthentificationStatus(ServerToClientEvent anEvent)
	{
		synchronized(this)
		{
			notify();
		} // TODO verify we are connected
	}
	
	@Override
	public void disconnected(Exception exception)
	{
		if (exception == null)
			Log.log.info("Disconnected");
		else
			Log.log.warning("Disconnected! " + exception.toString());
		
		synchronized(this)
		{
			notify();
		} // TODO and then?!
	}
	
	@Override
	public void resultModified(WorkflowID workflowID, Result aResult)
	{
		synchronized(this)
		{
			final Status status = aResult.getStatus();
			if (!status.isClosed() || aResult.zipFile()==null)
				return;
			this.result = aResult;
			notify();
		}
	}
}
