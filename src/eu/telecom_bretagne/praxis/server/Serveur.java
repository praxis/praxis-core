/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import java.io.File;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import org.jdom.Document;

import eu.telecom_bretagne.praxis.client.event.ApplicationListener;
import eu.telecom_bretagne.praxis.common.Application;
import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Console;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.common.events.RMICommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.SSLSocketCommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.SocketCommunicationFacade;
import eu.telecom_bretagne.praxis.core.execution.ExecutionLoop;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.ResultStore;
import eu.telecom_bretagne.praxis.core.resource.ResourceRepository;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;



/**
 * @author Sébastien Bigaret
 */
public class Serveur extends UnicastRemoteObject implements RemoteServerInterface, ApplicationListener
{
	/**
	 * This is used to log the number of clients connected, when a client {@link #registerClient(Client) connects} or
	 * {@link #removeClient(Client) disconnects}. This may be used by an external tool to monitor the number of
	 * connected users by examining the logs.
	 */
	public static final String NUMBER_OF_CLIENTS_CURRENTLY_CONNECTED = "Number of clients currently connected: ";

	public static class TooManyClientsException
	    extends Exception
	{
		private static final long serialVersionUID = 1L;
		public TooManyClientsException()
		{
			super("common.cnx_rejected.maximum_number_of_clients");
		}
	}

	private static final long serialVersionUID = 6870908801969839494L;

	/**
	 * Holds all the {@link Client clients} that are connected
	 */
	static private List<Client>                 clients    = Collections.synchronizedList(new ArrayList<Client>());
	
	/**
	 * List of all available (online) platforms
	 */
	static private List<Server>                 platforms  = Collections.synchronizedList(new ArrayList<Server>());
	
	static private ResultStore                  resultStore;
	
	static private String                       tmpDir;
	
	static transient private Serveur            serveur;
	
	static transient long                       sequence;

	static private int                          maximum_nb_of_clients;

	/**
	 * Null if clients are allowed to log in, or a String giving the reason why login is disabled.
	 * @see #getClientLoginDisabled()
	 * @see #setClientLoginDisabled(String)
	 */
	static private String client_login_disabled = null;

	static
	{
		tmpDir = PraxisPreferences.get(PraxisPreferences.rootNode, "prefix") + "/tmp";
		
		try
		{
			ResourceRepository.startRMIRegistry();
			if (Configuration.getBoolean("rmi.useSSL"))
				serveur    = new Serveur("unused_use_ssl"); // moved here because of RemoteException
			else
				serveur    = new Serveur();
			resultStore = new ResultStore();
		}
		catch (Exception e)
		{
			Log.log.log(Level.SEVERE, "Unable to create database", e);
			System.exit(-1);
		}
		Application.getApplication().addListener(serveur);
		setMaximumNumberOfClients(Configuration.getInt("server.maximum_number_of_clients"));
	}
	
	/**
	 * Builds a new server, <b>not</b> suitable for being used by RMI+SSL.
	 * @throws RemoteException
	 */
	private Serveur() throws RemoteException
	{
		super(Configuration.RMI_REGISTRY_PORT);
		addConsoleCommand_clients();
		addConsoleCommand_login();
		addConsoleCommand_maxNumberOfClients();
	}
	
	/**
	 * Builds a new server, suitable for being used by RMI+SSL.
	 * @param unused_this_is_just_the_constructor_for_ssl
	 *            completely ignored
	 * @throws RemoteException
	 */
	private Serveur(String unused_this_is_just_the_constructor_for_ssl) throws RemoteException
	{
		super(Configuration.RMI_REGISTRY_PORT,
		      new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
	}

	public static ResultStore resultStore()
	{
		return resultStore;
	}
	
	public static File executionDirectory(WorkflowID id)
	{
		return new File(tmpDir, id.hash20());
	}
	
	/* ---- ---- Platforms ---- ---- */
	/**
	 * Registers a new platform that is connected to the server. The method automatically notifies the connected
	 * clients that the list of available resources has changed.
	 * @param platform
	 *            visitor for the platform newly connected to the server
	 * @see #updateClientsResources()
	 */
	protected static void registerPlatform(Server platform)
	{
		platforms.add(platform);
		sequence++;
	}
	
	/**
	 * Removes the platform described by that visitor from the list of platforms connected to the server.
	 * @param platform
	 * @return <code>true</code> if the platform was effectively suppressed, <code>false</code> if the platform was
	 *         not previously registered.
	 */
	protected static boolean removePlatform(Server platform)
	{
		if ( platforms.remove(platform) )
		{
			sequence++;
			return true;
		}
		return false;
	}
	
	/**
   */
	public static PlatformDescription[] getPlatforms()
	{
		PlatformDescription [] descriptions = new PlatformDescription[platforms.size()];
		int idx=0;
		synchronized (platforms)
		{
			for (Server p: platforms)
				descriptions[idx++] = p.getPlatform();
		}
		return descriptions;
	}
	
	/* ---- ---- Clients ---- ---- */
	/**
	 * Sets whether clients are allowed to login or not
	 * @param reason
	 *            if null, clients are allowed to login. Otherwise this is the reason why clients logins is disabled (it
	 *            may be empty).
	 * @see #getClientLoginDisabled()
	 */
	static protected synchronized void setClientLoginDisabled(String reason)
	{
		client_login_disabled = reason;
	}

	/**
	 * Tells whether clients are allowed to login.
	 * @return {@code null} if clients are allowed to log in, or the reason why clients logins are disabled.
	 * @see #setClientLoginDisabled(String)
	 */
	static protected synchronized String getClientLoginDisabled()
	{
		return client_login_disabled;
	}

	/**
	 * Sets the maximum number of clients.
	 * 
	 * @param max
	 *            the maximum number of clients. If it is negative, sets the maximum to
	 *            {@link Integer#MAX_VALUE}.
	 */
	static protected synchronized void setMaximumNumberOfClients(int max)
	{
		maximum_nb_of_clients = max < 0 ? Integer.MAX_VALUE : max;
	}

	
	/**
	 * Returns the maximum number of clients allowed.
	 * 
	 * @return the maximum number of clients (a positive integer)
	 */
	static protected synchronized int getMaximumNumberOfClients()
	{
		return maximum_nb_of_clients;
	}

	/**
	 * Adds a new client to the list of the clients connected to the server
	 * @param client
	 *            (should not be null)
	 * @exception IllegalArgumentException
	 *                if a client with the same {@link Client#getLogin() login} is already registered
	 * @exception NullPointerException
	 *                if client is null
	 */
	protected static void registerClient(Client client) throws TooManyClientsException, IllegalArgumentException
	{
		synchronized(clients)
		{
			final int MAX_CLIENTS = getMaximumNumberOfClients();

			Log.log.finest("Registering new client (already connected: " + clients.size() + "/" + MAX_CLIENTS+ ")");
			if ( clients.size() >= MAX_CLIENTS )
				throw new TooManyClientsException();
			Log.log.fine("Registering client (may fail): " + client.toString());
			if (isClientRegistered(client.getLogin()))
				throw new IllegalArgumentException("A client is already registered with login: " + client.getLogin());
			
			Log.log.fine("Registered client: " + client.toString());
			clients.add(client);
			Log.log.info(NUMBER_OF_CLIENTS_CURRENTLY_CONNECTED+clients.size());
		}
		/* now that the client is registered, messages can be sent safely */
		clientConnected(client);
	}

	/**
	 * Called when a client connects: notify success and send it the state of its results stored on the server.
	 * @param client the client that has just connected
	 */
	protected static void clientConnected(Client client)
	{
		client.notifyAuthentificationSuccess();
		/*
		 * Does it have running processes? In this case, it should be re-attached to these processes. It may happen
		 * that the client connects when a process is running, but that the request for re-attaching it happens just
		 * after the process finished: in this case, the client misses the last notification. That's not a problem
		 * anyway, since the next step consists in sending him a summary of the available results.
		 */
		ArrayList <Result> results = resultStore.getResultsSummaryForUser(client.getLogin()); // null in case of error
		if (results != null)
			ExecutionLoop.executionLoop.reattachProcesses(client, results);

		/* if it has results, send them */
		client.sendResultsSummary();
	}

	/**
	 * Removes a client to the list of the clients connected to the server
	 * @param client
	 * @return <code>true</code> if the client was in the list of the connected clients --hence, indicating that it
	 *         has been removed; <code>false> if the client was not in the list.
	 */
	protected static boolean removeClient(Client client)
	{
		if (client != null)
			Log.log.fine("Unregistering client: " + client.toString());
		else
			Log.log.fine("Unregistering client: null");
		
		boolean ret_status = clients.remove(client);
		Log.log.info(NUMBER_OF_CLIENTS_CURRENTLY_CONNECTED+clients.size());
		if (Configuration.getBoolean("server.exit_on_last_client_exit") && clients.size()==0)
		{
			for (Server platform: platforms)
				platform.requestTermination("Exiting after last client exits");
			int idx=0;
			while (idx<120) /* wait at most 60s for the platforms to disconnect */
			{
				try { java.lang.Thread.sleep(500); } catch (InterruptedException e) { /* ignore */ }
				if (platforms.size()==0)
					break;
				idx+=1;
			}
			if (platforms.size()!=0)
				Log.log.warning("Exiting after the last client exits, some platforms were still connected");
			else
				Log.log.info("Exiting after the last client exits");
			Application.getApplication().exit();
		}
		return ret_status;
	}
	
	/**
	 * Returns the client registered with a given login.
	 * @param login
	 *            the login identifying a client
	 * @return the corresponding client if it exists, {@code null} if there is no such client.
	 */
	protected static Client getRegisteredClient(String login)
	{
		synchronized(clients)
		{
			for (Client client: clients)
			{
				if (client.getLogin().equals(login))
					return client;
			}
		}
		return null;
	}

	/**
	 * Tells whether there is a client currently connected to the server with the supplied login.
	 * @param login a client's login
	 * @return {@code true} if such a client exists, {@code false} otherwise
	 */
	public static boolean isClientRegistered(String login)
	{
		return getRegisteredClient(login)!=null;
	}
	
	public static void launchServer(String connection_mode)
	{
		if ("rmi".equals(connection_mode))
			startRMIServer();
		else if ("socket".equals(connection_mode))
			startSocketServer();
		else if ("socket-ssl".equals(connection_mode))
			startSSLSocketServer();
		else if ("direct".equals(connection_mode))
			startDirect();

	}
	public static void launch() throws Exception
	{
		// Get the platforms to launch...
		String[] connection_modes = Configuration.getArray("servers");

		if ( connection_modes.length==0 )
			return;
		
		Log.log.log(Level.INFO, "binding {0} to: {1}", new Object[]{serveur, RemoteComponents.RMI_SERVER_NAME});
		ResourceRepository.rmiRegistry.rebind(RemoteComponents.RMI_SERVER_NAME, serveur);

		for (String cm:connection_modes)
			if (!"".equals(cm))
				launchServer(cm);
	}
	
	/**
	 * 
	 */
	public static void startDirect()
	{
		/* nothing to do */
	}

	/**
	 * Starts the socket server on the specified port.
	 */
	public static void startSocketServer()
	{
		int portNumber = Integer.parseInt(Configuration.get("socket.port"));
		InetAddress bindAddress=null;
		String addr = Configuration.get("socket.server.bindAddress");
		if (addr!=null && !"".equals(addr))
	        try
            {
	            bindAddress = InetAddress.getByName(addr);
            }
            catch (UnknownHostException e)
            {
            	Log.log.log(Level.SEVERE, "Couldn't start the socket server", e);
            	// Pour le moment on lève une exception qui ne sera pas géré au-dessus, et le prg exit.
            	// Temporaire: la configuration devrait être vérifiée au chargement. TODO
            	throw new RuntimeException("Could not start the socket server", e);
            }
		SocketCommunicationFacade.launchServer(bindAddress, portNumber);
	}
	
	/**
	 * Starts the SSL socket server on the specified port.
	 */
	public static void startSSLSocketServer()
	{
		int portNumber = Integer.parseInt(Configuration.get("socket-ssl.port"));
		InetAddress bindAddress=null;
		String addr = Configuration.get("socket-ssl.server.bindAddress");
		if (addr!=null && !"".equals(addr))
	        try
            {
	            bindAddress = InetAddress.getByName(addr);
            }
            catch (UnknownHostException e)
            {
            	Log.log.log(Level.SEVERE, "Couldn't start the ssl-socket server", e);
            	// Pour le moment on lève une exception qui ne sera pas géré au-dessus, et le prg exit.
            	// Temporaire: la configuration devrait être vérifiée au chargement. TODO
            	throw new RuntimeException("Could not start the socket server", e);
            }
		SSLSocketCommunicationFacade.launchServer(bindAddress, portNumber);
	}

	/**
	 * Starts the RMI server.<br>
	 * 
	 */
	public static void startRMIServer()
	{
		RMICommunicationFacade.launchServer();
	}
	
	public static Serveur server()
	{
		return serveur;
	}
	
	@Override
    public boolean isAvailable(String prgID)
    {
		Log.log.fine(()->"Request for isAvailable("+prgID+")");
		synchronized(platforms)
		{
			for (Server p: platforms)
			{
				if ( p.platform.isAvailable(prgID) )
					return true;
			}
			return false;
		}
    }

	@Override
	public long sequence()
	{
		return sequence;
	}
	
	protected static Document availableResources = null;

	protected static synchronized Document _availableResources()
	{
		if (availableResources==null)
		{
			try
            {
				final String filename = Configuration.get("server.resources_clientTree");
	            availableResources = Facade_xml.read(FileResources.inputStreamForResource(filename),
		                            true);
            }
            catch (InvalidXMLException | ResourceNotFoundException e)
            {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
		}
		return availableResources;
	}

	@Override
	public Document availableResources()
	{
		return Serveur._availableResources();
	}

	protected void addConsoleCommand_clients()
	{
		Application.getApplication().console.addCommand(new Console.ConsoleCommand() {
			public static final String COMMAND="server.clients";
			@Override
			public String description()
			{
				return "List the # of clients connected\nOptions:\n\tls\tshow detail\n\tmsg-all <msg>\tsend message to all clients";
			}
			@Override
			public String[] commands() { return new String[]{ COMMAND }; }
			@Override
			public void execute(String command, String argument, PrintStream p)
			{
				if (COMMAND.equals(command))
					showClients(p, argument);
			}
			private void showClients(PrintStream p, String args)
			{
				p.format("# of registered clients: %d\n", Serveur.clients.size());
				if (args==null || "".equals(args))
					return;
				if ("ls".equals(args))
					for (Client client: Serveur.clients)
						p.println(client);
				if (args.startsWith("msg"))
				{
					String [] login_msg=args.substring(3).trim().split(" ", 2);
					Client client = Serveur.getRegisteredClient(login_msg[0]);
					if (client!=null && !"".equals(login_msg[1]))
						client.sendAndForwardMessage(login_msg[1], null);
				}
				if (args.startsWith("msg-all"))
				{
					String msg=args.substring(7).trim();
					if (!"".equals(msg))
						for (Client client: Serveur.clients)
							client.sendAndForwardMessage(msg, null);
				}
			}
		});
	}

	protected void addConsoleCommand_login()
	{
		Application.getApplication().console.addCommand(new Console.ConsoleCommand() {
			public static final String COMMAND="server.clients_login";
			@Override
			public String description()
			{
				return "Allows or disallows clients to log in\nOptions:\n\ton\tallow clients to login\n\toff <msg>\tforbid client to log in (with an optional message; use \n to insert a newline)";
			}
			@Override
			public String[] commands() { return new String[]{ COMMAND }; }
			@Override
			public void execute(String command, String argument, PrintStream p)
			{
				if (COMMAND.equals(command))
					login(p, argument);
			}
			private void login(PrintStream p, String args)
			{
				p.format("# of registered clients: %d\n", Serveur.clients.size());
				if (args==null || "".equals(args))
				{
					String reason = Serveur.getClientLoginDisabled();
					if ( reason == null )
						p.println("Clients login: enabled");
					else
					{
						p.println("Clients login: disabled");
						if ( ! "".equals(reason) )
							p.println("Reason: "+reason);
					}
					return;
				}

				if ("on".equals(args))
				{
					Serveur.setClientLoginDisabled(null);
					p.println("Client login: enabled");
				}
				if (args.startsWith("off"))
				{
					Serveur.setClientLoginDisabled(args.substring(3).trim());
					p.println("Client login: disabled");
				}
			}
		});
	}

	protected void addConsoleCommand_maxNumberOfClients()
	{
		Application.getApplication().console.addCommand(new Console.ConsoleCommand() {
			public static final String COMMAND="server.max_clients";
			@Override
			public String description()
			{
				return "Displays and sets the maximum number of clients";
			}
			@Override
			public String[] commands() { return new String[]{ COMMAND }; }
			@Override
			public void execute(String command, String argument, PrintStream p)
			{
				if (COMMAND.equals(command))
					max_users(p, argument);
			}
			private void max_users(PrintStream p, String args)
			{
				if (args!=null && !"".equals(args))
				{
					try
					{
						setMaximumNumberOfClients(Integer.parseInt(args));
					}
					catch (NumberFormatException e)
					{
						p.format("Error: %s is not an integer\n", args);
						return;
					}
				}
				final String max = getMaximumNumberOfClients()==Integer.MAX_VALUE ?
				                       "unlimited" : Integer.toString(getMaximumNumberOfClients());
				p.format("Maximum number of clients: %s\n", max);
			}
		});
	}

	@Override
	public void applicationExiting()
	{
		// stop receiving anything TODO

		// Informs the execution loop it should stop
		ExecutionLoop.executionLoopThread.interrupt();

		// disconnect platforms
		for (Server platform: new ArrayList<Server>(platforms)) //copy before iterating since disconnect() ultimately modifies the list
			platform.cnx.disconnect();

		// wait for the execution loop to stop
		while (ExecutionLoop.executionLoopThread.isAlive())
			try { Thread.sleep(50); } catch (InterruptedException e) { /* ignore */ }

		// store its status
		try
		{
			ExecutionLoop.saveState();
			Log.log.info("State of the current execution loop was successfully saved");
		}
		catch (java.io.IOException e)
		{
			Log.log.log(java.util.logging.Level.SEVERE, "Could not save the state of the current execution loop", e);
		}
	}

}
