// Copyright (c) 2012, Institut Mines Telecom - Telecom Bretagne
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//   Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
//
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
//   Neither the name of the Institut Mines Telecom - Telecom Bretagne nor
//   the names of its contributors may be used to endorse or promote products
//   derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package eu.telecom_bretagne.praxis.common;

import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.io.IOException;

// Note: extending sun.net.spi.DefaultProxySelector and overriding select()
//       so that it is synchronized is also possible, but doing it this way
//       avoids a dependency to such an internal proprietary API.

/**
 * A simple wrapper around a ProxySelector, making select() a synchronized
 * method.  Wrapping the default proxy selector with a wrapper on it
 * avoids having random crashes (SIGSEGV) of the JVM on GNU/Linux (w/ d-bus)
 * when the property {@code java.net.useSystemProxies} is set to {@code true}.
 * <p>
 * How to use: simply replace the default proxy selector as soon as possible
 * in your application code:
 * <pre>
 * {@code
 * ProxySelector defaultProxySelector = ProxySelector.getDefault();
 * if (defaultProxySelector!=null)
 *     ProxySelector.setDefault(new ProxySelectorWrapper(defaultProxySelector));
 * </pre>
 *
 * @see <a href="http://mail.openjdk.java.net/pipermail/jdk7u-dev/2012-August/003911.html">http://mail.openjdk.java.net/pipermail/jdk7u-dev/2012-August/003911.html</a>
 * @see <a href="http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7188755">http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7188755</a>
 * @see <a href="http://java.net/jira/browse/GLASSFISH-12213">http://java.net/jira/browse/GLASSFISH-12213</a>
 * @see <a href="https://bugs.launchpad.net/ubuntu/+source/openjdk-6/+bug/885195">https://bugs.launchpad.net/ubuntu/+source/openjdk-6/+bug/885195</a>
 *
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class ProxySelectorWrapper extends ProxySelector
{
	private final ProxySelector wrappedSelector;

	ProxySelectorWrapper(ProxySelector proxySelector)
	{
		wrappedSelector = proxySelector;
	}

	public synchronized java.util.List<Proxy> select(URI uri)
	{
		return wrappedSelector.select(uri);
	}

	public void connectFailed(URI uri, SocketAddress sa, IOException ioe)
	{
		wrappedSelector.connectFailed(uri, sa, ioe);
	}
}
