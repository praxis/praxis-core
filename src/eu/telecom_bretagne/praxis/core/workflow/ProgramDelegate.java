/* License: please refer to the file README.license located at the root directory of the project */
// LICENSE
/**
 *
 */
package eu.telecom_bretagne.praxis.core.workflow;

import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;

/**
 * A delegate for the class {@link eu.telecom_bretagne.praxis.core.workflow.Program}.
 * @author Sébastien Bigaret
 */
public class ProgramDelegate
{
	/**
	 * Called by a program when it is {@link Program#fromXML(Element, XMLWarnings) initialized from XML}, when no
	 * description can be found in the resource repository that correspond to its id.<br>
	 * The default delegate tries to find in the resource repository a description that has the same provider and
	 * name, but with a different version.<br>
	 * <br>
	 * Praxis projects can subclass this delegate and register their own version to address different migration needs.
	 * For example, if a program is renamed, this allows a project to transparently adapt existing workflows using the
	 * old name.<br>
	 * Subclasses overriding the method may choose not to call the superclass implementation.
	 * @param prg_description_id
	 *            id of the resource for which a description is requested
	 * @param prg_description_full_name
	 *            A human readable description of the original description.
	 * @param warnings
	 *            implementing methods add warnings to this object, when necessary. For example, when an alternate
	 *            description is found, this method adds the following warning so that users are informed of the
	 *            substitution:
	 *
	 *            <pre>
	 * warnings.add(Workflow.XMLWarnings.PRG_DESC_REPLACED_BY_ALTERNATIVE,
	 *              new String[] { xml_node.getAttributeValue(Program.PRG_INFO), prg_description_id });
	 * </pre>
	 *
	 *            The warning message is usually a key registered in I18N resources, but this is not required, for
	 *            example:
	 *
	 *            <pre>
	 * warnings.add(&quot;program {0} replaced by {1}&quot;, new String[] { xml_node.getAttributeValue(Program.PRG_INFO),
	 *                                                           prg_description_id });
	 * </pre>
	 *
	 *            Overriding methods may choose not to add a warning; in this case, if the returned value is non-null,
	 *            the user will not be informed that a substitution happened.
	 * @param xml_exc
	 *            a prepared exception that the method may choose to raise if something goes wrong. The call typically
	 *            looks like:
	 * 
	 *            <pre>
	 * throw xml_exc.setMsg(&quot;message&quot;).setIsFatal(isErrorFatal);
	 * </pre>
	 * 
	 *            If it is needed, implementors should use this exception rather than building a new one: the supplied
	 *            exception contains extra information that makes it possible to identify where, in a workflow, the
	 *            error happened.<br>
	 *            Implementors should be very careful using this. In most situations, adding a warning and returning
	 *            null is sufficient.
	 * @return the requested description, or null if none can be found. When non-null, the returned description will
	 *         be used to build the program.
	 */
	public ProgramDescription findAlternateProgramDescription(String prg_description_id,
	                                                          String prg_description_full_name,
	                                                          XMLWarnings warnings, InvalidXMLException xml_exc)
	{

		ProgramDescription prgDesc = null;

		if (!prg_description_id.contains("___")) // old version, try to find the real one
		{
			if ("Clustalw".equals(prg_description_id))
				prg_description_id = "clustalw";
			if ("muscle_alignment".equals(prg_description_id))
				prg_description_id = "muscle";
			for (String id: RemoteComponents.resourceRepository().getResources())
			{
				ProgramDescription p = RemoteComponents.resourceRepository().programDescriptionForId(id);
				if (p.isActive() && p.getPrgName().equals(prg_description_id))
				{
					Log.log.fine("Mapping old prg.description's id:" + prg_description_id + " to: " + p.id());
					prg_description_id = p.id();
					prgDesc = p;
				}
			}
		}

		if (prgDesc == null && prg_description_id.contains("___"))
		{
			/*
			 * The corresponding program is not offered (anymore): try to find an other version
			 */
			prgDesc = RemoteComponents.resourceRepository().alternateProgramDescriptionForId(prg_description_id);
			if (prgDesc != null)
			{
				// msg I18N + its parameters
				warnings.add(Workflow.XMLWarnings.PRG_DESC_REPLACED_BY_ALTERNATIVE,
				             new String[] { prg_description_full_name, prgDesc.fullName() } );
			}
		}
		return prgDesc;
	}

}
