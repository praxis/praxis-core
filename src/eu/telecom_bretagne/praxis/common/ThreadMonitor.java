/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;
/*
 * @(#)ThreadMonitor.java	1.6 05/12/22
 * 
 * Copyright (c) 2006 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * -Redistribution of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduce the above copyright notice, 
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 * 
 * Neither the name of Sun Microsystems, Inc. or the names of contributors may 
 * be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL 
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN MIDROSYSTEMS, INC. ("SUN")
 * AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST 
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, 
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY 
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE, 
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that this software is not designed, licensed or intended
 * for use in the design, construction, operation or maintenance of any
 * nuclear facility.
 */

/*
 * @(#)ThreadMonitor.java	1.6 05/12/22
 */

import static java.lang.management.ManagementFactory.THREAD_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.getThreadMXBean;
import static java.lang.management.ManagementFactory.newPlatformMXBeanProxy;

import java.io.IOException;
import java.lang.management.LockInfo;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

/**
 * Example of using the java.lang.management API to dump stack trace
 * and to perform deadlock detection.
 *
 * @author  Mandy Chung
 * @version %% 12/22/05
 */
public class ThreadMonitor {
    private MBeanServerConnection server;
    private ThreadMXBean tmbean;
    private ObjectName objname;

    // default - JDK 6+ VM
    private String findDeadlocksMethodName = "findDeadlockedThreads";
    private boolean canDumpLocks = true;

    /**
     * Constructs a ThreadMonitor object to get thread information
     * in a remote JVM.
     */
    public ThreadMonitor(MBeanServerConnection server) throws IOException {
       this.server = server;
       this.tmbean = newPlatformMXBeanProxy(server,
                                            THREAD_MXBEAN_NAME,
                                            ThreadMXBean.class);
       try {
           objname = new ObjectName(THREAD_MXBEAN_NAME);
        } catch (MalformedObjectNameException e) {
            // should not reach here
            throw new InternalError(e.getMessage(), e);
       }
       parseMBeanInfo(); 
    }

    /**
     * Constructs a ThreadMonitor object to get thread information
     * in the local JVM.
     */
    public ThreadMonitor() {
        this.tmbean = getThreadMXBean();
    }

    /**
     * Prints the thread dump information to System.out.
     */
    public void threadDump(StringBuffer sb) {
        if (canDumpLocks) {
            if (tmbean.isObjectMonitorUsageSupported() &&
                tmbean.isSynchronizerUsageSupported()) {
                // Print lock info if both object monitor usage 
                // and synchronizer usage are supported.
                // This sample code can be modified to handle if 
                // either monitor usage or synchronizer usage is supported.
                dumpThreadInfoWithLocks(sb);
            }
        } else {
            dumpThreadInfo(sb);
        }
    }

    private void dumpThreadInfo(StringBuffer sb) {
       sb.append("Full Java thread dump\n");
       long[] tids = tmbean.getAllThreadIds();
       ThreadInfo[] tinfos = tmbean.getThreadInfo(tids, Integer.MAX_VALUE);
       for (ThreadInfo ti : tinfos) {
           printThreadInfo(ti, sb);
       }
    }

    /**
     * Prints the thread dump information with locks info to System.out.
     */
    private void dumpThreadInfoWithLocks(StringBuffer sb) {
       sb.append("Full Java thread dump with locks info\n");

       ThreadInfo[] tinfos = tmbean.dumpAllThreads(true, true);
       for (ThreadInfo ti : tinfos) {
           printThreadInfo(ti, sb);
           LockInfo[] syncs = ti.getLockedSynchronizers();
           printLockInfo(syncs, sb);
       }
       sb.append("\n");
    }

    private static String INDENT = "    ";

    private void printThreadInfo(ThreadInfo ti, StringBuffer sb) {
       // print thread information
       printThread(ti, sb);

       // print stack trace with locks
       StackTraceElement[] stacktrace = ti.getStackTrace();
       MonitorInfo[] monitors = ti.getLockedMonitors();
       for (int i = 0; i < stacktrace.length; i++) {
           StackTraceElement ste = stacktrace[i];
           sb.append(INDENT + "at " + ste.toString() + "\n");
           for (MonitorInfo mi : monitors) {
               if (mi.getLockedStackDepth() == i) {
                   sb.append(INDENT + "  - locked " + mi + "\n");
               }
           }
       }
       sb.append("\n");
    }
                                                                                
    private void printThread(ThreadInfo ti, StringBuffer sb) {
       sb.append("\"" + ti.getThreadName() + "\"" +
                 " Id=" + ti.getThreadId() +
                 " in " + ti.getThreadState());
       if (ti.getLockName() != null) {
           sb.append(" on lock=" + ti.getLockName());
       }
       if (ti.isSuspended()) {
           sb.append(" (suspended)");
       }
       if (ti.isInNative()) {
           sb.append(" (running in native)");
       }
       sb.append("\n");
       if (ti.getLockOwnerName() != null) {
            sb.append(INDENT + " owned by " + ti.getLockOwnerName() +
                               " Id=" + ti.getLockOwnerId() +"\n");
       }
    }
    /*
    private void printMonitorInfo(ThreadInfo ti, MonitorInfo[] monitors, StringBuffer sb) {
       sb.append(INDENT + "Locked monitors: count = " + monitors.length + "\n");
       for (MonitorInfo mi : monitors) {
    	   sb.append(INDENT + "  - " + mi + " locked at " + "\n");
           sb.append(INDENT + "      " + mi.getLockedStackDepth() +
                     " " + mi.getLockedStackFrame() + "\n");
       }
    }
    */
    private void printLockInfo(LockInfo[] locks, StringBuffer sb) {
       sb.append(INDENT + "Locked synchronizers: count = " + locks.length + "\n");
       for (LockInfo li : locks) {
           sb.append(INDENT + "  - " + li + "\n");
       }
       sb.append("\n");
    }

    /**
     * Checks if any threads are deadlocked. If any, print
     * the thread dump information.
     */
    public boolean findDeadlock(StringBuffer sb) {
       long[] tids;
       if (findDeadlocksMethodName.equals("findDeadlockedThreads") && 
               tmbean.isSynchronizerUsageSupported()) {
           tids = tmbean.findDeadlockedThreads();
           if (tids == null) { 
               return false;
           }

           sb.append("Deadlock found :-\n");
           ThreadInfo[] infos = tmbean.getThreadInfo(tids, true, true);
           for (ThreadInfo ti : infos) {
               printThreadInfo(ti, sb);
               printLockInfo(ti.getLockedSynchronizers(), sb);
               sb.append("\n");
           }
       } else {
           tids = tmbean.findMonitorDeadlockedThreads();
           if (tids == null) { 
               return false;
           }
           ThreadInfo[] infos = tmbean.getThreadInfo(tids, Integer.MAX_VALUE);
           for (ThreadInfo ti : infos) {
               // print thread information
               printThreadInfo(ti, sb);
           }
       }

       return true;
    }


    private void parseMBeanInfo() throws IOException {
        try {
            MBeanOperationInfo[] mopis = server.getMBeanInfo(objname).getOperations();

            // look for findDeadlockedThreads operations;
            boolean found = false;
            for (MBeanOperationInfo op : mopis) {
                if (op.getName().equals(findDeadlocksMethodName)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                // if findDeadlockedThreads operation doesn't exist,
                // the target VM is running on JDK 5 and details about
                // synchronizers and locks cannot be dumped.
                findDeadlocksMethodName = "findMonitorDeadlockedThreads";
                canDumpLocks = false;
            }   
        } catch (IntrospectionException | InstanceNotFoundException | ReflectionException e) {
            throw new InternalError(e.getMessage(), e);
        }
    }
}
