/**
 *
 */
package eu.telecom_bretagne.praxis.core.workflow.io.xml;

import java.util.ArrayList;
import java.util.Locale;
import java.util.function.BiFunction;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput.INPUT_TYPE;

/**
 * @author Sébastien Bigaret
 */
public abstract class WorkflowInputParser
{

    static final String INPUT = "input";

    static final String INFILE = "infile";

    public static WorkflowInput fromXML(Workflow workflow, StartElement startElement, XMLEventReader eventReader,
                                        XMLWarnings warnings)
    throws InvalidXMLException
    {
        // attributes: id, type, x, y, name (all required)
        //   id is NOT stored here, but in the workflow's map {@link Workflow#inputs}
        final String type;
        String name = null;
        
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW_INPUT);

        WorkflowInput workflowInput = new WorkflowInput(WorkflowInput.INPUT_TYPE.FILE);

        final BiFunction<StartElement, String, String> attributeValue =
            (element, aname) -> element.getAttributeByName(new QName(aname)).getValue();

        // build WorkflowInput using attribute "type"
        type = attributeValue.apply(startElement, "type");
        try
        {
            workflowInput = new WorkflowInput(INPUT_TYPE.valueOf(type.toUpperCase(Locale.ENGLISH)));
        }
        catch (IllegalArgumentException e)
        {
            throw xml_exc.setMsg("Invalid input type ("+type+"): "+e.toString());
        }
        if ( ! (workflowInput.isOfType(INPUT_TYPE.FILE) || workflowInput.isOfType(INPUT_TYPE.DIRECTORY)) )
        {
            eu.telecom_bretagne.praxis.common.Utile.unimplemented("Unimplemented method for type:"+type);
        }

        workflowInput.setName(attributeValue.apply(startElement, "name"));
        xml_exc.setName(name);

        try
        {
            workflowInput.setX(Integer.valueOf(attributeValue.apply(startElement, "x")));
            workflowInput.setY(Integer.valueOf(attributeValue.apply(startElement, "y")));
        }
        catch (NumberFormatException e)
        {
            throw xml_exc.setMsg("Attribute x and/or y: not an integer");
        }
        
        
        /* Now get infiles */
        ArrayList<String> infiles = new ArrayList<>();
        collect_infiles:
        while ( eventReader.hasNext() )
        {
            final XMLEvent event;
            try
            {
                event = eventReader.nextEvent();
            }
            catch (Exception e)
            {
                throw (InvalidXMLException)xml_exc.setMsg("Unable to read <infile/> nodes").initCause(e);
            }
            if ( event.isEndElement() )
                if ( INPUT.equals(event.asEndElement().getName().getLocalPart()) )
                    break collect_infiles;

            if ( !event.isStartElement() )
                continue;

            startElement = event.asStartElement();

            
            if ( INFILE.equals(startElement.getName().getLocalPart()) )
                try
                {
                    infiles.add(eventReader.nextEvent().asCharacters().getData());
                }
                catch (XMLStreamException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        // CHECK changes from previous behaviour: calling setContent() triggers propertyChangeListeners
        // -- it was not the case before
        workflowInput.setContent(infiles);
        return workflowInput;
    }
}
