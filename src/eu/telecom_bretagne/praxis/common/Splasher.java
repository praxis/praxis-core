/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.util.logging.Level;


/*
 * @(#)Splasher.java 2.0 January 31, 2004
 * 
 * Written by Werner Randelshofer, Staldenmattweg 2, Immensee, CH-6405, Switzerland. 2003-2005
 * 
 * This software is in the public domain.
 * NB: removed copyright statement since Werner cancels it by specifying this is public domain.
 */

/**
 * The splasher invokes the splash-screen, then praxis' {@link eu.telecom_bretagne.praxis.common.Launcher launcher}.<br>
 * If you wonder why we do not use java 6 integrated splash screen, see <a
 * href="http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6397447">bug ID: 6397447</a>.
 * @author werni
 * @see SplashWindow
 * @see <a href="http://www.javalobby.org/java/forums/t18109.html">Original article by Werner Randelshofer</a>
 */
public class Splasher
{
	/**
	 * Shows the splash screen, launches the praxis' {@link eu.telecom_bretagne.praxis.common.Launcher launcher} and
	 * then disposes the splash screen.
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args)
	{
		try
		{
			SplashWindow.splash("images/splash.jpg");
		}
		catch (Throwable t) // We explicitly and intentionally ignore any error here
		{
			Log.log.info("Could not display splash screen");
			Log.log.log(Level.FINE, "Failed to launch splash screen", t);
		}
		try
		{
			SplashWindow.invokeMain("eu.telecom_bretagne.praxis.common.Launcher", args);
		}
		catch (Throwable t)
		{
			/* in case of error, this should have been handled by the Launcher; simply dispose the splash screen. */
		}
		SplashWindow.disposeSplash();
	}
	
}
