/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.net.ProxySelector;

import eu.telecom_bretagne.praxis.client.ErrorDialog;
import eu.telecom_bretagne.praxis.core.execution.ExecutionLoop;
import eu.telecom_bretagne.praxis.platform.Platform;
import eu.telecom_bretagne.praxis.platform.PlatformToClientBridge;
import eu.telecom_bretagne.praxis.server.Serveur;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class Launcher
{
	/**
	 * This is the "root" delegate: "one delegate to rule them all"!<br />
	 * Praxis projects defining their own delegates should:
	 * <ul>
	 * <li>Subclass this class and override {@link #initialize(boolean, boolean, boolean)};
	 * <li>register the delegates within the <code>initialize()</code> method;
	 * <li>declare the class in the configuration file, using key <code>"application.init"</code>.
	 * </ul>
	 * The launcher then automatically calls <code>initialize()</code>.
	 * @author Sébastien Bigaret
	 */
	public static class InitialisationDelegate
	{   
		/**
		 * Subclasses override this method to register the project's delegate.<br>
		 * This method does nothing.
		 * @param client indicates whether initialization specific to the client-side should be performed
		 * @param server indicates whether initialization specific to the server-side should be performed
		 * @param platform indicates whether initialization specific to the platform-side should be performed
		 */
		public void initialize(boolean client, boolean server, boolean platform)
		{ /* does nothing */ }
	}
	
	/**
	 * Tells whether the launcher has already been called. This is useful in the context of unittests, where the
	 * launcher can be called several times by different test suites.<br>
	 * The method {@link #main(String[])} simply returns after it has been called once.
	 */
	protected static boolean                      alreadyLaunched  = false;
	
	protected static final InitialisationDelegate default_delegate = new InitialisationDelegate();
	
	protected static InitialisationDelegate       delegate         = default_delegate;
	
	protected static void setDelegate(InitialisationDelegate delegate)
	{
		if (delegate==null)
			Launcher.delegate = Launcher.default_delegate;
		else
			Launcher.delegate = delegate;
	}
	
	protected static Class<?> getUI()
	{
		Class<?> UI = null;
		try
		{
			UI = Class.forName("eu.telecom_bretagne.praxis.client.ui.UI");
		}
		catch (Throwable e) { /* ignore */ }
		return UI;
	}
	
	protected static void checkJVM()
	{
		final String vendor = System.getProperty("java.vendor");
		final String version = System.getProperty("java.version");

		final int firstDotPosition = version.indexOf('.');
		final int jvmVersion = Integer.parseInt(version.substring (0, firstDotPosition));

		// we do not want to display anything for mac os x / vendor="Apple Inc.", the jvm shipped with mac os is okay.
		// since java 7, vendor is Oracle Corporation
		if (!"Sun Microsystems Inc.".equals(vendor) && !"Apple Inc.".equals(vendor) && !"Oracle Corporation".equals(vendor))
			ErrorDialog.showWarningMessage("JVM Warning",
			                           "We strongly recommend that you use a Java Virtual Machine provided by Oracle!\n\n"
			                           + "You're currently using the following JVM:\n  - version: "
			                           + version
			                           + "\n  - by: "
			                           + vendor
			                           + "\nYou may encounter errors, slowdowns and unexpected behaviour in general.");

		else if (jvmVersion >= 9)
		{
			ErrorDialog.showErrorMessage("Error",
			                                "Error: this application requires java 8 but java "+jvmVersion+" is used.");
			System.exit(-2);
		}
		else
			Log.log.info(()->"Running with JVM version " + version + " by " + vendor + ", fine");
	}

	public static void fixWmClass()
	{
		// cf. http://elliotth.blogspot.fr/2007/02/fixing-wmclass-for-your-java.html
		//     http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6528430
		//       JDK-6528430 : need system property to override default WM_CLASS
		final String appName = Configuration.get("application.name", null);
		if ( appName == null ) // we see no reason to forbid empty strings or strings made of whitespaces only
			return;
		try
		{
			final java.awt.Toolkit toolkit = java.awt.Toolkit.getDefaultToolkit();
			final Class<?> xtoolkit = toolkit.getClass();

			if ( "sun.awt.X11.XToolkit".equals(xtoolkit.getName()) )
			{
				final java.lang.reflect.Field awtAppClassName = xtoolkit.getDeclaredField("awtAppClassName");
				awtAppClassName.setAccessible(true);
				awtAppClassName.set(null, appName);
			}
		}
		catch (Exception e)
		{
			Log.log.log(java.util.logging.Level.INFO, "Could not set WM_CLASS", e);
		}
	}

	/**
	 * This is the launcher for all praxis projects. It is responsible to launch the elements declared in the
	 * configuration, such as: a regular client, a "bridge" client, the {@link Serveur server} and/or {@link Platform
	 * platforms}.
	 * 
	 * @param args the options supplied when executing the launcher.
	 */
	public static void main(String[] args) throws Exception
	{
		synchronized(Launcher.class)
		{
			if (alreadyLaunched)
				return;
			alreadyLaunched = true;
		}
		if (args==null)
			args=new String[0];
		if (args.length>0 && "--version".equals(args[0]))
		{
			System.out.printf("%s\n", (Object[])ReleaseInfo.getVersionInfo());
			System.exit(0);
		}
		if ((args.length == 1 && !"--version".equals(args[0])) || args.length != 0)
		{
			System.err.println("Usage : Launcher");
			System.err.println("        --version   output version information and exit");
			System.err.println("Exiting");
			System.exit(1);
		}

		checkJVM();
		fixWmClass();
		
		ProxyAutoDetect.initialize();
		
		if (   Configuration.isDefined("client")
			&& Configuration.isDefined("servers")
		    && Configuration.isDefined("platforms"))
		{
			System.setProperty("STANDALONE_EXEC", "YES");
		}

		delegate.initialize(Configuration.isDefined("client"),
		                    Configuration.isDefined("servers"),
		                    Configuration.isDefined("platforms"));

		//if (System.getSecurityManager() == null)
        //    System.setSecurityManager(new SecurityManager());

        if (Configuration.isDefined("servers"))
			Serveur.launch();
		
		if (Configuration.isDefined("platforms"))
			Platform.launch();
		
		if (Configuration.isDefined("bridge"))
		{
			try
			{
				PlatformToClientBridge.launch();
			}
			catch (Throwable t)
			{
				ErrorDialog.showErrorMessage("Connection error",
				                                    "Error: unable to connect to the server\nReason: "+t.toString());
				System.exit(1);
			}
		}
		ExecutionLoop.executionLoopThread.start();

		Class<?> UI = getUI();
		if (Configuration.isDefined("client") && getUI()==null)
			throw new RuntimeException("Launcher is configured to launch the client but eu.telecom_bretagne.praxis.client.ui.UI could not be found");

		if (Configuration.isDefined("client") )
		{
			java.lang.reflect.Method connect = UI.getDeclaredMethod("connectOrQuit");
			connect.invoke(null);
		}
		if (Configuration.isDefined("client")  && !Configuration.getBoolean("client.no_gui"))
		{
			// we let these calls raise in case something goes wrong
			java.lang.reflect.Method launch = UI.getDeclaredMethod("launch");
			launch.invoke(null);
		}
	}
	
}
