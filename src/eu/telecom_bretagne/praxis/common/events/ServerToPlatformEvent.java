/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;


/**
 * The data stored in this object depends on its type:
 * <ul>
 * <li><code>username</code> and <code>workflow</code>  are currently NOT set
 * <li>START_EXECUTION:
 *     <ul>
 *     <li>{@link Event#data}: the execution id (String)
 *     <li>{@link Event#file_conveyor}: a zip with the all the files necessary for the execution
 *     </ul>
 * <li>CANCEL_EXECUTION:
 *     <ul>
 *     <li>{@link Event#data}: the execution id (String)
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>MESSAGE:
 *     <ul>
 *     <li>{@link Event#data}: the transmitted message (String)
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>TERMINATION:
 *     <ul>
 *     <li>{@link Event#data}: why the server requests termination (String)
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * </ul>
 */
public class ServerToPlatformEvent extends Event
{
	static final long serialVersionUID = -8847876489404979851L;
	
	public interface ServerToPlatformEventListener
	extends EventListener
	{
		public void serverRequestsExecution(Event event);
		public void serverCancelsExecution(Event event);
		public void serverRequestsTermination(Event event);
		public void serverSentMessage(Event event);
	}
	
	public static enum Type { START_EXECUTION, CANCEL_EXECUTION, MESSAGE, TERMINATION }
	
	Type type;
	
	public ServerToPlatformEvent(Object source, Type type)
	{
		super(source);
		this.type = type;
	}
	
	@Override
	public void dispatch_to(EventListener listener)
	throws IncompatibleListenerException
	{
		ServerToPlatformEventListener l;
		try {
			l = (ServerToPlatformEventListener) listener;
		}
		catch (ClassCastException e) {
			throw new IncompatibleListenerException(listener, this);
		}
		switch (type)
		{
			case START_EXECUTION:  l.serverRequestsExecution(this); break;
			case CANCEL_EXECUTION: l.serverCancelsExecution(this); break;
			case MESSAGE:          l.serverSentMessage(this); break;
			case TERMINATION:      l.serverRequestsTermination(this); break;
		}
	}
	
	@Override
	public String toString()
	{
		return "<ServerToPlatform type:"+type.toString()+">";
	}
}
