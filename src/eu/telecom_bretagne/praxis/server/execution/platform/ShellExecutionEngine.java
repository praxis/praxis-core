/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server.execution.platform;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.StringWriter;
import java.util.List;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.SystemExecution;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.SimpleFormatterPlatform;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;




/**
 * Handles portions of workflows executed as a shell script.
 * @author Sébastien Bigaret
 */
public class ShellExecutionEngine
    extends SimpleFormatterPlatform
{
	private static final long serialVersionUID = -630535554800111089L;

	/**
	 * In addition to the keys accepted by the superclass, the configuration file for the Shell execution engine
	 * accepts the following key:
	 * <ul>
	 * <li> {@value #SCRIPT_INIT}: if set, the generated scripts will always be prepended by the content of
	 * {@link #getInitializationCode()}</li>
	 * </ul>
	 * @author Sébastien Bigaret
	 */
	public static class ShellExecutionEngineConfiguration
	    extends ExecutionEngineConfiguration
	{
		private static final long serialVersionUID = 2819419131029337500L;
		/** The name of the configuration item for {@link #getInitializationCode()} is {@value}. */
		public static final String SCRIPT_INIT = "script-init";
		
		/**
		 * 
		 * @param platform
		 * @param xml the xml description of the platform.
		 */
		public ShellExecutionEngineConfiguration(PlatformDescription platform)
		{
			super(platform);
		}
		
		public String getInitializationCode()
		{
			return platform.getConfiguration(SCRIPT_INIT, "");
		}
		
		@Override
		public ExecutionEngine getEngine(WorkflowID workflowID, ExecutionID executionID)
		{
			return new ShellExecutionEngine(this, workflowID, executionID);
		}
	}
	
	public static final class ShellExecutionEngineConfigurationFactory
	    implements ExecutionEngine.ExecutionEngineConfigurationFactory
	{
		/** Returns <code>"shell"</code> */
		public String key() { return "shell"; }
		
		/**
		 * Builds and returns a new shell Platform
		 * @exception IllegalArgumentException
		 *                if the supplied document's root element has no attribute <code>os</code> or if its value
		 *                is not a valid argument for {@link ShellExecutionEngine#set_OS(String)}.
		 */
		public ExecutionEngineConfiguration build(PlatformDescription platform)
		{
			return new ShellExecutionEngineConfiguration(platform);
		}
	}
	
	public static enum TARGETS { SHELL_SCRIPT }
	
	/**
	 * 
	 */
	protected SystemExecution process;
	
	/** 
	 * @return {@link #SHELL_SCRIPT}
	 */
	@Override
	public Enum<? extends Enum<?>> defaultTarget()
	{
		return TARGETS.SHELL_SCRIPT;
	}

	@Override
	public Class<? extends Enum<?>> getTargets()
	{
		return ShellExecutionEngine.TARGETS.class;
	}
	
	public String scriptForID(final String id)
	{
		java.util.Map<Enum<?>, List<String>> targets_code = scripts_for_prg.get(id);
		if (targets_code==null)
			return "";
		List<String> target_code = targets_code.get(TARGETS.SHELL_SCRIPT);
		if (target_code==null)
			return "";
		
		StringBuilder sb = new StringBuilder();
		for (String code: target_code)
			sb.append(code);

		return sb.toString();
	}
	
	/**
	 * @param prg
	 * @param aConfiguration
	 */
	@Override
	public void prepareExecution(Activity activity, ExecutionEngineConfiguration aConfiguration)
	throws CannotExecuteException
	{
		super.prepareExecution(activity, aConfiguration);

		for (Program program: activity.getExecutionSet())
		{
			final String prgID = program.getProgramID();
			insertCodeToTarget(prgID, TARGETS.SHELL_SCRIPT,
			                   ((ShellExecutionEngineConfiguration) aConfiguration).getInitializationCode() + "\n");
			/* We currently use tcsh as the shell.
			 * Under tcsh, if a script ends with a line with no LF, this last line is completely ignored, so we make
			 * sure here that this does not happen by wlaways adding the LF character.
			 */
			addCodeToTarget(prgID, TARGETS.SHELL_SCRIPT, "\n");
		}
	}
	
	protected ShellExecutionEngine(ExecutionEngineConfiguration configuration, WorkflowID workflowID, ExecutionID executionID)
	{
		super(configuration, workflowID, executionID);
	}
	
	public ShellExecutionEngineConfiguration configuration()
	{
		return (ShellExecutionEngineConfiguration) configuration;
	}
	
	/**
	 * Executes the shell scripts.
	 * @return The Result object summarizing the execution. The object provides the following
	 *         {@link ProgramResult#get(String) informations}:
	 *         <ul>
	 *         <li>start: start of the execution,</li>
	 *         <li>end: end of the execution,</li>
	 *         <li>stdout: the content of the standard output stream,</li>
	 *         <li>stderr: the content of the standard error stream,</li>
	 *         <li>exit_value: the exit value of the executed shell script,</li>
	 *         <li>script: the shell script that has been executed,</li>
	 *         <li>files_not_found:</li>
	 *         </ul>
	 */
	@Override
	public Result execute(File executionDirectory, File zippedInputFiles) throws InterruptedException,
	        InterruptedIOException
	{
		// this is only used to identify the current exec. within log messages
		String log_exec_id = executionDirectory.toString();
		
		// uncompress the script and the related files into the appropriate dir
		if (zippedInputFiles!=null) // do not remove it, we need to restore it before every execution
			Utile.unzip(zippedInputFiles, executionDirectory, false, false);
		
		List<String> sentFiles = new java.util.ArrayList<String>(java.util.Arrays.asList(executionDirectory.list()));
		// sentFiles.remove("script.txt");
		
		// time to execute the script
		// TODO: treat IOException + InterruptedException individually AND return a useful status to the server!!
		Log.log.info(()->"Executing workflow: " + log_exec_id);
		
		for (String prgID: scripts_for_prg.keySet())
		{
			result.setExecStatus(prgID, ProgramResult.ProgramStatus.RUNNING);
			getProgressMonitor().setRunningExecutionProgress(result);

			Result prgResult = new Result(result.workflowID(), null);

			/* dos2unix = Runtime.getRuntime().exec("dos2unix "+scriptFile); */
			File scriptFile = new File(executionDirectory, "script.txt");
			try
			{
				FileOutputStream out = new FileOutputStream(scriptFile);
				out.write(scriptForID(prgID).getBytes());
				out.write("\n".getBytes()); // tcsh peculiarity: it does not execute the last line, if not terminated by EOL
				out.flush();
				out.close();
			}
			catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
			
			/* move input files */
			File[] createdFiles = prepareInputFiles(prgID, executionDirectory, prgResult);
			// TODO check prg_result.status
			
			/* start process */
			process = new SystemExecution("tcsh " + scriptFile.getName(), executionDirectory);

			StringWriter logs = new StringWriter();
			StringWriter errs = new StringWriter();

			prgResult.setResultForPrg(prgID, "start", ""+new java.util.Date());
			int exitValue = process.execute(logs, 51200, errs, 51200); /* 50 Ko */
			prgResult.setResultForPrg(prgID, "end", ""+new java.util.Date());
			Log.log.fine(()->"shell process finished. Exit value for prg_id: "+prgID+": " + exitValue);
			
			/* Put into result the relevant informations */
			// IllegalStateException will be raised by stdout()/stderr() if the process couldn't be executed
			prgResult.setResultForPrg(prgID, "stdout", logs.toString());
			prgResult.setResultForPrg(prgID, "stderr", errs.toString());
			prgResult.setResultForPrg(prgID, "script", scriptForID(prgID));
			prgResult.setResultForPrg(prgID, "exit_value", ""+exitValue);
			prgResult.setResultForPrg(prgID, "files_not_found", "");

			if (exitValue == 0)
			{
				prgResult.setStatus(Result.Status.OK);
				prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.OK);
			}
			else
			{
				prgResult.setStatus(Result.Status.ERROR);
				prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.ERROR);
				prgResult.setResultForPrg(prgID, "failure_reason",
				                           "Voir les sorties stderr/stdout pour plus de détails sur l'échec\n"+
				                           "Refer to sections stderr & stdout for further details on the failure"); // i18n devrait être fait côté client, cf. note similaire dans Resource
			}
			
			/* time to move the files */
			renameOutputFiles(prgID, executionDirectory, prgResult);
			for (File createdFile: createdFiles)
				Utile.deleteRecursively(createdFile); // may be a directory

			/* move all EXTRA files into the prgID/ directory (i.e. that were not identified as output files) */
			// TODO quick fix here, renameOutputFiles() should probably take care of this, e.g. being cleanup/handleOutputFiles() instead
			for (File f: executionDirectory.listFiles())
				if (f.isFile() && ( ! "script.txt".equals(f.getName()) ) && (!sentFiles.contains(f.getName())))
					Utile.renameFile(f, new File(new File(executionDirectory, prgID), f.getName())); // TODO set WARNING state if it returns false?
			result.mergeWith(prgResult);
			getProgressMonitor().setRunningExecutionProgress(result);
			prgResult.dumpContent(executionDirectory);

			if (zippedInputFiles!=null) // Restore the input files before the next program begins
				Utile.unzip(zippedInputFiles, executionDirectory, false, false);
		}
		
		
		
		Log.log.info(()->"End of execution: " + log_exec_id + "status: " + result.getStatus());
		if (zippedInputFiles!=null)
			zippedInputFiles.delete();
		// TODO if false, this means that the execution has been stopped
		// if(platform.executeWorkflow(executionID))
		{
			File zippedResults = new File(executionDirectory, "resultat.zip");
			Log.log.info(()->"Sending results to the server: " + zippedResults);
			
			// add to 'sentFiles' any file you do NOT want to send back
			sentFiles.add("script.txt");
			Utile.zipDirectory(executionDirectory, zippedResults, sentFiles);
			result.setZipFile(zippedResults);
			return result;
		}
		// else return null;
	}
	
	/**
	 * Waits until the current process is cancelled
	 */
	@Override
	public void cancel()
	{
		while (process == null)
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{ /* ignore */}
		process.cancel();
	}
}
