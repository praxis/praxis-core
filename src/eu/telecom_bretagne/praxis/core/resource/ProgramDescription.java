/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.resource;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.ParameterType;


/**
 * @author Sébastien Bigaret
 *
 */
public class ProgramDescription implements Serializable {
	
	private static final long serialVersionUID = 5109602824916560908L;

	public static final String ID_SEPARATOR= "___";
    
	/** Indicates whether the program is active, i.e. if it can be used in the design of a workflow */
	protected boolean active = true;

    /** provider of the program.
     */
    protected String prg_provider;
    
    /** name of the program.
     */
    protected String prg_name;
    
    /** version of the program.
     */
    protected String prg_version;
    
    /** Short description */
    protected String short_description;
    
    /** Description of the program */
    protected String description;

    /** A {@link LinkedHashMap} guarantees that the iteration will always
     * respect the order in which params has been put in the map. This is
     * important for {@link #getParametersSortedForDisplay()} which relies
     * on this property.
     */
    protected LinkedHashMap <String,ParameterDescription> parameters;
    
    /**
     * 
     * @param active
     * @param prg_provider
     * @param prg_name
     * @param prg_version
     * @param short_description
     * @param description
     * @param parameters
     */
    /* this was used by the JMX framework
    @ConstructorProperties({"prgProvider", "prgName", "prgVersion", "short_description", "description", "parameters"}) 
	*/
    public ProgramDescription(boolean active, String prg_provider, String prg_name, String prg_version,
                              String short_description, String description, ParameterDescription[] parameters)
    {
    	if (prg_provider.contains(ID_SEPARATOR) || prg_name.contains(ID_SEPARATOR) || prg_version.contains(ID_SEPARATOR))
    		throw new IllegalArgumentException("Parameters prg_provider, prg_name and prg_version should not contain the char.sequence:'"+ID_SEPARATOR+"'");
    	this.active = active;
    	this.prg_provider = prg_provider;
		this.prg_name = prg_name;
		this.prg_version = prg_version;
    	this.short_description = short_description;
    	this.description = description;

    	this.parameters = new LinkedHashMap <String, ParameterDescription>();
        for (ParameterDescription param: parameters)
        {
        	/* if ( this.parameters.containsKey(param.id) ) // TODO */
            this.parameters.put(param.id, param);
            param.setParent(this);
        }
    }

	/**
	 * Calls {@link #ProgramDescription(boolean, String, String, String, String, String, ParameterDescription[])} with
	 * the same parameters, plus parameter {@code active} set to {@code true}.
	 * @param prg_provider
	 * @param prg_name
	 * @param prg_version
	 * @param short_description
	 * @param description
	 * @param parameters
	 */
    public ProgramDescription(String prg_provider, String prg_name, String prg_version,
                              String short_description, String description, ParameterDescription[] parameters)
    {
    	this(true, prg_provider, prg_name, prg_version, short_description, description, parameters);
    }
    
    /**
     * 
     * @param xml
     * @throws InvalidXMLException 
     */
    public ProgramDescription(Element xml) throws InvalidXMLException
    {
        fromXML(xml);
    }

    /** Builds an <b>uninitialized</b> ProgramDescription
    * For internal use only (incl.unittests)
    */
    ProgramDescription() { /* left intentionally blank, callers are warned */ }

    /**
     * Returns the list of parameters that are
     * {@link ParameterType#isInput() inputs} of the program.
     * @return input parameters
     */
    public List <ParameterDescription> inputParameters() {
        List <ParameterDescription> inputs = new ArrayList<ParameterDescription>();
        for (ParameterDescription param: parameters.values())
            if ( param.getType().isInput() )
                inputs.add(param);
        return inputs;
    }

    /**
     * Returns the list of parameters that are
     * {@link ParameterType#isOutput() outputs} of the program.
     * @return output parameters
     */
    public List <ParameterDescription> outputParameters() {
        List <ParameterDescription> outputs = new ArrayList<ParameterDescription>();
        for (ParameterDescription param: parameters.values())
            if ( param.getType().isOutput() )
                outputs.add(param);
        return outputs;
    }

    /** Returns all the receivers' parameters. The returned array presents
     * them in the order that should be used when displaying them in an
     * interface.
     */
    /* Same order as in the xml in particular */
    public ParameterDescription[] getParameters() {
        return parameters.values().toArray(new ParameterDescription[parameters.size()]);
    }

    /** Returns the arrays of parameters, sorted by their position
     * @return Returns the parameters.
     */
    public ParameterDescription[] getParametersSortedByPosition() {
        ParameterDescription [] params = parameters.values().toArray(new ParameterDescription[parameters.values().size()]);
        
        Arrays.sort(params, new Comparator<ParameterDescription>(){
            public int compare(ParameterDescription a, ParameterDescription b)
            { return ((Integer)a.position).compareTo(b.position); }
        });
        
        return params;
    }

    /** Returns the size of the set of the program's parameters */
    public int getParametersSize() { return parameters.size(); }
    



    /**
     * Reads the provided xml element and builds the corresponding
     * description. During this operation, the receiver is completely
     * reinitialized.
     * @param xml the XML element containing the object's properties
     * @throws InvalidXMLException TODO pointer to DTD
     */
    public void fromXML(Element xml) throws InvalidXMLException
    {
        List <?> params;
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.PROGRAM);
        
        if ( !xml.getName().equalsIgnoreCase("program_description") )
            throw xml_exc.setMsg("root element is not <program_description/>");
        
        final String _active = xml.getAttributeValue("active");
        if (_active != null)
            active = Facade_xml.boolean_for(xml.getAttributeValue("active"));
        else
            active = true; // defaults to true

        Element prg_info = xml.getChild("program");
        if (prg_info==null)
        	throw xml_exc.setMsg("Prg. description has no <program/>");
        
        prg_provider = prg_info.getAttributeValue("provider");
        prg_name = prg_info.getAttributeValue("name");
        prg_version = prg_info.getAttributeValue("version");
        
        short_description = xml.getChildText("short_description");
        description = xml.getChildText("description");
        
        try {
            final XPath xpath = XPath.newInstance("/program_description/parameters/parameter");
            params = xpath.selectNodes(xml);
        }
        catch (JDOMException e) {
            String msg = "Unable to select <parameter/> nodes";
            if ( e.getMessage() != null )
                msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        parameters = new LinkedHashMap <String, ParameterDescription>();
        for (Object xparam: params)
        {
            ParameterDescription param = new ParameterDescription(this, (Element)xparam);
            if ( parameters.containsKey(param.id) )
                throw xml_exc.setMsg("Duplicate parameter id:"+param.id);
            parameters.put(param.id, param);
        }
    }

    public Element toXML()
    {
        return null;
    }
    //TODO public List <ParameterDescription> getInputs();
    
    //TODO public List <ProgramDescription> getOutputs();

    /** Returns the corresponding {@link ParameterDescription}
     * @return the description attached to the supplied id, or <tt>null</tt>
     * if no such object exists within the <tt>ProgramDescription</tt>.
     */
    public ParameterDescription getParameterWithId(String id) {
        return parameters.get(id);
    }

    
    /* Getters and setters */
	/**
	 * Tells whether the description is active or not
	 * @return {@code true} if the description can be used in workflows that are been designed, {@code false} if they
	 *         are for the exclusive use of programs in already executed workflows --in other words, in results.
	 */
    public boolean isActive()
    {
    	return active;
    }
	
	/**
	 * Sets the active flag. See {@link #isActive()} for details.
	 * @param active the active flag
	 */
	public void setActive(boolean active)
	{
		this.active = active;
	}

	/**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the id.
     */
    public String id() {
        return prg_provider+ID_SEPARATOR+prg_name+ID_SEPARATOR+prg_version;
    }

    /**
	 * Returns the full name of the program (program's provider, name and version concatenated, separated by
	 * whitespaces)
	 * @return Returns the full name.
	 */
	public String fullName()
	{
		return prg_provider + " " + prg_name + " " + prg_version;
	}
	
    /**
	 * Returns the name of the program
	 * @return Returns the name.
	 */
    public String getPrgProvider() {
        return prg_provider;
    }

    /**
	 * Returns the name of the program
	 * @return Returns the name.
	 */
    public String getPrgName() {
        return prg_name;
    }

    /**
	 * Returns the version of the program
	 * @return Returns the version of the program.
	 */
    public String getPrgVersion() {
        return prg_version;
    }

    /**
     * Returns a read-only view of the internal parameters map.
     * @return a read-only view of the internal parameters map.
     */
    Map <String, ParameterDescription> getParametersMap() {
        return Collections.unmodifiableMap(parameters);
    }

    /**
     * Resets the internal parameters' map with a copy of the supplied map.
     * @param parameters The parameters to set.
     */
    void setParameters(LinkedHashMap<String, ParameterDescription> parameters) {
        this.parameters = new LinkedHashMap<>(parameters);
    }

    /** Returns the short description of the program
     * @return Returns the short_description.
     * @see #getName()
     */
    public String getShort_description() {
        return short_description;
    }

    /**
     * @param short_description The short_description to set.
     */
    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    /* Serializable interface */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
    {
    	in.defaultReadObject();
        for (ParameterDescription param: parameters.values())
        {
        	//this.parameters.put(param.id, param);
            param.setParent(this);
        }
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException
    {
    	out.defaultWriteObject();
    }

    @Override
    public String toString()
    {
    	return id();
    }
}
