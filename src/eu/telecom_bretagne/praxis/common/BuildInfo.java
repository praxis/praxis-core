/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

/**
 * BuildInfo contains the date for the last framework's build, along with the corresponding commit identifier.
 */
public interface BuildInfo
{
	// these informations are in the manifest, but we want to have them even when not packaged in a jar */

	/** Unique identifier for the commit being built */
	final String COMMIT	= "Commit description";

	/** Date  of the last build */
	final String DATE	= "yyyy-mm-ddTHH:MM:SS+ZZ:ZZ";

	static public String[] getVersionInfo()
	{
		return new String[]{ COMMIT, DATE };
	}
}
