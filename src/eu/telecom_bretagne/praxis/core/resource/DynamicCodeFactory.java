/* (c) Sébastien Bigaret */
package eu.telecom_bretagne.praxis.core.resource;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

import eu.telecom_bretagne.praxis.common.Log;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtMethod;
import javassist.LoaderClassPath;
import javassist.Modifier;
import javassist.NotFoundException;

public class DynamicCodeFactory
{
	
	/**
	 * A sequence number used in handle_dependencies to avoid name collisions, see comments in code for details
	 */
	private static long sequence = 0;
	
	private DynamicCodeFactory()
	{ /* avoids the creation of the default public constructor */}
	
	/**
	 * Transforms a name into a valid java identifier. The method examines each character in the name and determines
	 * whether its a valid character for a java identifier (using {@link Character#isJavaIdentifierStart(char)} for
	 * the 1st character and {@link Character#isJavaIdentifierPart(char) for the others}: when it's not it replaces
	 * the character with an underscore.
	 * @param name
	 *            the name to transform
	 * @return the valid java identifier computed from the supplied name.
	 */
	public static String javaIdentifierFrom(String name)
	{
		if (name == null || name.length() == 0)
			throw new IllegalArgumentException("A null or empty string cannot be turned into a valid java identifier");
		final StringBuilder javaIdentifier = new StringBuilder();
		char c;
		
		c = name.charAt(0);
		javaIdentifier.append(Character.isJavaIdentifierStart(c) ? c : '_');
		for (int i = 1; i < name.length(); i++)
		{
			c = name.charAt(i);
			javaIdentifier.append(Character.isJavaIdentifierPart(c) ? c : '_');
		}
		return javaIdentifier.toString();
	}
	
	/**
	 * @param prg
	 */
	public static void handle_dependencies(ProgramDescription prg)
	{
		ClassPool pool = new ClassPool();
		/*
		 * The following line makes sure we have the necessary class loaders registered in the class pool; it is not
		 * needed in a standard java application (with one class loader in the whole app.), but it definitely is when
		 * different class loaders co-exists in an application --this is the case when using one-jar, in an eclipse
		 * plugin etc.
		 */
		pool.insertClassPath(new LoaderClassPath(ClassLoader.getSystemClassLoader()));
		pool.insertClassPath(new LoaderClassPath(ParameterDescription.class.getClassLoader()));
		CtClass ctroot = null, ctchild;
		Class< ? > child = null;
		try
		{
			ctroot = pool.get("eu.telecom_bretagne.praxis.core.resource.ParameterDescription");
		}
		catch (NotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * we need a LinkedHashMap here, so that we keep the very same order when iterating on parameters, see comment
		 * in {@link ParameterDescription#parameters}.
		 */
		LinkedHashMap<String, ParameterDescription> new_map = new LinkedHashMap<>();
		
		for (Map.Entry<String, ParameterDescription> entry: prg.getParametersMap().entrySet())
		{
			String key = entry.getKey();
			ParameterDescription param = entry.getValue();
			ParameterDescription new_param;
			
			/*
			 * Prepare a valid java identifier; we also add a sequence number to avoid name collision: for example, if
			 * a program has two parameters "+paramName" and "-paramName", without a sequence number we would get the
			 * same name after transformed into a valid java identifier
			 */
			sequence += 1;
			String prg_id_escaped = javaIdentifierFrom(prg.id());
			String param_id_escaped = javaIdentifierFrom(param.id);
			String specialized_class_name = "praxis.DynamicallyCreatedParameterDescription_" + prg_id_escaped + "_"
			                                + param_id_escaped + "_" + sequence;
			ctchild = pool.makeClass(specialized_class_name, ctroot);
			
			try
			{
				// ctconstructor = new CtConstructor(new CtClass[]{ctroot}, ctchild);
				// ctconstructor.setBody("{super($1);}");
				CtConstructor ctconstructor = new CtConstructor(new CtClass[] {}, ctchild);
				ctconstructor.setBody("{}");
				ctconstructor.setModifiers(Modifier.setPublic(ctconstructor.getModifiers()));
				ctchild.addConstructor(ctconstructor);
				
				/*
				 * Construction de la méthode: public Boolean isActivated(Parameter param)
				 */
				CtMethod isActivated = null;
				try
				{
					isActivated = computeMethod_isActivated(pool, ctchild, param);
				}
				catch (CannotCompileException | NotFoundException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ctchild.addMethod(isActivated);
				
				/*
				 * Construction de la méthode: protected Boolean _isaValidValue(String value)
				 */
				CtMethod _isavalidValue = null;
				try
				{
					_isavalidValue = new CtMethod(pool.get("java.lang.Boolean"), "_isaValidValue",
					                              new CtClass[] { pool.get("java.lang.String") }, ctchild);
				}
				catch (NotFoundException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try
				{
					/* $0==this, $1 is the 1st arg, etc. */
					_isavalidValue.setBody("return Boolean.valueOf("
					                       + param.getValueConstraintAsBooleanExpression("$1") + ");");
				}
				catch (CannotCompileException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				_isavalidValue.setModifiers(Modifier.setProtected(_isavalidValue.getModifiers()));
				ctchild.addMethod(_isavalidValue);
				
			}
			catch (CannotCompileException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try
			{
				// Converts this class using the appropriate class loader: the class inherits from ParameterDescription
				child = ctchild.toClass(ParameterDescription.class.getClassLoader(), null);
			}
			catch (CannotCompileException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			new_param = null;
			
			try
			{
			    new_param = (ParameterDescription) (child.getConstructor(new Class[] {}).newInstance());
			}
			catch (InstantiationException | IllegalAccessException | IllegalArgumentException
			       | InvocationTargetException | NoSuchMethodException | SecurityException e)
			{
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
			// child.makeClassInitializer();
			// System.out.println(new_param+" -> "+new_param.isActivated());
			if (new_param == null)
			{
				Log.log.severe("ERROR for prg.param: " + prg.id() + "(" + prg.fullName() + ")." + param.getId());
				System.exit(0);
			}
			new_param.shallowCopy(prg, param);
			new_map.put(key, new_param);
			
			/* Following advices in doc. for ClassPool: let's call detach() "to avoid huge memory consumption". */
			ctchild.detach();
			
		}
		prg.setParameters(new_map);
	}

	/**
	 * Computes the method <code>isActivated</code> corresponding to the supplied parameter description.
	 * @param classPool
	 *            the global ClassPool
	 * @param ctClass
	 *            the class in which the method should be added
	 * @param param
	 *            the {@link ParameterDescription} from which the method is computed
	 * @return the corresponding CtMethod
	 * @throws CannotCompileException
	 *             if the expression found in ParameterDescription is not a valid java expression.
	 */
	public static CtMethod computeMethod_isActivated(ClassPool classPool, CtClass ctClass, ParameterDescription param)
	throws CannotCompileException, NotFoundException
	{
		CtMethod isActivated = null;
		isActivated = new CtMethod(classPool.get("java.lang.Boolean"), "isActivated",
		                           new CtClass[] { classPool.get("eu.telecom_bretagne.praxis.core.workflow.Parameter") },
		                           ctClass);
		/* $0==this, $1 is the 1st arg, etc. */
		isActivated.setBody("return Boolean.valueOf(" + param.getDependenceAsBooleanExpression("$1") + ");");
		isActivated.setModifiers(Modifier.setPublic(isActivated.getModifiers()));
		return isActivated;
	}
}
