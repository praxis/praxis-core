/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.DatatypeConverter;

/**
 * Utility methods for praxis.
 * @author Sébastien Bigaret
 */
public class Utile
{
	/**
	 * Une classe très simple, contenant un seul objet. À l'origine, on voulait pouvoir locker (synchronize()) sur une
	 * variable de type <i>immutable</i> (comme une String ou un booléen), et se servir de ce lock dans un pattern
	 * wait()/notify(), avec le notify() indiquant une modification de cette même variable! Bien entendu, ce n'est pas
	 * possible avec cette variable puisque l'affectation d'une variable de type immutable conduit à un nouvel objet.
	 * D'où cet objet.
	 * @author Sébastien Bigaret
	 */
	public static class SimpleContainer
	{
		public Object data;
	}
	
	public static enum ResourceAvailability
	{
		AVAILABLE, NOT_AVAILABLE
	}
	
	/**
	 * The buffer size for byte[] buffers used in I/O
	 */
	private static final int   IO_BUFFER_SIZE              = 1024;

	/**
	 * Used by {@link #searchWindowsAppPathsInRegistry(String)}, contains the result of a 'reg query' for the standard
	 * "App Paths".
	 */
	private static String appPaths32 = null;
	
	/**
	 * Used by {@link #searchWindowsAppPathsInRegistry(String)}, contains the result of a 'reg query' for the
	 * "App Paths" in "Wow6432Node" on 64-bits systems.
	 */
    private static String appPaths6432Node = null;

	/**
	 * Renames a file. In some cases, the builtin 'renameTo()' fails, causing strange bugs. This method catches this
	 * sort of events and it copies the original file then deletes it in such cases.
	 * @param source
	 *            the file to rename
	 * @param destination
	 *            the new path for the source file; note that
	 *            <ul>
	 *            <li>if it exists before the method is called, it is overwritten,</li>
	 *            <li>if the destination's parent directory is not {@code null} and does not exist, it is created.</li>
	 *            </ul>
	 * @return {@code true} if the file was successfully renamed, {@code false} otherwise.
	 */
	public static boolean renameFile(File source, File destination)
	{
		if (source.equals(destination))
			return true;
		
		if (source.renameTo(destination))
			return true;
		
		Log.log.finest("java.io.File#renameTo() failed: copying the file instead");
		
		/* renamed failed: copy the file then remove it */
		if (destination.getParent()!=null)
			if (!(destination.getParentFile().exists() || destination.getParentFile().mkdirs()))
				return false;
		try (FileInputStream in = new FileInputStream(source);
		     FileOutputStream out = new FileOutputStream(destination))
		{
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) >= 0)
			{
				out.write(buffer, 0, bytesRead);
			}
			out.flush();
		}
		catch (IOException e)
		{
			Log.log.log(Level.SEVERE, "Failed to copy the file", e);
			return false;
		}
		source.delete();
		return true;
	}
	
    /**
     * Make a byte-to-byte copy of a file to another.
     * 
     * @param source
     *        the file to cpy
     * @param destination
     *        the cloned file; note that if it exists before the method is called, it is overwritten.
     * @throws IOException
     */
    public static void copyFile(File source, File destination) throws IOException
    {
        if ( source.equals(destination) )
            return;

        try (FileInputStream in = new FileInputStream(source);
             FileOutputStream out = new FileOutputStream(destination))
        {
            copyStream(in, out);
        }
    }

    public static void copyStream(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ( ( bytesRead = in.read(buffer) ) != -1 )
            out.write(buffer, 0, bytesRead);
    }

	/**
	 * Make a deep (recursive) copy of a directory to another. If srcDir is not a directory, this is equivalent to
	 * <code>copyFile(srcDir, dstDir)</code>.
	 * @param srcDir
	 *            the directory to copy
	 * @param dstDir
	 *            the copied directory.<br>
	 *            If the destination directory does not exists, the method creates it.<br>
	 *            Note that any files copied from source to destination that already exists in the destination are
	 *            overwritten during the process.
	 * @throws IOException
	 *             if an error occurs while copying the files. it can happen at different time: when creating the
	 *             destination directory, or when copying the files. When the exception is raised, the destination
	 *             directory is left as-is (some files may have been copied into it).
	 */
	public static void deepCopyFile(File srcDir, File dstDir) throws IOException
	{
		if (!srcDir.exists())
		{
			return;
		}
		
		if (!srcDir.isDirectory())
		{
			copyFile(srcDir, dstDir);
			return;
		}
		
		if (!dstDir.exists() && !dstDir.mkdir())
				throw new IOException("Unable to create the destination directory: "+dstDir);
		
		if (!dstDir.exists() || !dstDir.isDirectory())
			throw new IOException("Cannot copy a directory into a file");
		final File[] srcFiles = srcDir.listFiles();
		if ( srcFiles == null )
		    return;
		
      for ( File srcFile: srcFiles )
      {
			final File destination = new File(dstDir, srcFile.getName());
			
			if (srcFile.isDirectory())
			{
				destination.mkdirs();
				deepCopyFile(srcFile, destination);
			}
			else
			{
				copyFile(srcFile, destination);
			}
		}
	}
	
	/**
	 * Deletes the specified file. If it is a directory, its content is recursively deleted
	 * @param file
	 *            the file to be deleted. If null, the return value is true
	 * @return true if the file was successfully deleted, false otherwise. If file is a directory, a false value may
	 *         indicate that one of its files could not be deleted; in any cases, when the operation fails, it may
	 *         have succeeded in deleting some of the necessary child files.
	 */
	public static boolean deleteRecursively(File file)
	{
		Log.log.finest(()->"Deleting: "+file);
		if (file==null)
			return true;
		File[] filesInDir = file.listFiles();
		boolean ret = true;
		if (filesInDir != null)
		{
			for (int i = 0; i < filesInDir.length; i++)
			{
				ret &= deleteRecursively(filesInDir[i]);
			}
		}
		ret &= file.delete();
		return ret;
	}
	
	/**
	 * 
	 * @param <T>
	 * @param objects
	 * @param delimiter
	 * @return
	 */
	// http://snippets.dzone.com/posts/show/91
	public static <T> String join(final Iterable<T> objects, final String delimiter) {
	    Iterator<T> iter = objects.iterator();
	    if (!iter.hasNext())
	        return "";
	    StringBuilder buffer = new StringBuilder(String.valueOf(iter.next()));
	    while (iter.hasNext())
	        buffer.append(delimiter).append(String.valueOf(iter.next()));
	    return buffer.toString();
	}
	
	/**
	 * Zip the specified directory into a given file, ignoring the files listed in excludedFiles. Note: files whose
	 * names begins with '.nfs' are ignored
	 * @param directory
	 *            the place where the files that should be zipped live
	 * @param zipFile
	 *            the zip-file to build
	 * @param excludedFiles
	 *            a list of strings representing the paths of files to ignore. It may be {@code null}.
	 * @return true if the zip file was correctly created, false otherwise
	 */
	public static boolean zipDirectory(File directory, File zipFile, List<String> excludedFiles)
	{
		// CHECK: directory should be a directory
		int nbEntries = 0;
		try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile)))
		{
			File[] files = directory.listFiles();
			for (int i = 0; i < files.length; i++)
			{
				if ( !(    (excludedFiles != null && excludedFiles.contains(files[i].getName()))
						|| files[i].equals(zipFile) || files[i].getName().startsWith(".nfs")) ) // CHECK
				{
					zip_addEntry(out, files[i], files[i].getName(), null);
				}
			}
			out.flush();
		}
		catch (ZipException ze)
		{
			if (nbEntries!=0)
				ze.printStackTrace();
			return false;
		}
		catch (IOException fne)
		{
			fne.printStackTrace(); // FIXME at least FileNotFound+IOException
			return false;
		}
		return true;
	}
	
	/**
	 * Builds a zip archive containing a list of files
	 * @param files
	 *            list of files to include in the archive
	 * @param zipFile
	 *            the archive itself
	 */
	public static void zipFiles(Map<String, File> files, File zipFile) throws IOException
	{
		try ( ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile)) )
		{
			for (Map.Entry<String, File> entry: files.entrySet())
			{
				String filename = entry.getKey();
				File file = entry.getValue();

				zip_addEntry(out, file, filename, null);
			}
			out.close();
		}
		catch (IOException ioe)
		{
			Log.log.log(Level.WARNING, "Error while zipping files", ioe);
			throw ioe;
		}
	}
	
	/**
	 * Adds a regular file to the zip output stream.
	 * @param zout the zip out stream to which the entry should be added
	 * @param sourceFile path to the regular file to add
	 * @param destinationFileName the name under which the entry is added
	 * @param comment the comment to attach to the zip entry (see {@link ZipEntry#setComment(String)).
	 *                If {@code null}, no comment is added.
	 * @throws IOException if for whatever reason the entry could not be added
	 */
	protected static void addRegularFile(ZipOutputStream zout, File sourceFile, String destinationFileName, String comment)
	        throws IOException
	{
		final byte[] buffer = new byte[IO_BUFFER_SIZE];

		ZipEntry entry = new ZipEntry(destinationFileName);
		if (comment!=null)
			entry.setComment(comment);
		zout.putNextEntry(entry);
		try (FileInputStream in = new FileInputStream(sourceFile))
		{
			for (int b = in.read(buffer); b >= 0; b = in.read(buffer))
			{
				zout.write(buffer, 0, b);
			}
		}
		zout.closeEntry();
		zout.flush();
	}

    /**
     * Adds an entry to the zip output stream. The method properly handles files and directories; if sourceFile is a
     * directory, its content is recursively added to the zip output stream.
     * 
     * @param zout
     *        the zip out stream to which the entry should be added
     * @param sourceFile
     *        path to the entry to add, either a regular file or a directory
     * @param destinationFileName
     *        the name under which the entry is added. Paths should be supplied using the platform-specific convention
     *        (using {@link File#separatorChar} as the name-separator character).
     * @param comment
     *        the comment to attached to the zip entry (see {@link ZipEntry#setComment(String)). If {@code null}, no
     *        comment is added. Note that a comment is added if and only the entry represents a regular file, not a
     *        directory.
     * @throws IOException
     *         if for whatever reason the entry could not be added
     */
    public static void zip_addEntry(final ZipOutputStream zout, final File source,
                                    final String destinationDirectoryName, final String comment)
    throws IOException
    {
        Log.log.finest(() -> "adding: "+source.getPath()+" in "+destinationDirectoryName);
        // Quoting http://www.pkware.com/documents/casestudies/APPNOTE.TXT
        // "In a zip, all slashes should be forward slashes '/'"
        final String zipDestinationDirectoryName = destinationDirectoryName.replace(File.separatorChar, '/');

        if (!source.isDirectory())
        {
            addRegularFile(zout, source, zipDestinationDirectoryName, comment);
            return;
        }
        File[] files = source.listFiles();
        if ( files == null )
            return;
        for (int i=0; i<files.length; i++)
        {
            if (files[i].isDirectory())
                // forward slash only, NOT File.separator: see comment above
                zip_addEntry(zout, files[i], zipDestinationDirectoryName + "/" + files[i].getName(), null);
            else
                addRegularFile(zout, files[i], zipDestinationDirectoryName + "/" + files[i].getName(), null);
        }
    }

	/**
	 * Uncompresses a zip-file into a directory
	 * @param zipFile
	 *            a zip file
	 * @param directory
	 *            where the file(s) enclosed in zipFile should be dropped
	 * @param deleteOnExit
	 *            if true, the unzipped files are registered for deletion when the virtual machines exits. Note that
	 *            the directory itself is not registered for deletion
	 * @param deleteZipFile if {@code true}, the zip files is deleted before the method returns
	 * @return the number of entries that were uncompressed
	 */
	public static int unzip(File zipFile, File directory, boolean deleteOnExit, boolean deleteZipFile)
	{
		if (zipFile==null)
			return 0;
		int nb_entries = 0;
		try (ZipInputStream in = new ZipInputStream(new FileInputStream(zipFile)))
		{
			ZipEntry entry = in.getNextEntry();
			long t0 = GregorianCalendar.getInstance().getTimeInMillis();
			Log.log.fine(()->"Uncompressing: " + zipFile);
			
			final byte[] buffer = new byte[IO_BUFFER_SIZE];
			while (entry != null)
			{
				nb_entries += 1;
				Log.log.fine("  inflating: " + entry.getName());
				File tmpFile = new File(directory, entry.getName());
				if (tmpFile.isDirectory())
				{
					tmpFile.mkdir();
					continue;
				}
				File parentDir = tmpFile.getParentFile(); 
				if (parentDir!=null && !parentDir.exists())
					parentDir.mkdirs();

				try ( FileOutputStream out = new FileOutputStream(tmpFile) )
				{
					
					for (int b = in.read(buffer); b >= 0; b = in.read(buffer))
					{
						out.write(buffer, 0, b);
					}
					out.flush();
				}
				if (deleteOnExit)
					tmpFile.deleteOnExit();
				entry = in.getNextEntry();
			}
			long t1 = GregorianCalendar.getInstance().getTimeInMillis();
			Log.log.fine(()->(t1 - t0) + "ms. needed to uncompress " + zipFile);
		}
		catch (IOException ioe)
		{
			Log.log.severe("Error while decompressing: " + zipFile);
			ioe.printStackTrace();
		}
		if (deleteZipFile)
			zipFile.delete();
		return nb_entries;
	}
	
	/**
	 * Uncompresses a zip-file into a directory. Equivalent to
	 * {@link #unzip(File, File, boolean, boolean) unzip(zipFile, directory, deleteOnExit, false)}.
	 * @param zipFile
	 *            a zip file
	 * @param directory
	 *            where the file(s) enclosed in zipFile should be dropped
	 * @param deleteOnExit if true, the unzipped files are registered for deletion when the virtual machines exits.
	 * @return the number of entries that were uncompressed
	 */
	public static int unzip(File zipFile, File directory, boolean deleteOnExit)
	{
		return unzip(zipFile, directory, deleteOnExit, true);
	}

	/**
	 * Uncompresses a zip-file into a directory. Equivalent to {@link #unzip(File, File, boolean) unzip(zipFile,
	 * directory, false)}.
	 * @param zipFile
	 *            a zip file
	 * @param directory
	 *            where the file(s) enclosed in zipFile should be dropped
	 * @return the number of entries that were uncompressed
	 */
	public static int unzip(File zipFile, File directory)
	{
		return unzip(zipFile, directory, false);
	}
	
	/**
	 * Converts a exception stacktrace to a String
	 * @param th
	 * @return a multi-line string with the exception and its stack trace
	 */
	public static String exceptionToString(Throwable th)
	{
		String stTrace = th.toString() + "\n";
		StackTraceElement[] stackElements = th.getStackTrace();
		for (int i = 0; i < stackElements.length; i++)
		{
			stTrace = stTrace + "    at: " + stackElements[i].toString() + "\n";
		}
		return stTrace;
		
		/**
		 * cf. http://www.devx.com/tips/Tip/14848 StringWriter sout = new StringWriter(); PrintWriter out = new
		 * PrintWriter(sout); th.printStackTrace(out); out.close(); sout.close(); stTrace = sout.toString();
		 */
	}
	
	/**
	 * Turns every byte in the array into its 2 characters hexadecimal representation, and returns the corresponding
	 * string.
	 * @param bytes
	 *            the array to be converted.
	 * @return the returned length is twice as long as the supplied array
	 */
	public static String byteArrayToHex(byte[] bytes)
	{
		return DatatypeConverter.printHexBinary(bytes);
	}
	
	/**
	 * Converts a string made of 2-characters hexadecimal byte into the corresponding array of bytes.
	 * @param hex
	 *            the string representing the array
	 * @return array of bytes; its length is half the length of the string
	 * @throws NumberFormatException
	 *             when the format is incorrect
	 */
	public static byte[] hexToByteArray(String hex) throws NumberFormatException
	{
		return DatatypeConverter.parseHexBinary(hex);
	}

	/**
	 * Serialize an object using the standard java serialization mechanism, and return the resulting byte array.
	 * This is a helper method, simply wrapping a ByteArrayOutputStream in an ObjectOutputStream and calling
	 * writeObject() before returning the written bytes.
	 * @param object
	 *            the object to serialize
	 * @return the serialized object, as a byte array.
	 * @throws IOException
	 */
	public static byte[] serialize(Object object) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = null;
		oos = new ObjectOutputStream(baos);
		oos.writeObject(object);
		oos.flush();
		return baos.toByteArray();
	}

	/**
	 * Builds a string representation of this object, by serializing it and transforming the stream into an
	 * hexadecimal representation of it.
	 * @param object
	 *            the object to serialize
	 * @return the object transformed to a String
	 * @see #serialize(Object)
	 * @see #deserializeObject(String)
	 */
	public static String serializeObject(Object object) throws IOException
	{
		return Utile.byteArrayToHex(serialize(object));
	}
	
	/**
	 * Takes an "hexadecimal representation" of the serialized object (see {@link #serializeObject(Object)}, and
	 * rebuild it.
	 * @param hexString
	 *            the hexadecimal representation of the serailized object, as returned by
	 *            {@link #serializeObject(Object)}
	 * @return the object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @see #serializeObject(Object)
	 */
	public static Object deserializeObject(String hexString) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream bais = new ByteArrayInputStream(Utile.hexToByteArray(hexString));
		ObjectInputStream ois;
		ois = new ObjectInputStream(bais);
		return ois.readObject();
	}
	
	/**
	 * Extracts from the filename the name without the extension, and the extension itself. The extension is the part
	 * of the filename that appears after the last "." (point) character.
	 * @param filename
	 * @return a String[2], containing the filename without the extension and the extension
	 */
	public static String[] nameAndExtensionForFilename(String filename)
	{
		int dot = filename.lastIndexOf('.');
		String file_wo_ext = filename;
		String ext = "";
		if (dot != -1)
		{
			file_wo_ext = filename.substring(0, dot);
			ext = filename.substring(dot);
		}
		return new String[] { file_wo_ext, ext };
	}
	
	/**
	 * Given a file, returns the first file of the form "filename-n.ext" which does not exists, where n is an integer
	 * and 'ext' is the file extension. Please note that the file is NOT created: callers have the responsability of
	 * creating the corresponding file or directory.<br>
	 * For example, with a file whose abstract path is 'dir/example.txt', it may returns a file corresponding to
	 * 'dir/example-2.txt'.
	 * @param file
	 *            the base file
	 * @return a file whose name does not collide with an already existing file. It can be the provided file itself,
	 *         if it does not already exist.
	 */
	public static File nonCollidingFileForFile(File file)
	{
		if (!file.exists())
			return file;
		File result = file;
		
		String[] name_n_ext = nameAndExtensionForFilename(file.getPath());
		int n = 2;
		while (result.exists())
		{
			result = new File(name_n_ext[0] + "-" + n + name_n_ext[1]);
			n += 1;
		}
		return result;
	}
	
	/**
	 * 
	 * @param prefix
	 * @param suffix
	 * @param directory
	 * @return the temporary directory, or null if it could not be created
	 */
	public static File createTempDirectory(String prefix, String suffix, File directory)
	{
		File dir=null;
		if (directory.exists() && ( ! directory.isDirectory() || ! directory.canWrite()) )
		{
			Log.log.fine(directory.getPath()+" exists but it is not a directory or it is not writable");
			return null;
		}
		if ( ! directory.exists() && !directory.mkdirs() )
		{
			Log.log.fine("Cannot create directory "+directory.getPath());
			return null;
		}
		java.security.SecureRandom r = new java.security.SecureRandom();
		while ( dir==null || dir.exists() )
		{
			long n = r.nextLong(); // this and the following directly taken from java.io.LazyInitialization.generateFile()
			if (n==Long.MIN_VALUE)
				n=0;
			 else
				n = java.lang.Math.abs(n);
			dir = new File(directory, prefix + Long.toString(n) + suffix);
			if (dir.mkdir())
				break;
		}
		return dir;
	}
	
	/**
	 * Throws a {@link RuntimeException} with the message "unimplemented". This method is used as a marker for places
	 * in the code where there is a need for a real implementation.
	 * @throws RuntimeException
	 */
	public static void unimplemented()
	{
		unimplemented("Unimplemented");
	}
	
	/**
	 * Throws a {@link RuntimeException} with the message "unimplemented". This method is used as a marker for places
	 * in the code where there is a need for a real implementation.
	 * @throws RuntimeException
	 */
	public static void unimplemented(String msg)
	{
		throw new RuntimeException(msg);
	}
	
	/**
	 * Returns the array of 3 elements: (connection method, server hostname, port number) based on the supplied
	 * connection string.
	 * @param connectionString
	 *            a string of the form "method:host:port"
	 * @return an array of 3 elements: (connection method, server hostname, port number)
	 * @throws IllegalArgumentException if the connection string is invalid
	 */
	public static String[] serverForString(String connectionString)
	{
		if (connectionString == null || "".equals(connectionString))
			return null;
		String[] srv_elements = connectionString.split(":");
		switch (srv_elements.length)
		{
			case 3:
			case 5:
				return srv_elements;
			default:
				throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Returns the content of a file as a {@link String}. Please note that you should use this method for text files
	 * <b>only</b>; using it on binary files will lead to unexpected results, such as data corruption.
	 * @param aFile
	 *            the file to dump into a String
	 * @return the content of the file, or null if it could not be read
	 */
	public static String getContentOfTextFile(File aFile)
	{
		try (FileReader reader = new FileReader(aFile))
		{
			return getContent(reader);
		}
		catch (IOException e)
		{
			return null;
		}
	}
	
	/**
	 * Returns the content of an input stream as a {@link String}. Please note that you should use this method for
	 * text streams <b>only</b>; using it on binary streams will lead to unexpected results, such as data corruption.
	 * @param stream
	 *            the stream to read
	 * @return the content of the stream, or null if it could not be read
	 */
	public static String getContentOfTextStream(InputStream stream)
	{
		return getContent(new InputStreamReader(stream));
	}
	
	/**
	 * Returns the content of a reader as a {@link String}.
	 * @param reader
	 *            the reader to read
	 * @return the content of the reader, or null if it could not be read
	 */
	public static String getContent(Reader reader)
	{
		StringBuilder contents = new StringBuilder();
		String line;
		try
		{
			BufferedReader input = new BufferedReader(reader);
			try
			{
				while ((line = input.readLine()) != null)
				{
					contents.append(line).append("\n");// System.getProperty("line.separator"));
				}
			}
			finally
			{
				input.close();
			}
		}
		catch (IOException ex)
		{
			return null;
		}
		return contents.toString();
	}
	
   /**
    * Reads at most <code>maximumSize</code> characters (bytes) of a file and return its content. <br>
    * Please note that you should use this method for text files <b>only</b>; using it on binary files will lead to
    * unexpected results.
    * @param aFile
    *            the file to be read
    * @param maximumSize
    *            the maximum number of bytes to read
    * @param messageIfTruncated
    *            this message is added to the returned string when if the file was actually larger than {@code
    *            maximumSize}. It may be null.
    * @return the content of the file if its size is lower than <code>maximumSize</code>, otherwise the first
    *         <code>maximumSize</code> characters. In the latter case, the string {@code messageIfTruncated} is
    *         appended to the returned string.
    */
	public static String getContentOfTextFile(File aFile, int maximumSize, String messageIfTruncated)
	{
	    byte[] buffer = new byte[maximumSize];
	    int offset = 0;
	    try ( BufferedInputStream input = new BufferedInputStream(new FileInputStream(aFile)) )
	    {
	        int c;
	        while (offset < maximumSize && (c = input.read(buffer, offset, buffer.length-offset)) != -1)
	            offset += c;
	    }
	    catch (IOException ex)
	    {
	        return null;
	    }
	    if (messageIfTruncated != null && offset>=maximumSize)
	        return new String(buffer,0,maximumSize)+messageIfTruncated;
	    else
	        return new String(buffer,0,offset);
	}

	
	/**
	 * search within the system's path (environment variable PATH) the requested executable file
	 * @param executableName
	 *            the executable to search. On windows operating system, the following four names are searched:
	 *            <code>executableName</code>, <code>executableName+".exe"</code>, <code>executableName+".bat"</code>
	 *            and <code>executableName+".com"</code> (in that order)
	 * @return the File object pointing to the found executable, or null if it could not be found
	 */
	public static File searchSystemPath(String executableName)
	{
		for (String path: System.getenv("PATH").split(File.pathSeparator))
		{
			String[] executables = { executableName };
			if (org.apache.commons.lang.SystemUtils.IS_OS_WINDOWS)
			{
				if (executableName.endsWith(".exe") || executableName.endsWith(".bat") || executableName.endsWith(".com"))
					executables = new String[] { executableName };
				else
					executables = new String[] { executableName, 
					                             executableName+".exe", executableName+".bat", executableName+".com" };
			}
			
			for (String name: executables)
			{
				File execFile = new File(path, name);
				if (execFile.exists() && execFile.canExecute())
					return execFile;
			}
		}
		return null;
	}
	
	/**
	 * Tests whether the method {@link #browse(URI)} is supported on the current platform.
	 * @return {@code true} if the method {@link #browse(URI)} is supported
	 */
	public static boolean isBrowseSupported()
	{
		if (!org.apache.commons.lang.SystemUtils.IS_OS_LINUX)
			return Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE);
		File url_open = searchSystemPath("xdg-open");
		if (url_open==null)
			url_open = searchSystemPath("gnome-open");
		return ( (url_open != null && url_open.canExecute())
				|| Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE) );
	}

	/**
	 * Opens an URI in the default browser. This method basically calls {@link Desktop#browse(URI)}, except on Linux
	 * where it tries to open it with {@code xdg-open} and {@code gnome-open}. Reason is that on Linux, Desktop.browse()
	 * uses a different browser when the URI points to a local file (URI starting with {@code file://}), while
	 * {@code xdg-open} and {@code gnome-open} opens such URI with the default web browser (the same as the one
	 * used when the URI starts with {@code http://} or {@code https://}).
	 * 
	 * @param uri
	 *            the URI to open in the default browser
	 * @return {@code true} if the URI could be opened
	 * @see #isBrowseSupported()
	 */
	public static boolean browse(URI uri)
	{
		if (!org.apache.commons.lang.SystemUtils.IS_OS_LINUX)
		{
			try
            {
	            java.awt.Desktop.getDesktop().browse(uri);
            }
            catch (IOException e)
            {
            	return false;
            }
			return true;
		}
		// Linux
		/*
		 * Desktop.browse() calls gnome_open_url() which behaves differently for http:// or file://, while
		 * xdg-open does not (and neither does gnome-open).
		 */
		File url_open = searchSystemPath("xdg-open");
		if (url_open==null)
			url_open = searchSystemPath("gnome-open");
		if (url_open==null)
		{
			try
			{
				java.awt.Desktop.getDesktop().browse(uri);
			}
			catch (IOException e)
			{
				return false;
			}
			return true;
		}
		try
        {
	        Runtime.getRuntime().exec(new String[]{url_open.toString(),uri.toString()});
        }
        catch (IOException e)
        {
	        e.printStackTrace();
			return false;
        }
		return true;
	}
	
	/**
	 * Determines whether a program is registered within the Windows registry, and returns a array suitable for
	 * executing it by creating a {@link ProcessBuilder}. The method searches if the program is declared in the
	 * windows registry within one of the two following keys:
	 * <ul>
	 * <li>{@code HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths},</li>
	 * <li>{@code HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\App Paths};</li>
	 * </ul>
	 * the 1st one always exists, the second one exists in 64-bits Windows --these entries are those searched by
	 * windows when e.g. launching @ code cmd /c <prg>} , if the program is not in the {@code PATH}. If a sub-entry is
	 * found, then the program can be launched with the command: {@code cmd /c start <executableName>}.
	 * @implementation The check is done through a "reg query".
	 * @param executableName
	 *            name of the program to search
	 * @return the command for this program, which can be used to {@link ProcessBuilder#ProcessBuilder(String...)}, or
	 *         {@code null} if no such program was found.
	 */
	public static String[] searchWindowsAppPathsInRegistry(String executableName)
	{
		if (!org.apache.commons.lang.SystemUtils.IS_OS_WINDOWS)
			return null;

		// appPaths32key is present on both windows 32-bits and 64-bits
		final String appPaths32Key = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths";
		final String appPaths64Key = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\App Paths";
		String[] cmd = null;
		String[] cmdTmpl = new String[] {"cmd", "/c", "start", "exec. name here"};
		String[] executables;
		
		// if the name is fully qualified, we search that one only
		if (executableName.endsWith(".exe") || executableName.endsWith(".bat") || executableName.endsWith(".com"))
			executables = new String[] { executableName };
		// if not, add .exe, .bat and .com
		else
			executables = new String[] { executableName, 
			                             executableName+".exe", executableName+".bat", executableName+".com" };
		
        if (appPaths32==null)
        {
        	final String[] reg_query = new String[] { "reg", "query", appPaths32Key };
        	SystemExecution execution = new SystemExecution(reg_query, null, new File(System.getProperty("user.dir")));
        	try {
        		execution.execute(new StringWriter(), -1, null, -1);
        		appPaths32 = execution.getStdout().toString().toLowerCase();
        	} catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
        }
        if (appPaths6432Node==null)
        {
        	final String[] reg_query = new String[] { "reg", "query", appPaths64Key };
        	SystemExecution execution = new SystemExecution(reg_query, null, new File(System.getProperty("user.dir")));
        	try {
        		execution.execute(new StringWriter(), -1, null, -1);
        		appPaths6432Node = execution.getStdout().toString().toLowerCase();
        	} catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
        }
		
		/*
		 * Note: we were previously making that kind of query:
		 * 
		 * String[] regQuery=new String[] { "reg", "query", appPaths32Key, "/f", "executable name here", "/k", "/e" };
		 * 
		 * This seems to be valid on windows 64 bits, but it is definitively invalid on 32 bits XP windows. Same: a
		 * simple 'req query ...path..\<executable name>' seemed ok... until we noticed that on some windows systems,
		 * this command fails with "Error: more data available" due to NULL characters in some value that are not
		 * correctly handled (see Microsoft's knowledge base for details) --for example, we were bitten by this bug
		 * when searching "wordpad.exe".
		 * 
		 * The only solution that seems portable (at least AFAIK) is to query the "...App Paths" iself and get all
		 * registered keys, then to search within the results whether the executable can be found.
		 */
        for (String name: executables)
        {
        	// Search for "App Paths\<execName>"
            Matcher m32 = Pattern.compile("\\\\"+name.toLowerCase()+"(\\p{Blank}|\r|\n|$)").matcher(appPaths32);
            Matcher m64 = Pattern.compile("\\\\"+name.toLowerCase()+"(\\p{Blank}|\r|\n|$)").matcher(appPaths6432Node);

            if ( m32.find() || m64.find())
            {
                cmd = cmdTmpl.clone();
                cmd[3] = name;
                return cmd;
            }
        }
		return null;
	}
	
	/**
	 * Returns the names of the files/directories contained in a given directory.<br>
	 * @param directory
	 *            the directory
	 * @return a map containing the files and directories found in the requested directory. More specifically: within
	 *         the map, the keys represent the file/dir. name relative to the requested directory; the corresponding
	 *         values are:
	 *         <ul>
	 *         <li>File objects for regular files,</li>
	 *         <li><code>null</code></li> for directories.
	 *         </ul>
	 */
	public static LinkedHashMap<String, File> listFiles(File directory)
	{
		return internalListFiles(directory, "", new LinkedHashMap<String, File>());
	}
	
	/**
	 * The recursive method implementing {@link #listFiles(File)}.
	 * @param directory
	 *            the currently examined directory.
	 * @param parentName
	 *            the name of the parent directory; in non-empty, it is expected to end with the character slash (
	 *            <code>"/"</code>).
	 * @param files
	 *            the map being built, passed as an argument when the method calls itself on sub-directories. The map
	 *            itself is modified by this method.
	 * @return The updated map passed as a parameter.
	 */
	protected static LinkedHashMap<String, File> internalListFiles(File directory, String parentName,
	                                                               LinkedHashMap<String, File> files)
	{
		File[] dirContent = directory.listFiles();
		Arrays.sort(dirContent, (o1, o2) -> o1.getName().compareTo(o2.getName()));
		for (File file: dirContent)
		{
			if (file.isDirectory())
			{
				files.put(parentName+file.getName(), null);
				/* void */ internalListFiles(file, parentName+file.getName()+"/", files);
			}
			else
				files.put(parentName+file.getName(), file);
		}
		return files;
	}
	
	/**
	 * Breaks a string into pieces. Whitespaces (space and tab characters) are separators; quoted string (with either
	 * simple or double quotes) are detected as a shell does. Note that the quotes themselves cannot be escaped with
	 * the backslash character (see the 3rd example, below): use {@code "'"} to insert a single quote, and {@code '"'}
	 * for a double-quote.<br>
	 * Examples (showing the plain strings):
	 * <ul>
	 * <li>{@code python 'my "file".py'} is splitted into {@code [python, my "file".py]},</li>
	 * <li>{@code cmd -p'v1' --p3="v'3"} is splitted into {@code [cmd, -pv1, --p3=v'3]},</li>
	 * <li>{@code cmd -p'v1' --p3="v\"3"} throws {@code IllegalArgumentException}.</li>
	 * </ul>
	 * @param cmdline
	 *            the string to split. TrailingIt may be {@code null}.
	 * @return the array of strings after {@code cmdline} has been split. A zero-length array is returned if cmdline
	 *         is {@code null} or if it consists in whitespaces only.
	 * @throws IllegalArgumentException
	 *             if there is an unbalanced quote.
	 */
	public static String[] splitCommandLine(String cmdline) throws IllegalArgumentException
	{
		if (cmdline==null)
			return new String[0];
		
		StringTokenizer t = new StringTokenizer(cmdline, "\"' \t", true);
		ArrayList<String> args = new ArrayList<>();
		String token;
		final int noQuotes = 0;
		final int doubleQuote = 1;
		final int simpleQuote = 2;
		int context = noQuotes;
        // 'currentArg' is added to 'args' and reset when encountering whitespace
		StringBuilder currentArg = new StringBuilder();
		while (t.hasMoreElements())
		{
			token = t.nextToken();
			switch (context)
			{
				case noQuotes:
					if (token.equals("\""))
						context = doubleQuote;
					else if (token.equals("\'"))
						context = simpleQuote;
					else if (!token.equals(" ") && !token.equals("\t"))
						currentArg.append(token);
					else
						if (currentArg.length()!=0) // if not, there is simply more than one whitespace between args
						{
							args.add(currentArg.toString());
							currentArg.setLength(0);
						}
					break;
				case doubleQuote:
					if (token.equals("\""))
						context = noQuotes;
					else
						currentArg.append(token);
					break;
				case simpleQuote:
					if (token.equals("'"))
						context = noQuotes;
					else
						currentArg.append(token);
					break;
				default:
				    assert false : "context must be limited to values 0, 1 and 2";
			}
		}
		if (currentArg.length()!=0) // may be null if the cmdline ends with whitespaces
			args.add(currentArg.toString());
		if ( context == doubleQuote || context == simpleQuote )
			throw new IllegalArgumentException("There are unbalanced quotes");
		return args.toArray(new String[args.size()]);
	}
	
	/**
	 * Calls {@link Closeable#close()} and ignore any IOException raised.
	 * @param closable the input stream to be closed.  It may be {@code null}.
	 */
	public static void closeSilently(Closeable closable)
	{
		if ( closable == null )
			return;
		try
		{
			closable.close();
		}
		catch (IOException e)
		{
			Log.log.log(Level.FINE, "exception ignored", e);
		}
	}

	public static void main(String[] args)
	{
		String test = "cmd -p'v1' --p3=\"v\"'\"'\"3\"";//"cmd -p'v1' --p3=\"v'3\"";
		System.out.println("Parsing: "+test);
		try{
			for (String a: splitCommandLine(test))
				System.out.print("["+a+"] ");
		} catch (Throwable e) {e.printStackTrace();}
		System.out.println();
	}

}
