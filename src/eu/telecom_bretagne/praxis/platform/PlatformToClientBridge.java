/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.platform;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.jdom.Document;
import org.jdom.Element;

import eu.telecom_bretagne.praxis.client.Client;
import eu.telecom_bretagne.praxis.client.ErrorDialog;
import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Environment;
import eu.telecom_bretagne.praxis.common.I18N;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.ServerToClientEventListener;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;
import eu.telecom_bretagne.praxis.server.execution.platform.WorkflowExecutionEngine;

/**
 * This platform forwards the request of its "downstream" server {@code Sd} to a client connected to another,
 * "upstream", server {@code Su}. It collaborates with a {@link WorkflowExecutionEngine} to execute workflows
 * directly. It also registers itself as a listener of messages sent by the server {@code Su} and takes care of
 * forwarding the execution results to its server {@code Sd}.
 * @author Sébastien Bigaret
 *
 */
public class PlatformToClientBridge
    extends AbstractPlatform
    implements ServerToClientEventListener
{
	protected Client client;

	public PlatformToClientBridge(CommunicationFacade cnx, Document configuration, String[] client_connection)
	throws IOException
	{
		super(cnx, configuration);
		this.client = new Client();
		launchClient(client_connection);
	}

	protected void launchClient(String[] connection) throws IOException
	{
		if ( ! Environment.useLooseCredentials() )
		{
			Log.log.warning("CSLC unset, cannot start client in bridge");
			throw new IOException("Cannot start the client in the bridge");
		}
		String login = PraxisPreferences.get("client", Environment.getLooseCredentialsConfigurationName());
		client.initConnection(connection);
		client.getConnection().addListener(this);
		client.connectAndWaitAnswer(login, login, false);
	}

	@Override
	public void receivedAuthentificationStatus(ServerToClientEvent event)
	{
		// TODO directly copy-pasted from praxis.client.Client#connectAndWaitAnswer() because we do not have any clean way to do this right now
		if (event.data!=null && event.data instanceof String)
		{
			Log.log.info( (String) event.data);
			event.data = new String[] { (String) event.data };
		}
		if (event.data!=null && event.data instanceof String[])
		{
			final String error_title;
			final String error;
			String [] data = (String[]) event.data;
			if (data.length > 1)
				error=I18N.s(data[0], Arrays.copyOfRange(data, 1, data.length)).replace("\\n", "\n");
			else if (data.length == 1)
				error = I18N.s(data[0]);
			else
				error = "Unknown error";

			if (data.length >= 1)
				error_title = I18N.s(data[0] + "_title");
			else
				error_title = "Unknown error";

			Log.log.severe(()->error_title+": "+error);

			ErrorDialog.showErrorMessage(error_title, error);
			System.exit(-1);// TODO list & identify all exit status
		}
	}

	@Override
	public void receivedAvailableResults(ServerToClientEvent event)
	{
		// taken from StorageManager
		@SuppressWarnings("unchecked")
		ArrayList <Result> results = (ArrayList<Result>) event.data;
		ArrayList <Result> finished_exec = new ArrayList<Result>();
		
		for (Result result: results)
		{
			switch (result.getStatus())
			{
				case CANCELLED:
				case ERROR:
				case OK:
				case WARNING:
					finished_exec.add(result);
					break;
				case RUNNING:
				case SUSPENDED:
/*					try
					{
						//updateResult(result);
					}
					catch (java.io.IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
*/					break;
				case NOT_STARTED:
					break;
			}
		}
		// possiblement faire un seul avec receivedExecutionStatus() + traiter le pb de l'update dans le cas où c'est pas fini?
		if (finished_exec.size() > 0)
			client.requestResults(finished_exec);
	}

	@Override
	public void receivedExecutionResults(ServerToClientEvent event)
	{
		final Result result = (Result) event.data;
		if (result==null)
		{
			Log.log.severe("Received a null result!?!");
			return;
		}
		result.setZipFile(event.file_conveyor);
		WorkflowID receivedWFID = result.workflowID();
		/* now that we have the full result, restore its original workflow ID and send it back to the server */

		if (result.data!=null)
		{
			result.setWorkflowXML((WorkflowID) result.data, result.getWorkflowXML());
		}
		else
		{
			// this is the result of an execution that was not sent by us. It may come from a different configuration
			// where the bridge is not activated.  Log it and send it as-is.
			Log.log.info(()->"Received a result with result.data==null. This was not sent by the bridge. "+result);
		}
		this.sendResults(result);
		if (result.data!=null && result.getStatus().isClosed())
		{
			Log.log.fine(()->"Requesting deletion for "+result);
			ArrayList<ExecutionID> results = new ArrayList<ExecutionID>();
			results.add(result.executionID());
			client.requestsDeletionOfResults(receivedWFID, results);
		}
	}

	@Override
	public void receivedExecutionStatus(ServerToClientEvent event)
	{
		final Result result = (Result) event.data;
		if (result == null)
		{
			Log.log.severe("Received a null result!?!");
			return;
		}
		if (result.getStatus().isClosed())
			client.requestResult(result);
		else
		{
			// Communicate the update using the original workflow ID
			Result _result = new Result(result);
			_result.setWorkflowXML((WorkflowID) result.data, result.getWorkflowXML());
			sendProgress(_result);
		}
	}

	@Override
	public void receivedMessage(ServerToClientEvent event)
	{
		if (event.forward)
			ErrorDialog.showWarningMessage("Message from server", (String)((Object[]) event.data)[0]);
	}

	@Override
	public void disconnected(Exception exception)
	{
		// TODO
	}

	/* Platform */
	@Override
	public void serverCancelsExecution(Event event)
	{
		// TODO
	}

	@Override
	public void serverRequestsExecution(Event event)
	{
		WorkflowExecutionEngine executionEngine = (WorkflowExecutionEngine) event.data; 
		executionEngine.getResult().setZipFile(event.file_conveyor);
		/*
		 * The original workflowID was replaced and saved in the WFExecEngine, let's keep it under the pillow,
		 * we'll need it when sending back the results to the downstream server.
		 */
		executionEngine.getResult().data = executionEngine.getOriginalWorkflowID();
		/* TODO ugly bug fix
		 * 
		 */
		Workflow wf = null;
		try
		{
			wf =  new Workflow(new java.io.ByteArrayInputStream(executionEngine.getResult().getWorkflowXML()), null, false);
		}
		catch (InvalidXMLException e)
		{
			e.printStackTrace(); // abort, but should not happen
		}
		for (WorkflowInput input: wf.getInputs())
		{
			if (input.filepaths().length==0)
				continue;
			final ArrayList<String> normalizedPaths = new ArrayList<String>();
			for (String path: input.filepaths())
				normalizedPaths.add(path.replace(File.separatorChar, '/'));
			input.setContent(normalizedPaths);
		}
		Result r = executionEngine.getResult();
		java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		try
        {
	        wf.save(baos, true);
        }
        catch (IOException e)
        {
	        e.printStackTrace(); // abort, but should not happen
        }
		r.setWorkflowXML(baos.toByteArray());
		client.requestExecution(r);
	}

	public static void launch() throws IOException
	{
		// Get the bridge to launch...
		String bridge_id = Configuration.get("bridge");
		
		// ...and the appropriate connections
		String[] platform_conn = Utile.serverForString(Configuration.get("bridge.platform.connection"));
		String[] client_conn = Utile.serverForString(Configuration.get("bridge.client.connection"));
		
		if ("".equals(bridge_id))
			return;
		
		Document conf;
		conf = AbstractPlatform.initConfiguration(Configuration.get("platform."+bridge_id));
		
		/* Retrieve the resources declared on the upStream server, and declare them as our resources */
		RemoteComponents remoteComponents = new RemoteComponents(platform_conn[0], platform_conn[1], Integer.parseInt(platform_conn[2])); // TODO NumberFormatException
		remoteComponents.init();
		Element programs = conf.getRootElement().getChild("programs");
		/*
		 * "local" resources /may/ be declared on the remote server, most of the times because they were present at
		 * compile-time and packed in the jar along with all the others. However, resources with provider named
		 * "local" are *never* shared: if they are present/declared on the remote side, we must ignore them, or they
		 * may shadow the one locally declared.
		 */
		for (String resource: remoteComponents.getResourceRepository().getResources())
			if (!resource.startsWith("local"))
				programs.addContent(new Element("program").setAttribute("id", resource));
		
		CommunicationFacade cnx = CommunicationFacade.buildConnection("Platform", platform_conn);
		cnx.start();
		new PlatformToClientBridge(cnx, conf, client_conn);
	}
}
