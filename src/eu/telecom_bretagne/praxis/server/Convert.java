/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;


/**
 *
 * Lecture des fichiers xml de workflow.
 * @author ROBIN Blandine
 * @version 1.0
 * @version $Revision: 1.5 $
 */


public class Convert {
	
	private File executionRepertoire;
	
	public Convert(File executionReperotire)
	{
		this.executionRepertoire = executionReperotire;
	}

  	/** Méthode de lecture du fichier pour la partie 'declar_fic' */
    public String creer_script(Document doc)
  	{
	    // Declaration d'un element DocType
	    //DocType docType = doc.getDocType();
	    //System.out.println("doctype: " + docType.getSystemID());
	    //docType.setSystemID("scenari.dtd");
	    
	    // Extraction de l'element 'racine'
	    Element root = doc.getRootElement();
	    System.out.println("root: " + root.getName());
	    
	    String titre = "cd " + executionRepertoire.getPath().replaceAll("\\\\","/") + "\n";
	    titre= titre + "echo Creation du script du workflow : "+root.getChild("workflow").getChild("name").getText() +">suivi.txt\n" ;
	    titre = titre.concat("\nsource /home/nymphea/services/logiciels/gcg/gcgstartup\ngcg\ngcgsupport");
	    String scriptemp=new String("");
	    // Liste des enfants de 'declar_fic' nommes 'fichiers'
	    List <Element> declarfic_children = ((Element)(root.getChildren("declar_fic").get(0))).getChildren();
	
	    // Pour chaque element 'fichiers'
		Hashtable <String,String> hash = new Hashtable<String, String>();
	    for(Element xfile: declarfic_children)
	    {
	    	String attIDvalue = xfile.getAttributeValue("ID_f");
		    List <Element> fichiers = xfile.getChildren();
		    String nom = fichiers.get(0).getText();
		    String origin = fichiers.get(0).getAttributeValue("origin");
			if(origin.equalsIgnoreCase("file"))
			{
				//si le client avoir Windows les repertoires seront separees pour / sinon, si il avoir unix seront \\
				int debut = Math.max(nom.lastIndexOf("\\"),nom.lastIndexOf("/")) + 1;		    	
				nom = nom.substring(debut,nom.length());
			}
			else if(origin.equalsIgnoreCase("BD"))
		    {
		    	if(nom.endsWith(":*")) nom = nom.substring(0,nom.indexOf(":*"));
		    }
		    hash.put(attIDvalue,nom);
	    }
	
		//*********************************************************************************
	 	// Méthode de lecture du fichier pour la partie 'scenario'
	
	    // Extraction de l'element nomme 'scenario'
	    Element workflow = root.getChild("scenario");
	
	    // Extraction des elements 'program' a partir de la balise 'workflow'
	    List <Element> programs = workflow.getChildren("program");
	
	    // pour chaque balise 'program', extraction des parametres
	    for(Element prg: programs)
	    {
	    	// Extraction balise parameters
	      	Element parameters = prg.getChild("parameters");
	
	      	// Extraction ensemble des balises 'parameter' dans une liste
	      	List <Element> parameter = parameters.getChildren("parameter");
	
	      	// Traitement du premier parametre : extraction du  de la balise 'code'(correspond a prog -Default)
	      	Element parameter0 = parameter.get(0);
	      	String contain = parameter0.getChildText("code");
	      	scriptemp=scriptemp.concat("\n"+ contain + " ");
	
	      	// Taille de la liste contenant tous les parametres d'un programme
	      	int taillelistparam = parameter.size();
	
	      	// parcours de cette liste pour extraire le contenu des balises 'code' et 'data' (ou 'pipe')
	      	for (int j=1; j<taillelistparam; j++)
	      	{
		        // Le contenu de chaque balise 'code', 'data', 'parameter type' et 'vdef' est inclus dans un tableau
		        String lignecode = parameter.get(j).getChildText("code");
		        String lignedata = parameter.get(j).getChildText("data");
		        String valuetype = parameter.get(j).getAttributeValue("type");
		        String vdefvalue = parameter.get(j).getChildText("vdef");
		        //on determine si les elements '@' ou '{*}' correspondant au traitement des listes sont requis
		        String vdef1 = "";
		        String vdef2 = "";
		        if (vdefvalue != null)
		        {
		        	if (vdefvalue.startsWith("@")){vdef1="@";}
		        	else if (vdefvalue.endsWith("{*}")){vdef2="{*}";}
		        	else if (vdefvalue.endsWith(":*")){vdef2=":*";}
		        }		        	
		
		        // Si l'attribut 'type' du parametre a pour valeur input et qu'un attribut IDfref existe
		        if(valuetype.compareToIgnoreCase("input") == 0
		           && (parameter.get(j).getChild("data").getAttributeValue("ID_fREF") != null))
		
		          // Extraction du code correspondant dans la hashtable pour le parametre en cours d'etude
		        {
		        	String key = parameter.get(j).getChild("data").getAttributeValue("ID_fREF");
			        String attIDrefvalue=hash.get(key);
			        scriptemp=scriptemp.concat(lignecode +vdef1+attIDrefvalue+vdef2 + " ");
		        }
		        else
		        {
		        	// On teste si la balise 'data' existe
		          	if(parameter.get(j).getChild("data") != null)
		          	{
			            // 'data' existe, on teste alors si elle a des elements enfants
		            	if(parameter.get(j).getChild("data").getChildren().size()>0) //haschildren() deprecated
		            	{
		              		// affichage du contenu de l'element enfant 'pipe'
		              		String lignepipe = parameter.get(j).getChild("data").getChildText("pipe");
		              		scriptemp=scriptemp.concat(lignecode+vdef1+lignepipe+vdef2 + " ");
		            	}
		            	else
		            	{
		              		// 'data'n'a pas d'enfant, on affiche alors son contenu
		              		scriptemp=scriptemp.concat(lignecode+vdef1+lignedata+vdef2 + " ");
		                }
		          	}
		          	else
		          	{
			            // la balise 'data' n'existe pas
		            	// on regarde si la balise 'parameter' contient un attribut 'isactivated'
		            	if(parameter.get(j).getAttribute("isactivated") != null)
		            	{
		              		// l'attribut est présent donc on affiche le contenu de la balise 'code'
		              		String code = parameter.get(j).getChildText("code");
		              		scriptemp=scriptemp.concat(code + " ");
		            	}
		          	}
		        }
	      	}
	
	      	// si l'element @ est present, creation d'une 2eme ligne de code dans le script, identique à la première, sans le @
	      	//(certains programmes GCG prennent en entree soit des fichiers simples soit des listes)
	
	      	int index = scriptemp.indexOf("@");
	      	if (index != -1)
	      	{
		        String s1 = new String();
	        	String s2 = new String();
	        	String s3 = new String();
	        	s1=scriptemp.substring(0,index);
	        	s2=scriptemp.substring(index+1,scriptemp.length());
	        	s3=s1.concat(s2);
	        	scriptemp = scriptemp.concat(s3);
	      	}
			//titre=titre.concat(scriptemp).concat("\necho "+ contain + " termine >> suivi.txt");
			titre=titre.concat(scriptemp);
	      	scriptemp="";
	    }
	    //System.out.println(titre);
	    titre = titre.concat("\n");
	    return titre;
  	}
}
