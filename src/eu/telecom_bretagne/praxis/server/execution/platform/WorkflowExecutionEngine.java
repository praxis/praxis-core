/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.server.execution.platform;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.Process.ProcessContext;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;

public class WorkflowExecutionEngine extends ExecutionEngine
{
	private static final long serialVersionUID = 8599030500992930897L;

	public static enum TARGETS { WORKFLOW }

	protected Workflow workflow;
	protected WorkflowID originalWorkflowID;
	
	public static class WorkflowExecutionEngineConfiguration
	extends ExecutionEngineConfiguration
	{
		private static final long serialVersionUID = 4530433974117293927L;

		/**
		 * 
		 * @param platform the description of the platform
		 */
		public WorkflowExecutionEngineConfiguration(PlatformDescription platform)
		{
			super(platform);
		}
		
		@Override
		public ExecutionEngine getEngine(WorkflowID workflowID, ExecutionID executionID)
		{
			return new WorkflowExecutionEngine(this, workflowID, executionID);
		}
	}

	public static final class WorkflowExecutionEngineConfigurationFactory
	implements ExecutionEngine.ExecutionEngineConfigurationFactory
	{
		/** Returns <code>"workflow"</code> */
		public String key() { return "workflow"; }
		
		/**
		 * Builds and returns a new workflow Platform
		 * @exception IllegalArgumentException
		 *                if the supplied document's root element has no attribute <code>os</code> or if its value
		 *                is not a valid argument for {@link ShellExecutionEngine#set_OS(String)}.
		 */
		public ExecutionEngineConfiguration build(PlatformDescription platform)
		{
			return new WorkflowExecutionEngineConfiguration(platform);
		}
	}
	
	public WorkflowExecutionEngine(ExecutionEngineConfiguration configuration, WorkflowID workflowID, ExecutionID executionID)
    {
		super(configuration, workflowID, executionID);
    }

	public Workflow getWorkflow()
	{
		return workflow;
	}

	public WorkflowID getOriginalWorkflowID()
	{
		return originalWorkflowID;
	}

	@Override
    public void cancel()
    {
	    // TODO Auto-generated method stub
	    
    }

	@Override
    public Enum< ? extends Enum< ? >> defaultTarget()
    {
		return TARGETS.WORKFLOW;
    }

	@Override
	public void prepareExecution(Activity activity, ExecutionEngineConfiguration aConfiguration)
	        throws CannotExecuteException
	{
		try
		{
			super.prepareExecution(activity, aConfiguration);
			workflow = new Workflow("bridge");
			for (Program prg: activity.getExecutionSet())
			{
				//Program prg = activity.getProgram().copyForParamDialog(); // TODO CHECK this
				//workflow.addProgram(activity.getProgram().getProgramID(), prg);
				workflow.addProgram(prg.getProgramID(), prg);
				prepareExecution(activity, prg, aConfiguration);
			}
			//File wd = activity.getContainer().getContext().workingDirectory;
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workflow.save(baos, true);
			/*
			 * The workflow will be submitted through a client; it was built from an existing workflow but this is a
			 * different one, so the result's workflow ID must reflect that. The original one will be reset when the
			 * results are sent back to the server.
			 * NB: yes, the result's wfID must be different; suppose we keep the same wfID and execID, if two programs
			 * are executed in row from the same WF and execution, a request for getting results for the second one
			 * will result in getting the results of the 1st one!
			 */
			originalWorkflowID = result.workflowID();
			result.setWorkflowXML(workflow.id(), baos.toByteArray());
		}
		catch (Throwable t)
		{
			throw new CannotExecuteException("", t);
		}
	}

	private void prepareExecution(Activity activity, Program prg, ExecutionEngineConfiguration aConfiguration)
	throws Throwable
	{
			for (Parameter input: prg.getInputParameters())
			{
				if (!input.isActivated())
					continue;
				WorkflowInput newInput;
				if (input.getPrgInput()!=null)
				{
					if (activity.getExecutionSet().contains(input.getPrgInput().getProgram()))
						continue;
					newInput = workflow.addNewInput(0, 0, "", WorkflowInput.INPUT_TYPE.FILE);// TODO DIRECTORY
					ArrayList<String> content = new ArrayList<String>();
					File prev_result = new File(ProcessContext.RESULTS_SUBDIR,
					                            input.getPrgInput().getProgram().getProgramID());
					prev_result = new File(prev_result, input.getPrgInput().getDescription().getVdef());
					content.add(prev_result.getPath());
					newInput.setContent(content);
					input.setPrgInput(null);
				}
				else // workflow input
				{
					// TODO wow CHECK pas forcement, ici on peut avoir des inputs qui sont des inputs "de l'interieur" de l'executionSet
					newInput = workflow.addNewInput(0, 0, input.getInput().getName(), WorkflowInput.INPUT_TYPE.FILE);// TODO DIRECTORY + filepaths
					newInput.setContent(Arrays.asList(input.getInput().filepaths()));
				}

				input.setInput(newInput);
			}
	}

	@Override
    public Result execute(File executionDirectory, File zippedInputFiles) throws Exception
    {
	    // TODO Auto-generated method stub
	    return null;
    }

	@Override
    public Class< ? extends Enum<?>> getTargets()
    {
	    // TODO Auto-generated method stub
	    return null;
    }   

}
