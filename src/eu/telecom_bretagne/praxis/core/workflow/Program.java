/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.workflow;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.core.execution.ExecutionSet;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.ParameterType;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;


/**
 * Program == a program in a workflow
 * 
 * @author Sébastien Bigaret
 *
 */
public class Program
    implements Cloneable, java.io.Serializable
{
	
	private static final long serialVersionUID = 6530589464262698377L;

	protected static final String          PRG_NODE           = "program";
	
	protected static final String          PRG_DESCRIPTION_ID = "idref";
	
	protected static final String          PRG_COORD_X        = "x";
	
	protected static final String          PRG_COORD_Y        = "y";
	
	public static final String             PRG_INFO           = "info";

	public static ProgramDelegate          delegate           = new ProgramDelegate();
	
	/** This is the id of the corresponding {@link ProgramDescription} */
	protected String                       prg_description_id;
	
	/** */
	protected List<Parameter>              parameters         = new ArrayList<Parameter>();
	
	/** An optional annotation attached to the program, supplied by the designer */
	protected String                       annotation;
	
	/** */
	protected int                          x;
	
	/** */
	protected int                          y;
	
	/** The workflow in which the program is included, if any */
	protected Workflow                     workflow;
	
	/** 
	 * 
	 */
	protected PropertyChangeSupport        propChgSupport     = new PropertyChangeSupport(this);
	
	/**
	 * Caches the program description, after it has been requested once. This is definitely an precautionary measure:
	 * the {@link RemoteComponents.ResourceRepositoryProxy#programDescriptionForId(String) ResourceRepositoryProxy}
	 * takes care of returning the same object, given a prg id. However, we also cache the description here; this way
	 * it is make very clear hopefully that {@link #getDescription()} must always return the same object (because some
	 * external modules implicitly expect this --GUI, building workflows from XML, etc.<br/>
	 * The caching behaviour is disabled when {@link RemoteComponents#cacheResourceRepositoryAnswers} is set to false.
	 * @see #getDescription()
	 */
	protected transient ProgramDescription description;
	
	protected transient ExecutionSet set;
	
	/**
	 * Builds a new program
	 * 
	 * @param xml_node
	 *            the XML element describing the program
	 * @param warnings
	 *            the object is which warnings aimed at the user are stored
	 * @param parent
	 *            the workflow containing the program. It cannot be {@code null}.
	 * @throws InvalidXMLException
	 *             if the object cannot be created from the supplied XML element.
	 * @throws NullPointerException
	 *             if {@code parent} is {@code null}.
	 */
	public Program(Element xml_node, Workflow parent, XMLWarnings warnings) throws InvalidXMLException
	{
		setWorkflow(parent);
		fromXML(xml_node, warnings);
	}
	
	/**
	 * Builds a new program
	 * 
	 * @param prgDesc
	 *            this program's description
	 * @param parent
	 *            the workflow containing the program. It cannot be {@code null}.
	 * @throws NullPointerException
	 *             if {@code parent} is {@code null}.
	 */
	public Program(ProgramDescription prgDesc, Workflow parent)
	{
		setWorkflow(parent);
		initWith(prgDesc);
	}
	
	/**
	 * Builds an <b>non-initialized</b> Program. For internal use only (incl.unittests)
	 */
	public Program()
	{}
	
	public Program(String prg_description_id)
	{
		this.prg_description_id = prg_description_id;
	}

	protected void initWith(ProgramDescription prgDesc)
	{
		this.prg_description_id = prgDesc.id();
		this.description = null; // reset the cached description, we may be called with an alternate description
		for (ParameterDescription paramDesc: prgDesc.getParameters())
		{
			if (paramDesc.getType() == ParameterType.COMMAND || paramDesc.getType() == ParameterType.CODE)
				continue;
			parameters.add(new Parameter(paramDesc, this));
		}
	}
	
	/**
	 * Returns the list of parameters that are {@link ParameterType#isInput() inputs} of the program.
	 * @return input parameters
	 */
	public List<Parameter> getInputParameters()
	{
		List<Parameter> inputs = new ArrayList<Parameter>();
		for (Parameter param: parameters)
			if (param.getDescription().getType().isInput())
				inputs.add(param);
		return inputs;
	}
	
	/**
	 * Returns the list of all {@link Parameter#isActivated() activated} {@link Parameter#isInput() input} parameters
	 * that are not connected
	 * @return the list of the disconnected active parameters
	 */
	public List<Parameter> getDisconnectedInputFiles()
	{
		List<Parameter> inputs = new ArrayList<Parameter>();
		for (Parameter param: parameters)
			if (param.isInput() && param.isActivated() && param.getInput() == null && param.getPrgInput() == null)
				inputs.add(param);
		return inputs;
	}
	
	/**
	 * Returns the list of parameters that are {@link ParameterType#isOutput() outputs} of the program.
	 * @return output parameters
	 */
	public List<Parameter> getOutputParameters()
	{
		List<Parameter> outputs = new ArrayList<Parameter>();
		for (Parameter param: parameters)
			if (param.getDescription().getType().isOutput())
				outputs.add(param);
		return outputs;
	}
	
	/**
	 * Returns the list of parameters that are {@link ParameterType#isOutput() outputs} of the program.
	 * @return output parameters
	 */
	public List<Parameter> getValidLinkedOutputParameters()
	{
		List<Parameter> outputs = new ArrayList<Parameter>();
		for (Parameter param: parameters)
			if (param.isOutput())
				if (param.isActivated() && param.getOutputs().length > 0)
					outputs.add(param);
		return outputs;
	}
	
	/**
	 * Returns all the receivers' parameters, sorted by their {@link Parameter#getDescription() description}'s
	 * {@link ParameterDescription#getPosition() position}.
	 */
	public Parameter[] getParameters()
	{
		Parameter[] params = parameters.toArray(new Parameter[parameters.size()]);
		
		Arrays.sort(params, new Comparator<Parameter>() {
			public int compare(Parameter a, Parameter b)
			{
				return ((Integer) a.getDescription().getPosition()).compareTo(b.getDescription().getPosition());
			}
		});
		
		return params;
	}
	
	/** Returns the size of the set of the program's parameters */
	public int getParametersSize()
	{
		return parameters.size();
	}
	
	/**
	 * Returns the parameter corresponding to the {@link ParameterDescription} whose id is the provided one.
	 * @param ref_id
	 *            a {@link ParameterDescription#getId() ParameterDescription id}
	 * @return <tt>null</tt> if no such parameter exists.
	 * @throws NullPointerException
	 *             if ref_id is null
	 */
	public Parameter getParameterWithID(String ref_id)
	{
		for (Parameter p: parameters)
			if (ref_id.equals(p.ref_id))
				return p;
		return null;
	}
	
	/**
	 * Reads the provided xml element and builds the corresponding Program. During this operation, the receiver is
	 * completely reinitialized.
	 * 
	 * @param xml_node
	 *            the XML element describing the program
	 * @param warnings
	 *            the object is which warnings aimed at the user are stored
	 * @throws InvalidXMLException
	 *             TODO pointer to DTD
	 * @see #toXML()
	 */
	public void fromXML(Element xml_node, XMLWarnings warnings) throws InvalidXMLException
	{
		List<Element> params;
		InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.PROGRAM);

		if (!xml_node.getName().equalsIgnoreCase(PRG_NODE))
			throw xml_exc.setMsg("root element is not " + PRG_NODE);
		
		prg_description_id = xml_node.getAttributeValue(PRG_DESCRIPTION_ID);
		xml_exc.setName(prg_description_id);
		annotation = xml_node.getChildText("annotation");
		
		ProgramDescription prgDesc = getDescription();
		
		if ( this.workflow == null ) // this is only possible when using the constructor reserved for testing: log it
			Log.log.severe("Check the tests: workflow should no be null!");

		if ( ( prgDesc==null || !prgDesc.isActive() )
			 && this.workflow!=null && ! this.workflow.wasExecuted() ) // avoid NullPtrExc anyway
		{
			prgDesc =
				delegate.findAlternateProgramDescription(prg_description_id,
				                                         xml_node.getAttributeValue(Program.PRG_INFO), warnings,
				                                         xml_exc);
		}
		
		if (prgDesc == null)
		{
			warnings.add(Workflow.XMLWarnings.PRG_DESC_DOES_NOT_EXIST_1, new String[] { xml_node
			        .getAttributeValue(PRG_INFO) });
			throw xml_exc.setMsg("Unable to find the corresponding prog's description ").setIsFatal(false);
		}

		description = prgDesc;

		/* initialize the program, then change the parameters */
		/* TODO should we detect and report that some parameters in the PrgDesc were not set in the xml? */
		parameters = new ArrayList<Parameter>();
		this.initWith(prgDesc);
		
		try
		{
			x = Integer.valueOf(xml_node.getAttributeValue(PRG_COORD_X));
			y = Integer.valueOf(xml_node.getAttributeValue(PRG_COORD_Y));
		}
		catch (NumberFormatException e)
		{
			throw xml_exc.setMsg("Coordinates x and/or y are not integers: " + e.toString());
		}
		
		try
		{
			final XPath xpath = XPath.newInstance("./parameter");
			params = xpath.selectNodes(xml_node);
		}
		catch (JDOMException e)
		{
			throw xml_exc.setMsg("unable to select <parameter/> nodes");
		}
		
		for (Element xparam: params)
		{
			Parameter p = null;
			try
			{
				p = new Parameter(xparam, this, warnings);
			}
			catch (InvalidXMLException exc)
			{
				if (exc.isFatal())
					throw exc;
			}
			/* replace the default one with the one we just built */
			if (p == null)
				continue;
			if (parameters.remove(getParameterForDescription(p.getDescription())))
				parameters.add(p);
			else
				/*
				 * since the whole prg was first built from scratch, the parameter must be present! The only case
				 * where it is not the case is w/ param.type==COMMAND or CODE
				 */
				warnings.add(Workflow.XMLWarnings.PRG_CONTAINS_USELESS_PARAMETER, new String[] { prg_description_id,
				        p.getDescription().getId(), p.getDescription().getType().name() });
			
		}
		// reorder parameters to match the order in its description
		List<Parameter> initial = new ArrayList<>(parameters);
		parameters.clear();
		for (ParameterDescription paramDesc: this.getDescription().getParameters())
		{
			for (Parameter p: initial)
				if (p.getDescription().getId().equals(paramDesc.getId()))
					parameters.add(p);
		}
	}
	
	/**
	 * Builds and returns the XML representation of self.
	 * This is equivalent to {@link #toXML(boolean) toXML(false)}
	 * @return the xml equivalent for <tt>this</tt>
	 * @see #fromXML(Element, XMLWarnings)
	 */
	public Element toXML()
	{
		return toXML(false);
	}
	
	/**
	 * Builds and returns the XML representation of self
	 * 
	 * @param includeUserBoundParameters
	 *            if true, parameters whose description are {@link ParameterDescription#isUserBound() bound to a user}
	 *            are included.
	 * @return the xml equivalent for <tt>this</tt>, minus parameters that should not be exported if
	 *         {@code includeUserBoundParameters} is {@code true}.
	 * @see #fromXML(Element, XMLWarnings)
	 */
	public Element toXML(boolean includeUserBoundParameters)
	{
		Element prg = new Element(PRG_NODE);
		
		if (annotation != null)
			prg.addContent(new Element("annotation").setText(annotation));
		
		prg.setAttribute(PRG_DESCRIPTION_ID, prg_description_id);
		prg.setAttribute(PRG_COORD_X, x + "").setAttribute(PRG_COORD_Y, y + "");
		prg.setAttribute(PRG_INFO, getDescription().fullName());
		
		//prg.addContent(new Element("name").setText(prg_description_id)); // TODO: gérer à la fois les id (du prg,
		// stocké par le workflow) ET le "name"
		
		for (Parameter param: parameters)
		{
			if (!includeUserBoundParameters && param.getDescription().isUserBound())
				continue;
			prg.addContent(param.toXML());
		}
		
		return prg;
	}
	
	/**
	 * Returns the description corresponding to the receiver.<br/>
	 * The component usually caches the description returned by {@link RemoteComponents#resourceRepository()};
	 * however, this caching behaviour is disabled when the resources' repository
	 * {@link RemoteComponents#cacheResourceRepositoryAnswers disables its cache}.<br/>
	 * Please note that in a standard, normal praxis project, different processes (GUI, or when building a workflow
	 * from XML) expect that the receiver always returns the same object --see
	 * {@link RemoteComponents#cacheResourceRepositoryAnswers} for details.
	 * @return the description attached to this program.
	 */
	public ProgramDescription getDescription()
	{
		/** see documentation on 'description' for details about this caching behaviour */
		if (!RemoteComponents.remoteComponent.cacheResourceRepositoryAnswers)
			return RemoteComponents.resourceRepository().programDescriptionForId(prg_description_id);
		if (description == null)
			description = RemoteComponents.resourceRepository().programDescriptionForId(prg_description_id);
		return description;
	}
	
	/* Getters and setters */

	/**
	 * @return the workflow
	 */
	public Workflow getWorkflow()
	{
		return workflow;
	}
	
	/**
	 * @param workflow
	 *            the workflow to set
	 * @throws NullPointerException if {@code workflow} is {@code null}.
	 */
	public void setWorkflow(Workflow workflow) throws NullPointerException
	{
		if (workflow == null)
			throw new NullPointerException("Illegal null workflow");
		Workflow _workflow = this.workflow;
		this.workflow = workflow;
		propChgSupport.firePropertyChange("workflow", _workflow, workflow);
	}
	
	/**
	 * Returns the id assigned to this program within its workflow
	 * @throws NullPointerException
	 *             if the {@link #getWorkflow() workflow is null}
	 */
	public String getProgramID() throws NullPointerException
	{
		return workflow.getIdForProgram(this);
	}
	
	/**
	 * Returns the annotation attached to this object; this is a field left to the user's disposal
	 * @return the optional annotation, it may be null
	 */
	public String getAnnotation()
	{
		return annotation;
	}
	
	/**
	 * Sets the optional annotation for this object; this is a field left to the user's disposal
	 * @param annotation
	 *            the new annotation; it may be null or empty
	 */
	public void setAnnotation(String annotation)
	{
		String _annotation = this.annotation;
		if (annotation.equals(""))
			this.annotation = null;
		else
			this.annotation = annotation;
		propChgSupport.firePropertyChange("annotation", _annotation, annotation);
	}
	
	/**
	 * Returns the x-coordinate of the program's graphical position.
	 * @return coordinate on the x-axis
	 */
	public int getX()
	{
		return x;
	}
	
	/**
	 * Sets the x-coordinate of the program's graphical representation.
	 * @param x
	 *            x-coordinate.
	 */
	public void setX(int x)
	{
		int _x = this.x;
		this.x = x;
		propChgSupport.firePropertyChange("x", _x, x);
	}
	
	/**
	 * Returns the y-coordinate of the program's graphical representation.
	 * @return the y-coordinate
	 */
	public int getY()
	{
		return y;
	}
	
	/**
	 * Sets the y-coordinate of the program's graphical representation.
	 * @param y
	 *            y-coordinate
	 * @see #getY()
	 */
	public void setY(int y)
	{
		int _y = this.y;
		this.y = y;
		propChgSupport.firePropertyChange("y", _y, y);
	}
	
	/** */
	public void setXYCoordinates(int x, int y)
	{
		setX(x);
		setY(y);
	}
	
	/**
	 * Returns the ExecutionSet this activity belongs to. Execution sets are built when the execution is planned.<br/>
	 * Activities are assigned to or removed from an execution set by the sets themselves, see
	 * {@link ExecutionSet#add(eu.telecom_bretagne.praxis.core.workflow.Program)} and {@link ExecutionSet#remove(Object)}.
	 * @return the execution set this activity belongs to
	 */
	public ExecutionSet getExecutionSet()
	{
		return set;
	}
	
	
	public void setExecutionSet(ExecutionSet aSet)
	{
		this.set = aSet;
	}
	
	/**
	 * Tells whether the program is ready to be executed. This is the case when all its {@link Parameter#isActivated()
	 * activated} parameters values are {@link Parameter#hasaValidValue() valid}, and when all its inputs are connected.
	 * 
	 * @return {@code true} if the program is ready to be executed, {@code false} otherwise.
	 * @see Workflow#isReadyForExecution()
	 */
	public boolean isReadyForExecution()
	{
		if (this.getDisconnectedInputFiles().size() != 0)
			return false;
		for (Parameter parameter: parameters)
			if (parameter.isActivated() && !parameter.hasaValidValue())
				return false;
		return true;
	}
	
	/**
	 * Sets this program's workflow to null, and {@link Parameter#invalidate() invalidates} all of its parameters.
	 * @see Workflow#deleteProgram(Program)
	 */
	public void invalidate()
	{
		for (Parameter parameter: parameters)
		{
			parameter.invalidate();
		}
		workflow = null;
	}
	
	public Parameter getParameterForDescription(ParameterDescription paramDesc)
	{
		for (Parameter param: parameters)
			if (param.getDescription() == paramDesc) /* we really mean '==', not equals() */
				return param;
		
		return null;
	}
	
	/**
	 * TODO: add big fat warning, do not use, for GUI only
	 * @see #applyChanges(Program, Map)
	 */
	public Program copyForParamDialog()
	{
		Program clone = null;
		try
		{
			clone = (Program) this.clone();
		}
		catch (CloneNotSupportedException e)
		{ /* ignored, won't happen */}
		
		clone.parameters = new ArrayList<Parameter>();
		for (Parameter p: parameters)
		{
			/* Only include parameters that can be displayed, nothing more */
			/* WARNING: applyChanges() needs inputs & outputs: they should be copied even if they are not displayed */
			final ParameterDescription paramDescription = p.getDescription();
			if ( paramDescription.isHidden() )
				continue;
			final ParameterDescription.ParameterType type = paramDescription.getType();
			if ( type == ParameterType.CODE || type == ParameterType.COMMAND)
				continue;
			clone.parameters.add(p.shallowCopyFor(clone));
		}
		return clone;
	}
	
	/**
	 * Apply the settings stored in the supplied program to the receiver. <br/>
	 * This method is normally used in conjonction with {@link #copyForParamDialog()}; for example, when a GUI dialog
	 * is opened to modify the settings of a program's parameters, it first copies the program and make the
	 * modifications on the copy. When it comes to apply the new settings, the copied version is sent back to the
	 * original program using this method, which sets its own parameters according to the values set in the copied
	 * one.<br/>
	 * <br/>
	 * The method applies changes to "regular" parameters only, i.e. to parameters that are not input or output
	 * parameters. When applying the changes, some input and/or output parameters may be
	 * {@link Parameter#isActivated() deactivated}; when a i/o parameter is deactivated, the method takes care of
	 * removing the corresponding link. The removed links are returned in a map, which can be later supplied to the
	 * method again using parameter <code>linksToRestore</code>. This makes it possible to revert the changes, calling
	 * the method with a copy of the original program along with the map that was returned when applyChanges() was
	 * called the first time.
	 * @param newSettingsForProgram
	 *            a Program object containing the settings to apply to the receiver.
	 * @param linksToRestore
	 *            a map corresponding to the links that should be restored, as returned by a previous invocation of
	 *            applyChanges().
	 * @return a map corresponding to the links that have been invalidated by applying the changes. This dictionary
	 *         maps a i/o parameter id with its associated object (either an other parameter, or a
	 *         {@link WorkflowInput}.
	 * @throws IllegalArgumentException
	 *             if the supplied program's {@link #getDescription() description} is not equal to this program's
	 *             description.
	 */
	public Map<String, Object> applyChanges(Program newSettingsForProgram, Map<String, Object> linksToRestore)
	{
		if (!this.getDescription().equals(newSettingsForProgram.getDescription()))
			throw new IllegalArgumentException(
			                                   "Supplied program should have the same description as the one supplied when this object was created");
		
		/* 1st, let's find out the input and outputs that will be deactivated when applying changes */
		List<Parameter> ioParameters = new ArrayList<Parameter>();
		ioParameters.addAll(getInputParameters());
		ioParameters.addAll(getOutputParameters());
		HashMap<String, Object> originalLinks = new HashMap<String, Object>();
		
		for (Parameter before: ioParameters)
		{
			if (!before.isActivated())
				continue;
			Parameter after = newSettingsForProgram.getParameterWithID(before.ref_id);
			if ( after == null)
				continue;
			if (after.isActivated())
				continue;
			/*
			 * - before is currently activated, and it will be deactivated after changes are applied;
			 * - if it is not connected, it's no use saving this non-information;
			 * - if it is connected, save the information and  disconnect it.
			 */
			if (before.getInput() != null)
			{
				originalLinks.put(before.ref_id, before.getInput());
				before.setInput(null);
			}
			else
				if (before.getPrgInput() != null)
				{
					originalLinks.put(before.ref_id, before.getPrgInput());
					before.setPrgInput(null);
				}
				else
					if (before.getOutputs().length > 0)
					{
						originalLinks.put(before.ref_id, before.getOutputs());
						for (Parameter output: before.getOutputs())
							before.removeOutput(output);
					}
		}
		
		/* 2nd, apply changes */
		for (Parameter copied_param: newSettingsForProgram.getParameters())
		{
			
			ParameterType parameterType = copied_param.getDescription().getType();
			
			Parameter param = getParameterWithID(copied_param.ref_id);
			
			if (parameterType.equals(ParameterType.ENUM))
				param.setItem(copied_param.getItem());
			
			if (parameterType.equals(ParameterType.INT) || parameterType.equals(ParameterType.FLOAT)
			    || parameterType.equals(ParameterType.STRING) || parameterType.equals(ParameterType.TEXT)
			    || parameterType.equals(ParameterType.BOOLEAN))
			{
				param.setData(copied_param.getData());
			}
		}
		
		propChgSupport.firePropertyChange(null, null, null);
		
		/* 3rd, restore the links as requested */
		if (linksToRestore != null)
		{
			for (Entry<String, Object> entry: linksToRestore.entrySet())
			{
				Parameter p = getParameterWithID(entry.getKey());
				Object value = entry.getValue();
				if (p.isInput())
				{
					if (value instanceof WorkflowInput)
						p.setInput((WorkflowInput) value);
					else
						p.setPrgInput((Parameter) value);
				}
				else
				{ // this is an output
					for (Parameter input: (Parameter[]) value)
						p.addOutput(input);
				}
			}
		}
		return originalLinks;
	}
	
	@Override
	public String toString()
	{
		return "prg:" + getProgramID() + "(" + getDescription().fullName() + ")";
	}
	
	
	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		propChgSupport.addPropertyChangeListener(listener);
	}
	
	public PropertyChangeSupport getListeners()
	{
		return propChgSupport;
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		propChgSupport.removePropertyChangeListener(listener);
	}
}
