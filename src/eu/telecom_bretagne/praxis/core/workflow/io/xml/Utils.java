/**
 * 
 */
package eu.telecom_bretagne.praxis.core.workflow.io.xml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * @author Sébastien Bigaret
 */
public interface Utils
{
    public static String getPCDATA(final StartElement startElement, XMLEventReader eventReader)
    throws XMLStreamException
    {
        String pcdata = null;
        while ( eventReader.hasNext() )
        {
            XMLEvent event = eventReader.nextEvent();
            if ( event.isEndElement() )
                if ( startElement.getName().getLocalPart().equals(event.asEndElement().getName().getLocalPart()) )
                    break;
            if ( !event.isCharacters() )
                continue;
            // We have multiple characters, like in PCDATA+comment+PCDATA, we only select PCDATA and concatenate them
            if ( pcdata == null )
                pcdata = "";
            pcdata += event.asCharacters().getData();
        }
        return pcdata;
    }


}
