/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jdom.Attribute;
import org.jdom.CDATA;
import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.Base64Coder;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult.ProgramStatus;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;


/**
 * The Result class represents gathers all information about an execution, from the time the execution starts to its
 * end.
 * @author Sébastien Bigaret
 */
public class Result
    implements Serializable
{
	private static final long serialVersionUID = -4330880835412944366L;

	/**
	 * The {@code Status} enumeration represents the different status of a workflow during its execution.
	 */
	public enum Status
	{
		/** Used to indicate that an execution has not been started yet */
		NOT_STARTED("eu.telecom_bretagne.praxis.core.execution.Result.Status.NOT_STARTED"),
		/** Used to indicate that an execution has been started */
		RUNNING("eu.telecom_bretagne.praxis.core.execution.Result.Status.RUNNING"),
		/** Used to indicate that an execution is suspended */
		SUSPENDED("eu.telecom_bretagne.praxis.core.execution.Result.Status.SUSPENDED"),
		/** Used to indicate that an execution was successful */
		OK("eu.telecom_bretagne.praxis.core.execution.Result.Status.OK"),
		/** Used to indicate that an execution was successful, but that some facts require the user's attention */
		WARNING("eu.telecom_bretagne.praxis.core.execution.Result.Status.WARNING"),
		/** Used to indicate that an execution failed */
		ERROR("eu.telecom_bretagne.praxis.core.execution.Result.Status.ERROR"),
		/** Used to indicate that the execution was cancelled */
		CANCELLED("eu.telecom_bretagne.praxis.core.execution.Result.Status.CANCELLED"),;
		
		private String I18N_msg;
		Status(String i18n)
		{
			I18N_msg = i18n;
		}
		
		/**
		 * Returns the id of the {@link eu.telecom_bretagne.praxis.common.I18N common.I18N} message associated to this
		 * object
		 */
		public String getI18N()
		{
			return I18N_msg;
		}

		/**
		 * Indicates whether the execution to which the status is attached is currently running.<br>
		 * The following status are said to be active: {@link #NOT_STARTED}, {@link #SUSPENDED} and {@link #RUNNING}.
		 * @return {@code true} if the status is active.
		 */
		public boolean isActive()
		{
			return this.equals(NOT_STARTED) || this.equals(SUSPENDED) || this.equals(RUNNING);
		}

		/**
		 * Indicates whether the execution to which the status is attached is finished.<br>
		 * This is equivalent to: {@code !isActive()}.
		 * @return {@code true} if the status is closed.
		 * @see #isActive()
		 */
		public boolean isClosed()
		{ 
			return !isActive();
		}
		
		public ProgramStatus programStatusForActiveStatus()
		{
			if (!this.isActive())
				throw new IllegalArgumentException("Illegal status, only active status are allowed ("+this+")");
			ProgramStatus prgStatus = ProgramStatus.UNKNOWN;
			switch (this)
			{
				case NOT_STARTED:
				case SUSPENDED:
					prgStatus = ProgramStatus.UNSET;
					break;
				case RUNNING:
				default: // just to keep the compiler happy, and remove warning, but we've covered all possible cases here
					prgStatus = ProgramStatus.RUNNING;
					break;
			}
			return prgStatus;
		}
	}
	
	/**
	 * A comparator used to sort Results by creation date. Please note that you will probably sort arrays of results
	 * having the same workflowID: this comparator does not check whether the results have the same workflowID and it
	 * uses the creation date <b>only</b> to sort the elements.
	 */
	public static Comparator<Result> DateComparator = new Comparator<Result>()
	{
		public int compare(Result r1, Result r2)
		{
			return r1.creationDate.compareTo(r2.creationDate);
		}
	};

	protected WorkflowID                        workflowID;
	
	protected ExecutionID                       executionID;
	
	protected byte[]                            workflowXML      = null;
	
	public String                               key;
	
	/** A data container that can be used for any purpose */
	public Serializable                         data;

	/** The name is a short description of a result */
	protected String name;

	/**
	 * The set of files generated by the program(s), gathered in a zip. <br/>
	 * @see #readyToSerialize()
	 */
	protected File                              zipFile;
	
	protected Status                            status           = Status.NOT_STARTED;
	
	/**
	 * The moment when the object was created.
	 */
	protected Date                              creationDate     = new Date();
	
	/**
	 * The date attached to the element. See {@link #getDate()} for details.
	 */
	protected Date                              date             = creationDate;

	/** The programs associated to this object. */
	protected Map<String, ProgramResult> programs         = new HashMap<String, ProgramResult>();

	/**
	 * Used to make sure that {@link #readyToSerialize()} is called before serialization is triggered --see
	 * {@link #writeObject(ObjectOutputStream)} (doc.&code) for details.
	 */
	private boolean                             readyToSerialize = false;
	
	/**
	 * Used as a marker to distinguish between several version of {@link Result} and {@link ProgramResult}.<br>
	 * <ul>
	 * <li>Version 0: initial version;</li>
	 * <li>Version 1: ProgramStatus was introduced, major changes includes: start, end, and outputs registered in
	 * ProgramStatus.</li>
	 * </ul>
	 * The framework uses this version number to be able to read and interpret all results that have been produced in
	 * the past.
	 */
	private int                                 version          = 1;

	/**
	 * Creates a new Result object. Its state is {@link Status#NOT_STARTED} at creation time.
	 * @param workflowID
	 *            the workflow id
	 * @param execID
	 *            identification for the execution for which this objet holds the results
	 */
	public Result(WorkflowID workflowID, ExecutionID execID)
	{
		this.workflowID = workflowID;
		this.executionID = execID;
	}
	
	/**
	 * Recreates a Result object from its xml serialization.
	 * @param xmlFile
	 *            the file where the result object was serialized to.
	 * @throws InvalidXMLException
	 *             if the xml file is not valid
	 * @see {@link #fromXML(Element)}
	 */
	public Result(File xmlFile) throws InvalidXMLException
	{
		fromXML(Facade_xml.read(xmlFile).detachRootElement());
	}
	
    public Result(Result result)
    {
    	this(result.workflowID, result.executionID);
    	this.data = result.data;
    	this.key = result.key;
    	this.creationDate = (Date) result.date.clone();
    	this.date = (Date) result.date.clone();
    	this.programs = new HashMap<String, ProgramResult>();
    	this.status = result.status;
    	this.workflowXML = result.workflowXML;
    	this.zipFile = result.zipFile;
    	for (String prgID: result.programs.keySet())
    	{
    		this.programs.put(prgID, new ProgramResult(result.programs.get(prgID)));
    	}
    }
    
	/**
	 * Merge this object with the one supplied. The informations contained within the maps that are specific to
	 * individual programs are simply merged; the resulting status depends on the objects' statuses, as shown here:
	 * 
	 * <pre>
	 *     | OK   WARN ERR   CAN (this)
	 * ----+---------------------
	 * OK  | OK   WARN ERR   CAN
	 * WARN| WARN WARN ERR   CAN
	 * ERR | ERR  ERR  ERR   CAN
	 * CAN | CAN  CAN  CAN   CAN
	 * </pre>
	 * 
	 * <b>Warning:</b> this method is intended for internal use mostly; if you want to use it, look at the code of
	 * this method <b>and</b> the code of {@link ProgramResult#updateWith(ProgramResult)} (which is
	 * protected, yes) and see if it fits your needs. In particular, be aware that it may overwrite valuable
	 * informations if not called under precise control. <br>
	 * Note that if this object's status is NOT_STARTED, the resulting status will be aResult's status: this way, a
	 * brand new Result object act as a neutral object when an other one is merged with him. But be careful! the
	 * opposite is false, you cannot merge an NOT_STARTED result into another result. In other words, when calling
	 * mergeWith(), the receiver may be NOT_STARTED but the parameter shouldn't.<br>
	 * Note that it is also illegal to merge two results that were not built with the same WorkflowID.
	 * @param aResult
	 *            the results to merge into this object. Its status should not be {@link Status#NOT_STARTED}.
	 * @throws IllegalArgumentException
	 *             when <code>aResult.status==NOT_STARTED</code> or when the two objects do not have the same workflow id
	 */
	public synchronized void mergeWith(Result aResult)
	{
		mergeWith(aResult, true);
	}
	
	public synchronized void mergeWith(Result aResult, boolean updateStatus)
	{
		if (!aResult.workflowID.equals(this.workflowID))
			throw new IllegalArgumentException("Cannot merge two results with a different workflowID");
		if (updateStatus && aResult.status == Status.NOT_STARTED)
			throw new IllegalArgumentException("Cannot merge a result with an NOT_STARTED status");
		
		for (ProgramResult prgInfo: aResult.programs.values())
		{
			this.getProgramInfo(prgInfo.id, true).updateWith(prgInfo);
		}
		
		this.date = new Date();
		
		if (!updateStatus)
			return;

		if (this.status == Status.NOT_STARTED || this.status == Status.RUNNING)
		{
			this.status = aResult.status;
			return;
		}
		if ((aResult.status == Status.OK) || (this.status == aResult.status) || (this.status == Status.CANCELLED))
			return; // keep this.status
				
		if (this.status == Status.OK) {
			this.status = aResult.status;
			return;
		}
		if (aResult.status == Status.CANCELLED)
		{
			this.status = Status.CANCELLED;
			return;
		}
		// the only 2 cases left are this:WARN,result=ERR or vice-versa
		this.status = Status.ERROR;
	}
	
	/**
	 * Returns the execution ID the results stored in this object are associated to.
	 */
	public ExecutionID executionID()
	{
		return executionID;
	}
	
	/**
	 * Returns the workflow ID the results stored in this object are associated to.
	 */
	public WorkflowID workflowID()
	{
		return workflowID;
	}
	
	/**
	 * Returns the workflow whose execution's results are summarized in this object.
	 * @return the workflow.
	 */
	public byte[] getWorkflowXML()
	{
		return workflowXML;
	}
	
	/**
	 * Sets the workflow whose execution's results are summarized in this object.
	 */
	public void setWorkflowXML(byte[] xml)
	{
		this.workflowXML = xml;
	}
	
	/**
	 * Sets the workflow whose execution's results are summarized in this object, and resets its workflow ID.<br>
	 * <b>WARNING:</b>In almost every situations, the result's workflow ID supplied at creation time is <b>not</b>
	 * supposed to be modified.  Use only if you know what you're doing.
	 */
	public void setWorkflowXML(WorkflowID workflowID, byte[] xml)
	{
		this.workflowID = workflowID;
		this.workflowXML = xml;
	}

	/**
	 * Returns the status of the execution related to this object.
	 * @return the status of the execution.
	 */
	public Status getStatus()
	{
		return status;
	}
	
	/**
	 * Sets the status of the execution related to this object.
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(Status status)
	{
		this.status = status;
		this.date = new Date();
	}
	
	/**
	 * Returns the creation date of this object. It is the date when the execution was decided by a user; it is
	 * different from the moment where the 1st execution of a workflow's program actually begins.
	 * @return the creation date of the object.
	 */
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	/**
	 * The date attached to the element. Its meaning depends on the status:
	 * <ul>
	 * <li>if the status is {@link Status#isActive() active}: it is the last time the status has been set,</li>
	 * <li>if the status is {@link Status#isClosed() closed}: it is the moment when the execution was marked as
	 * finished.</li>
	 * </ul>
	 * @return the date of the last update of this object's status.
	 */
	public Date getDate()
	{
		return date;
	}

	/**
	 * Returns the name that was assigned to this result.
	 * 
	 * @return the name of the result; it may be {@code null} or empty.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Sets the name of this object. A result's name is a short description supplied by the user.
	 * 
	 * @parameter name the name to assign to this result
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/* --- programs' info --- */
	/**
	 * Returns the ProgramInfo object registered for the supplied id.
	 * @param prgID
	 *            the ID of the program for which information is requested.
	 * @return the ProgramInfo associated to {@code prgID}, or {@code null} if no such object exists.
	 */
	public ProgramResult getProgramInfo(String prgID)
	{
		return programs.get(prgID);
	}
	
	/**
	 * Returns the ProgramInfo object registered for the supplied id. If there is no such object, one is created and
	 * registered before being returned.
	 * @param prgID
	 *            the ID of the program for which information is requested.
	 * @param create
	 *            if {@code true}, a new ProgramInfo is created and registered in case none is registered with the
	 *            supplied ID.
	 * @return the ProgramInfo associated to {@code prgID}. If the Result object has no such object: it returns
	 *         {@code null} if {@code create} is {@code false}, or it creates, registers and returns a new ProgramInfo
	 *         object if {@code create} is {@code true}.
	 */
	public ProgramResult getProgramInfo(String prgID, boolean create)
	{
		ProgramResult prgInfo = programs.get(prgID); //TODO NON null or empty
		if (prgInfo == null && create)
		{
			prgInfo = new ProgramResult(prgID);
			programs.put(prgID, prgInfo);
		}
		return prgInfo;
	}

	/**
	 * Returns the IDs of the programs registered within this object.
	 * @return the IDs of all registered programs.
	 */
	public String[] getProgramIDs()
	{
		return programs.keySet().toArray(new String[programs.size()]);
	}
	
	/**
	 * Sets some information for the supplied program. If some information has already been stored for the same
	 * program and key, it is replaced by the one supplied here.<br>
	 * The methods always {@link #getProgramInfo(String, boolean) creates and registers} a new ProgramInfo if none is
	 * registered with the supplied {@code prgID}.
	 * @param prgID
	 *            ID of a program
	 * @param key
	 *            the key under which the information should be stored in this object
	 * @param value
	 *            the information to be set
	 */
	public void setResultForPrg(String prgID, String key, String value)
	{
		getProgramInfo(prgID, true).put(key, value);
	}
	
	/**
	 * Appends some information for the supplied program.<br>
	 * The methods always {@link #getProgramInfo(String, boolean) creates and registers} a new ProgramInfo if none is
	 * registered with the supplied {@code prgID}.
	 * @param prgID
	 *            ID of a program
	 * @param key
	 *            the key under which the information should be stored in this object
	 * @param value
	 *            the additional information to be added
	 * @see ProgramResult#add(String, String)
	 */
	public void addInfoForPrg(String prgID, String aKey, String aValue)
	{
		getProgramInfo(prgID, true).add(aKey, aValue);
	}

	/* --- execution status --- */
	/**
	 * Returns the execution status associated to a program.
	 * @param prgID
	 *            ID of a program
	 * @return the corresponding program's exec. status
	 * @see #setExecStatus(String, ProgramStatus)
	 */
	public ProgramStatus getExecStatus(String prgID)
	{
		return programs.get(prgID).status;
	}
	
	/**
	 * Sets the execution status for the supplied program.<br>
	 * The methods always {@link #getProgramInfo(String, boolean) creates and registers} a new ProgramInfo if none is
	 * registered with the supplied {@code prgID}.<br>
	 * Equivalent to {@code getProgramInfo(prgID, true).status = programStatus}.
	 * @param prgID
	 *            ID of a program
	 * @param programStatus
	 *            the status to be set
	 * @see #getExecStatus(String)
	 */
	public void setExecStatus(String prgID, ProgramStatus programStatus)
	{
		getProgramInfo(prgID, true).status = programStatus;
	}
	
	/**
	 * Sets the zip file
	 * @param zipFile
	 */
	public synchronized void setZipFile(File zipFile)
	{
		this.zipFile = zipFile;
	}
	
	/**
	 * Returns a pointer to the zipped archive of all files produced by an execution
	 * @param zipFile
	 */
	public synchronized File zipFile()
	{
		return zipFile;
	}
	
	/**
	 * Informs the objet that serialisation can happen. If not called prior to serialisation, the object will refuse
	 * to be serialized; this prevents code from accidentally sending the object through serialization without
	 * handling its internal zipFile as well.
	 */
	public synchronized void readyToSerialize()
	{
		this.readyToSerialize = true;
	}
	
	public void dumpContent(File directory)
	{
		for (String prg_id: getProgramIDs())
		{
			try {
				// TODO harcoded dir/prg_id/: remove it! The way files are stored is decided by the exec.engine, it should not be there!
				File prg_dir = new File(directory, prg_id);
				if (!prg_dir.exists() && !prg_dir.mkdirs())// if the prg failed, the directory does not exist
					continue;
				PrintStream out = new PrintStream(new File(prg_dir, prg_id+".exec_trace"));
				ProgramResult prgInfo = getProgramInfo(prg_id);
				for (String _key: prgInfo.keys())
				{
					String line ="";
					for (int i=0; i<_key.length(); i++)
						line += "-";
					out.println(line);
					out.println(_key);
					out.println(line);
					out.println(prgInfo.get(_key));
					out.println();
				}
				out.flush();
				out.close();
			}
			catch (IOException ioe)
			{
				ioe.printStackTrace();
			}
		}
	}
	
	/**
	 * Makes this object a shallow copy of the supplied result. The attributes' values (including the transient
	 * fields' values, but <b>excluding</b> the creationDate) of the object are simply assigned to this object's attribute: the attributes' values are
	 * <b>not</b> cloned.
	 * @param result
	 *            the result whose attributes' values will be copied to this object
	 */
	public void shallowCopy(Result result)
	{
		this.date = result.date;
		this.name = result.name;
		this.executionID = result.executionID;
		this.data = result.data;
		this.key = result.key;
		this.programs = result.programs;
		this.readyToSerialize = result.readyToSerialize;
		this.status = result.status;
		this.workflowID = result.workflowID;
		this.workflowXML = result.workflowXML;
		this.zipFile = result.zipFile;
	}
	
	private void readObject(ObjectInputStream in) throws IOException
	{
		//in.defaultReadObject();
		try
        {
			Element e = ( (Document) in.readObject() ).detachRootElement();
	        this.fromXML(e);
        }
        catch (InvalidXMLException | ClassNotFoundException e)
        {
	        throw new IOException("Unable to deserialize the result", e);
        }
		if (creationDate==null)
			creationDate = date;
	}
	/**
	 * Serialize the object onto the stream, see {@link Serializable} for details. This object will refuse
	 * serialization if its {@link #zipFile} is set and {@link #readyToSerialize()} has not been called.
	 * @param out
	 * @throws NotSerializableException
	 *             if readyToSerialize() has not been called prior to serialization when the objet's {@link #zipFile}
	 *             is set
	 */
	private void writeObject(ObjectOutputStream out) throws IOException
	{
		synchronized(this)
		{
			try
			{
				if (this.zipFile != null && !readyToSerialize)
					throw new java.io.NotSerializableException();
				//out.defaultWriteObject();
				out.writeObject(toXMLDocument());
			}
			finally
			{
				readyToSerialize = false;
			}
		}
	}
	
	/**
	 * Serializes this object into an XML Document.
	 * @return the XML Document representing the object.
	 * @see #toXML()
	 */
	public Document toXMLDocument()
	{
		return new Document(toXML());
	}
	
	/**
	 * Helper function encoding the supplied {@code value} in base64 if needed before putting it as a CDATA content in
	 * the provided {@code element}. It also adds an attribute {@code "base64_encoded"} which is {@code "true"} or
	 * {@code "false"} depending on whether the value has been base 64 encoded or not.
	 * @param element
	 *            the element in which the value should be put, possibly base64 encoded.
	 * @param value
	 *            the xml Element which receives the value.
	 * @see #decodeXMLText(Element)
	 */
	protected static void encodeXMLText(Element element, String value)
	{
		boolean base64 = false;
		if (org.jdom.Verifier.checkCDATASection((value))!=null)
		{
			value = Base64Coder.encodeString(value);
			base64 = true;
		}
		element.setAttribute("base64_encoded", Boolean.toString(base64));
		element.setContent(new org.jdom.CDATA(value));
	}
	
	/**
	 * Helper method taking an element and returning its text content. It searches the element for an attribute
	 * {@code "base64_encoded"}; if it exists and is equal to {@code "true"} the value is base64-decoded before being
	 * returned.<br>
	 * This method is the inverse of {@link #encodeXMLText(Element, String)}.
	 * @param element
	 *            the (non-{@code null}) element whose text content should be returned.
	 * @return the text content of the element.
	 * @throws IllegalArgumentException
	 *             if {@code element} has text content that could not be decoded (applicable only if attribute {@code
	 *             "base64_encoded"} exists and is {@code "true"}).
	 * @throws NullPointerException
	 *             if the element is {@code null}.
	 */
	protected static String decodeXMLText(Element element) throws IllegalArgumentException, NullPointerException
	{
		String item_value = element.getText();
		final Attribute base64_encoded = element.getAttribute("base64_encoded");
		if ( base64_encoded!=null && "true".equals(base64_encoded.getValue()) )
			item_value = Base64Coder.decodeString(item_value);
		return item_value;
	}

	/**
     * Builds an XML element representing the object
     * @return the XML element representing the object
     * @see #fromXML(Element)
     */
    public Element toXML()
    {
        Element resultXML = new Element("result");
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    	
    	resultXML.setAttribute("version", ""+version);
        resultXML.addContent(new Element("creationDate")
                             .setText(""+creationDate.getTime())
                              // add an human-readable attribute date, but it will be ignored when deserializing
                             .setAttribute("date", dateFormat.format(creationDate)));

    	resultXML.addContent(new Element("date")
                             .setText(""+date.getTime())
                              // add an human-readable attribute date, but it will be ignored when deserializing
                             .setAttribute("date", dateFormat.format(date)));
    	if (name!=null)
    		resultXML.addContent(new Element("name").setText(name));

        resultXML.addContent(new Element("workflowID").setText(workflowID.dumpID()));
        resultXML.addContent(new Element("executionID").setText(executionID.dumpID()));
        resultXML.addContent(new Comment("The global status"));
        resultXML.addContent(new Element("status").setText(status.name()));
        if (key!=null && !"".equals(key))
        {
        	resultXML.addContent(new Comment("key is for internal use"));
        	resultXML.addContent(new Element("key").setText(key));
        }

        Element zipFileXML =  new Element("zipFile");
        File parent = zipFile;
        while ( parent != null )
        {
        	zipFileXML.addContent(0, new Element("path").setText(parent.getName()));
        	parent = parent.getParentFile();
        }
        resultXML.addContent(zipFileXML);
        
        resultXML.addContent(new Comment("The detailed status, by program\nEvery value that is not made of ASCII " +
                                         "printable characters only is base64-encoded"));
        Element prgStatusXML = new Element("programs_status");
        resultXML.addContent(prgStatusXML);
        
        /*
         * Now iterate on prg_exec_status: it is always updated before any individual results are set. Iterating on
         * prg_individual_results results in potential loss of informations: even if this is equivalent for a Result
         * object representing a completed execution, prg_individual_results may have partial information, or no
         * information at all, in a Result object representing a running execution. But in the case of a running
         * execution, the Result object always gets info. on the individual prgs' statuses *before* any individual
         * results are set.
         * One case where this is important is when the execution engine sends information on a running execution:
         * it uses Result objects for that purpose, and the serialization mechanism is based on the XML serialization
         * one.
         */
        for (ProgramResult prgInfo: programs.values())
        {
        	Element prgXML = new Element("program").setAttribute("id", prgInfo.id);
        	prgXML.setAttribute("status", prgInfo.status.name());
        	if (prgInfo.start != null)
        		prgXML.setAttribute("start", ""+prgInfo.start.getTime());
        	if (prgInfo.end != null)
        		prgXML.setAttribute("end",   ""+prgInfo.end.getTime());
        	prgStatusXML.addContent(prgXML);
        	write_results:
        	{
        		if (prgInfo.additional_info==null)
        			break write_results;
        		for (String _key: prgInfo.keys())
        		{
        			if ( prgInfo.get(_key)==null )
        			{
        				// ignore any null key, but log them: they come from a buggy behaviour // TODO loglevel bug
        				Log.log.warning(()->"Got a null value for key:"+_key+", this should not have happened");
        				continue;
        			}
        			Element xmlValue = new Element("value");
        			encodeXMLText(xmlValue, prgInfo.get(_key));
        			prgXML.addContent(new Element("item").setAttribute("key", _key).addContent(xmlValue));
        		}
        	}
        	write_outputs:
        	{
        		if (prgInfo.outputs==null)
        			break write_outputs;
        		for (String paramID: prgInfo.outputs.keySet())
        		{
        			Element xmlOutput = new Element("output").setAttribute("parameterID", paramID);
        			for (String pathElement: prgInfo.getOutputFileComponents(paramID))
        			{
            			Element xmlPathElement = new Element("pathElement");
            			encodeXMLText(xmlPathElement, pathElement);
            			xmlOutput.addContent(xmlPathElement);
        			}
        			prgXML.addContent(xmlOutput);
        		}
        	}
        }

        if (workflowXML!=null)
        {
        	resultXML.addContent(new Comment("The executed workflow corresponding to this result (base64-encoded if needed)"));
        	final Element workflow_element = new Element("workflow");
       		encodeXMLText(workflow_element, new String(workflowXML, Charset.forName("UTF-8")));
        	resultXML.addContent(workflow_element);
        }

        if ( data != null )
        {
        	resultXML.addContent(new Comment("Data container, serialized and base64 encoded"));
        	try {
        		resultXML.addContent(new Element("data").addContent(new CDATA(new String(Base64Coder.encode(Utile.serialize(data))))));
            } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
        }
        //this.prg_individual_results = result.prg_individual_results;
        return resultXML;
    }
    
    public void fromXML(Element xml) throws InvalidXMLException
    {
		InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.RESULT);

		if ( !xml.getName().equalsIgnoreCase("result") )
			throw xml_exc.setMsg("root element is not result");
		
		try
		{
			version=Integer.parseInt(xml.getAttributeValue("version", "0"));
		}
		catch (NumberFormatException e)
		{
			throw xml_exc.setMsg("Invalid version");
		}
		// creationDate
		String s = xml.getChildText("creationDate");
		try
		{
			creationDate = new Date(Long.parseLong(s));
		}
		catch (NumberFormatException nfe)
		{
			throw xml_exc.setMsg("Invalid creationDate");
		}
		// date
		s = xml.getChildText("date");
		try
		{
			date = new Date(Long.parseLong(s));
		}
		catch (NumberFormatException nfe)
		{
			throw xml_exc.setMsg("Invalid date");
		}
		// name
		name = xml.getChildText("name");
		// workflowID
		s = xml.getChildText("workflowID");
		try
		{
			workflowID = new WorkflowID(s);
		}
		catch (Exception e)
		{
			throw xml_exc.setMsg("Invalid workflowID");
		}
		// executionID
		s = xml.getChildText("executionID");
		try
		{
			executionID = new ExecutionID(s);
		}
		catch (Exception e)
		{
			throw xml_exc.setMsg("Invalid executionID");
		}
		// status
		s = xml.getChildText("status");
		try
		{
			status = Status.valueOf(s);
		}
		catch (Exception e) // IllegalArgumentException && NullPointerException
		{
			throw xml_exc.setMsg("Invalid status");
		}
		// key
		key = xml.getChildText("key"); // may be null

		// zipFile
		if (xml.getChild("zipFile")!=null)
		{
			zipFile=null;
			for (Object path: xml.getChild("zipFile").getChildren("path"))
			{
				String _path = ((Element)path).getText();
				zipFile = new File( zipFile, _path ); // equiv. to File(_path) if zipFile==null
			}
		}
		
		// programs' information
		
		// if we're called from readObject(), default initialization of variables has not been done
		this.programs = new HashMap<String, ProgramResult>();
		
		Element prgsXML = xml.getChild("programs_status");
		if (prgsXML!=null)
		{
			for (Object _prg: prgsXML.getChildren("program"))
			{
				Element prg = (Element) _prg;
				String prgID = prg.getAttributeValue("id");
				if (prgID == null || "".equals(prgID))
					throw xml_exc.setMsg("Invalid prg id");

				ProgramResult prgInfo = getProgramInfo(prgID, true); // check it does not exist already??

				try	{ prgInfo.start = new Date(Long.parseLong(prg.getAttributeValue("start"))); }
				catch (NumberFormatException nfe) { /* use default: null */ }
				try	{ prgInfo.end = new Date(Long.parseLong(prg.getAttributeValue("end"))); }
				catch (NumberFormatException nfe) { /* use default: null */ }

				String prgStatus = prg.getAttributeValue("status");
				try
				{
					prgInfo.status = ProgramStatus.valueOf(prgStatus);
				}
				catch (Exception e) // IllegalArgumentException && NullPointerException
				{
					throw xml_exc.setMsg("Invalid status for prg: "+prgID);
				}
				for (Object _item: prg.getChildren("item"))
				{
					Element item = (Element) _item;
					String item_key = item.getAttributeValue("key");
					if (item_key == null || "".equals(item_key))
						throw xml_exc.setMsg("Invalid empty or absent key in prg: "+prgID);
					
					String item_value;
					try
					{
						item_value =  decodeXMLText(item.getChild("value"));
					}
					catch (IllegalArgumentException e)
					{
						throw xml_exc.setMsg("Invalid base64-encoded value in prg: "+prgID+ " for key: "+item_key);
					}
					catch (NullPointerException e)
					{
						throw xml_exc.setMsg("Invalid: absent value in prg: "+prgID+ " for key: "+item_key);
					}
					prgInfo.put(item_key, item_value);
				} // end for <item>
				for (Object _output: prg.getChildren("output"))
				{
					Element output = (Element) _output;
					String parameterID = output.getAttributeValue("parameterID");
					if (parameterID == null || "".equals(parameterID))
						throw xml_exc.setMsg("Invalid empty or absent parameterID in prg: "+prgID);
					// iterate on <pathElement/>
					ArrayList <String> path = new ArrayList<String>();
					for (Object _pathElement: output.getChildren("pathElement"))
					{
						Element pathElement = (Element) _pathElement;
						String p = null;
						try
						{
							p = decodeXMLText(pathElement);
						}
						catch (IllegalArgumentException e)
						{
							throw xml_exc.setMsg("Invalid base64-encoded value in prg: "+prgID+ " for output: "+parameterID);
						}
						catch (NullPointerException e)
						{
							throw xml_exc.setMsg("Invalid: absent value in prg: "+prgID+ " for output: "+parameterID);
						}
						path.add(p);
					}
					prgInfo.setOutputComponents(parameterID, path.toArray(new String[path.size()]));
				} // end for <output>
			} // end for <program>
		}
		// workflow
		final Element workflow_element = xml.getChild("workflow");
		if ( workflow_element == null || "".equals(workflow_element.getText()) )
			workflowXML = null;
		else
		{
			try
			{
				workflowXML = decodeXMLText(workflow_element).getBytes(Charset.forName("UTF-8"));
			}
			catch (IllegalArgumentException e)
			{
				throw xml_exc.setMsg("Invalid base64-encoded workflow");
			}
		}

		String _data = xml.getChildText("data");
		_data = (_data==null) ? null: _data.trim();
		if ( _data != null && !"".equals(_data) )
		{
			try
			{
				java.io.ByteArrayInputStream bais = new java.io.ByteArrayInputStream(Base64Coder.decode(_data));
				ObjectInputStream ois = new ObjectInputStream(bais);
				this.data = (Serializable) ois.readObject();
			}
            catch (IllegalArgumentException | IOException | ClassNotFoundException e)
            {
                xml_exc.setMsg("Invalid base64-encoded data").initCause(e);
                throw xml_exc;
            }
  		}
    }

    /**
     * Return the implementation version of this object
     * @return the version of the implementation used to build this object.
     * @see #version
     */
    public int getVersion()
    {
    	return version;
    }

	@Override
	public String toString()
	{
		String repr = "[Result";
		repr += " status:"+status;
		repr += " execID:"+executionID;
		repr += "{";
		for (String prgID: getProgramIDs())
			repr += prgID+":"+getExecStatus(prgID);
		repr += "}";
		repr += "]";
		return repr;
	}

	public boolean pointsToTheSameExecutionThan(Result result)
	{
		if (result==null)
			return false;
		return workflowID.equals(result.workflowID) && executionID.equals(result.executionID);
	}
}
