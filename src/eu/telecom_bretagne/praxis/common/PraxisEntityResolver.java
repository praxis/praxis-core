/* License: please refer to the file README.license located at the root directory of the project */
/*
 * Created on Apr 7, 2004
 */
package eu.telecom_bretagne.praxis.common;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;


/**
 * This is the entity resolver used within the project, to avoid unnecessary HTTP request to retrieve the distant DTD
 * when a local one is available. It is used by the {@link eu.telecom_bretagne.praxis.common.Facade_xml Facade_xml} when building jdom
 * Document from files.
 * @author Sébastien Bigaret
 * @see eu.telecom_bretagne.praxis.common.Facade_xml
 */
public class PraxisEntityResolver
    implements EntityResolver
{
	/**
	 * a private cache mapping a Public ID to the corresponding {@link URL}
	 */
	static private Map<String, URL> DTD_cache;
	
	/**
	 * Initializes {@link #DTD_cache the cache of DTDs} with InputSources for {@link XMLConstants#SC_DTD_PUBID},
	 * {@link XMLConstants#PROGRAM_DTD_PUBID}, {@link XMLConstants#PLATFORM_DTD_PUBID},
	 * {@link XMLConstants#HRCHY_DTD_PUBID} and {@link XMLConstants#RD_DTD_PUBID}
	 */
	static
	{
		try
		{
			DTD_cache = new HashMap<String, URL>();
			DTD_cache.put(XMLConstants.WORKFLOW_DTD_PUBID, FileResources.url(XMLConstants.WORKFLOW_DTD_FILE));
			DTD_cache.put(XMLConstants.WORKFLOW_DTD_PUBID_v2, FileResources.url(XMLConstants.WORKFLOW_DTD_FILE_v2));
			// Current DTD v3.1 can be used for descriptions using v3.0
			DTD_cache.put(XMLConstants.PROGRAM_DESCRIPTION_DTD_PUBID_v3_0, FileResources
					        .url(XMLConstants.PROGRAM_DESCRIPTION_DTD_FILE));
			DTD_cache.put(XMLConstants.PROGRAM_DESCRIPTION_DTD_PUBID_v3_1, FileResources
			        .url(XMLConstants.PROGRAM_DESCRIPTION_DTD_FILE));
			DTD_cache.put(XMLConstants.PLATFORM_DTD_PUBID, FileResources.url(XMLConstants.PLATFORM_DTD_FILE));
			DTD_cache.put(XMLConstants.HRCHY_DTD_PUBID, FileResources.url(XMLConstants.HRCHY_DTD_FILE));
			DTD_cache.put(XMLConstants.EXT_VIEWERS_DTD_PUBID, FileResources.url(XMLConstants.EXT_VIEWERS_DTD_FILE));
		}
		catch (FileResources.ResourceNotFoundException exc)
		{
			Log.log.log(Level.SEVERE, "DTD not available: exiting", exc);
			System.exit(-1); // TODO: exit? or common.exit() somehow?
		}
	}
	
	/**
	 * Registers a new DTD into the entity resolver.
	 * @param publicID the DTD's public ID
	 * @param url the URL the DTD can be obtained from
	 */
	public static void registerDTD(String publicID, URL url)
	{
		DTD_cache.put(publicID, url);
	}
	
	/**
	 * Returns the input source corresponding to the requested DTD if it belongs to the ones declared by praxis, or
	 * otherwise return null. The public IDs handled here are those declared in {@link eu.telecom_bretagne.praxis.common.XMLConstants}
	 * under constants ending with <code>_DTD_PUBID</code>
	 * @param publicId
	 *            the Public ID for the requested external entity
	 * @param systemId
	 *            the System ID for the requested external entity
	 * @return an InputSource describing the requested entity, or null if no such entity can be found among the one
	 *         declared by praxis.
	 * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
	 */
	public InputSource resolveEntity(String publicId, String systemId)
	// throws SAXException, IOException
	{
		Log.log.fine(()->"publicID" + publicId + " " + systemId);
		InputSource input_src = null;
		/*
		 * InputSources coming from decryptedStream() cannot be stored and used one after the other: the 1st one works
		 * as expected, and subsequent ones fail.
		 */
		try
		{
			URL dtd = DTD_cache.get(publicId);
			if (dtd != null)
				input_src = new InputSource(FileResources.inputStreamForResource(dtd));
		}
		catch (ResourceNotFoundException e)
		{
			// should not happen: resources where previously checked in the static initializer
			e.printStackTrace();
		}
		return input_src;
	}
}
