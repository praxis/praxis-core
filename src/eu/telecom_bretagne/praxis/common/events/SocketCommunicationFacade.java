/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.server.Server;


public class SocketCommunicationFacade
    extends CommunicationFacade
{
	protected static final int BACKLOG=50;
    protected Socket socket;
    protected InputStream is;
    protected OutputStream os;

    static private class SocketServer extends Thread
    {
    	protected InetAddress bindAddress;
        protected int port;
		
		/**
		 * Create a SocketServer with the specified port and address to bind to. If bindAddress is null, the server
		 * will accept connections on all local addresses (see also
		 * {@link ServerSocket#ServerSocket(int, int, InetAddress)}
		 * @param bindAddress
		 *            the local address the server will bind to.
		 * @param port
		 *            the local TCP port
		 */
        public SocketServer(InetAddress bindAddress, int port)
        {
        	this.bindAddress = bindAddress;
        	this.port = port;
        }
        @Override
        public void run() {
            try
            {
            	//InetAddress address = InetAddress.getLocalHost();
                ServerSocket serverSocket = new ServerSocket(port, BACKLOG, bindAddress);
                Log.log.info(()->"Socket server started on port "+port);
                while (true)
                {
                    @SuppressWarnings("resource") // socket is intentionally not closed here!
                    Socket socket = serverSocket.accept();
                    SocketCommunicationFacade cnx = new SocketCommunicationFacade("Server (socket)", socket);
                    new Server(cnx);
                    cnx.start();
                }
            }
            catch (Exception execp)
            { // TODO and then?
                Log.log.log(Level.WARNING, "Error", execp);
            }

        }
    }
    public static void launchServer(InetAddress bindAddress, int port)
    {
        new SocketServer(bindAddress, port).start();
    }

    protected SocketCommunicationFacade(String name, Socket socket)
    throws IOException
    {
        super(name);
        this.socket = socket;
        is = socket.getInputStream();
        os = socket.getOutputStream();
    }
    
    @SuppressWarnings("resource")
	public SocketCommunicationFacade(String name, InetAddress adr, int port)
    throws IOException
    {
        this(name, new Socket(adr,port));
    }

    @Override
    public synchronized void disconnect(Exception exc) {
    	super.disconnect(exc);
        try { is.close(); }     catch (IOException e) { /* ignore */ }
        try { os.close(); }     catch (IOException e) { /* ignore */ }
        try { socket.close(); } catch (IOException e) { /* ignore */ }
    }

    @Override
    public boolean isConnected() {
        return socket.isConnected() && !socket.isClosed();
    }

    @Override
    public void send(Event evt) {
        Log.log.finest(()->"Sending "+evt+" on: "+socket);
        try
        {
            synchronized(os) { evt.writeTo(os); }
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        eventSent(evt);
        Log.log.fine(()->"SENT "+evt);
    }

    /** The corresponding thread first waits for connect() being called, 
     * then handles every queued event. The thread can be started before it
     * is actually connected, and events can be queued before the effective
     * connection as well.
     */
    @Override
    public void run()
    {
        while (!isConnected())
            synchronized(this)
            {
                try{ wait(1); } catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
            }
        int successive_failure = 0;
        while (isConnected()) {
            Event event;
            try
            {
                event = Event.readFrom(is);
                handle_event(event);
                successive_failure = 0;
            }
            catch (SocketException e)
            {
            	/* we can get SocketException: Socket closed when disconnecting, simply ignore it */
            	if (isConnected()) {
            		Log.log.log(Level.SEVERE, "", e);
            		successive_failure++;
            	}
            }
            catch (EOFException e)
            {
            	disconnect(e);
            }
            catch (/*IOException | ClassNotFoundException | */ Exception e)
            {
                Log.log.log(Level.SEVERE, "", e);
            	successive_failure++;
            }
            if ( successive_failure > 5 )
            {
            	Log.log.severe("Too much failures, exiting");
            	disconnect();
            }
        }
    }
        
    @Override
    public String toString()
    {
      return socket.getInetAddress().getHostName() + ":" + socket.getPort() + " bound on local: " + socket.getLocalPort();
    }


}
