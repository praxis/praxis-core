/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class ActivityContext
extends ExecutionObject.Context
{
    private static final long serialVersionUID = 8037251191903035516L;
	private File workingDirectory;

	/**
	 * Returns the working directory.
	 * @return the working directory
	 */
	public File getWorkingDirectory()
	{
		return workingDirectory;
	}

	@Override
	public String toString()
	{
		String repr = "[ActivityContext ";
		repr += "wd:"+( (workingDirectory!=null)?workingDirectory:"null" );
		repr += "]";
		return repr;
	}
}
