/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class DetailedErrorDialog extends JDialog {
    private static final long serialVersionUID = -7947873845343086872L;
    JButton detailsButton;
    private JPanel detailsPanel = new JPanel();
    private boolean detailsShown = false;
    private Dimension originalDimension;
    
    public DetailedErrorDialog(Frame owner, String title, String err_msg, String details) {
        super(owner, title, true);
        initDialog(err_msg, details, false);
    }

    public DetailedErrorDialog(Frame owner, String title, String err_msg, String details, boolean htmlFlag) {
        super(owner, title, true);
        initDialog(err_msg, details, htmlFlag);
    }

    public DetailedErrorDialog(Frame owner, String title, String err_msg, Throwable e)
    {
        super(owner, title, true);
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        initDialog(err_msg, sw.toString(), false);
    }

    private void initDialog(String err_msg, String details, boolean htmlFlag) {
        JButton closeButton = new JButton(I18N.s("common.close"));
        detailsButton = new JButton(I18N.s("common.dialog.detailed_error.show_details"));

        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        detailsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setShowDetails(!detailsShown);
            }
        });
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        Object [] options;
        if (details == null)
            options = new Object[]{closeButton};
        else {
            options = new Object[]{closeButton, detailsButton};
            
            JEditorPane textArea = new JEditorPane();
            textArea.setEditable(false);
        	textArea.setContentType("text/plain");
            if (htmlFlag)
            	textArea.setContentType("text/html");
            textArea.setText(details);
            detailsPanel.setLayout(new BorderLayout());
            detailsPanel.add(new JScrollPane(textArea), BorderLayout.CENTER);
            detailsPanel.validate();
        }

        int msgType = JOptionPane.WARNING_MESSAGE;
        JOptionPane optionPane = new JOptionPane(err_msg, msgType, JOptionPane.YES_NO_OPTION, null, options) {
            private static final long serialVersionUID = -7209152997129609238L;
			@Override
        	public int getMaxCharactersPerLineCount() {
        		return 80;
        	}
        };
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(optionPane, BorderLayout.NORTH);
        originalDimension = optionPane.getPreferredSize();
        originalDimension.width += 50;
        this.pack();
    }

    @Override
    public void setVisible(boolean visible)
    {
        if (visible)
        {
        	//this.pack();
            //this.setSize(Math.min(getSize().width, 500), getSize().height+300);
            //normal_size = getSize();
            this.setLocationRelativeTo(null);
        }
        super.setVisible(visible);
    }
    
    public DetailedErrorDialog setShowDetails(boolean b) {
        if (b) {
            this.getContentPane().add(detailsPanel, BorderLayout.CENTER);
            this.validate();
            detailsButton.setText(I18N.s("common.dialog.detailed_error.hide_details"));
            detailsShown = true;
        }
        else {
            this.getContentPane().remove(detailsPanel);
            this.validate();
            detailsButton.setText(I18N.s("common.dialog.detailed_error.show_details"));
            detailsShown = false;
        }
        this.setIgnoreRepaint(true);
        this.pack();
        Dimension d = this.getSize();
        d.width=originalDimension.width;
        if (d.height>500)
        	d.height=500;
        this.setSize(d);
        this.setIgnoreRepaint(false);
        this.repaint();
        return this;
    }
    
    /**
     */
    public static void main(String[] args) {
        DetailedErrorDialog d = new DetailedErrorDialog(null, "titre", "erreur mon gars", "beh ouais c la merde\n rhoo trop ouais hjk ghkdjf hgkfdj hjfkdhgdfh kjgdfhjk \nhgfdjkhgkjdfhgkjdfhgkj fdhkjghfdkj gdhfkj");
        d.setVisible(true);
        System.exit(0);
    }
    
}
