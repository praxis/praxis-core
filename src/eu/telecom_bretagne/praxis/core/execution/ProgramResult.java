/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@code ProgramResult} class contains informations about the execution of a single program. This
 * includes:
 * <ul>
 * <li>its execution {@link #status},</li>
 * <li>its {@link #start start date} and {@link #end end date},</li>
 * <li>additional {@link #get(String) informations}: any value associated to the execution by the execution engine.
 * For example, the {@link eu.telecom_bretagne.praxis.server.execution.platform.ShellExecutionEngine
 * ShellExecutionEngine} adds an {@code "exit_status"} key containing the shell script's exit value.</li>
 * <li>Its {@link #getOutput(String) outputs}, mapping its parameters' ids to the corresponding files.</li>
 * </ul>
 * The output files' paths are relative; the path they are relative to depends on the context. On the client-side, it
 * can be retrieved from the
 * {@link eu.telecom_bretagne.praxis.client.StorageManager#directoryForExecution(eu.telecom_bretagne.praxis.core.workflow.WorkflowID, ExecutionID)
 * client's StorageManager}.
 * @author Sébastien Bigaret
 */
public class ProgramResult
    implements Serializable
{
    private static final long serialVersionUID = 4271604093897707307L;

	/**
	 * The {@code ProgramStatus} enumeration represents the different status of a program during an execution.
	 */
	public enum ProgramStatus
	{
		/** Used to indicate that the program has not been started yet */
		UNSET,
		/** Used to indicate that the program is waiting to be executed */
		WAITING,
		/** Used to indicate that the program has started */
		RUNNING,
		/** Used to indicate that the program was successfully executed */
		OK,
		/** Used to indicate that the program failed to execute */
		ERROR,
		/**
		 * Used to indicate that the decision was made not to execute the program (for example, because of the failure
		 * of a previous process on which that execution depends)
		 */
		NOT_EXECUTED,
		/**
		 * Used by an ExecutionEngine when its execution mechanism cannot identify individual failures within the set
		 * of programs it executes. For example, an ExecutionEngine that produces and executes a single shell script
		 * for a set of programs may not be able to tell which program has failed when the script fails: in this case,
		 * that execution engine will use the UNKNOWN status. A receiver observing the UNKNOWN status knows the
		 * program was in a set whose execution failed as a whole.
		 */
		UNKNOWN,
		/**
		 * Used to indicate that the program has been cancelled by the user.
		 */
		CANCELLED,
	}

	/**
     * This object stores information about the execution of the program with this id.
     */
	public final String           id;

	/** The status of the current execution */
	public ProgramStatus          status;

	/**
	 * The date on which the execution started. It is {@code null} until the execution starts.
	 */
	public Date                   start;

	/**
	 * The date on which the execution finished (either successfully or not). It is {@code null} until the execution
	 * ends.
	 */
	public Date                   end;

	protected Map<String, String> additional_info  = new HashMap<String, String>();

	/**
	 * The dictionary mapping parameters' ids with filenames.
	 * @implementation We do not store paths as Strings since Result objects (into this object is inserted) move from
	 *                 the client, the server and the platforms: those tiers are not necessarily on the same OS hence
	 *                 the File.separator can be different on each of them.
	 */
	protected Map<String, File>   outputs          = new HashMap<String, File>();

	public ProgramResult(String id)
	{
		this.id = id;
	}

	/**
	 * Builds a new ProgramResult which is a copy of {@code prgResult}.
	 * @param prgResult the object to copy into this.
	 */
	public ProgramResult(ProgramResult prgResult)
	{
		this(prgResult.id);
		this.start  = prgResult.start!=null ? (Date) prgResult.start.clone() : null;
		this.end    = prgResult.end != null ? (Date) prgResult.end.clone()   : null;
		this.status = prgResult.status;
		this.additional_info = new HashMap<String, String>(additional_info);
		this.outputs = new HashMap<String, File>(prgResult.outputs);
	}

	/**
	 * Returns the information associated to the supplied key.
	 * @param key a key
	 * @return the associated value, or {@code null} if no information was associated to the key.
	 * @see #keys() keys() for a list of available keys.
	 */
	public String get(String key)
	{
		return additional_info.get(key);
	}

	/**
	 * Appends a string to the value associated to key. If there were no value associated to the key, this is
	 * equivalent to {@link #put(String, String).
	 * @param key a key
	 * @param value the value to add to the value already registered under the supplied key.
	 */
	public void add(String key, String value)
	{
		String v = additional_info.get(key);
		if (v == null)
			v = "";
		additional_info.put(key, v + value);
	}

	/**
	 * Associates a value to the supplied key. The value that was associated with the key, if any, is replaced by the
	 * specified value.
	 * @param key
	 * @param value
	 * @return the value that was previously associated to this key, or {@code null} if there was no such value.
	 */
	public String put(String key, String value)
	{
		return additional_info.put(key, value);
	}

	/**
	 * Returns the keys available for {@link #get(String)}.
	 * @return
	 */
	public String[] keys()
	{
		return additional_info.keySet().toArray(new String[additional_info.size()]);
	}

	/**
	 * Returns the path of the file generated by the execution of the program, and associated to its parameter
	 * identified by {@code paramID}.
	 * @param paramID
	 *            ID of the parameter
	 * @return the associated file, or {@code null} if there is no such association.
	 * @see #setOutput(String, File)
	 * @see #getOutputsIDs()
	 * @see eu.telecom_bretagne.praxis.core.resource.ParameterDescription#getId()
	 */
	public File getOutput(String paramID)
	{
		return outputs.get(paramID);
	}

	/**
	 * Returns the parameters' IDs registered as outputs.
	 * @return the list of IDs that were registered as outputs
	 * @see #getOutput(String)
	 */
	public String[] getOutputsIDs()
	{
		return outputs.keySet().toArray(new String[outputs.size()]);
	}
	
	/**
	 * Associates the path of the file generated by the execution of the program to its corresponding parameter
	 * identified by {@code paramID}.
	 * @param paramID
	 *            ID of the parameter
	 * @see #getOutput(String)
	 */
	public void setOutput(String paramID, File output)
	{
		outputs.put(paramID, output);
	}

	/**
	 * Updates this object with the informations contained by the supplied one. Helper method for
	 * {@link Result#mergeWith(Result)}. Handle with care, and use the code, Luke.
	 * @param prgInfo
	 */
	protected void updateWith(ProgramResult prgInfo)
	{
		this.status = prgInfo.status;
		// leave start and end untouched: these info. are updated by the ExecutionLoop and should not be modified
		// in any way by a Result coming from a Platform.
		this.additional_info.putAll(prgInfo.additional_info);
		this.outputs.putAll(prgInfo.outputs);
	}

	/**
	 * Splits the path returned by {@link #getOutput(String)} into pieces.<br>
	 * This is an helper method used when serializing a {@link Result} object in XML: we cannot use File.getPath()
	 * since the transmitter and receiver of the xml flow can run on a different OS with different file separators.
	 * @param paramID
	 *            the ID of the parameter for which the output file's components are requested.
	 * @return the components of the path associated to {@code paramID}.
	 * @see #setOutputComponents(String, String[])
	 */
	protected String[] getOutputFileComponents(String paramID)
	{
		ArrayList<String> value = new ArrayList<String>();
		File output = getOutput(paramID);
		while (output != null)
		{
			value.add(0, output.getName());
			output = output.getParentFile();
		}
		return value.toArray(new String[value.size()]);
	}

	/**
	 * Builds a path for the supplied path components and sets the resulting file object as the output associated to
	 * {@code parameterID}.<br>
	 * This is the inverse of method {@link #getOutputFileComponents(String)}, see that method for context and
	 * details.
	 * @param parameterID
	 *            the ID of the parameters to which the reconstructed path should be assigned.
	 * @param pathComponents
	 *            component of the path.
	 */
	protected void setOutputComponents(String parameterID, String[] pathComponents)
	{
		File outputFile = null;
		for (String child: pathComponents)
		{
			if (outputFile == null)
				outputFile = new File(child);
			else
				outputFile = new File(outputFile, child);
		}
		setOutput(parameterID, outputFile);
	}
}
