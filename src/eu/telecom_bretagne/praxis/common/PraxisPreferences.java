/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.rmi.dgc.VMID;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import eu.telecom_bretagne.praxis.client.SimpleCommandLine;

/**
 * A utility wrapper for the <code>java.util.prefs.Preferences</code> class.
 */
public class PraxisPreferences
{
	static final String CLASS = PraxisPreferences.class.getName();
	/**
	 * Used to customize the initialization of preferences in a praxis project.<br>
	 * <br>
	 * When a project needs to add preferences (default) values, it overrides this class and implements the needed
	 * methods. It then registers an instance of this class using
	 * {@link PraxisPreferences#setProjectPreferences(ProjectPreferences)}.<br>
	 * <br>
	 * To add a value, implementers use one of the <code>put...()</code> methods, namely:
	 * <ul>
	 * <li> {@link PraxisPreferences#put(String, String, Object)}</li>
	 * <li> {@link PraxisPreferences#put(String, String, String)}</li>
	 * <li> {@link PraxisPreferences#putBoolean(String, String, boolean)}</li>
	 * <li> {@link PraxisPreferences#putDefault(String, String, Object)}</li>
	 * <li> {@link PraxisPreferences#putInt(String, String, int)}</li>
	 * </ul>
	 * Each of these methods takes a <code>node</code> as its first parameter. Acceptable values are currently:
	 * {@link PraxisPreferences#rootNode}, <code>"tools"</code>, <code>"display"</code> and <code>"client"</code>. <br>
	 * <br>
	 * Preferences are initialized right after the {@link Configuration} has been loaded.
	 * <p>
	 * Keep in mind that this may be called while no display is available, for example through the initialisation of
	 * {@link SimpleCommandLine}. In this case, any call depending on the existence of a valid display will fail (
	 * {@link Toolkit#getScreenSize()} will raise an {@code HeadlessException} e.g.). Your methods should be ready to
	 * transparently ignore this when it happens.
	 * @author Sébastien Bigaret
	 */
	// NB the value "update" is intentionally not documented here
	public static class ProjectPreferences
	{
		// for the moment being, this is disabled:
		//we need an API to create nodes and register them in the map "praxisNodes"
		//public void createNodes() {}
		/**
		 * Called by the framework at initialization time. Subclass this method if you need to add some custom
		 * preferences on the client-side.<br>
		 * Please note that it will be called if and only if the client is launched; for example, if the praxis'
		 * server part is deployed on its own, it will not call this method. <br>
		 * The default implementation does nothing.
		 * @see #initCommon()
		 */
		public void initClient() { /* does nothing */ }
		
		/**
		 * Called by the framework at initialization time. Subclass this method if you need to add you own values to
		 * the application's preferences. This method is always called when the preferences are initialized.<br>
		 * The default implementation does nothing.
		 */
		public void initCommon() { /* does nothing */ }
	}
	
	protected static final Preferences PRAXIS_ROOT_NODE;
	public static final Preferences ROOT_NODE;
	private static HashMap<String, Preferences> praxisNodes;
	public static final String rootNode;
	protected static boolean initialized = false;
	protected static ProjectPreferences projectPreferences = null;
	
	static
	{
		PRAXIS_ROOT_NODE = Preferences.userNodeForPackage(PraxisPreferences.class);
		rootNode = Configuration.get("preferences.key");
		ROOT_NODE = PRAXIS_ROOT_NODE.node(rootNode);
		init();
	}

	/**
	 * 
	 * @param prefs
	 * @throws IllegalStateException
	 */
	public static void setProjectPreferences(ProjectPreferences prefs)
	throws IllegalStateException
	{
		if (initialized)
			throw new IllegalStateException("Preferences have already been initialized");
		projectPreferences = prefs;
	}

	protected static void createNodes()
	{
		praxisNodes = new HashMap<String, Preferences>();
				
		try
		{
			// Nodes creation (empty) if not exists
			if (!PRAXIS_ROOT_NODE.nodeExists(rootNode))
			{
				PRAXIS_ROOT_NODE.node(rootNode);
				PRAXIS_ROOT_NODE.node(rootNode+"/tools");
				PRAXIS_ROOT_NODE.node(rootNode+"/display");
				PRAXIS_ROOT_NODE.node(rootNode+"/update");
				PRAXIS_ROOT_NODE.node(rootNode+"/client");

				PRAXIS_ROOT_NODE.flush();
			}
		}
		catch (BackingStoreException e)
		{
			Log.log.log(Level.SEVERE, "Got exception", e);
			e.printStackTrace();
			// Nothing to do if it's not possible to store the workspace...
			// Good bye!
			System.exit(1);
		}
		praxisNodes.put(rootNode, PRAXIS_ROOT_NODE.node(rootNode));
		praxisNodes.put("tools", PRAXIS_ROOT_NODE.node(rootNode+"/tools"));
		praxisNodes.put("display", PRAXIS_ROOT_NODE.node(rootNode+"/display"));
		praxisNodes.put("update", PRAXIS_ROOT_NODE.node(rootNode+"/update"));
		praxisNodes.put("client", PRAXIS_ROOT_NODE.node(rootNode+"/client"));
		
		// disabled: see comments in ProjectReferences, above
		//if (projectPreferences!=null)
		//	projectPreferences.createNodes();
	}

	/**
	 * Initialize the preferences for a praxis project
	 */
	protected static synchronized void init()
	{
		if (initialized)
			return;
		createNodes();
		initCommon();
		initClient();
		initialized = true;
		Configuration.resetLocale(PraxisPreferences.get("client", "language"));
	}
	
	protected static void initCommon()
	{
		String workspace;
		searchWorkspaceLocation:
		{
			workspace = System.getProperty("praxis.workspace");
			if (workspace!=null && !"".equals(workspace))
				break searchWorkspaceLocation;
			workspace = PraxisPreferences.get("client", "last_workspace");
			if (workspace!=null && !"".equals(workspace))
				break searchWorkspaceLocation;
			workspace = Configuration.get("workspace_directory");
		}
		File workspaceDirectory = new File(workspace);

		if (!workspaceDirectory.isAbsolute())
		{
			// relative to the HOME directory
			String myDocumentsDirName = Environment.getUserHome();
			workspaceDirectory = new java.io.File(myDocumentsDirName, workspaceDirectory.getPath());
		}
		// use put(), not putDefault(): "prefix" points to the current workspace, always.
        PraxisPreferences.put(rootNode, "prefix", workspaceDirectory.getAbsolutePath());
        // Create directory if necessary
        if(!workspaceDirectory.exists())
        {
          workspaceDirectory.mkdirs(); // TODO check return value
          Log.log.log(Level.INFO, "%s: workspace directory created.", workspaceDirectory);
        }
        else
        {
          Log.log.log(Level.FINE, "%s: workspace directory already exists.", workspaceDirectory);
        }
		if (projectPreferences!=null)
			projectPreferences.initCommon();
	}

	protected static void initClient()
	{
		Log.log.entering(CLASS, "initClient()");
		try
		{
			Class.forName("eu.telecom_bretagne.praxis.client.ClientPrefs");
		}
		catch (Exception err) // ClassNotFoundException, LinkageError, ExceptionInInitializerError
		{
			Log.log.fine("eu.telecom_bretagne.praxis.client.ClientPrefs is not in classpath: ignored");
			return;
		}
		if (Configuration.isDeclared("ignore.client"))
		{
			Log.log.fine("Configuration key ignore.client declared: ignored");
			return;
		}
		
		if (Environment.useLooseCredentials())
		{
			String confName=Environment.getLooseCredentialsConfigurationName();
			String login=PraxisPreferences.get("client", confName);
			if (login==null || "".equals(login))
			{
				login=new VMID().toString().replace(':', '_');
				PraxisPreferences.put("client", confName, login);
			}
		}
		// Cas impossible à gérer de manière automatique : le workspace existe déjà
		// mais il n'est pas référencé dans le noeud "prefix". Solutions possibles :
		// a - Créer manuellement dans la base de registre le noeud rootNode et
		// son sous-noeud "prefix" puis y stocker la chemin du workspace
		// existant.
		// b - Lancer le projet praxis une permière fois (création automatique du noeud
		// "prefix" et du rép. "Mes documents\..._workspaces") puis
		// modification manuelle de la valeur pour le faire pointer sur le
		// répertoire du workspace existant.
		
		// Default dimension for the main frame
		try
		{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int frameWidth  = screenSize.width*4/5;
		int frameHeight = screenSize.height*4/5;
		int posX        = (screenSize.width-frameWidth)/2; 
		int posY        = (screenSize.height-frameHeight)/2;
		int dividerLocationGlobalSplitPane    = 250;
		//int dividerLocationWorkspaceSplitPane = (frameWidth - dividerLocationGlobalSplitPane)*7/10;  // 70% de l'espace restant
		int dividerLocationWorkspaceSplitPane = frameWidth - 425;     // 250 colonne de gauche et 175 colonne de droite 
		int dividerLocationResultsSplitPane   = 200;
		
		PraxisPreferences.putDefault("display", "posX",         posX);
		PraxisPreferences.putDefault("display", "posY",         posY);
		PraxisPreferences.putDefault("display", "frameWidth",   frameWidth);
		PraxisPreferences.putDefault("display", "frameHeight",  frameHeight);
		PraxisPreferences.putDefault("display", "splitPane1",   dividerLocationGlobalSplitPane);
		PraxisPreferences.putDefault("display", "splitPane2",   dividerLocationWorkspaceSplitPane);
		PraxisPreferences.putDefault("display", "splitPane3",   dividerLocationResultsSplitPane);
		PraxisPreferences.putDefault("display", "showGrid",     true);
		PraxisPreferences.putDefault("display", "snapToGrid",   true);
		PraxisPreferences.putDefault("display", "gridSize",     "small");
		PraxisPreferences.putDefault("display", "antiAliasing", true);
		PraxisPreferences.putDefault("display", "arrowStyle",   "orthogonal");
		PraxisPreferences.putDefault("display", "openAllResults", false);
		}
		catch (java.awt.HeadlessException | java.awt.AWTError e)
		{
			/*
			 * No display: ignore this here. We may be launching a real GUI client, or a command-line version of it
			 * (in which case this is completely ok); if this is a GUI client, the failure will occur when the interface
			 * will actually be launched. So, we just ignore it.
			 */
		}
		
		// Automatic updates
		PraxisPreferences.putBoolean("update","automatic_update",true);
		PraxisPreferences.putBoolean("update","proxy",false);
		
		Log.log.log(Level.INFO, "Workspaces dir referenced and display settings created.");
		
		// pref. client.server: le serveur utilisé la dernière fois par le client
		if (PraxisPreferences.get("client","server") == null)
			PraxisPreferences.put("client","server","");
		
		if (projectPreferences!=null)
			projectPreferences.initClient();
		Log.log.exiting(CLASS, "initClient()");
	}

	/**
	 * Returns the value associated with the specified key in this node or <code>null</code> if the key does not
	 * exist.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key whose associated value is to be returned.
	 * @return the value associated with key, or <code>null</code> if no value is associated with key.
	 */
	public static String get(String node, String key)
	{
		return praxisNodes.get(node).get(key, null);
	}

	/**
	 * Returns the integer value associated with the specified key in this node or <code>Integer.MIN_VALUE</code> if
	 * the key does not exist.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key whose associated value is to be returned.
	 * @return the integer value associated with key, or <code>Integer.MIN_VALUE</code> if no value is associated with
	 *         key.
	 */
	public static int getInt(String node, String key)
	{
		return praxisNodes.get(node).getInt(key, Integer.MIN_VALUE);
	}
	

	/**
	 * Returns the boolean value associated with the specified key in this node or <code>false</code> if the key does
	 * not exist.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key whose associated value is to be returned.
	 * @return the boolean value associated with key, or <code>false</code> if no value is associated with that key.
	 */
	public static boolean getBoolean(String node, String key)
	{
		return praxisNodes.get(node).getBoolean(key, false);
	}
	

	/**
	 * Put the specified String value with the specified key in the specified node.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key with which the string form of value is to be associated.
	 * @param value
	 *            value whose string form is to be associated with key.
	 * @throws BackingStoreException
	 */
	public static void put(String node, String key, String value)
	{
		Preferences pref = praxisNodes.get(node);
		pref.put(key, value);
		try
		{
			pref.flush();
		}
		catch (BackingStoreException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Associates the specified value with the supplied key in the specified node
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            the key with which the value is to be associated in the specified node
	 * @param value
	 *            either a String, a Boolean or an Integer value to be associated with the key in the specified node
	 * @throws IllegalArgumentException
	 *             if value is not a {@link String}, a {@link Boolean} or an {@link Integer}
	 */
	public static void put(String node, String key, Object value)
	{
		Preferences pref = praxisNodes.get(node);
		if (value instanceof String)
			pref.put(key, (String) value);
		else
			if (value instanceof Boolean)
				pref.putBoolean(key, (Boolean) value);
			else
				if (value instanceof Integer)
					pref.putInt(key, (Integer) value);
				else
					throw new IllegalArgumentException("Unsupported class " + value.getClass());
		
		try
		{
			pref.flush();
		}
		catch (BackingStoreException e)
		{
			e.printStackTrace();
		}
	}
  
  /**
	 * Associates the specified value with the supplied key in the specified node, if no such association already
	 * exists.
	 * @param node
	 *            the preference's node in which the default value is to be associated with the specified key, if
	 *            needed
	 * @param key
	 *            the key with which the default value is to be associated in the specified node, if needed
	 * @param value
	 *            either a String, a Boolean or an Integer default value to be associated with the key in the
	 *            specified node, if needed
	 * @throws IllegalArgumentException
	 *             if defaultValue is not a {@link String}, a {@link Boolean} or an {@link Integer}
	 */
	public static void putDefault(String node, String key, Object defaultValue)
	{
		Preferences pref = praxisNodes.get(node);
		if (pref.get(key, null) == null)
			put(node, key, defaultValue);
		
	}
	

	/**
	 * Put the specified integer value with the specified key in the specified node.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key with which the string form of value is to be associated.
	 * @param value
	 *            value whose string form is to be associated with key.
	 * @throws BackingStoreException
	 */
	public static void putInt(String node, String key, int value)
	{
		Preferences pref = praxisNodes.get(node);
		pref.putInt(key, value);
		try
		{
			pref.flush();
		}
		catch (BackingStoreException e)
		{
			e.printStackTrace();
		}
	}
	

	/**
	 * Put the specified boolean value with the specified key in the specified node.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key with which the string form of value is to be associated.
	 * @param value
	 *            the value whose string form is to be associated with key.
	 * @throws BackingStoreException
	 */
	public static void putBoolean(String node, String key, boolean value)
	{
		Preferences pref = praxisNodes.get(node);
		pref.putBoolean(key, value);
		try
		{
			pref.flush();
		}
		catch (BackingStoreException e)
		{
			e.printStackTrace();
		}
	}
	

	/**
	 * Removes all of the preferences (key-value associations) in this preference node. This call has no effect on any
	 * descendants of this node.
	 * @param node
	 *            the preference's node in which the value is to be associated with the specified key
	 * @param key
	 *            key whose mapping is to be removed from the preference node.
	 */
	public static void remove(String node, String key)
	{
		Preferences pref = praxisNodes.get(node);
		pref.remove(key);
		try
		{
			pref.flush();
		}
		catch (BackingStoreException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the user preference <code>/<project's rootNode>/prefix</code>. The value stored in this field is the path to the directory where
	 * workflows are searched.
	 * @return the value stored in user's preference field <code>/<project's root>/prefix</code>, or the empty string if it is
	 *         not set.
	 */
	static public File workspace() {
		//TODO: make sure that it is set
		return new File(PraxisPreferences.get(PraxisPreferences.rootNode,"prefix")); 
	}
}
