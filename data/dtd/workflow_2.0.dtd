<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- ...................................................................... -->
<!--    Definition d'une entite parametre interne  True:"1",False:rien      -->
<!-- ...................................................................... -->
<!ENTITY % boolean "CDATA">

<!-- ...................................................................... -->
<!--                 Description de scenarios complets                      -->
<!-- ...................................................................... -->
<!ELEMENT scenario (id?,name,user,annotation?,inputs+, programs)>
<!ELEMENT id (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT user (#PCDATA)>
<!ELEMENT annotation (#PCDATA)>

<!-- ...................................................................... -->
<!--           Declaration des fichiers d'entree du scenario                -->
<!-- ...................................................................... -->
<!ELEMENT inputs (input)*>
<!-- ...................................................................... -->
<!--     Chaque balise fichiers correspond a un input d'un programme        -->
<!--     Si un programme a deux inputs on aura deux balises fichiers        -->
<!-- ...................................................................... -->
<!ELEMENT input (infile)*>
<!ATTLIST input
	id CDATA #REQUIRED
	type (file | database | directory) #REQUIRED 
	x CDATA #REQUIRED
	y CDATA #REQUIRED
	name CDATA #REQUIRED
>
<!--type (file|dir | seq | BD)-->

<!-- ...................................................................... -->
<!--          Description de tous les fichiers d'un input                   -->
<!-- ...................................................................... -->
<!ELEMENT infile (#PCDATA)>

<!-- ...................................................................... -->
<!ELEMENT programs (program*)>

<!-- ...................................................................... -->
<!--                        Description des programmes                      -->
<!-- ...................................................................... -->
<!-- ...................................................................... -->
<!--   Un programme est defini par son identifiant, une ref. vers la descr. -->
<!--   du programme, et l'ensemble de ses parametres                        -->
<!--
     idref: l'identifiant de la description de programme
     info:  un nom � afficher. �a ne sert pas � identifier le prg., mais � pouvoir afficher de quel programme il
            s'agissait, m�me sans une description valide (cf. note plus bas)
-->
<!ELEMENT program (annotation?,parameter+)>
<!ATTLIST program
	id CDATA #REQUIRED
	x CDATA #REQUIRED
	y CDATA #REQUIRED
    idref CDATA #REQUIRED
    info CDATA #IMPLIED
>

<!ELEMENT parameter (info?, data)>

<!-- le contenu de cette balise donne le nom du param�tre ainsi que la valeur en clair.
     L'id�e est de pouvoir afficher le contenu & le param�trage d'un sc�nario m�me apr�s que la description du
     programme correspondant a chang�: on peut ainsi afficher le contenu alors m�me qu'il n'est plus valide.
     NB: pour permettre de faire la diff�rence entre les �l�ments activ�s et non activ�s (au sens des d�pendances) au
     moment de la sauvegarde, seuls les param�tres actifs au moment de la sauvegarde ont la balise <info/>

     Note importante: le contenu de cet �l�ment n'est destin� qu'� un affichage statique, en particulier quand la
     description du prg. correspondant a chang�/n'existe plus etc. En aucun cas elle ne peut servir � construire un
     sc�nario valide: le param�trage effectif d'un programme valide est toujours construit uniquement
     � partir de l'attribut idref et de <data/>.
     
     <info
         name = "nom du param�tre � afficher"
         value= "valeur � afficher"
     />
-->
<!ELEMENT info EMPTY>
<!ATTLIST info
	name  CDATA #REQUIRED
	value CDATA #REQUIRED
>

 <!-- idref: reference � l'id du parametre dans la description du prg -->
 <!ATTLIST parameter
	idref CDATA #REQUIRED
>

<!-- ...................................................................... -->
<!ELEMENT data (#PCDATA)>
<!ATTLIST data
	input_id CDATA #IMPLIED
>
