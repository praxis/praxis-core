/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import org.jdom.Document;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.RemoteComponents;
import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;


/**
 * The repository holds the descriptions of programs that can be used to build workflows.
 * @author Sébastien Bigaret
 */
public class ResourceRepository extends UnicastRemoteObject implements RemoteResourceRepositoryInterface
{
	private static final long serialVersionUID = 6732529484059570407L;

	public static final String PRGS_RELOADED_DIR_PATH = Configuration.get("descriptions.reload", null);
	
	/** The hierarchy of resources, <b>currently unused</b> */
	protected final Hierarchy hierarchy;
	
	/**
	 * The repository itself, mapping programs' names to their corresponding
	 * {@link ProgramDescription}
	 */
	private final Map <String, ProgramDescription> programs;
	
	/**
	 * The repository, stored here, is a singleton
	 * @see ResourceRepository#repository()
	 */
	protected static ResourceRepository repository = null;
	
	public static Registry rmiRegistry;
	
	public static synchronized ResourceRepository repository()
	{
		if (repository == null)
		{
			try
			{
				if (Configuration.getBoolean("rmi.useSSL"))
					repository = new ResourceRepository("ignored value, just use SSL");
				else
					repository = new ResourceRepository();					
			}
			catch (RemoteException e)
			{
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
				System.exit(-1); //TODO
			}
			for (ProgramDescription[] p: Configuration.getDeclaredDescriptions())
				repository.registerDescriptions(p);
		}
		return repository;
	}
	
	private ResourceRepository() throws RemoteException
	{
		super(Configuration.RMI_REGISTRY_PORT);
		programs = new HashMap <String, ProgramDescription>();
		hierarchy = new Hierarchy(Configuration.get("server.allowed_resources"));
		Log.log.log(Level.INFO, "binding {0} to: {1}", new Object[]{this, RemoteComponents.RMI_RESOURCE_REPOSITORY_NAME});
		if (ResourceRepository.rmiRegistry!=null)
			ResourceRepository.rmiRegistry.rebind(RemoteComponents.RMI_RESOURCE_REPOSITORY_NAME, this);
	}
	private ResourceRepository(String unused_this_is_just_the_constructor_for_ssl) throws RemoteException
	{
		super(Configuration.RMI_REGISTRY_PORT, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
		programs = new HashMap <String, ProgramDescription>();
		hierarchy = new Hierarchy(Configuration.get("server.allowed_resources"));
		Log.log.log(Level.INFO, "binding {0} to: {1}", new Object[]{this, RemoteComponents.RMI_RESOURCE_REPOSITORY_NAME});
		if (ResourceRepository.rmiRegistry!=null)
			ResourceRepository.rmiRegistry.rebind(RemoteComponents.RMI_RESOURCE_REPOSITORY_NAME, this);
	}
	
	/**
	 * @param descriptions
	 */
	public void registerDescriptions(ProgramDescription[] descriptions)
	{
		for (int i=0; i<descriptions.length; i++)
		{
			final String descriptionId = descriptions[i].id();
			Log.log.finest(()->"Registering description for id: "+descriptionId);
			programs.put(descriptionId, descriptions[i]);
		}
		dynamicallyReloadDescriptions();
	}
	
	/* (non-Javadoc)
	 * @see eu.telecom_bretagne.praxis.core.resource.ResourceRepositoryInterface#isAvailable(java.lang.String)
	 */
	public synchronized boolean isDeclared(String prg_id)
	{
		return programs.containsKey(prg_id);
	}
	
	@Override
	public synchronized String[] getResources()
	{
		Log.log.fine(()->"Request: getResources()");
		return programs.keySet().toArray(new String[]{});
	}
	
	/* (non-Javadoc)
	 * @see eu.telecom_bretagne.praxis.core.resource.ResourceRepositoryInterface#programDescriptionForName(java.lang.String)
	 */
	@Override
	synchronized public ProgramDescription programDescriptionForId(String id)
	{
		Log.log.fine(()->"Request: programDescriptionForId("+id+")");
		return programs.get(id);
	}

	@Override
	public ProgramDescription alternateProgramDescriptionForId(String id) throws RemoteException,
	        IllegalArgumentException, NullPointerException
    {
		final String[] provider_name_version = id.split(ProgramDescription.ID_SEPARATOR);
		if ( provider_name_version.length !=  3 )
			throw new IllegalArgumentException("Invalid id: "+id);
		// let's do it simply: if we find one, we return it
		
		String alternateVersion = "";
		ProgramDescription alternatePrgDesc = null;
		for (ProgramDescription prg: programs.values())
		{
			if (!prg.isActive())
				continue;
			if ( provider_name_version[0].equals(prg.prg_provider)
					&& provider_name_version[1].equals(prg.prg_name) )
				if ( alternateVersion.compareToIgnoreCase(provider_name_version[2])<0 )
				{
					alternateVersion = provider_name_version[2];
					alternatePrgDesc = prg;
				}
		}
		return alternatePrgDesc;
    }
	
	synchronized public ProgramDescription[] programDescriptions()
	{
		return programs.values().toArray(new ProgramDescription[programs.values().size()]);
	}
	
	/**
	 * Looks in the directory {@link #PRGS_RELOADED_DIR_PATH} for xml files, and loads them into the
	 * repository. If a program's description is already declared in {@link AllDescriptions}, it is overwritten.
	 */
	synchronized protected void dynamicallyReloadDescriptions()
	{
		if (PRGS_RELOADED_DIR_PATH==null || "".equals(PRGS_RELOADED_DIR_PATH))
			return;
		
		File prg_directory=null;
		String[] prgs_filenames = {};
		try
		{
			prg_directory = FileResources.file(PRGS_RELOADED_DIR_PATH);
			prgs_filenames = prg_directory.list(new FilenameFilter() {
				public boolean accept(File f, String s) {
					return s.endsWith(".xml");
				}
			});
		}
		catch (ResourceNotFoundException e1)
		{
			Log.log.info("Directory "+PRGS_RELOADED_DIR_PATH+" not found: there is no program's description to dynamically (re)load");
			return;
		}
		catch (IllegalArgumentException e2)
		{
			/*
			 * The resource is probably in a jar shipped with one-jar and we got IllegalArgumentException: URI is not
			 * hierarchical. If this is the case, we'll find a file named INDEX containing the list of the files
			 * containing the descriptions.
			 */
			BufferedReader br = null;
			try
            {
	            URL index = FileResources.url(PRGS_RELOADED_DIR_PATH+"/INDEX");
	            InputStream indexIS = index.openStream();
	            ArrayList <String> prgs = new ArrayList<String>();
	            br = new BufferedReader(new InputStreamReader(indexIS));
	            String prg;
	            while ((prg=br.readLine())!=null)
	            {
	            	if (!"INDEX".equals(prg))
	            		prgs.add(PRGS_RELOADED_DIR_PATH+"/"+prg);
	            }
	            prgs_filenames = prgs.toArray(new String[prgs.size()]);
            }
            catch (ResourceNotFoundException e)
            {
    			Log.log.info("File "+PRGS_RELOADED_DIR_PATH+"/INDEX not found: there is no program's description to dynamically (re)load from the jar");
            }
            catch (java.io.IOException e)
            {
    			Log.log.info("File "+PRGS_RELOADED_DIR_PATH+"/INDEX could not be read: no program's description were dynamically (re)loaded");
            }
			finally
			{
				if ( br != null ) try { br.close(); } catch (IOException e) { /* ignore */ }
			}
            
		}
		for (int i=0; i<prgs_filenames.length; i++) {
			String filename = prgs_filenames[i];
			Log.log.fine(()->"Trying to load "+filename);
			ProgramDescription prg_desc = null;
			try {
				prg_desc = load(new File(prg_directory, filename));
			} catch (InvalidXMLException e) {
				Log.log.severe("Unable to load "+filename+": invalid format: "+e.toString());
				continue;
			}
			catch (ResourceNotFoundException e) {
				/* unlikely to happen: we come from a File.list(), above! but still */
				Log.log.severe("Unable to load "+filename+": "+e.toString());
				continue;
			}
			String prg_desc_id = prg_desc.id();
			if (programs.get(prg_desc_id) != null)
				Log.log.fine(()->"Program "+prg_desc_id+" reloaded from .xml");
			else
				Log.log.fine(()->"Program "+prg_desc_id+" loaded from .xml");
			programs.put(prg_desc_id, prg_desc);
		}
	}
	
	/**
	 * Loads and registers the {@link ProgramDescription} stored in the
	 * given xml-file.
	 * @param name name under which the program should be registered
	 * @param file the xml-file describing the program
	 * @return the name of the program that was loaded, or \c null if it
	 * couldn't be loaded.
	 * @throws InvalidXMLException 
	 * @throws ResourceNotFoundException 
	 */
	static synchronized protected ProgramDescription load(File file)
	throws InvalidXMLException, ResourceNotFoundException
	{
		Document prog_doc;
		InputStream resourceInputStream = FileResources.inputStreamForResource(file);
		prog_doc = Facade_xml.read(resourceInputStream, true);
		try { resourceInputStream.close(); } catch (IOException e) { /* ignore */ }
		if ( prog_doc == null )
			return null;
		
		ProgramDescription prog = new ProgramDescription(prog_doc.getRootElement());
		
		DynamicCodeFactory.handle_dependencies(prog);
		
		return prog;
	}
	
	/**
	 * Registers or replaces the formerly registered program description with the one supplied.<br/>
	 * <b>ATTENTION:</b> callers are warned that this method is designed for special situations where the descriptions
	 * change during the lifetime on an application. In a standard, normal praxis project descriptions do not change,
	 * see {@link RemoteComponents#cacheResourceRepositoryAnswers} for further details.
	 * @param prgDescription
	 *            the program description to (re-)register
	 */
	synchronized public void reloadDescription(ProgramDescription prgDescription)
	{
		/*
		 * CHECK add a way for remote clients to be notified of this changes. For example, a simple sequence/version
		 * number being incremented here may be enough, clients noticing a different version number would invalidate
		 * their cache so that they get an updated version. Not sure this is needed though; for the moment being the
		 * repository is not supposed to change in a normal, "standard" praxis project. Situations where this is
		 * needed are handled differently for now (setting RemoteComponents.cacheResourceRepositoryAnswers to false).
		 */
		String prg_desc_id = prgDescription.id();
		if (programs.get(prg_desc_id) != null)
			Log.log.fine(()->"Program "+prg_desc_id+" reloaded");
		else
			Log.log.fine(()->"Program "+prg_desc_id+" loaded");
		programs.put(prg_desc_id, prgDescription);
	}
	
	public static void startRMIRegistry() throws Exception
	{
		/* cf. http://blogs.sun.com/jmxetc/entry/jmx_connecting_through_firewalls_using */
		int port = Configuration.RMI_REGISTRY_PORT;
		
		// Ensure cryptographically strong random number generator used to choose the object number
		// -- see java.rmi.server.ObjID
		System.setProperty("java.rmi.server.randomIDs", "true");
		
		int currentPort = (port==0) ? 1025:port;
		do
		{
			try
			{
				createRegistry(currentPort);
				break;
			}
			catch (RemoteException e)
			{
				if (port != 0)
					// if it was set in the configuration file, immediately raise
					throw e;
				if ( currentPort>10000 )
					// we've tried hard enough, abort
					throw e;
				Log.log.fine("Port already in use: "+currentPort+", trying the next one");
				currentPort++;
			}
		}
		while (true);
		Configuration.RMI_REGISTRY_PORT = currentPort;
		repository();// make sure this is triggered, so that it is registered 
	}

	/**
     * @param port
     * @throws RemoteException
     */
    private static void createRegistry(int port) throws RemoteException
    {
	    if (Configuration.getBoolean("rmi.useSSL"))
	    {
	    	final SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();
	    	final SslRMIServerSocketFactory ssf = new SslRMIServerSocketFactory();
	    	
	    	rmiRegistry = LocateRegistry.createRegistry(port, csf, ssf);
	    }
	    else
	    {
	    	rmiRegistry = LocateRegistry.createRegistry(port);
	    }
    }
}
