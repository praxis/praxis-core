/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Application;
import eu.telecom_bretagne.praxis.common.Console;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.ExecutionObject.Requester;
import eu.telecom_bretagne.praxis.core.execution.Process.ProcessContext;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult.ProgramStatus;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.Serveur;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;



public class ExecutionLoop implements Runnable, Serializable
{
	public static class NotEnabledException
	    extends Exception
	{
		private static final long serialVersionUID = 8881329748041332645L;
	}
	
	private static final long serialVersionUID = -2477797887488172580L;

	public static final int           MAX_STEPS_WORKFLOWS  = 5;
	
	public static final int           MAX_STEPS_PROCESSES  = 5;
	
	public static final int           MAX_STEPS_ACTIVITIES = 10;
	
	public static final int           MAX_STEPS_RESULTS    = 10;
	
	public static final ExecutionLoop executionLoop        = restoreState();
	public static final Thread        executionLoopThread  = new Thread(executionLoop);
	
	/** The queue of the workflows submitted by the clients/requesters */
	protected final Queue<Process>      submittedProcesses   = new LinkedList<Process>();
	
	/** The queue of active activities, i.e. those which have been submitted by their processes */
	protected final Queue<Activity>     activities           = new LinkedList<Activity>();
	
	/** The queue of waiting results, as returned by Resources */
	protected final Queue<Result>       results              = new LinkedList<Result>();
	
	/** */
	protected Semaphore                 jobs = new Semaphore(0);
	
	
	/** */
	protected static class PMK implements Serializable
	{
        private static final long serialVersionUID = 8656338061129012501L;
        
		WorkflowID wfID; ExecutionID execID;
		PMK(WorkflowID wfID, ExecutionID execID)
		{
			this.wfID = wfID;
			this.execID = execID;
		}
		@Override
		public boolean equals(Object key)
		{
			if (key==null)
				return false;
			if (!(key instanceof PMK)) return false;
			PMK _key = (PMK)key;
			return wfID.equals(_key.wfID) && execID.equals(_key.execID);
		}
		@Override
		public int hashCode()
		{
			return this.wfID.hashCode()*31+this.execID.hashCode();
		}
	}
	protected final ConcurrentHashMap<PMK, Process> processes = new ConcurrentHashMap<PMK, Process>(16,(float) 0.75,1);

	/** */
	protected boolean                 enabled              = true;
	
	private ExecutionLoop() {}
	
	public Process createProcess(String name, ProcessContext pContext, Requester requester, Result r)
	{
		Process process = new Process(pContext.executionID, name);
		process.setContext(pContext);
		process.setRequester(requester);
		process.setResult(r);
		return process;
	}
	
	/**
	 * 
	 */
	public void add(Process process) throws NotEnabledException
	{
		if (!enabled)
			throw new NotEnabledException();
		synchronized(submittedProcesses)
		{
			submittedProcesses.add(process);
			jobs.release();
		}
	}
	
	/**
	 * 
	 */
	public void add(Activity activity)
	{
		synchronized(activities)
		{
			if (activities.contains(activity))
			{
				Log.log.severe(()->"Activity already added! "+activity);
				return;
			}
			activities.add(activity);
			jobs.release();
		}
	}
	
	/**
	 * 
	 */
	public void add(Result result)
	{
		synchronized(results)
		{
			results.add(result);
			jobs.release();
		}
	}
	/**
	 * Return the next workflow to handle
	 * @return the next workflow to handle, or null is the workflows' queue is empty
	 */
	protected Process getNextSubmittedProcess()
	{
		synchronized(submittedProcesses)
		{
			return submittedProcesses.poll();
		}
	}
	
	/**
	 * Return the next activity to handle
	 * @return the next activity to handle, or null is the activities' queue is empty
	 */
	protected Activity getNextActivity()
	{
		synchronized(activities)
		{
			return activities.poll();
		}
	}
	
	/**
	 * Return the next result to handle
	 * @return the next result to handle, or null is the results' queue is empty
	 */
	protected Result getNextResult()
	{
		synchronized(results)
		{
			return results.poll();
		}
	}

	public Process getProcess(WorkflowID wfID, ExecutionID execID)
	{
		return processes.get(new PMK(wfID,execID));
	}
	
	/**
	 * 
	 */
	protected void handleSubmittedProcesses()

	{
		int idx = 0;
		for (idx = 0; idx < MAX_STEPS_WORKFLOWS; idx++)
		{
			Process p = getNextSubmittedProcess();
			if (p == null)
				break;
			jobs.tryAcquire(); // should never fail
			/* We do not keep any references to the process: instead, only its (running) activities will be stored */
			if (p.start(this))
				processes.put(new PMK(p.getContext().workflowID, p.getContext().executionID), p);
			else
				handleFinishedProcess(p);
		}
	}
	
	protected void handleActivities()
	{
		for (int idx=0; idx<MAX_STEPS_ACTIVITIES; idx++)
		{
			Activity a = getNextActivity();
			if (a==null)
				break;
			jobs.tryAcquire(); // should never fail
			// time to execute the activity

			// mark start
			Result processResult = a.getContainer().result;
			Result result = new Result(processResult.workflowID, processResult.executionID);
			for (Program prg: a.getExecutionSet())
			{
				ProgramResult pResult = result.getProgramInfo(prg.getProgramID(), true);
				pResult.status = ProgramStatus.WAITING;
				pResult.start = new java.util.Date();
				processResult.mergeWith(result, false);
				processResult.getProgramInfo(prg.getProgramID()).start = pResult.start;
			}
			result.setStatus(Status.RUNNING);
			Serveur.resultStore().updateResult(processResult);
			
			a.setResult(result);
			Log.log.info(()->"Executing activity: "+a);
			try
			{
				PlatformDescription.executeActivity(a);
			}
			catch (CannotExecuteException cee)
			{
				handleExecutionFailure(a, cee);
			}
			catch (Exception e)
			{
				Log.log.log(Level.SEVERE, "Unexpected exception while executing an activity", e);
				handleExecutionFailure(a, e);
			}
		}
	}

	/**
	 * Handles an execution failure, by building then {{@link #add(Result) injecting} the appropriate Result object into
	 * this execution loop.
	 * 
	 * @param activity
	 *            the activity which failed to execute
	 * @param exception
	 *            the exception that was thrown
	 */
	protected void handleExecutionFailure(Activity activity, Exception exception)
	{
		ProcessContext processContext = activity.getContainer().getContext();
		Result result = new Result(processContext.workflowID,
		                           processContext.executionID);
		result.key = activity.getKey();
		result.setStatus(Result.Status.ERROR);

		for (Program prg: activity.getExecutionSet())
		{
			final String prgID = prg.getProgramID();
			result.setExecStatus(prgID, ProgramResult.ProgramStatus.ERROR);

			final String exc_msg = exception.getMessage();
			result.setResultForPrg(prgID, "failure_reason", exc_msg == null ? "unknown": exc_msg);

			StringWriter debug = new StringWriter();
			debug.write("The following information is for debugging purpose only: you can safely ignore it, "
			            +"or you can send it to the development team along with the workflow causing the problem\n");
			exception.printStackTrace(new PrintWriter(debug));
			result.setResultForPrg(prgID, "failure_reason_debug", debug.toString());
		}
		result.dumpContent(processContext.resultsDirectory());

		// we do not modify the activity (setting its result or state), instead we inject the result into the execution loop
		ExecutionLoop.executionLoop.add(result);
	}
	
	protected void handleFinishedProcess(Process process)
	{
		Result result = process.getCurrentExecutionSummary();
		ProcessContext pContext = process.getContext();
        //result.setWorkflowXML(workflowXML);
        Log.log.info(()->"Marking process as finished: "+process);
        // zip the results
        File zipped_results = new File(pContext.workingDirectory(),"results.zip");
        boolean zip = Utile.zipDirectory(pContext.resultsDirectory(), zipped_results, new LinkedList<String>());
        Utile.deleteRecursively(pContext.resultsDirectory());

        result.setZipFile(zip ? zipped_results : null);
        Serveur.resultStore().updateResult(result);
        
        processes.remove(new PMK(process.getContext().workflowID, process.getContext().executionID));
        process.setResult(result);
        process.setState(result.getStatus());
	}

	protected void handleResults()
	{
		for (int idx=0; idx<MAX_STEPS_RESULTS; idx++)
		{
			Result result = getNextResult();
			if (result == null)
				break;
			jobs.tryAcquire(); // should never fail
			Log.log.info(()->"Handling result: "+result);
			
			final Process process = getProcess(result.workflowID(), result.executionID());
			if (process==null)
			{
				// If we got an invalid result from a platform, ignore it
				Log.log.severe(()->"No process for result: dropping it: "+result);
				continue;
			}
			final Activity activity = process.getActivity(result.key);
			if (activity==null) // see comments above
			{
				Log.log.severe(()->"No activity for result: dropping it: "+result);
				continue;
			}
			activity.setResult(result); // this takes care of informing the requester (client) if appropriate

			// For the moment being, if we received an result indicating the progress of an execution WITHIN an
			// execution set, we do not examine it and we stop here.
			// (sent by platform in an event w/ type=EXECUTION_PROGRESS
			if (result.getStatus().equals(Result.Status.RUNNING))
				continue;

			// mark end
			for (Program prg: activity.getExecutionSet())
			{
				activity.getContainer().result.getProgramInfo(prg.getProgramID()).end = new java.util.Date();
			}
			// no need to store it in resultStore, this will be done in the next step
			
			if (!process.isClosed())
			{
				// NB: we don't want to update the process.result itself until the process is finished (see javadoc for this field)
				Serveur.resultStore().updateResult(process.getCurrentExecutionSummary());
				/* activatedSteps = */ process.activateNextSteps(this);
			}
			/*
			 * NB: activatedSteps==0 does NOT mean that the process is closed: in a wf with two different branches that
			 * can be run in parallel, when the 1st branch ends no activities is activated but the 2nd branch is still
			 * running.
			 */
			/* The process may be closed after activateNextSteps() is called */
			if (process.isClosed())
				handleFinishedProcess(process);
		}
	}
	
	/**
	 * Reattaches a requester to the supplied list of processes. This is typically needed when a client disconnects
	 * then re-connects to the server, so that it receives again notifications for its running workflows.
	 * @param client
	 *            the requester to re-attach to the processes
	 * @param fromResults
	 *            the list of results designating the processes that should be re-attached to the requester. The list
	 *            is filtered, results whose status are {@link Status#isClosed() closed} are not taken into account.
	 * @implementation ExecutionLoop handles nothing like the notion of a "client", so it could also be used to attach
	 *                 a process to a different client --event if we do not have any use-case for such a thing for the
	 *                 moment being.
	 * @throws NullPointerException if {@code fromResults} is {@code null}
	 */
	public void reattachProcesses(Requester client, ArrayList<Result> fromResults)
	{
		// instead of iterating on results then on the internal processes, build a pre-filtered map { wfID: execIDs }
		HashMap <WorkflowID, ArrayList<ExecutionID>> wfID_execIDs = new HashMap<WorkflowID, ArrayList<ExecutionID>>();
		for (Result result: fromResults)
		{
			if (result.getStatus().isClosed())
				continue;
			if (!wfID_execIDs.containsKey(result.workflowID))
				wfID_execIDs.put(result.workflowID, new ArrayList<ExecutionID>());
			wfID_execIDs.get(result.workflowID).add(result.executionID);
		}
		
		Enumeration <PMK> pmks = processes.keys();
		while (pmks.hasMoreElements())
		{
			PMK pmk = pmks.nextElement();
			if (wfID_execIDs.containsKey(pmk.wfID)) {
				if (wfID_execIDs.get(pmk.wfID).contains(pmk.execID)) {
					/* Re-attach it */
					Process p = processes.get(pmk);
					if (p!=null) // enumerations returned by ConcurrentHashMap reflect a state that may have changed
						p.setRequester(client);
				}
			}
		}
	}
	
	public void mainLoop()
	{
		mainLoop:
		while ( ! Thread.interrupted() )
		{
			Log.log.log(Level.FINE, "proc: {0} inv-0: {1} jobs: {2} wf: {3} activity: {4} results: {5} ",
			            new Object[]{processes.size(),
			                         jobs.availablePermits()-(submittedProcesses.size()+activities.size()+results.size()),
			                         jobs.availablePermits(), 
			                         submittedProcesses.size(), activities.size(), results.size()});
			
			try
			{
				waitForSomethingToDo:
				while ( ! Thread.currentThread().isInterrupted() ) // do not reset the interrupt state
				{
					Log.log.log(Level.FINEST, "acquire");
					jobs.acquire();
					jobs.release(); // there's something to do!
					break waitForSomethingToDo;
				}
			}
			catch (InterruptedException e)
			{ 
				break mainLoop;
			}

			if ( Thread.interrupted() ) break mainLoop;
			handleSubmittedProcesses();
			if ( Thread.interrupted() ) break mainLoop;
			handleActivities();
			if ( Thread.interrupted() ) break mainLoop;
			handleResults();
		}
	}
	
	/**
	 * @return
	 */
	// equivalent to: context_signature
	public Process.ProcessContext getContextTemplate()
	{
		Process.ProcessContext pContext = new Process.ProcessContext();
		pContext.executionID = new ExecutionID();
		return pContext;
	}

	@Override
    public void run()
    {
		addConsoleCommand();
	    mainLoop();
    }

	protected void addConsoleCommand()
	{
		Application.getApplication().console.addCommand(new Console.ConsoleCommand() {
			@Override
			public String description()
			{
				return "";
			}
			@Override
			public String[] commands() { return new String[]{"execloop.show", "execloop.stop"}; }
			@Override
			public void execute(String command, String argument, PrintStream p)
			{
				if ("execloop.show".equals(command))
					showProcesses(p, argument);
				if ("execloop.stop".equals(command))
					p.println("Unimplemented");
			}
			private void showProcesses(PrintStream p, String args)
			{
				ExecutionLoop _execLoop = ExecutionLoop.this;
				p.format("\t# of processes: %d -- activities: %d -- results: %d\n",
				         _execLoop.processes.size(), _execLoop.activities.size(), _execLoop.results.size());
				p.format("\t# of submitted processes: %d -- jobs: %d\n",
				         _execLoop.submittedProcesses.size(),_execLoop.jobs.availablePermits());
			}
		});
	}

	public static void saveState() throws java.io.IOException
	{
		java.io.File file = new File(PraxisPreferences.workspace(), "execution.loop");
		// c'est pas parfait parce qu'il peut être started en attendant... mais bref
		if (executionLoopThread.isAlive())
			throw new IllegalStateException("Thread is alive");
		java.io.FileOutputStream fos = new java.io.FileOutputStream(file);
		java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fos);
		oos.writeObject(executionLoop);
		oos.flush();
		fos.close();
	}

	protected static ExecutionLoop restoreState()
	{
		java.io.File file = new File(PraxisPreferences.workspace(), "execution.loop");
		if ( ! file.exists() || !file.isFile() || !file.canRead() )
			return new ExecutionLoop();
		try
		{
			java.io.FileInputStream fis = new java.io.FileInputStream(file);
			java.io.ObjectInputStream ois = new ObjectInputStream(fis);
			ExecutionLoop execLoop = (ExecutionLoop) ois.readObject();
			fis.close();
			file.delete();
			Log.log.info("Restored the state of the previously saved execution loop");
			return execLoop;
		}
		catch (Throwable t)
		{
			Log.log.severe("Could not restore the state of a previously saved execution loop");
			return new ExecutionLoop();
		}
	}

	private void readObject(ObjectInputStream in) throws java.io.IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		for (Process process: processes.values())
			for (Activity activity: process.activities)
				WorkflowPlanner.assignPlatform(activity);
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException
	{
		for (Result result: results)
			result.readyToSerialize();
		out.defaultWriteObject();
	}
}
