package eu.telecom_bretagne.praxis.common;

import java.net.ProxySelector;

import org.apache.commons.lang.SystemUtils;

import com.github.markusbernhardt.proxy.ProxySearch;
import com.github.markusbernhardt.proxy.ProxySearch.Strategy;

public class ProxyAutoDetect
{
	/**
	 * Initializes the proxy auto-detection by setting the {@link ProxySelector#setDefault(ProxySelector) default proxy
	 * selector}.  If the configuration item {@code proxy.auto_detect.enabled} is false, it does nothing.
	 */
	public static void initialize()
	{
		if ( ! Configuration.getBoolean("proxy.auto_detect.enabled"))
			return;

		ProxySelector.setDefault(ProxyAutoDetect.getDefaultProxySearch().getProxySelector());
	}

	static ProxySearch getDefaultProxySearch()
	{
		final ProxySearch proxySearch = new ProxySearch(); // https://github.com/MarkusBernhardt/proxy-vole

		/* 
		 * NB: Strategy BROWSER: is equivalent to IE on windows, and FIREFOX on others
		 *     See source code for
		 *     - com.github.markusbernhardt.proxy.ProxySearch#addStrategy(Strategy)
		 *     - com.github.markusbernhardt.proxy.ProxySearch#getDefaultBrowserStrategy()
		 *     - com.github.markusbernhardt.proxy.util.PlatformUtil#getDefaultBrowser()
		 */
		if ( SystemUtils.IS_OS_LINUX )
		{
		    proxySearch.addStrategy(Strategy.JAVA);
			proxySearch.addStrategy(Strategy.OS_DEFAULT);
			proxySearch.addStrategy(Strategy.GNOME);
			proxySearch.addStrategy(Strategy.KDE);
			proxySearch.addStrategy(Strategy.FIREFOX);
			proxySearch.addStrategy(Strategy.ENV_VAR);
		}
		else if ( SystemUtils.IS_OS_WINDOWS )
		{
		    proxySearch.addStrategy(Strategy.JAVA);
			proxySearch.addStrategy(Strategy.OS_DEFAULT);
			proxySearch.addStrategy(Strategy.WIN);
			proxySearch.addStrategy(Strategy.FIREFOX);
			proxySearch.addStrategy(Strategy.IE);
			proxySearch.addStrategy(Strategy.ENV_VAR);
		}
		else
		{
		    proxySearch.addStrategy(Strategy.JAVA);
			proxySearch.addStrategy(Strategy.OS_DEFAULT);
			proxySearch.addStrategy(Strategy.FIREFOX);
			proxySearch.addStrategy(Strategy.ENV_VAR);
		}
		// TODO WPAD ? quand est-ce que c'est appelé dans l'auto-détection?
		return proxySearch;
	}

}
