/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import eu.telecom_bretagne.praxis.common.Environment;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;


/**
 * @author Sébastien Bigaret
 */
public class ClientPrefs {
	
	private ClientPrefs() {}

	public static final String clientPrefs_usage=
		"Usage : client_prefs <cmd>\n" +
		"  w/ cmd: '-s': show, '-e': edit prefix, -c <cslc>: set CSCL";
	
	public static void main(String[] args)
	throws BackingStoreException, IOException {
		final String CLIENT="client";
		final String PREFIX="prefix";
		Preferences root_node;
		if ( ( args.length < 1 || args.length > 2 )
				|| ( "-e".equals(args[0]) && args.length > 2 )
				|| ( "-s".equals(args[0]) && args.length > 1 )
				|| ( "-c".equals(args[0]) && args.length != 2 ) ) {
			System.err.println(clientPrefs_usage);
			System.exit(-1);
		}
		root_node=Preferences.userRoot().node(PraxisPreferences.rootNode); /* Create if necessary */
		
		String prefix = root_node.get(PREFIX, "<unset>");
		root_node.get(PREFIX, prefix);
		root_node.flush();
		
		/* show values, always */
		System.out.println("Current prefix: "+prefix);
		System.out.println("User ID / CSLC login:"+PraxisPreferences.get(CLIENT, Environment.getLooseCredentialsConfigurationName()));
		if ( "-e".equals(args[0]) ) {
			String line = "";
			if ( args.length < 2 ) {
				System.out.print("Enter new value > ");
				System.out.flush();
				InputStreamReader isr = new InputStreamReader ( System.in );
				BufferedReader stdin = new BufferedReader ( isr );
				line = stdin.readLine();
			}
			else {
				line = args[1];
			}
			if ( line != null && line.length() != 0 ) {
				root_node.put(PREFIX, line);
				root_node.flush();
				System.out.println("prefix set to value: "
				                   + root_node.get(PREFIX, "<unset>"));
			} else {
				System.out.println("prefix value left untouched");
			}
		}
		if ( "-c".equals(args[0]) ) {
			String cslc = args[1];
			if ( cslc.length() != 0 ) {
				PraxisPreferences.put(CLIENT, Environment.getLooseCredentialsConfigurationName(), cslc);
				System.out.println("CSLC set to value: "
				                   + PraxisPreferences.get(CLIENT, Environment.getLooseCredentialsConfigurationName()));
			} else {
				System.out.println("CSLC value left untouched");
			}
		}
	}
	
}
