#! /bin/bash
shopt -s nullglob

# transform original/praxis_XX files into .properties files

cd ${0%/*}/original
for lang in el en es fr; do
    t9n="praxis_${lang}.properties"
    encoding=("praxis_${lang}.encoding".*)
    encoding="${encoding##*.}"
    if [ -z "${encoding}" ]; then
        echo "Warning: could not find encoding for ${t9n}, skipping" >&2
        continue
    fi
    echo "${t9n}: ${encoding} to ascii..." >&2

    native2ascii -encoding "${encoding}" "${t9n}" "../${t9n}"
    # we use java.text.MessageFormat, escape simple quotes
    sed -i -e "s/'/''/g" "../${t9n}"
    # remove the emacs' coding system
    sed -i -e '/^# *-\*- coding:/ d' "../${t9n}"
done
cd -

# This file was initially encoded with upper-cased hexadecimal
# sed -i -E 's/\\u([a-zA-Z0-9]{4})/\\u\U\1/g'  "praxis_fr.properties"
