#! /bin/bash
# Check that keys in praxis_*.keys (built with extract_keys) are used in
# the sources folders

dirs="../../src ../../../praxis-swing-gui/src"

check()
{
  echo -e "#\n# keys in $1 which are not used in the source folders:\n#"
  cat "$1" | while read key; do
    if ! grep -qr "$key" $dirs ; then
        echo $key
    fi
  done
}
check praxis_en.keys
check praxis_fr.keys
