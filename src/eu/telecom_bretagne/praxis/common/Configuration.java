/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.server.RMISocketFactory;
import java.security.KeyStore;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;
import eu.telecom_bretagne.praxis.common.Launcher.InitialisationDelegate;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;


public final class Configuration
{
	private static final String REVISION = "revision";

	/** This class name */
	private static final String CLASS = PraxisPreferences.class.getName();

	/*
	 * Java par.8.7 Static Initializers The static initializers and class variable initializers are executed in
	 * textual order.
	 */
	protected static final Properties defaults;
	protected static final Properties common;
	protected static final Properties configuration;

	protected static final String[] int_keys = {"client.result.max_file_size",
	                                            "server.rmi.registry.port",
	};

	protected static final ArrayList <ProgramDescription[]> declaredDescriptions = new ArrayList<ProgramDescription[]>();

	static
	{
		defaults = new Properties();
		common = new Properties(defaults);
		configuration = new Properties(common);

		try
		{
			final InputStream defaultsStream = Environment.getConfigurationDefaults();
			if (defaultsStream != null)
			{
				defaults.load(defaultsStream);
				defaultsStream.close();
			}

			final InputStream configurationStream = FileResources.inputStreamForResource(Environment.getConfigurationFile());
			configuration.load(configurationStream);
			configurationStream.close();

			final String commonPath = configuration.getProperty("common.config","").trim();
			if (!"".equals(commonPath))
			{
				final InputStream commonStream = FileResources.inputStreamForResource(commonPath);
				common.load(commonStream);
				commonStream.close();
			}
			ArrayList<String> errors = checkConfiguration();
			injectSSLCertificates(errors);

			if (!errors.isEmpty())
				throw new IllegalArgumentException(Utile.join(errors, "\n"));

			if ("true".equals(get("rmi.useHTTP")))
				/*FIX sun.rmi*/RMISocketFactory.setSocketFactory(new RMIMasterSocketFactory(new sun.rmi.transport.proxy.RMIHttpToCGISocketFactory()));
			else
				/*FIX sun.rmi*/RMISocketFactory.setSocketFactory(new RMIMasterSocketFactory());
			// windows specific: use system LAF
			// note: do not use the system LAF on linux, it is slooow (+some items, such as JTrees, are not displayed as they should)
			if ("WINDOWS".equals(Environment.getPlatformOS()))
				javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
		}
		catch (Throwable e) // catch all, yes we mean it!
		{
			final String err = "Could not load the configuration file, exiting";
			Log.log.log(Level.SEVERE, err, e);
			// it is severe enough to print it on the stderr as well
			System.err.println(err);
			e.printStackTrace(System.err);
			
			System.exit(-1);
		}
	}

	/**
	 * The rmi registry port. Configuration key: {@code "server.rmi.registry.host"}
	 */
	public static final String        RMI_REGISTRY_HOST = get("server.rmi.registry.host");

	/**
	 * The rmi registry port. Configuration key: {@code "server.rmi.registry.port"}
	 */
	public static int                 RMI_REGISTRY_PORT = Integer.parseInt(get("server.rmi.registry.port", "0"));

	/**
	 * @param key
	 * @param errors
	 */
	private static void checkColor(String key, ArrayList<String> errors)
	{
		try
		{
			decodeColor(get(key));
		}
		catch (IllegalArgumentException e)
		{
			errors.add("Key "+key+": invalid color (current value: "+get(key)+")");
		}
	}

	private static void checkDescriptionProviderClass(String key, ArrayList<String> errors)
	{
		final String value = get(key);
		if (value==null || "".equals(value))
		{
			Log.log.fine("no descriptions to check");
			return;
		}
		try
		{
			final Class<?> p;
			p=Class.forName(value+".AllDescriptions");
			final ProgramDescription[] descriptions;
			descriptions = (ProgramDescription[]) p.getMethod("getDescriptions").invoke(null);
			declaredDescriptions.add(descriptions);
		}
		catch (ClassNotFoundException e)
		{
			errors.add("Key "+key+": class not found (current value: "+value+")");
		}
		catch (ClassCastException e)
		{
			errors.add("Key "+key+": return type of the method "+value+".getDescriptions() must be ProgramDescription[]");
		}
		catch (NoSuchMethodException e)
		{
			errors.add("Key "+key+": class "+value+" does not declare the static method getDescriptions()");
		}
		catch (Throwable t)
		{
			/* NullPointerException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException*/
			errors.add("Key "+key+": "+get(key)+"().getDescriptions() threw "+t.toString());
			t.printStackTrace();
		}
	}

	private static void checkInitialisationDelegate(String key, ArrayList<String> errors)
	{
		final String value = get(key);
		if (value==null || "".equals(value))
		{
			Log.log.fine("no initialisation delegate to check");
			return;
		}
		try
		{
			final Class<?> p;
			p=Class.forName(value);
			InitialisationDelegate delegate = (InitialisationDelegate) p.newInstance();
			Launcher.setDelegate(delegate);
		}
		catch (ClassNotFoundException | NoClassDefFoundError e)
		{
			errors.add("Key "+key+": class not found (current value: "+value+")");
		}
		catch (InstantiationException e)
		{
			errors.add("Key "+key+": cannot create an instance of class "+value+" (the class cannot be abstract, and it must have a nullary constructor)");
			e.printStackTrace();
		}
		catch (ClassCastException e)
		{
			errors.add("Key "+key+": the supplied class should be a subclass of eu.telecom_bretagne.praxis.common.InitialisationDelegate");
		}
		catch (Throwable t)
		{
			/* NullPointerException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException*/
			errors.add("Key "+key+": "+get(key)+"().getDescriptions() threw "+t.toString());
			t.printStackTrace();
		}
	}
	protected static ArrayList<String> checkConfiguration()
	{
		final String marker = "-CHECK_";
		final String ignored_marker = "ignore.";
		final String system_marker = "system.";

		ArrayList<String> errors = new ArrayList<String>();
		ArrayList <String> ignored = new ArrayList<String>();

		// get ignore pragmas
		for (String key: configuration.stringPropertyNames())
		{
			if ( key.startsWith(ignored_marker) )
				ignored.add(key.substring(ignored_marker.length()));
		}

		// get system properties
		for (String key: configuration.stringPropertyNames())
		{
			if ( key.startsWith(system_marker) )
			{
				final String _key = key.substring(system_marker.length());
				final String value = get(key);
				Log.log.fine(()->"Setting system property "+key+"="+value);
				System.setProperty(_key, get(key));
				if ("java.util.logging.config.file".equals(_key))
					resetLogManager();
			}
		}

		for (String key: configuration.stringPropertyNames())
		{
			if (!key.endsWith(marker))
				continue;
			boolean ignore=false;
			for (String ignored_item: ignored)
				if (key.startsWith(ignored_item))
				{
					ignore=true;
					break;
				}
			if (ignore)
				continue;
			key = key.substring(0,key.length()-marker.length());
			final String type = key.substring(key.lastIndexOf('_')+1);
			key = key.substring(0, key.lastIndexOf('_'));

			if ("DIR".equals(type))
				checkDirectory(key, errors);
			else if ("BOOLEAN".equals(type))
				checkAndNormalizeBoolean(key, errors);
			else if ("FILE".equals(type))
				checkFile(key, errors);
			else if ("FILE-EXTENSION".equals(type))
				checkFileExtension(key, errors);
			else if ("INT".equals(type))
				checkInteger(key, errors);
			else if ("COLOR".equals(type))
				checkColor(key, errors);
			else if ("REGEXP".equals(type))
				checkRegexp(key, errors);
			else if ("DESCRIPTIONS".equals(type))
				checkDescriptionProviderClass(key, errors);
			else if ("INIT".equals(type))
				checkInitialisationDelegate(key, errors);
		}
		// last: application_revision
		try
		{
			String revision = Configuration.get(REVISION).trim();
			if (revision.startsWith("$Rev: "))
			{
				// Old format: "$Rev: nnn$"
				revision = revision.substring(6).replace('$', ' ').trim();
			}
			set(REVISION, revision);
			checkInteger(REVISION, errors);
		}
		catch (NullPointerException | NumberFormatException e)
		{
			errors.add("Invalid revision");
		}

		return errors;
	}

	/**
	 * Searches the keys javax.net.ssl.trustStore/keyStore and uses the ressources pointed by these keys to load the
	 * corresponding certificates into the trust/key manager.  This is equivalent to defining the same system
	 * properties (http://java.sun.com/j2se/1.5.0/docs/guide/security/jsse/JSSERefGuide.html), however do this
	 * programmatically here allows us to store the certificates as resources within jars.
	 * @param errors
	 */
	protected static void injectSSLCertificates(ArrayList<String> errors)
	{
		TrustManager[] trustManagers = null;
		KeyManager[] keyManagers = null;
		KeyStore keystore;
		try
		{
			if (!"".equals(get("javax.net.ssl.trustStore", "")))
			{
				TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				keystore = KeyStore.getInstance(KeyStore.getDefaultType());
				try ( InputStream is = ClassLoader.getSystemResourceAsStream(get("javax.net.ssl.trustStore")) )
				{
					keystore.load(is, get("javax.net.ssl.trustStorePassword").toCharArray());
				}
				trustManagerFactory.init(keystore);
				trustManagers = trustManagerFactory.getTrustManagers();
			}
			if (!"".equals(get("javax.net.ssl.keyStore", "")))
			{
				KeyManagerFactory keyMgrF = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
				keystore = KeyStore.getInstance(KeyStore.getDefaultType());
				try ( InputStream is = ClassLoader.getSystemResourceAsStream(get("javax.net.ssl.keyStore")) )
				{
					keystore.load(is, get("javax.net.ssl.keyStorePassword").toCharArray());
				}
				keyMgrF.init(keystore, get("javax.net.ssl.keyStorePassword").toCharArray());
				keyManagers = keyMgrF.getKeyManagers();
			}
			if (trustManagers==null && keyManagers==null)
				return;
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(keyManagers, trustManagers, null);
			SSLContext.setDefault(sc);
		}
		catch(Throwable e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param key
	 * @param errors
	 */
	private static void checkAndNormalizeBoolean(String key, ArrayList<String> errors)
	{
		final String error = "Key "+key+": not a valid boolean value (current value: "+get(key)+")";
		final String value=get(key, "").toLowerCase();
		if ("".equals(value)) // null or empty value: false
		{
			configuration.setProperty(key,"false");
			return;
		}
		if (   "1".equals(value)
				|| "true".equals(value)
				|| "y".equals(value) )
		{
			configuration.setProperty(key,"true");
			return;
		}
		if (   "0".equals(value)
				|| "false".equals(value)
				|| "n".equals(value) )
		{
			configuration.setProperty(key,"false");
			return;
		}
		errors.add(error);
	}

	/**
	 * @param key
	 * @param errors
	 */
	private static void checkDirectory(String key, ArrayList<String> errors)
	{
		final String error = "Key "+key+": the directory does not exists, or it is not readable (current value: "+get(key)+")";
		try
		{
			if (!FileResources.file(get(key)).isDirectory())
				errors.add(error);
		}
		catch (NullPointerException | ResourceNotFoundException | SecurityException e)
		{
			e.printStackTrace();
			errors.add(error);
		}
	}

	/**
	 * @param key
	 * @param errors
	 */
	private static void checkFile(String key, ArrayList<String> errors)
	{
		//Log.log.finest("checkFile() for key: "+key+" and value: "+get(key));
		final String error = "Key "+key+": the filename does not exists, or it is not readable (current value: "+get(key)+")";
		try ( InputStream is = FileResources.inputStreamForResource(get(key)) )
		{
			/* do nothing, we just want to check if an error is raised */
		}
		catch (NullPointerException | ResourceNotFoundException | IOException e)
		{
			errors.add(error);
		}
	}

	/**
	 * 
	 * @param key
	 * @param errors
	 */
	private static void checkFileExtension(String key, ArrayList<String> errors)
	{
		String fileExtension = get(key);
		if (fileExtension==null || "".equals(fileExtension))
		{
			errors.add("File extension "+key+" cannot be null or empty");
			return;
		}
		if (!fileExtension.startsWith("."))
			fileExtension = "."+fileExtension;
		configuration.setProperty(key, fileExtension);
	}

	/**
	 * @param key
	 * @param errors
	 */
	private static void checkInteger(String key, ArrayList<String> errors)
	{
		final String value = get(key);
		if ( value == null )
		{
			errors.add("Key "+key+": not an integer (it is null)");
			return;
		}
		try (Scanner scanner = new Scanner(value.trim()))
		{
			boolean error = false;
			if ( !scanner.hasNextInt() )
				error = true;
			else
				scanner.nextInt(); // Let the scanner advance to the next token

			if ( !error && scanner.hasNext() ) // the scanner found an int, but there's something after that
				error = true;

			if ( error )
			{
				errors.add("Key " + key + ": not an integer (current value: " + value + ")");
				return;
			}
		}
	}

	/**
	 * @param key
	 * @param errors
	 */
	private static void checkRegexp(String key, ArrayList<String> errors)
	{
		final String err = "Key "+key+": not a valid regular expression (current value: "+get(key)+")";
		try
		{
			Pattern.compile(get(key));
		}
		catch (PatternSyntaxException | NullPointerException e) { errors.add(err); }
	}

	/**
	 * Returns the color encoded either as a RGB hexadecimal string (e.g. 0xFF00FF), or as the name of a field in the
	 * class {@link java.awt.Color} (e.g. "BLACK")
	 * @param color
	 *            an encoded color
	 * @return The corresponding color, or null in case color is the empty string or if it is null
	 * @throws IllegalArgumentException if the color could not be decoded.
	 */
	public static Color decodeColor(String color) throws IllegalArgumentException
	{
		if (color==null)
			return null;
		color = color.trim();
		if ("".equals(color))
			return null;

		Color c = null;
		try
		{
			c=Color.decode(color);
		}
		catch (NumberFormatException e) { /* failed */ }
		if (c==null)
			try
			{
				c = (Color)Color.class.getField(color).get(null);
			}
			catch (ReflectiveOperationException | IllegalArgumentException | NullPointerException e)
			{ /* failed */ }
		if (c==null)
			throw new IllegalArgumentException("Invalid color: "+color);
		return c;
	}

	public static String get(String key)
	{
		return configuration.getProperty(key);
		/*
		if (key.indexOf(".") != -1)
		{
			String parent_key = key.substring(0, key.lastIndexOf("."));
			String parent_value = configuration.getProperty(parent_key);
			if (parent_value != null)
				value = parent_value + value;
		}
		return value;
		 */
	}

	public static String get(String key, String default_value)
	{
		String value = configuration.getProperty(key);
		if (value == null)
			return default_value;
		return value;
	}

	/**
	 * Returns the list of (non-empty) strings separated by spaces in the value corresponding to the supplied key.
	 * @param key
	 *            the key to search
	 * @return a list of strings
	 */
	public static String[] getArray(String key)
	{
		ArrayList<String> a= new ArrayList<String>();
		String[] strings = get(key,"").split(" ");
		for (String s:strings)
			if (!"".equals(s))
				a.add(s);

		return a.toArray(new String[a.size()]);
	}

	public static boolean getBoolean(String key)
	{
		return "true".equals(get(key, ""));
	}

	public static void setBoolean(String key, boolean value)
	{
		configuration.setProperty(key, value ? "true" : "false");
	}

	public static void set(String key, String value)
	{
		configuration.setProperty(key, value);
	}

	/**
	 * Returns the color stored as a RGB hexadecimal string (e.g. 0xFF00FF).
	 * @param hexRGBColor the hexadecimal RGB string
	 * @return The corresponding color, or null if the key is {@link #isDefined(String) not defined} or if it is
	 * invalid.
	 * @throws IllegalArgumentException if {@hexRGBColor} is invalid
	 */
	public static Color getColor(String hexRGBColor) throws IllegalArgumentException
	{
		String color_rgb_hex=get(hexRGBColor);
		return decodeColor(color_rgb_hex);
	}

	public static ProgramDescription[][] getDeclaredDescriptions()
	{
		return declaredDescriptions.toArray(new ProgramDescription[0][]);
	}

	public static int getInt(String key)
	{
		return Integer.parseInt(get(key));
	}

	public static Class<?> getClass(String key)
	{
		try
		{
			return Class.forName(get(key));
		}
		catch (ClassNotFoundException e)
		{
			return null;
		}
	}

	/**
	 * A key is declared if and only if it is present in one of the configuration files
	 * @param key the key
	 * @return true if the key is declared
	 */
	public static boolean isDeclared(String key)
	{
		return !(get(key)==null);
	}

	/**
	 * A key is said to be defined if and only if it is declared and it has a non-empty value
	 * @param key the key to check
	 * @return true if the key is defined
	 */
	public static boolean isDefined(String key)
	{
		return !("".equals(get(key,"")));
	}

	public static void main(String[] args) throws Exception
	{
		System.out.println(Configuration.get("praxis.img.icon106"));
	}

	/**
	 * Resets the locale used in the application.
	 * 
	 * @param language
	 *            the lowercase two-letter ISO-639 code of the local to use. If {@code null}, "en" is used instead.
	 * @see java.util.Locale
	 * @see I18N
	 */
	public static void resetLocale(final String language)
	{
		Log.log.entering(CLASS, "resetLocale()", language);
		if (language == null)
		{
			Log.log.exiting(CLASS, "resetlocale()");
			return;
		}
		Log.log.fine(() -> MessageFormat.format("Language: {0}", language));
		try
		{
			I18N.setLocale(new Locale(language));
		}
		catch (Throwable t)
		{
			Log.log.log(Level.SEVERE, "Failed to set the locale "+language, t);
		}
		Log.log.exiting(CLASS, "resetlocale()");
	}

	/**
	 * Tells the LogManager to re-read its configuration file
	 * @see LogManager#readConfiguration()
	 */
	protected static void resetLogManager()
	{
		String error=null;

		InputStream loggingProperties = null;

		try
		{
			// is there a logging.properties in the current directory?
			final String loggingPropertiesPathname = "logging.properties";
			if (new File(loggingPropertiesPathname).isFile())
				try
				{
					loggingProperties = new FileInputStream(new File(loggingPropertiesPathname));
				}
				catch (FileNotFoundException e)
				{
					Log.log.warning(() -> MessageFormat.format("Oops, file not found ({0})", e));
				}

			if ( loggingProperties == null )
			{
				// nothing in the current dir, let's search our class path
				try
				{
					final URL url = FileResources.url(loggingPropertiesPathname);
					loggingProperties = url.openStream();

				}
				catch (ResourceNotFoundException | IOException e)
				{
					Log.log.info("logging.properties not found in the resources");
				}	
			}
			try
			{
				if ( loggingProperties != null )
				{
					Log.log.info("Resetting the log manager");
					LogManager.getLogManager().readConfiguration(loggingProperties);
				}
				else
				{
					Log.log.info(()->"No need to reset the log manager, file "+loggingPropertiesPathname+" not found");
				}
			}
			catch (SecurityException e)
			{
				error="Could not reinitialize the logging properties (SecurityException)";
				Log.log.log(Level.WARNING, error, e);
			}
			catch (IOException e)
			{
				error="Could not reinitialize the logging properties (IOException)";
				Log.log.log(Level.WARNING, error, e);
			}
		}
		finally {
			try { if (loggingProperties!= null ) loggingProperties.close(); } catch (IOException e) { /* ignore */ }
		}
	}

	/** Private constructor so that the class cannot be instanciated elsewhere: this is a singleton */
	private Configuration()
	{ /* left intentionally blank */}
}
