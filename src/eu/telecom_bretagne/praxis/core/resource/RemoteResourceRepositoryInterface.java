/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.resource;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteResourceRepositoryInterface extends Remote
{
	public interface ResourceRepositoryInterface extends RemoteResourceRepositoryInterface
	{
		public boolean isDeclared(String prg_id);
		public ProgramDescription programDescriptionForId(String id);
		public ProgramDescription alternateProgramDescriptionForId(String id);
		public String[] getResources();
	}
	public boolean isDeclared(String prg_id) throws RemoteException;
	
	/**
	 * Returns the description registered with the supplied id.
	 * @param id
	 *            the id of the description to search within the repository
	 * @return the corresponding description, or <code>null</code> if no such description exists.
	 * @throws RemoteException
	 */
	public ProgramDescription programDescriptionForId(String id) throws RemoteException;
	
	/**
	 * Searches within the repository a description that can be substituted to the one with the supplied id. A
	 * description is said to be substitutable if at least its provider and its name are the same than the original
	 * one.
	 * @param id id of the description for which a substitute is searched.
	 * @return a description that can be substituted to the supplied one, or <code>null</code> if it cannot be found
	 * @throws RemoteException
	 */
	public ProgramDescription alternateProgramDescriptionForId(String id) throws RemoteException;
	
	public String[] getResources() throws RemoteException;
}
