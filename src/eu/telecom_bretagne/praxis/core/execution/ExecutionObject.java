/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import eu.telecom_bretagne.praxis.core.execution.Result.Status;


public abstract class ExecutionObject implements Serializable
{   
    private static final long serialVersionUID = -3925342982802428907L;

	/**
	 * 
	 * @author Sébastien Bigaret
	 *
	 */
	public static class Context extends HashMap<String, Object> {
		private static final long serialVersionUID = 8366001732046344278L;
	}

	public interface Requester
	{
		public abstract void handleEvent(EventAudit event);
	}

	protected ArrayList<EventAudit> events = new ArrayList<EventAudit>();

	private Context context = new Context();

	/** The unique key associated to this object */
	private Serializable key;

	private String name;

	/** The current state of this object.  At creation time, it is set to {@link Status#NOT_STARTED} */
	protected Status state = Status.NOT_STARTED;

	/** 
	 * Subclasses may replace the default context with their own.
	 * @param key
	 * @param name
	 */
	protected ExecutionObject(Serializable key, String name)
	{
		this.key = key;
		this.name = name;
	}

	protected void addEvent(EventAudit event)
	{
		events.add(event);
	}

	public Context getContext()
	{
		return this.context;
	}

	public Object getKey()
	{
		return key;
	}

	/**
	 * Returns the object's name
	 * @return the receiver's name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the name
	 * @param name the name to be set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	void setContext(Context pContext) // TODO check thuis, normally a context is updated, not replaced + EVENT
	{
		this.context = pContext;
	}

	/**
	 * @return the state
	 */
	public Status getState()
	{
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Status state)
	{
		this.state = state;
		this.addEvent(new EventAudit.StateEventAudit(this, state));
	}

	@Override
	public String toString()
	{
		String repr="name:"+name;
		repr += " state:"+state.name();
		if (key!=null)
			repr += " (key:"+key+")";
		repr += " context:"+context.toString();
		return repr;
	}
}
