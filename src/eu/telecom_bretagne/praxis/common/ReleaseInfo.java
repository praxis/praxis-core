/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;


/**
 * @author Sébastien Bigaret
 */
public abstract class ReleaseInfo
{
	public static final String release              = "3.1";

	public static final String release_date         = "2019-05-09";

	public static final int    application_revision = Configuration.getInt("revision");

	/**
	 * Returns a array of two strings: the release, and the release date
	 * @return the release and the release date
	 */
	static public String[] getVersionInfo()
	{
		return new String[]{ release, release_date };
	}
}
