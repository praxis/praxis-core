/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client;

import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.AUTHENTIFICATION;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.LOGOUT;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.REQUEST_AVAILABLE_RESULTS;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.REQUEST_CANCELLATION;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.REQUEST_DELETION_OF_RESULTS;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.REQUEST_EXECUTION;
import static eu.telecom_bretagne.praxis.common.events.ClientToServerEvent.Type.REQUEST_RESULT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.telecom_bretagne.praxis.client.event.ApplicationListener;
import eu.telecom_bretagne.praxis.common.Application;
import eu.telecom_bretagne.praxis.common.I18N;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.ReleaseInfo;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.events.ClientToServerEvent;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent;
import eu.telecom_bretagne.praxis.common.events.ServerToClientEvent.ServerToClientEventAdapter;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.protocol.connection.TransferProgressListener;





/**
 *
 * @author Sébastien Bigaret
 * @author Luiz Fernando Rodrigues
 */
public class Client implements ApplicationListener
{
	public static interface ClientDelegate
	{
		/** Called when the server has accepted the client credentials */
		public void authenticatedConnectionEstablished();
	}

	public static final Client uiClient = new Client();
	
	/**
	 * User's login
	 */
	private String username;
	
	/**
	 * List of opened workspaces.  This is used in particular by the {@link #uiClient}.
	 */
	protected List<Workspace> workspaces = new ArrayList<Workspace>();

    /**
	 */
	protected CommunicationFacade cnx;
	
	protected ClientDelegate delegate;

	public Client()
	{
		super();
		Application.getApplication().addListener(this);
	}
	
	/**
	 * Returns the username used to connect to the server
	 * @return the username used to connect to the server
	 */
	public String getUsername()
	{
		return username;
	}
	
	/**
	 * Sets the username used to connect to the server
	 * @param username the username used to connect to the server
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public ClientDelegate delegate()
	{
		return delegate;
	}

	public void setDelegate(ClientDelegate delegate)
	{
		this.delegate = delegate;
	}

	/**
	 * Returns the workspace having a given name.
	 * @param name
	 *            name of the workspace to retrieve
	 * @return the requested workspace
	 */
	public Workspace getWorkspace(String name)
	{
		for (int i = 0; i < workspaces.size(); i++)
		{
			Workspace ws = workspaces.get(i);
			if (ws.get_name().equals(name))
				return ws;
		}
		return null;
	}
	
	/**
	 * Creates a new workspace.
	 * @return a newly created workspace
	 */
	public Workspace createNewWorkspace()
	{
		String name = "sans_nom";
		int num = 0;
		// verifie qu'un 'sans nom' n'existe pas deja, sinon ajuste...
		Workspace ws = getWorkspace(name);
		while (ws != null)
		{
			num++;
			name = "sans_nom_" + num;
			ws = getWorkspace(name);
		}
		ws = new Workspace(name);
		workspaces.add(ws);
		return ws;
	}
	
	/**
	 * Unregisters a workspace.
	 * @param name
	 *            the name of the workspace to unregister
	 * @return the unregistered workspace, or {@code null} if no workspace could be found with the supplied name.
	 */
	public Workspace closeWorkspace(String name)
	{
		Workspace ws = getWorkspace(name);
		if (ws != null)
			workspaces.remove(ws);
		return ws;
	}

	/**
	 * The zipped file is deleted after the event has been sent.
	 * @param workflow
	 * @param zip
	 *            . It is deleted after the event has been sent.
	 */
	public void requestExecution(Result result)
	{
		// TODO: argument should be a workflow, not a name + zip?
		Log.log.finest("Requesting execution w/ zip's size: "+(result.zipFile()!=null?result.zipFile().length():"<null>"));
		ClientToServerEvent event = new ClientToServerEvent(REQUEST_EXECUTION);
		event.workflowID = result.workflowID();
		event.data = result;
		event.file_conveyor = result.zipFile();
		result.readyToSerialize();
        event.callback = new CommunicationFacade.EventHandlingCallback() {
			@Override
			public void eventSent(Event evt)
			{
				Log.log.finest("requestExecution: event has been sent");
				if (evt.file_conveyor != null)
					evt.file_conveyor.delete();
			}
		};
		cnx.send(event);
	}
	
	/**
	 * Arreter l'execution d'un workflow dans le serveur
	 * @param nomSC - Nom du workflow qui sera arreté
	 */
	public void requestCancellation(Workflow workflow)
	{
		ClientToServerEvent event = new ClientToServerEvent(REQUEST_CANCELLATION);
		event.workflowID = workflow.id();
		cnx.send(event);
	}
	
	
	/**
	 * 
	 * @param ws_name
	 * @param nomSC
	 * @param index
	 */
	public void requestResult(Result execution)
	{
		Log.log.fine(()->"Requesting result: "+execution);
		ClientToServerEvent event = new ClientToServerEvent(REQUEST_RESULT);
		event.workflowID = execution.workflowID();
		event.data = execution;
		cnx.send(event);
	}
	
	/**
	 * 
	 * @param ws_name
	 * @param nomSC
	 * @param index
	 */
	public void requestResults(ArrayList<Result> executions)
	{
		for (Result execution: executions)
			requestResult(execution);
	}
	
	/**
	 * Asks to the server the list of the results available for a given workflow
	 * @param workflow
	 */
	public void requestsAvailableResults(Workflow workflow)
	{
		ClientToServerEvent event = new ClientToServerEvent(REQUEST_AVAILABLE_RESULTS);
		event.workflowID = workflow.id();
		cnx.send(event);
	}
	
	/**
	 * Requests the deletion of some results
	 */
	public void requestsDeletionOfResults(WorkflowID workflowID, java.util.ArrayList<ExecutionID> results)
	{
		ClientToServerEvent event = new ClientToServerEvent(REQUEST_DELETION_OF_RESULTS);
		event.workflowID = workflowID;
		event.data = results;
		cnx.send(event);
	}
	
	public void disconnect()
	{
		if (cnx!=null && !cnx.isConnected())
		{
			cnx = null;
		}
		if (cnx == null)
			return;
		
		sendLogout();
		cnx.disconnect();
		cnx = null;
	}
	
	public void sendLogout()
	{
		if (cnx == null)
		{
			Log.log.warning("Called but connection is null");
			return;
		}
		ClientToServerEvent event = new ClientToServerEvent(LOGOUT);
		cnx.send(event);
		cnx.disconnect();
	}
	
	/**
	 * Verifier si le client est connecté
	 * @return - true si le client est connecté, false sinon
	 */
	public boolean isConnected()
	{
		return cnx!=null && cnx.isConnected();
	}
	
	public void initConnection(String[] server) throws IllegalArgumentException, IOException
	{
		if (cnx != null)
			throw new IllegalStateException("Client has already been initialized");

		cnx = CommunicationFacade.buildConnection("Client", server);
		cnx.start();
	}
	
	/** returns the current connection between the client and the server
	 * The 1st call actually creates the object
	 */
	public CommunicationFacade getConnection()
	{
		if (cnx == null)
			throw new IllegalStateException("Client has not been initialized");
		return cnx;
	}
	
	/**
	 * Enregister sur la connection un ecouter pour l'envoi de données au serveur
	 * @param ecouter - Un ecouteur
	 */
	public void enregistrerEnvoiEcouter(TransferProgressListener ecouter)
	{
		// TODO //connection.ajouterEnvoyerProgresEcouter(ecouter);
	}
	
	/**
	 * Deplacer un ecouteur de la connection pour l'envoi de données au serveur
	 * @param ecouter
	 */
	public void desenregistrerEnvoiEcouter(TransferProgressListener ecouter)
	{
		// TODO //connection.retirerEnvoyerProgresEcouter(ecouter);
	}
	
	/** Just a container, used :
	 * <ul><li>to store the result
	 *     <li>to serve as a semaphore when waiting for the answer from the
	 *     server, after credentials have been sent
	 * </ul>
	 * @see #connectAndWaitAnswer(String, String)
	 */
	private Utile.SimpleContainer connection_status = new Utile.SimpleContainer();
	
	public void connectAndWaitAnswer(String login, String password, boolean connectStorageManager)
	{
		cnx.addListener(new ServerToClientEventAdapter() {
			@Override
			public void receivedAuthentificationStatus(ServerToClientEvent event) {
				synchronized(connection_status) {
					
					// see comments in Server.clientSendsCredentials() for details on why we have we can receive
					// either String or String[] in event.data when an error occurs
					if (event.data!=null && event.data instanceof String)
					{
						event.data = new String[] { (String) event.data };
					}
					if (event.data!=null && event.data instanceof String[])
					{
						final String error_title;
						final String error;
						String [] data = (String[]) event.data;
						if (data.length > 1)
							error=I18N.s(data[0], Arrays.copyOfRange(data, 1, data.length)).replace("\\n", "\n");
						else if (data.length == 1)
							error = I18N.s(data[0]);
						else
							error = "Unknown error";

						if (data.length > 1)
							error_title = I18N.s(data[0] + "_title");
						else
							error_title = "Unknown error";

						ErrorDialog.showErrorMessage(error_title, error);
						System.exit(-1);// TODO list & identify all exit status
					}
					connection_status.data = ( event.data instanceof Integer );
					connection_status.notify();
				}
			}
			@Override
			public void disconnected(Exception exception)
			{
				synchronized(connection_status) {
					connection_status.data = new Boolean(false);
					connection_status.notify();
				}
			}
		});
		synchronized(connection_status)
		{
			connect(login, password, connectStorageManager);
			try
			{
				connection_status.wait();
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// The following line triggers a bug in eclipse (oxygen, photon) when e.g. trying to get this class' type
		// hierarchy.
		// This is related to https://bugs.eclipse.org/bugs/show_bug.cgi?id=474080
		// Note: the bug happens only if the condition is negated, and disappears when not doing anythiong but simply
		// visiting Utils.java in an editor (Utils declares the SimpleContainer)
		if ( connection_status.data != null && (Boolean) connection_status.data )
		{
			if (delegate != null)
				delegate.authenticatedConnectionEstablished();
		}
		else
			cnx.disconnect();

	}
	/**
	 * @param login user login
	 * @param password
	 */
	public void connect(String login, String password, boolean connectStorageManager)
	{
		/*
		 * If needed, connects the StorageManager before the authentification, so that it receives the first message
		 * sent after successful authentification.
		 */
		if ( connectStorageManager )
			getConnection().addListener(StorageManager.singleton);
		
		ClientToServerEvent event = new ClientToServerEvent(AUTHENTIFICATION);
		username = login;
		event.data = new Object[] { login, password, ReleaseInfo.application_revision };
		cnx.send(event);
	}

	@Override
    public void applicationExiting()
    {
		disconnect();
    }
	
}
