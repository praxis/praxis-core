<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE platform PUBLIC "-//Telecom Bretagne/DTD XML Praxis Platform 1.0//EN" "http://perso.telecom-bretagne.eu/~bigaret/praxis/dtd/platform.dtd">
<platform type="shell">
	<configuration/>
	<programs>
                <program id="test___cat___1.0"/>
                <program id="test___cat_input_with_vdef___1.0"/>
                <program id="test___sleep___1.0"/>
                <program id="test___failure___1.0"/>
                <program id="test___generic___1.0"/>
	</programs>
</platform>
