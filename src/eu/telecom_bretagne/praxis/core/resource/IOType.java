/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.resource;

import java.awt.Color;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.FileResources;


/**
 * This class contains the IO types used within a praxis project, namely: the name of the types, and the colors that
 * should be used to graphically represent them.<br>
 * It is not an enum, so that the i/o types can be dynamically loaded at runtime from the {@link #IOTYPES
 * corresponding resource}.<br>
 * It mimics an {@link Enum} in that it declares methods {@link #valueOf(String)} and {@link #values()}.<br>
 * <b>One should take extreme care when IOTypes are deserialized:</b> since these objects are supposed to act as Enum
 * constants, a deserialized object IOType <code>iotype</code> must immediately replaced by the object returned by
 * <code>IOType.from(iotype.name, iotype.colorName)</code>.
 * @author Sébastien Bigaret
 * @implementation To ensure that the class acts as if it was an Enum, the constructor is kept protected; instances
 *                 are retrieved through {@link #from(String, String)} which creates the object or return the one that
 *                 has been previously created, if any.
 */
public final class IOType implements Serializable
{
    private static final long serialVersionUID = -5331126754023343385L;

	/**
	 * This is the configuration key pointing to the resource (file) where the IO types are described.
	 * This file is itself a property file (in the sense of {@link Properties}) mapping a type's name with
	 * its color.
	 * @see Configuration#decodeColor(String) for details on the encoding of the colors.
	 */
	public static final String IOTYPES = "client.iotypes";

	/**
	 * The key identifying universal types with the configuration file for types (see {@link #IOTYPES}).<br />
	 * It is a comma-separated list of identifiers.
	 * @see #isUniversal
	 */
	public static final String UNIVERSAL_TYPES_DECLARATION = "universal_types";

	/**
	 * Maps IOTypes with their names.
	 * @implementation HashTable was chosen because it does not accept null values for key and value.
	 */
	protected static final Hashtable<String,IOType> knownTypes = new Hashtable<String, IOType>();

	
	/**
	 * Retrieves the i/o types from the property file pointed to by {@link #IOTYPES}
	 */
	static
	{
		Properties ptypes = null;
		try
        {
			ptypes = new Properties();
			ptypes.load(FileResources.inputStreamForResource(Configuration.get("client.iotypes")));
        }
        catch (Exception e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        List <String> universal_types = Arrays.asList(ptypes.getProperty(UNIVERSAL_TYPES_DECLARATION, "").split(" *, *"));
        ptypes.remove(UNIVERSAL_TYPES_DECLARATION);
        for (String s:ptypes.stringPropertyNames())
        {
        	IOType t = new IOType(s, ptypes.getProperty(s), universal_types.contains(s));
        	knownTypes.put(s, t);
        }
	}
	
	/** 
	 * Tells whether the supplied list of IOTypes contains at least one type compatible with all others 
	 * @param types the list of IOType objects to test
	 * @return true if the list contains an IOType accepting all
	 */
	public static boolean acceptsAll(List<IOType> types)
	{
		for (IOType type: types)
			if (type.isUniversal)
				return true;
		return false;
	}
	
	/**
	 * Lists are compatible if and only if:
	 * <ul>
	 * <li>either at least one of them has an IOType that {@link #acceptsAll(List) is compatible with everything},
	 * <li>or the intersection is not empty.
	 * </ul>
	 * @param l1
	 *            the 1st list to test
	 * @param l2
	 *            the 2nd list to test
	 * @return true if the two lists are compatible, false otherwise
	 */
	public static boolean areCompatible(List <IOType> l1, List <IOType> l2) {
		if (acceptsAll(l1) || acceptsAll(l2))
			return true;
        for (IOType t: l1)
            if ( l2.contains(t) )
                return true;
        return false;
    }
	
	/**
	 * 
	 * @param type
	 * @return
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
    public static Color colorForIOType(String type) throws IllegalArgumentException, NullPointerException
    {
    	return valueOf(type).color;
    }
	
	/**
	 * Returns the IOType identified by the supplied name.
	 * @param typeName
	 *            the name of the IOType to retrieve. It cannot be null.
	 * @return the IOType with that name
	 * @throws IllegalArgumentException
	 *             if no such IOType has ever been created.
	 * @throws NullPointerException
	 *             if typeName is null
	 */
    public static IOType valueOf(String typeName) throws IllegalArgumentException, NullPointerException
    {
    	IOType t = knownTypes.get(typeName); // throws NullPointerExc if null
    	if (t==null)
    		throw new IllegalArgumentException("Unknown type "+typeName);
    	return t;
    }
	
	/**
	 * Returns an array containing the available types. The available types are those which were previoulsy created
	 * using {@link #from(String, String)}.
	 * @return the array of known types
	 */
    public static IOType[] values()
    {
    	return knownTypes.values().toArray(new IOType[knownTypes.size()]);
    }
    
	/**
	 * Builds the corresponding IOType, or returns the one that was already created by a previous call if it exists.
	 * After the method is called once with a given name, subsequent calls with the same name always return the same
	 * object.
	 * @param name
	 *            the name for the I/O type
	 * @param colorName
	 *            the color used to graphically display the I/O type, see {@link Configuration#decodeColor(String)}
	 *            for details on how the color may be encoded. If the color cannot be decoded, it defaults to black.
	 *            Note that if the object has already been created by a previous call with a different color, the
	 *            supplied color is ignored and the returned object's color is the one supplied when it was created.
	 * @return the I/O type associated to the supplied name.
	 */
    public static IOType from(String name, String colorName, boolean isUniversal)
    {
    	IOType type;
    	try
    	{
    		type = valueOf(name);  // color is not checked
    	}
    	catch (IllegalArgumentException e)
    	{
    		type = new IOType(name, colorName, isUniversal);
    		knownTypes.put(name, type);
    	}
    	return type;
    }
    
	
	/** the name of the IOType, uniquely identifying it */
    public final String name;
    
    /** The color associated to the IOType, which should be used to graphically represent it */
	public final transient Color color;

	/**
	 * The encoded string corresponding to the IOType's color
	 * @see #from(String, String) for details about this encoding
	 */
    public final String colorName;
	
    /**
     * A universal type can be connected to any other types.
     */
    public final boolean isUniversal;
    
	/**
	 * Builds a new IOType with the supplied name and encoded color. If the color cannot be decoded, it defaults to
	 * black.
	 * @param name
	 *            the name of the IOType
	 * @param color
	 *            the object's color encoded as a String. See {@link #from(String, String)} for details.
	 */
	private IOType(String name, String color, boolean universal)
	{
		this.name = name;
		this.colorName = color;
		this.isUniversal = universal;
		Color c;
		try
		{
			c=Configuration.decodeColor(color);
		}
		catch (IllegalArgumentException e)
		{
			System.out.println("Unrecognized: "+color);
			c=Color.BLACK;
		}
		this.color = c;
	}

	/**
	 * Returns the name of the type. Equivalent to <i>type.colorName</i> (public field), but sometimes one need it (bean
	 * convention)
	 * @return
	 */
	public String getColorName()
	{
		return colorName;
	}

	/**
	 * Returns the name of the type. Equivalent to <i>type.name</i> (public field), but sometimes one need it (bean
	 * convention)
	 * @return
	 */
	public String getName()
	{
		return name;
	}
	
	@Override
    public String toString()
	{
		return this.name;
	}
}
