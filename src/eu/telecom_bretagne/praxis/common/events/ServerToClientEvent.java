/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;

/**
 * 
 * The data stored in this object depends on its type:
 * <ul>
 * <li>AUTHORIZATION_RESULT
 *     <ul>
 *     <li>{@link Event#data}: the application_revision of the server as an integer, or a I18N key + args (String[])
 *         if the authorization failed
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>EXECUTION_STATUS:
 *     <ul>
 *     <li>{@link Event#data}: the {@link Result} reflecting the status of the execution
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>EXECUTION_RESULTS: sets username  and workflow
 *     <ul>
 *     <li>{@link Event#data}: the corresponding {@link Result} object
 *     <li>{@link Event#file_conveyor}: a zipped file containing the requested results
 *     </ul>
 * <li>LIST_OF_AVAILABLE_RESULTS: sets username  and workflow
 *     <ul>
 *     <li>{@link Event#data}: a non-null list of {@link Result}
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>MESSAGE: sets username and workflow
 *     <ul>
 *     <li>{@link Event#data}: a Object[2], containing the message (String, always), and an Exception, if appropriate
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 */
public class ServerToClientEvent extends Event
{    
	static final long serialVersionUID = 9078747677607698045L;
	
	public interface ServerToClientEventListener
	extends EventListener
	{
		/** Invoked when the client receives an answer to its auth.message */
		public void receivedAuthentificationStatus(ServerToClientEvent event);
				
		/** Invoked when the client receives the status of an execution */
		public void receivedExecutionStatus(ServerToClientEvent event);
		
		/** Invoked when the client receives the results of an execution */
		public void receivedExecutionResults(ServerToClientEvent event);
		
		/** Invoked when the client receives the list of results available for a workflow */
		public void receivedAvailableResults(ServerToClientEvent event);
		
		/** Invoked when the client receives a message from the server */
		public void receivedMessage(ServerToClientEvent event);
		
	}
	public static class ServerToClientEventAdapter implements ServerToClientEventListener
	{
		public void receivedAuthentificationStatus(ServerToClientEvent event)  { }
		public void receivedAvailableResults(ServerToClientEvent event)        { }
		public void receivedExecutionResults(ServerToClientEvent event)        { }
		public void receivedExecutionStatus(ServerToClientEvent event)         { }
		public void receivedMessage(ServerToClientEvent event)                 { }
		public void disconnected(Exception exception)                          { }
	}
	public static enum Type {
		AUTHORIZATION_RESULT,
		EXECUTION_STATUS,
		EXECUTION_RESULTS,
		LIST_OF_AVAILABLE_RESULTS,
		MESSAGE,
	}
	
	Type type;
	
	public WorkflowID workflowID;
	
	public ServerToClientEvent(Type type)
	{
		super("no source");
		this.type = type;
	}
	
	@Override
	public void dispatch_to(EventListener listener)
	throws IncompatibleListenerException
	{
		ServerToClientEventListener l;
		try {
			l = (ServerToClientEventListener) listener;
		}
		catch (ClassCastException e) {
			throw new IncompatibleListenerException(listener, this);
		}
		switch (type)
		{
			case AUTHORIZATION_RESULT:          l.receivedAuthentificationStatus(this);  break;
			case EXECUTION_STATUS:              l.receivedExecutionStatus(this);         break;
			case EXECUTION_RESULTS:             l.receivedExecutionResults(this);        break;
			case LIST_OF_AVAILABLE_RESULTS:     l.receivedAvailableResults(this);        break;
			case MESSAGE:          			    l.receivedMessage(this);                 break;
		}
	}
	
	@Override
	public String toString()
	{
		return "<ServerToClient type:"+type.toString()+" wfID:"+workflowID+">";
	}
}
