/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.regex.Pattern;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;


/**
 * A Workspace is an organisational unit which gathers related 
 * {@link eu.telecom_bretagne.praxis.core.workflow.Workflow workflows} together.
 * 
 */
public class Workspace
    extends Observable
{
	protected List <Workflow> workflows = new ArrayList<Workflow>();
	private String name;
	
	/** The pattern object used by {@link #isaValidName(String)} */
	public static final Pattern validNamePattern = Pattern.compile("[a-zA-Z0-9._]+");
	
	/**
	 * Checks whether this is a valid name for a workspace. A valid name is
	 * exclusively made up of alpha-numeric characters plus: <tt>.</tt> (point)
	 * and <tt>_</tt> (underscore).
	 * @param name the name to check
	 * @return <code>true</code> if it is valid, <code>false</code> otherwise.
	 */
	public static boolean isaValidName(String name)
	{
		return validNamePattern.matcher(name).matches();
	}
	
	/**
	 * Constructeur de la classe Workspace,
	 * Cree un squelette vide, sauf la balise <TT>name</TT>.
	 * @param unName nom du Workspace
	 */
	public Workspace(String unName) {
		name = unName;
	}
	
	/**
	 * Accede en lecture a un <TT>Workflow</TT>
	 * @param unNom  nom du <TT>Workflow</TT> que l'on veut recuperer
	 * @return <TT>Workflow</TT> dont le nom est unNom
	 */
	public Workflow getWorkflow(String unNom) {
		for (int i = 0; i < workflows.size(); i++) {
			Workflow courant = workflows.get(i);
			
			if (courant.getName().compareTo(unNom) == 0) {
				return courant;
			}
		}
		
		return null;
	}
	
	/**
	 * The number of workflows currently loaded within the workspace.
	 * @return positive integer.
	 */
	public int getNumberOfWorkflow() {
		return workflows.size();
	}
	
	/**
	 * Accede en lecture a la balises <TT>name</TT>
	 * @return Texte de la balise <TT>name</TT>
	 */
	public String get_name() {
		return name;
	}
	
	/**
	 * Accede en ecriture a la balises <TT>name</TT>
	 * @param unNouveauNom  nouveau nom du <TT>Workspace</TT>
	 */
	public void set_name(String unNouveauNom) {
		name = unNouveauNom;
	}
	
	/**
	 * Ajoute au <TT>Workspace</TT> courant un <TT>Workflow</TT> vide
	 * @return le <TT>Workflow</TT> cree
	 */
	public Workflow createAndAddNewWorkflow(String workflowName) {
		Workflow newWF = new Workflow(workflowName);
		newWF.setUser(Client.uiClient.getUsername());
		workflows.add(newWF);
		setChangedAndNotify();
		return newWF;
	}

	/**
     * Marks the object as changed and notify observers
     * @see #setChanged()
     * @see #notifyObservers()
     */
    public void setChangedAndNotify()
    {
	    this.setChanged();
		this.notifyObservers();
    }
	

	/**
	 * Add a Workflow
	 * @param workflowName
	 * @param warnings 
	 * @return the added workflow, or null if it was already opened
	 * @throws InvalidXMLException 
	 * @throws IllegalArgumentException
	 */
	public Workflow openAndAddWorkflow(String workflowName, XMLWarnings warnings) throws InvalidXMLException {
		Workflow wf=null;
		wf = StorageManager.getWorkflow(workflowName, warnings);
		if (wf!=null)
		{
			workflows.add(wf);
			setChangedAndNotify();
		}
		return wf;
	}
	
	/**
	 * Removes a workflow
	 * @param wfName name of the workflow to remove
	 * @return the removed workflow, or <tt>null</tt> if no such workflow exists
	 */
	public Workflow removeWorkflow(Workflow workflow) {
		if (workflow != null) {
			workflows.remove(workflow);
			setChangedAndNotify();
			return workflow;
		}
		
		return null;
	}
	
	
	public void save()
	{
		eu.telecom_bretagne.praxis.common.Utile.unimplemented();
		
		/*
        File workspaceDirectory = new File(Application.getApplication().wsDirectory(name));
        
        if (!workspaceDirectory.exists())
            workspaceDirectory.mkdirs();
        
        for (Workflow wf: workflows)
            wf.save(new File(Application.getApplication().scFile(name, wf.getName())));
		 */
	}
	
	public Workflow[] getWorkflows() {
		return workflows.toArray(new Workflow[workflows.size()]);
	}
	
}
