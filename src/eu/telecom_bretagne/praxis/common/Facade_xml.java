/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 * This class is dedicated to the management of XML files (read & write)
 * @author Luiz Fernando Rodrigues
 * @author Sébastien Bigaret
 */
public class Facade_xml
{
	
	/**
	 * The SAXBuilder used by methods {@link #read(InputStream, boolean)} and {@link #read(Reader, boolean, boolean)}<br>
	 * <b>IMPORTANT NOTE</b>: SAXBuilder cannot be called concurrently, it is not thread-safe --calling it
	 * concurrently results in errors like:<br>
	 * <code>JDOMParseException: Error in building: FWK005 parse may not be called while parsing.: FWK005 parse may not be called while parsing</code>
	 * <br>
	 * It is therefore very important that every use of this object is protected against such concurrency errors; in
	 * this class, the methods using it (esp. the SAXBuilder's build() methods) <b>MUST</b> be declared
	 * <code>synchronized</code>.
	 */
	static private SAXBuilder builder;
	
	/**
	 * The initialization mainly consists in building once and for all the {@link SAXBuilder} that will be used in
	 * methods {@link #read(File)}, {@link #read(File, boolean)} etc. The SAXBuilder is initialized as a builder
	 * ignoring whitespaces, validating xml documents against their DTDs, and using the specific
	 * {@link PraxisEntityResolver}.
	 * @see #builder <code>builder</code> for an important implementation note
	 */
	static
	{
		builder = new SAXBuilder("com.ctc.wstx.sax.WstxSAXParser", true);
		builder.setIgnoringElementContentWhitespace(true);
		builder.setEntityResolver(new PraxisEntityResolver());
		/* we do NOT want to reuse the parser: if true, we get strange error when reading xml w/ no DTD,
		 * such as results XML serialization form, telling that no grammar was found even when validation
		 * is set to false
		 */
		// Note: this was true w/ the default parser, NOT w/ woodstox anymore
		//       Comments left here on purpose.
		builder.setReuseParser(true);
	}
	
	/**
	 * Construit un Document JDOM a partir de la lecture d'un fichier, avec ou sans validation..
	 * @param path
	 *            indique le chemin du fichier a lire
	 * @param validation
	 *            la lecture de 'unChemin' sera faite avec validation si true
	 * @return the returned value cannot be <tt>null</tt>
	 * @throws InvalidXMLException
	 *             when the document couldn't be built
	 */
	public static Document read(String path, boolean validation) throws InvalidXMLException
	{
		File file = new File(path);
		return read(file, validation);
	}
	
	/**
	 * Construit un Document JDOM a partir de la lecture d'un fichier, avec ou sans validation.
	 * @param file
	 *            indique le fichier a lire
	 * @param validation
	 *            la lecture du fichier sera faite avec validation si true
	 * @return the returned value cannot be <tt>null</tt>
	 * @throws InvalidXMLException
	 *             when the document couldn't be built
	 */
	public static Document read(File file, boolean validation) throws InvalidXMLException
	{
		Log.log.finest("file: " + file.getAbsolutePath());
		if (!file.exists()) // vérifie que le fichier existe physiquement
		{
			Log.log.warning("File does not exist: " + file.getPath());
			throw new InvalidXMLException("");
		}
		
		if (!file.canRead())
		{
			Log.log.warning("accès en lecture à " + file.getPath() + " refusé");
			throw new InvalidXMLException("file " + file.getPath() + " cannot be read");
		}
		BufferedInputStream bis = null;
		try
		{
			bis = new BufferedInputStream(new FileInputStream(file));
			return read(bis, validation);
		}
		catch (FileNotFoundException e)
		{
			throw new InvalidXMLException("File not found: " + file);
		}
		finally { if ( bis != null ) try { bis.close(); } catch (IOException e) { /* ignore */ } }
	}
	
	/**
	 * Reads a XML document from an InputSTream
	 * @param is
	 *            the input stream
	 * @param validation
	 *            whether the XML document should be validated
	 * @return the corresponding {@link Document}
	 * @throws InvalidXMLException
	 *             if an error occured ({@link JDOMException} or {@link IOException})
	 * @see #builder <code>builder</code> for an important implementation note
	 */
	public static synchronized Document read(InputStream is, boolean validation) throws InvalidXMLException
	{
		builder.setValidation(validation);
		try
		{
			return builder.build(is);
		}
		catch (JDOMException | IOException e)
		{
			// e.g. org.jdom.input.JDOMParseException (TBD)
			// java.net.NoRouteToHostException (sans proxy)
			Log.log.warning(e.toString());
			throw new InvalidXMLException(e.toString());
		}
	}
	
	/**
	 * Construit un Document JDOM a partir de la lecture d'un fichier, sans validation.
	 * @param path
	 *            indique le chemin du fichier a lire
	 * @return the returned value cannot be <tt>null</tt>
	 * @throws InvalidXMLException
	 *             when the document couldn't be built
	 */
	public static Document read(String path) throws InvalidXMLException
	{
		return read(new File(path));
	}
	
	/**
	 * Construit un Document JDOM a partir de la lecture d'un fichier, sans validation.
	 * @param file
	 *            indique le fichier a lire
	 * @return the returned value cannot be <tt>null</tt>
	 * @throws InvalidXMLException
	 *             when the document couldn't be built
	 */
	public static Document read(File file) throws InvalidXMLException
	{
		return read(file, false);
	}
	
	/**
	 * Builds a JDOM from the supplied reader.
	 * @param reader
	 *            the one from which the document should be build
	 * @param validation
	 *            indicates whether validation should be done
	 * @param ignoreErrors
	 *            if false, exceptions are catched and re-raised as IllegalArgumentException (see details, below) If
	 *            true, they are silently ignored and the method returns null.
	 * @throws IllegalArgumentException
	 *             if ignoreErrors is false, any exception raised is catched and a new IllegalArgumentException is
	 *             raised at its place, initialized with the original exception's {@link Throwable#getMessage()
	 *             message}.
	 * @return Document the Document, or null if an error occurred
	 * @see #builder <code>builder</code> for an important implementation note
	 */
	public static synchronized Document read(Reader reader, boolean validation, boolean ignoreErrors)
	        throws InvalidXMLException
	{
		Document doc = null;
		builder.setValidation(validation);
		try
		{
			doc = builder.build(reader);
		}
		catch (Exception e)
		{
			Log.log.fine(e.toString());
			if (!ignoreErrors)
				throw new InvalidXMLException(e.getMessage());
		}
		return doc;
	}
	
	/**
	 * Enregistre le contenu du document 'unDocument' dans le fichier indique par 'unChemin'.
	 * @param document
	 *            <TT>Document</TT> source
	 * @param path
	 *            indique le chemin du fichier destination
	 */
	public static void write(Document document, String path) throws IOException
	{
		write(document, new File(path));
	}
	
	/**
	 * Enregistre le contenu du document 'unDocument' dans le fichier indique par 'unChemin'.
	 * @param document
	 *            <TT>Document</TT> source
	 * @param file
	 *            indique le fichier destination
	 */
	public static void write(Document document, File file) throws IOException
	{
		// Test si le chemin est valide
		File parentDir = file.getParentFile();
		
		if (parentDir != null && !parentDir.exists())
		{
			Log.log.finest(()->"Creating directory: " + parentDir);
			parentDir.mkdirs();
		}
		
		// Test si le fichier existe deja
		if (file.exists())
		{
			Log.log.finest("Overriding existing file " + file.getPath());
		}
		
		try (BufferedWriter outFile =
			new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()),
			                                          Charset.forName("UTF-8"))))
		{
			write(document, outFile);
		}
	}
	
	public static void write(Document document, OutputStream output) throws IOException
	{
		BufferedWriter outFile = new BufferedWriter(new OutputStreamWriter(output, Charset.forName("UTF-8")));
		write(document, outFile);
	}
	
	protected static void write(Document document, Writer writer) throws IOException
	{
		Format fmt = Format.getPrettyFormat();
		fmt.setIndent("    ");
		//Normalization should not be used, or we'll corrupt some of the content we save in XML,
		// such as: stdout/stderr in Result, or data for parameters whose type is 'TEXT'.
		// In short, there are various situations where we must not change the content: multiple
		// whitespaces and newlines in particular.
		//fmt.setTextMode(Format.TextMode.NORMALIZE);
		XMLOutputter xmlOut = new XMLOutputter(fmt);
		xmlOut.output(document, writer);
		writer.close();
	}
	
	/**
	 * Writes an XML document with the an embedded DTD. The XML document is written using the UTF-8 charset
	 * @param document the xml document
	 * @param file the file into which the xml document is written. Nte that the file will be overwritten.
	 * @param dtd the DTD that should be embedded into the xml file
	 * @throws IOException in case the file couldn't be written
	 */
	public static void writeXMLWithDTD_UTF8(Document document, File file, String dtd) throws IOException
	{
		BufferedWriter outFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()),
		                                                                   Charset.forName("UTF-8")));
		outFile.write("<?xml version='1.0' encoding='UTF-8'?>\n");
		outFile.write(dtd);

		Format fmt = Format.getPrettyFormat();
		fmt.setIndent("    ");
		fmt.setTextMode(Format.TextMode.NORMALIZE);
		fmt.setOmitDeclaration(true);
		fmt.setOmitEncoding(true);
		document.setDocType(null);
		XMLOutputter xmlOut = new XMLOutputter(fmt);
		xmlOut.output(document, outFile);
		outFile.close();
	}

	/**
	 * Interprets the content of a <code>%boolean</code> XML entity
	 * @return true if str equals "1" or "true", false otherwise
	 */
	public static boolean boolean_for(String str)
	{
		return "1".equals(str) || "true".equals(str);
	}
	
}
