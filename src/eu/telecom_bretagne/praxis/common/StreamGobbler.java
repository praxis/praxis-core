/* License: please refer to the file README.license located at the root directory of the project */
/*
 * Created on Mar 29, 2005
 *
 */
package eu.telecom_bretagne.praxis.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.logging.Level;

/**
 * A thread that keeps reading an InputStream, appending the data read
 * to {@link #getStreamContent() its dedicated buffer}.
 * See for example <a href="http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html">When Runtime.exec() won't</a>
 */
public class StreamGobbler extends Thread {

	/**
	 * The stream from which content should be read.
	 */
	private InputStream stream;
    
	/**
	 * The writer into which the read content is written.
	 */
	private final Writer sw;
	
	/**
	 * The maximum number of bytes to read. If 0 or negative, no limit applies.
	 */
	private long limit = 0;
	
	/** Tells whether run() has been called */
	private boolean started = false;
	
	/**
	 * Builds a new gobbler, ready to read any data from the supplied input stream. The content that is read from that
	 * stream while the thread is {@link #run() running} can be retrieved by calling {@link #getStreamContent()}.
	 * @param stream
	 *            the InputStream that the thread will continously read.
	 * @param writer
	 *            the object into which the content read from the input stream is written. It can be null, in this
	 *            case the content is constantly read from the input stream without being stored.
	 */
	public StreamGobbler(InputStream stream, Writer writer)
	{
		this.stream = stream;
		sw = writer;
	}
	
	/**
	 * Returns the maximum number of bytes this will read from the input stream.
	 * @return the maximum number of bytes the gobbler will read.
	 * @see #setLimit(long)
	 */
	public long getLimit()
	{
		return this.limit;
	}
	
	/**
	 * Sets the maximum number of bytes to read from the input stream.<br>
	 * A negative or zero value suppresses any limitation; if you just want to "eat" the content without storing it,
	 * consider {@link #StreamGobbler(InputStream, Writer) building an object} with a {@code null} writer. <br>
	 * <br>
	 * Note that it is illegal to try to set the limit after the thread starts.
	 * @param limit
	 *            the maximum number of bytes to read. Setting it to 0 or any negative values disables the limitation.
	 * @throws IllegalStateException
	 *             if called after the thread starts.
	 * @see #getLimit()
	 */
	public void setLimit(long limit)
	{
		if (started)
			throw new IllegalStateException("Thread has already been started, the limit cannot be changed anymore");
		this.limit = limit;
	}
	
	/**
	 * Continuously reads the stream supplied at initialization time and
	 * writes its content into an internal buffer whose value can be
	 * retrieved by calling {@link #getStreamContent()}
	 */
	@Override
	public void run() {
		this.started = true;
		if (sw==null)
			run_nowriter();
		if (limit<1)
			run_nolimit();
		else
			run_limit();
		
	}
	
	private void run_nolimit() {
		try
		{
			int c;
			while ( (c = stream.read()) != -1)
			{
				synchronized(sw) { sw.write(c); }
			}
		}
		catch (IOException ioe)
		{ // explicitely ignore the error, simply log it
			Log.log.log(Level.FINER, "IOException raised while reading", ioe);
		}
	}
	
	private void run_limit() {
		long idx = 0;
		try
		{
			int c;
			while ( (idx<limit) && (c = stream.read()) != -1)
			{
				idx++;
				synchronized(sw) { sw.write(c); }
			}
			while ( stream.read() != -1) {/* limit exceeded: do nothing */}
		}
		catch (IOException ioe)
		{ // explicitly ignore the error, simply log it
			Log.log.log(Level.FINER, "IOException raised while reading", ioe);
		}
	}
	
	private void run_nowriter() {
		try
		{
			while ( stream.read() != -1) { /* do nothing, just eat it */ }
		}
		catch (IOException ioe)
		{ // explicitly ignore the error, simply log it
			Log.log.log(Level.FINER, "IOException raised while reading", ioe);
		}
	}
	
	/**
	 * Returns the content of the monitored input stream. This method can be safely called in a MT context
	 * @return the content that has been read, or {@code null} if this was built with a {@code null} writer.
	 */
	public String getStreamContent()
	{
		if ( sw == null )
			return null;
		synchronized (sw) { return sw.toString(); }
	}
}
