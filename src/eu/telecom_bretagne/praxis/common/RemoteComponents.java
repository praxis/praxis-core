/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Level;


import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.swing.JOptionPane;

import org.jdom.Document;

import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;
import eu.telecom_bretagne.praxis.core.resource.RemoteResourceRepositoryInterface;
import eu.telecom_bretagne.praxis.core.resource.ResourceRepository;
import eu.telecom_bretagne.praxis.core.resource.RemoteResourceRepositoryInterface.ResourceRepositoryInterface;
import eu.telecom_bretagne.praxis.server.RemoteServerInterface;
import eu.telecom_bretagne.praxis.server.Serveur;
import eu.telecom_bretagne.praxis.server.RemoteServerInterface.ServerInterface;

/**
 * @author Sébastien Bigaret
 */
public class RemoteComponents
{
	public static final String                   RMI_RESOURCE_REPOSITORY_NAME   = "PraxisResourceRepository";
	
	public static final String                   RMI_SERVER_NAME                = "PraxisServer";
	
	protected String method;
	protected String host;
	protected int port;
	
	protected ResourceRepositoryInterface resourceRepositoryProxy;
	
	protected ServerInterface             serverProxy;
		
	public static final RemoteComponents remoteComponent;
	static
	{
		if (Configuration.RMI_REGISTRY_HOST==null || "".equals(Configuration.RMI_REGISTRY_HOST.trim()))
		{
			remoteComponent = new RemoteComponents("direct", "", 0);
		}
		else
		{
			remoteComponent = new RemoteComponents("rmi", Configuration.RMI_REGISTRY_HOST, Configuration.RMI_REGISTRY_PORT);
		}
	}
		
	/**
	 * Determines whether the description obtained from the remote resources' repository should be cached or not.<br>
	 * Default is true, however in some situations it is desirable to disable the cache, especially when the
	 * descriptions are expected to change during the lifetime of the application. In this situation, the parameter
	 * should be set to false as soon as possible, ideally at application startup.<br>
	 * Setting this parameter does not reset the existing cache, if it exists; as a consequence, setting it to false
	 * then reverting its value to true makes the component re-use any value previously cached.<br>
	 * Callers should be careful when disabling the cache. It should not be disabled in a normal, standard praxis
	 * project, because many elements expects that the description returned by
	 * {@link eu.telecom_bretagne.praxis.core.workflow.Program#getDescription() Program.getDescription()} is always the same objects. However,
	 * there exists special contexts when this is needed, especially for applications working on the descriptions
	 * themselves (for example, an application for building the descriptions); this type of applications usually wants
	 * to display a program and its settings as they would appear in a workflow using the current description.
	 * @see eu.telecom_bretagne.praxis.core.workflow.Program#getDescription() Program.getDescription()
	 * @see ResourceRepository#reloadDescription(ProgramDescription)
	 */
	public boolean                        cacheResourceRepositoryAnswers = true;
	
	/**
	 * A proxy for the rmi object, whose purpose is to make sure that the behaviour of
	 * {@link #programDescriptionForId(String)} is the same on as the original one: given a program id, the method
	 * always returns the <i>same</i> object
	 * @author Sébastien Bigaret
	 */
	protected class ResourceRepositoryProxy
	    implements ResourceRepositoryInterface
	{
		/** the underlying object exported by rmi */
		protected RemoteResourceRepositoryInterface         rmiproxy;
		
		/** cache for programs' description */
		protected TreeMap<String, ProgramDescription> prgDescCache = new TreeMap<String, ProgramDescription>();
		
		/** cache for getResources() */
		protected String[]                            resources;
		

		/**
		 * Builds the proxy
		 * @param rmiproxy
		 *            the underlying object
		 */
		public ResourceRepositoryProxy(RemoteResourceRepositoryInterface rmiproxy)
		{
			this.rmiproxy = rmiproxy;
		}
		
		/**
		 * Simply forwards the request to the underlying mxbeanproxy
		 */
		@Override
		public String[] getResources()
		{
			String[] _ressources = null;
			if (cacheResourceRepositoryAnswers && this.resources != null)
				return this.resources;
			try
            {
				_ressources = rmiproxy.getResources();
            }
            catch (RemoteException e)
            {
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
	            return null;
            }
            if (cacheResourceRepositoryAnswers)
            	this.resources = _ressources;
            return _ressources;
		}
		
		/**
		 * Simply forwards the request to the underlying mxbeanproxy
		 */
		@Override
		public boolean isDeclared(String prg_id)
		{
			try
            {
	            return rmiproxy.isDeclared(prg_id);
            }
            catch (RemoteException e)
            {
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
	            return false;
            }
		}
		
		/**
		 * Returns the description corresponding to the supplied id. The method takes care of caching the values
		 * returned by the underlying mxbeanproxy, so that when the method always returns the same object when it is
		 * called with two parameters id1 and id2 such that both are null or <code>id1.equals(id2)==true</code>
		 */
		@Override
		public ProgramDescription programDescriptionForId(String id)
		{
			if (id == null)
				return null;
			
			ProgramDescription desc=null;
			if (!cacheResourceRepositoryAnswers)
			{
				try
				{
					return rmiproxy.programDescriptionForId(id);
				}
				catch (RemoteException e1)
				{
					Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e1);
				}
			}
			/*
			 * containsKey() instead of desc=cache.get(): we explicitly want to cache requests returning null, since
			 * the resource repository is immutable.
			 */
			if (!prgDescCache.containsKey(id))
			{
				try
                {
	                desc = rmiproxy.programDescriptionForId(id);
					prgDescCache.put(id, desc);
               }
                catch (RemoteException e)
                {
					Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
	                prgDescCache.put(id, null); //TODO handle remoteexception
                }
			}
			else
				desc = prgDescCache.get(id);
			return desc;
		}
		public ProgramDescription alternateProgramDescriptionForId(String id)
		{
			/*
			 * Differently from programDescriptionForId() we do not cache anything; the idea is that
			 * this API may change to propose more than one program. In this case, the choice will ultimately be
			 * done by the user so there is no need to cache this info.
			 */
			try
			{	
				return rmiproxy.alternateProgramDescriptionForId(id);
			}
			catch (RemoteException e)
			{
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
				//TODO handle remoteexception
			}
			return null;
		}

	}
	
	/**
	 * An object proxying requests to the server. The server itself is either distant (eg. exported through RMI) or
	 * present in the same JVM (this is the case when both the client and the server areshipped in a signle
	 * application).<br>
	 * Instead of being just exposed to the client, the proxy:
	 * <ul>
	 * <li>catches RemoteException that could be raised when invoking the distant methods exposed through the
	 * {@link RemoteServerInterface} interface;
	 * <li>caches values so that the server does not get flooded by unneeded requests (following the recommendations
	 * exposed in {@link RemoteServerInterface#sequence()}
	 * </ul>
	 * @author Sébastien Bigaret
	 * @implementation The ServerProxy also caches the availability of ressources: when isAvailable() is called, it
	 *                 first checks whether the sequence() has changed; if not, the cached value is returned. Also
	 *                 note that the sequence() itself is not checked every time isAvailable() is called, but every 30
	 *                 seconds at most.
	 */
	protected static class ServerProxy
    implements ServerInterface
    {
		/** the underlying object exported by rmi */
		protected RemoteServerInterface         remoteServerInterface;
		
		private final HashMap<String, Boolean> availabilityCache = new HashMap<String, Boolean>();
		private long lastSequence;
		private long lastCheck;
		public ServerProxy(RemoteServerInterface object)
		{
			this.remoteServerInterface = object;
			lastSequence = sequence();
			lastCheck = System.currentTimeMillis();
		}
		@Override
        public boolean isAvailable(String resourceID)
        {
			final long currentTime = System.currentTimeMillis();
			if (currentTime-lastCheck>30000)  // check every 30 seconds, at most.
			{
				lastCheck = currentTime;
				long currentSequence = sequence(); // store it in case it changes between his point...
				if (lastSequence != currentSequence)
				{
					availabilityCache.clear();
					lastSequence = currentSequence;  //... and this point!
				}
			}
			Boolean cachedValue = availabilityCache.get(resourceID);
			if ( cachedValue!=null )
				return cachedValue;
			
			try
			{
				cachedValue = remoteServerInterface.isAvailable(resourceID);
				availabilityCache.put(resourceID, cachedValue);
				return cachedValue;
			}
			catch (RemoteException e)
			{
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
				return false;
			}
        }

		@Override
        public long sequence()
        {
			try
			{
				return remoteServerInterface.sequence();
			}
			catch (RemoteException e)
			{
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
				return -1;
			}
        }
		@Override
		public Document availableResources()
		{
			try
			{
				return remoteServerInterface.availableResources();
			}
			catch (RemoteException e)
			{
				Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
				return null;
			}
		}
    }

	public RemoteComponents(String method, String host, int port)
    {
		this.method = method;
		this.host = host;
		this.port = port;
    }
	/**
	 * Takes care of initializing the object. In standalone execution, the proxies for the {@link ResourceRepository}
	 * and the {@link Serveur} are the real objects, so that callers directly access the objects without going through
	 * the JMX layer; in this case, the JMX layer is not activated at all.
	 */
	public synchronized void init()
	{
		if (resourceRepositoryProxy != null)
			return;
		
		if ("direct".equals(method))
		{
			resourceRepositoryProxy = new ResourceRepositoryProxy(ResourceRepository.repository());
			serverProxy = new ServerProxy(Serveur.server());
			return;
		}
		// rmi
		Registry registry = null;
        try
        {
        	if (Configuration.getBoolean("rmi.useSSL"))
        		registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
        	else
        		registry = LocateRegistry.getRegistry(host, port);
        }
        catch (RemoteException e)
        {
			// TODO très mauvaise idée: si nous ne sommes pas dans le client, sans display, ça n'a pas de sens
			Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error...", JOptionPane.ERROR_MESSAGE);
			System.exit(1815);
		}
        RemoteResourceRepositoryInterface resourceRepository=null;
        RemoteServerInterface server = null;
        Exception exc=null;
        try
        {
        	resourceRepository = (RemoteResourceRepositoryInterface) registry.lookup(RMI_RESOURCE_REPOSITORY_NAME);
        	server = (RemoteServerInterface) registry.lookup(RMI_SERVER_NAME);// (RMIServerInterface)Naming.lookup(rmiServerURL);
        }
        catch (RemoteException | java.rmi.NotBoundException e)
        {
        	exc=e;
        }

        if (exc!=null)
        {
			// TODO très mauvaise idée: si nous ne sommes pas dans le client, sans display, ça n'a pas de sens
			Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", exc);
			JOptionPane.showMessageDialog(null, exc.getLocalizedMessage(), "Error...", JOptionPane.ERROR_MESSAGE);
			System.exit(1815);
        }
		Log.log.log(Level.FINE,
		            "Connected to {0}:{1}, retrieved distant objects",
		            new String[]{host, ""+port});
		
		resourceRepositoryProxy = new ResourceRepositoryProxy(resourceRepository);
		serverProxy = new ServerProxy(server);
	}
	public ResourceRepositoryInterface getResourceRepository()
	{
		init();
		return resourceRepositoryProxy;
	}

	public ServerInterface getServer()
	{
		init();
		return serverProxy;
	}

	public static ResourceRepositoryInterface resourceRepository()
	{
		remoteComponent.init();
		return remoteComponent.resourceRepositoryProxy;
	}
	
	public static ServerInterface server()
	{
		remoteComponent.init();
		return remoteComponent.serverProxy;
	}

}
