/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;

public class Process
    extends ExecutionObject
{
	/**
	 * 
	 * @author Sébastien Bigaret
	 *
	 */
	public static class ProcessContext // TODO events on setContext()?
	extends ExecutionObject.Context
	{
		public static final String RESULTS_SUBDIR = "praxis_results";
        private static final long serialVersionUID = -6970145186278295133L;
		private File workingDirectory;
		public WorkflowID workflowID;
		public ExecutionID executionID;

		public void setWorkingDirectory(File workingDirectory)
		{
			this.workingDirectory = workingDirectory;
		}

		public File resultsDirectory()
		{
			return new File(workingDirectory, RESULTS_SUBDIR);
		}

		public File workingDirectory()
		{
			return workingDirectory;
		}

		@Override
		public String toString()
		{
			String repr = "[ProcessContext";
			repr += " Exec:"+workflowID.name()+"/"+executionID;
			repr += " wd:"+( (workingDirectory!=null)?workingDirectory:"null" );
			repr += "]";
			return repr;
		}

	}

	private static final long serialVersionUID = 8503234978745557909L;

	protected final ArrayList<Activity> activities = new ArrayList<Activity>();
	// if serialized, the requester will be reattached not after deserialization, but after the client reconnects.
	protected transient ExecutionObject.Requester requester;

	/**
	 * The attached result object. During an execution, the result object is not updated; more precisely, its programs
	 * contains the key for running activities, and only their start and end dates are updated. The summarized status
	 * of the execution, when needed, is recomputed using {@link #getCurrentExecutionSummary()}.
	 */
	protected Result result;
	
	/* package */ Process(ExecutionID key, String name)
	{
		super(key, name);
		setContext(new ProcessContext());
	}

	@Override
	protected void addEvent(EventAudit event)
	{
		super.addEvent(event);
		if (requester != null)
			requester.handleEvent(event);
	}

	/**
	 * @category getter
	 */
	public Activity getActivity(Object key)
	{
		for (Activity activity: activities)
			if (activity.getKey().equals(key))
				return activity;
		return null;
	}

	/**
	 * @param activity
	 * @return this process, so that the method can be chained one after the other:
	 *         <code>p.addActivity(a1).addActivity(a2)</code>
	 * @throws IllegalArgumentException
	 * @throws NullPointerException
	 */
	/* package */Process addActivity(Activity activity) throws IllegalArgumentException, NullPointerException
	{
		if ( activity.getContainer() !=null && activity.getContainer() != this )
			throw new IllegalArgumentException("activity's container should be this process");
		if (getActivity(activity.getKey())!=null)
			throw new IllegalArgumentException("activity has been already registered");
		activities.add(activity);
		activity.setContainer(this);
		return this;
	}

	private void removeActivity(Activity activity)
	{
		if (activities.remove(activity))
			activity.setContainer(null);
	}

	/**
	 * Returns the context associated to the process.
	 * @return the associated context
	 * @category getter
	 */
	@Override
	public ProcessContext getContext()
	{
		return (ProcessContext) super.getContext();
	}

	/**
	 * Sets the context associated to the process
	 * @param context
	 *            the context to be associated to the process.
	 * @throws IllegalArgumentException
	 *             if the supplied process is not a {@link ProcessContext} instance.
	 * @category setter
	 */
	@Override
	public void setContext(Context context)
	{
		if ( ! (context instanceof ProcessContext) )
		{
			throw new IllegalArgumentException();
		}
		super.setContext(context);
	}

	/**
	 * @return the requester. It may be {@code null}.
	 * @category getter
	 */
	public ExecutionObject.Requester getRequester()
	{
		return requester;
	}

	/**
	 * Return (a copy of) the Result object. Changes can safely be made on it without modifying the internal state;
	 * use {@link #setResult(Result)} to change the internal result.
	 * @return the result
	 * @category getter
	 */
	public Result getResult()
	{
		return new Result(this.result);
	}

	/**
	 * @param requester the requester to set
	 * @category setter
	 */
	public void setRequester(ExecutionObject.Requester requester)
	{
		this.requester = requester;
	}

	/**
	 * Sets the result. Note that a copy of the {@code result} is stored: changes made to the object after this method
	 * is called has no effect on the internally stored result.
	 * @param result the result to assign to this process.
	 * @category getter
	 */
	public void setResult(Result result)
	{
		this.result = new Result(result);
	}

	public boolean start(ExecutionLoop executionManager)
	{
		// Trigger the WF planner BEFORE setting the process' state to running: setting the state may trigger some
		// events and in this case, we want the activities to be already set up for the listeners.
		WorkflowPlanner.planner.plan(this);
		setState(Status.RUNNING);
		if (activities.size()==0)
			return false;
		activateNextSteps(executionManager);
		return true;
	}

	/**
	 * Tells whether a process is closed (i.e. its execution is finished).  A process is closed if all its activities
	 * have a closed status.
	 * @return {@code true} if the process is closed, {@code false} otherwise.
	 * @see Result.Status#isClosed()
	 * @see Activity#getState()
	 */
	public boolean isClosed()
	{
		for (Activity activity: activities)
			if (activity.getState().isActive())
				return false;
		return true;
	}

	/**
	 * Examines the activitis of this process and register with the execution manager those who can be started.
	 * @implementation NB: activatedSteps==0 does NOT mean that the process is closed: in a wf with two different
	 *                 branches that can be run in parallel, when the 1st branch ends no activities is activated but
	 *                 the 2nd branch is still running.
	 * @param executionManager
	 *            the execution loop in which runnable activities are registered.
	 * @return the number of activities that have been activated
	 */
	public int activateNextSteps(ExecutionLoop executionManager)
	{
		List <Activity> activatedActivities = updateActivitiesStatus();
		
		for (Activity activity: activatedActivities)
			executionManager.add(activity);
		
		return activatedActivities.size();
	}

	/**
	 * Examines the activities bound to this process:
	 * <ul>
	 * <li>activities that cannot be executed are marked as such;</li>
	 * <li>activities that can be executed are listed and returned, but their status is not changed (i.e. it remains
	 * NOT_STARTED).</li>
	 * </ul>
	 * @return the activities which are ready to be activated. Note that none of them had their status changed.
	 */
	/* package */ List<Activity> updateActivitiesStatus()
	{
		ArrayList <Activity> activatedActivities = new ArrayList<Activity>();
		boolean madeChanges = true;
		while (madeChanges)
		{
			madeChanges = false;
			for (Activity activity: activities)
			{
				/* Only "not_started" activities may change their state here */
				if (!Status.NOT_STARTED.equals(activity.getState()))
					continue;
				if (activatedActivities.contains(activity))
					continue;
				
				boolean activable = true; // if activity has no predecessors, it will be activated
				for (Activity predecessor: activity.getPredecessors())
				{
					Status predecessorState=predecessor.getState();
					if ( predecessorState.isActive() )
					{
						activable = false;
						break;
					}
					if (predecessorState.isClosed())
					{
						Result not_executed = new Result(getContext().workflowID, getContext().executionID);
						not_executed.setStatus(Result.Status.ERROR);
						
						for (Program prg: activity.getExecutionSet())
							not_executed.setExecStatus(prg.getProgramID(),
							                           ProgramResult.ProgramStatus.NOT_EXECUTED);
						switch (predecessorState)
						{
							case CANCELLED:
								activable = false;
								not_executed.setStatus(Status.CANCELLED);
								//activity.setState(Status.CANCELLED);
								activity.setResult(not_executed);
								madeChanges = true;
								break;
							case ERROR:
								activable = false;
								not_executed.setStatus(Status.ERROR);
								//activity.setState(Status.ERROR);
								activity.setResult(not_executed);
								madeChanges = true;
								break;
							default:
								/*
								 * NB: madeChanges isn't true when an activity's state changes to RUNNING, because
								 * this change does never trigger a change in the state of another activity
								 */
								break;
								
						}
					}
					if (!activable)
						break;
				}
				if (activable)
				{
					Log.log.info(()->"Registering activity for activation: "+activity);
					//activity.setState(Status.RUNNING); // no, this is done when ExecutionLoop.handleActivities() actually starts this activity
					activatedActivities.add(activity);
				}
			}
		}
		return activatedActivities;
	}

	/**
	 * Builds a summary of an execution currently running, or finished. The returned result object contains the
	 * process' status along with the statuses for all of its pending, running or finished activities.<br>
	 * @return the Result object representing the current state of the execution.
	 */
	public Result getCurrentExecutionSummary()
	{
		Result summary = new Result(this.result);
		if (activities.size()==0)
		{
			summary.setStatus(Status.ERROR);
			return summary;
		}
		// If this.isClosed() is false,
		// we leave the current status unchanged: 
		// if the process is running, we want to return a Result object saying that it is running.
		boolean isClosed = this.isClosed();
		for (Activity activity: activities)
		{
			if (activity.getResult() != null)
				summary.mergeWith(activity.getResult(), isClosed);
			else
			{ // Only applies to situations where all activities have not yet received a valid Result object
				for (Program prg: activity.getExecutionSet())
				{
					String prgID = prg.getProgramID();
					summary.setExecStatus(prgID, activity.getState().programStatusForActiveStatus());
				}
			}
		}
		return summary;
	}


	/**
	 * Returns the activity containing the supplied program.
	 * @param prg
	 *            the program to search for
	 * @return the activity containing the program, or {@code null} if it could not be found.
	 */
	public Activity activityContaining(Program prg)
    {
		for (Activity activity: activities)
			if ( activity.getExecutionSet().contains(prg) )
				return activity;
		return null;
    }

	/**
	 * Merges the two activities and return the merged one. After the call, both activities are removed from this
	 * process (their {@link Activity#getContainer() container} is {@code null}), and their execution sets are empty.
	 * @param a1
	 *            the first activity to merge
	 * @param a2
	 *            the second activity to merge
	 * @return the resulting activity
	 * @throws IllegalArgumentException
	 *             if {@code a1}'s or {@code a2}'s container is not self, or if their
	 *             {@link ExecutionSet#getPlatform() platforms} are not the same (they may be null).
	 */
	public Activity merge(Activity a1, Activity a2)
	{
		if ( this != a1.getContainer() || this != a2.getContainer() )
			throw new IllegalArgumentException("At least one of the two activities' containers does not equal to this");

		if (a1.getExecutionSet().getPlatform() != a2.getExecutionSet().getPlatform())
			throw new IllegalArgumentException("Sets should be assigned to the same platform");

		Activity merge = new Activity(this);

		if (a1.getResult()==null && a2.getResult()==null)
			/* nothing to do */;
		else if (a1.getResult()==null)
			merge.setResult(a2.getResult());
		else if (a2.getResult() == null)
			merge.setResult(a1.getResult());
		else
		{
    		final Result merged_result = new Result(a1.getResult());
    		merged_result.mergeWith(a2.getResult());
    		merge.setResult(merged_result);
		}
		final ExecutionSet merged_set = merge.getExecutionSet();
		merged_set.setPlatform(a1.getPlatform());
		merged_set.mergeWith(a1.getExecutionSet());
		merged_set.mergeWith(a2.getExecutionSet());

		addActivity(merge);
		removeActivity(a1);
		removeActivity(a2);
		return merge;
	}

	/**
	 * Returns the activity containing the supplied program.
	 * @param prg
	 *            the program to search for
	 * @return the activity containing the program, or {@code null} if it could not be found.
	 */
	public Activity activityContaining(String prgID)
    {
		for (Activity activity: activities)
			for (Program prg: activity.getExecutionSet())
				if ( prg.getProgramID().equals(prgID) )
					return activity;
		return null;
    }

	@Override
	public String toString()
	{
		String repr = "[Process";
		repr += " closed:"+isClosed();
		repr += " "+super.toString();
		repr += "]";
		return repr;
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException
	{
		if (result!=null)
			result.readyToSerialize();
		out.defaultWriteObject();
	}
}
