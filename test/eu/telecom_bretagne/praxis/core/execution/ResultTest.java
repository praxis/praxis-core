/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;

import org.junit.*;

import static org.junit.Assert.*;

public class ResultTest
{   
	@Test
	public void parseXMLResult() throws Exception
	{
		Result result = new Result(new File("test_data/results", "result.ok"));
		assertNotNull("workflowID is not null", result.workflowID);
		assertNotNull("executionID is not null", result.executionID);
		assertEquals(Result.Status.ERROR, result.status);
		assertTrue(1252684256320L == result.creationDate.getTime());
		assertTrue(1252684256640L == result.date.getTime());
		assertNull(result.key);
		assertNotNull(result.workflowXML);
	}
	

}
