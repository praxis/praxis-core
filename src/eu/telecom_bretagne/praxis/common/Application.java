/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.PrintStream;
import java.util.ArrayList;

import eu.telecom_bretagne.praxis.client.event.ApplicationListener;
import eu.telecom_bretagne.praxis.common.Console.ConsoleCommand;

/**
 * @author Sébastien Bigaret
 */
public class Application
{
	static Application singleton = new Application();

	protected ArrayList<ApplicationListener> appListeners = new ArrayList<ApplicationListener>();

	/**
	 * Indicates whether {@link #exit()} has been called. The value only change once (from false to true) in the
	 * object's lifetime.
	 * @see #exiting()
	 */
	private boolean exiting = false;

	/**
	 * The console attached to the application. Note that it is run only if configuration key "console" is true.
	 */
	public final Console console;

	static public Application getApplication()
	{
		return singleton;
	}

	/** Object Application is a singleton
	 * @see #getApplication()
	 */
	private Application() {
		console = new Console(System.in, System.out);
		console.addCommand(new ConsoleCommand() {
			private long lastCmdTime = 0;
			@Override
			public String description() { return "Exit the application"; }
			@Override
			public String[] commands() { return new String[] { "exit" }; }
			@Override
			public void execute(String command, String argument, PrintStream out)
			{
				if (System.currentTimeMillis()-lastCmdTime > 2000)
					out.println("Process termination: to confirm that you want to terminate the process, re-type 'exit' within 2 seconds");
				else
					Application.this.exit();
				lastCmdTime = System.currentTimeMillis();
			}
		});
		// the console object is always built so that other can addCommand() without having to check for null e.g.
		if (Configuration.getBoolean("console"))
			console.start();
	}

	public void exit()
	{
		exiting = true;
		ArrayList<ApplicationListener> _listeners;
		synchronized(appListeners)
		{
			_listeners = new ArrayList<ApplicationListener>(appListeners);
		}
		for (ApplicationListener listener: _listeners)
			listener.applicationExiting();
		System.exit(0);
	}

	/**
	 * Tells whether the application is exiting, i.e. if exit() has been called.
	 * This is specially useful for code that may be indirectly triggered by
	 * {@link ApplicationListener#applicationExiting()}.
	 * @return {@code true} if {@link #exit()} has been called.
	 */
	public boolean exiting()
	{
		return exiting;
	}

	public void addListener(ApplicationListener listener)
	{
		synchronized(appListeners)
		{
			if (!appListeners.contains(listener))
				appListeners.add(listener);
		}
	}

	public void removeListener(ApplicationListener listener)
	{
		synchronized(appListeners)
		{
			appListeners.remove(listener);
		}
	}
}
