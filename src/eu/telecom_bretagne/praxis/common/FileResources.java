/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * (c) 2006 Sébastien Bigaret
 * @author Sébastien Bigaret
 */
public class FileResources {
    /** The class is made of static methods only and cannot be instanciated */
    private FileResources() { /* avoids the creation of the default public constructor */ }

	/**
	 * Raised by {@link FileResources} static methods when the requested
	 * resource cannot be found.
	 */
	public static class ResourceNotFoundException extends Exception {
		private static final long serialVersionUID = 2505815643366370856L;

		public ResourceNotFoundException(String msg) {
			super(msg);
		}
	}

    /**
     * Search for a file in the system's resources, and returns its url.
     * @param relativePath the path of a system file relatively tothe project's installation path
     * @return the corresponding url
     * @throws ResourceNotFoundException in case it couldn't be found
     */
    public static URL url(String relativePath) throws ResourceNotFoundException {
        URL url = FileResources.class.getClassLoader().getResource(relativePath);
        if ( url==null )
            throw new ResourceNotFoundException("Resource not found: "+relativePath);
        return url;
    }

    /**
     * Returns the "system" file associated to the supplied path
     * @param relativePath the path of the system file to be retrieved,
     * relatively to the installation path
     * @return the associated File object
     * @throws ResourceNotFoundException when the file cannot be found
     */
    public static File file(String relativePath) throws ResourceNotFoundException {
        File file = null;
        try {
            file = new File(url(relativePath).toURI());
        } catch (URISyntaxException e) {
            // ignore: this won't happen, the URL directly comes from
            // ClassLoader.getSystemResource()
        	e.printStackTrace();
        }
        return file;
    }

    /**
     * 
     * @param relativePath
     * @return
     * @throws ResourceNotFoundException
     */
    protected static InputStream stream(String relativePath) throws ResourceNotFoundException {
        InputStream inputStream = FileResources.class.getClassLoader().getResourceAsStream(relativePath);
        if ( inputStream==null )
            throw new ResourceNotFoundException("Resource not found: "+relativePath);
        return inputStream;
    }

    
    
    public static InputStream inputStreamForResource(String relativePath)
    throws ResourceNotFoundException
    {
    	return stream(relativePath);
    }
    public static InputStream inputStreamForResource(URL url)
    throws ResourceNotFoundException
    {
        InputStream inputStream;
        try
        {
	        inputStream = url.openStream();
        }
        catch (IOException e)
        {
            throw new ResourceNotFoundException("Resource not found: "+url);
        }
        return inputStream;
    }
    
    public static InputStream inputStreamForResource(String relativePath, boolean ciphered)
    throws ResourceNotFoundException
    {
        return inputStreamForResource(file(relativePath), ciphered);
    }
    
    /**
     * Returns an input stream for the given resource.
     * @param resource the (non null) file for which we want an input stream
     * To avoid I/O errors such as files not existing, the supplied resource
     * should have been retrieved using {@link #file(String)}.
     * @return the appropriate input stream
     * @throws ResourceNotFoundException
     */
    public static InputStream inputStreamForResource(File resource)
    throws ResourceNotFoundException
    {
    	boolean ciphered = Environment.ciphered_mode();
        return inputStreamForResource(resource, ciphered);
    }

    /**
     * Returns an input stream for the given resource.
     * @param resource the (non null) file for which we want an input stream
     * To avoid I/O errors such as files not existing, the supplied resource
     * should have been retrieved using {@link #file(String)}.
     * @param ciphered whether the resource is expected to be ciphered or not.
     * @return the appropriate input stream, deciphered if parameter ciphered is true.
     * @throws ResourceNotFoundException
     */
    @SuppressWarnings("resource")
    public static InputStream inputStreamForResource(File resource, boolean ciphered)
    throws ResourceNotFoundException
    {
        try
        {
        	return inputStreamForStream(new FileInputStream(resource), resource.getName(), ciphered);
        }
        catch (FileNotFoundException e1)
        { throw new ResourceNotFoundException("Resource not found: "+resource); }
    }
    
    protected static InputStream inputStreamForStream(InputStream stream, String filename, boolean ciphered)
    throws ResourceNotFoundException
    {
    	final InputStream is;
        if (!ciphered) {
        	return stream;
        }
        try
        {
        	is = CipherUtils.decryptedStream(stream, filename);
        }
        catch (InvalidKeyException e) {
        	throw new ResourceNotFoundException("(IKE) not found:"+filename);
        }
        catch (NoSuchAlgorithmException e) {
        	throw new ResourceNotFoundException("(NSAE) not found:"+filename);
        }
        catch (NoSuchPaddingException e) {
        	throw new ResourceNotFoundException("(NSPE) not found:"+filename);
        }
        catch (IOException e) {
        	throw new ResourceNotFoundException("(IOE) not found:"+filename);
        }
        return is;
    }
    
    public static String stringForResource(File resource, String resource_content)
    {
        String result = null;
        if (!Environment.ciphered_mode())
            return resource_content;
        try
        {
            result = CipherUtils.encrypt(resource, resource_content, "ISO-8859-1");
        }
        catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
               | IllegalBlockSizeException | BadPaddingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return result;
    }
}
