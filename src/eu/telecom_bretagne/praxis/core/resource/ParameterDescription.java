/* License: please refer to the file README.license located at the root directory of the project */

/* (c) Sébastien Bigaret */
/**
 * TODO
 * - COMMAND: implictly/explicitly ishidden="1"
 * - INPUT/OUTPUTS: impl./expl. ismandatory="1"
 */
package eu.telecom_bretagne.praxis.core.resource;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.I18N;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;


/**
 * @author Sébastien Bigaret
 *
 */
public class ParameterDescription implements Serializable
{

	private static final long serialVersionUID = 201862362024355036L;

	public enum ParameterType
	{
		COMMAND, CODE, INPUT, INPUT_DIRECTORY, OUTPUT, OUTPUT_DIRECTORY, ENUM, BOOLEAN, INT, FLOAT, STRING, TEXT;

		/**
		 * Tells whether the type denotes an input parameter.
		 * @return true if the type is INPUT or INPUT_DIRECTORY
		 */
		public boolean isInput()
		{
			return this.equals(INPUT) || this.equals(INPUT_DIRECTORY);
		}

		/**
		 * Tells whether the type denotes an output parameter.
		 * @return true if the type is OUTPUT or OUTPUT_DIRECTORY
		 */
		public boolean isOutput()
		{
			return this.equals(OUTPUT) || this.equals(OUTPUT_DIRECTORY);
		}
		
	}

    protected String id;
    
    protected ParameterType type;
    
    protected boolean ismandatory;
    protected boolean ishidden;
    
    /** 
     * When true, indicates that the parameter's value must not be exported.
     */
    protected boolean isUserBound;
    
    protected String description;
    
    protected String displayName;
    
    protected int position;

    /**
	 * A positive integer indicating whether the parameter should be indented when displayed. 0 means no indentation,
	 * 1: one indentation, 2: two indentations etc.
	 */
	protected int indentation;
    
    /** This is the element from which the specific action needed to activate
     * the parameter and (if necessary) to provide its value --it may be a
     * command-line option, a string to be supplied to the standard input,
     * etc.  It is either a simple string, or a Formatter (formatter: not
     * implemented yet)
     */
    protected String valueTemplate;
	
	/**
	 * The target for the value template. Just like the value template itself, this is for use by an ExecutionEngine
	 * so its usage is specific to the chosen one. For example, a execution engine dedicated to the execution of local
	 * programs may use it to decide whether the "value template" is part of the command line or whether it should be
	 * directed to the standard input.<br>
	 * An empty value is not valid: it contains an non-empty string or a null value. A null value is usually
	 * considered equivalent to the default value by an execution engine --however what this default value is, or
	 * whether a null value is equivalent to the default ultimately depends on the execution engine.
	 */
    private String valueTemplateTarget;

    protected String vdef;
    
    protected List<IOType> types; /* for inputs/outputs */

    protected String dependence;

    protected String valueConstraint;
    protected String valueConstraintDescription;
    
    protected int parameterCount;

    protected transient ProgramDescription parent;
    
    /**
	 * A marker, just here to indicate that attributes {@link #isaValidValue(String)} and
	 * {@link #isActivated(Parameter)} have not been initialized yet by their corresponding methods
	 * @see #isActivated(Parameter)
	 * @see #isaValidValue(String)
	 */
    protected static final Method marker_method = ParameterDescription.class.getMethods()[0];
    
    /**
	 * The method called by {@link #isaValidValue(String)}: isaValidValue() searches for the appropriate method
	 * through reflection and it uses this variable as a cache, so that the search through reflection is done exactly
	 * once.
	 */
    protected transient Method isaValidValue_method = marker_method;

    /**
	 * The method called by {@link #isActivated(Parameter)}: isActivated() searches for the appropriate method
	 * through reflection and it uses this variable as a cache, so that the search through reflection is done exactly
	 * once.
	 */
    protected transient Method isActivated_method = marker_method;

    
    public static class Item implements Serializable {
        private static final long serialVersionUID = -3056090798891885860L;
        protected String id; /* item id */
        protected String description = "";
        protected String valueTemplate; /* TBD: cf. rq. ci-dessus */
        protected String valueTemplateTarget;
        protected boolean isFormatter = false;
        protected boolean isDefault = false;
        /** for internal use only */
        protected Item() {}

        /* this was used by the JMX framework
        @ConstructorProperties({"id", "description", "valueTemplate", "valueTemplateTarget", "formatter", "default"}) 
        */
        public Item(String id, String description, String code, String codeTarget,
                    boolean isFormatter, boolean isDefault)
        {
        	this.id = id;
        	this.description = description;
        	this.valueTemplate = code;
        	this.valueTemplateTarget = codeTarget;
        	this.isFormatter = isFormatter;
        	this.isDefault = isDefault;
        }
        /**
         * @return the id
         */
        public String getId() { return id; }
        /**
         * @return the code
         */
        public String getValueTemplate() {
        	return valueTemplate;
        }
		
		/**
		 * returns the target that is associated to the object's {@link #getValueTemplate() value template}
		 * @return the target assigned to the value template. The returned value is either a non-empty String or a
		 *         null value
		 */
        public String getValueTemplateTarget() {
        	if ("".equals(valueTemplateTarget))
        		this.valueTemplateTarget = null; // normalize
        	return this.valueTemplateTarget;
        }
       /**
         * @return the description
         */
        public String getDescription() { return description; }
        /**
         * @return the isDefault
         */
        public boolean isDefault() { return isDefault; }
        /**
         * @return the isFormatter
         */
        public boolean isFormatter() { return isFormatter; }

        @Override
        public String toString() { /* used by the default renderer for JCombobox */
            return description;
        }
    }
    /** List of items supplied to {@link ParameterType#ENUM} parameters.
     * When the description is initialized with an
     * {@link #fromXML(Element) xml file}, the items are ordered in the same
     * way as they are in the xml.
     */
    protected List <Item> vlist; /* for ENUM */

    /**
     * @throws InvalidXMLException 
     * 
     */
    public ParameterDescription(ProgramDescription parent, Element xml) throws InvalidXMLException
    {
    	this.parent = parent;
    	fromXML(xml);
    }
	
	/**
	 * Builds a <b>non initialized</b> object. Callers should take care of properly initializing it (usually by calling
	 * {@link #fromXML(Element)} or {@link #shallowCopy(ParameterDescription)}. In particular, the {@link #type} is
	 * not allowed to be {@code null} in a valid description (see also: {@link #setVdef(String)}).<br>
	 * @implementation <b>Do NOT remove<b>: even if it seems uncalled, it is called in
	 *                 {@link DynamicCodeFactory#handle_dependencies(ProgramDescription)} through a newInstance() call
	 */
    protected ParameterDescription() { /* empty, see comments above */ }

	public ParameterDescription(String valueTemplate, String valueTemplateTarget, String dependence,
	                            String valueConstraint, String valueConstraintDescription, String description,
	                            String displayName, String id, int indentation, boolean ishidden, boolean ismandatory,
	                            boolean isUserBound, int position, ParameterType type, List<IOType> types,
	                            String vdef, Item[] vlist, int parameterCount)
    {
		this.valueTemplate = valueTemplate;
		setValueTemplateTarget(valueTemplateTarget);
		this.dependence = dependence;
		this.valueConstraint = valueConstraint;
		this.valueConstraintDescription = valueConstraintDescription;
		this.description = description;
		this.displayName = displayName;
		this.id = id;
		this.indentation = indentation;
		this.ishidden = ishidden;
		this.ismandatory = ismandatory;
		this.isUserBound = isUserBound;
		this.position = position;
		setTypeAndVdef(type, vdef);
		this.types = types;
		this.vlist = ( vlist==null || vlist.length==0 ) ? null : Arrays.asList(vlist);
		this.parameterCount = parameterCount;
    }
    
    protected void shallowCopy(ProgramDescription aParent, ParameterDescription aParam) {
    	parent = aParent;
        dependence = aParam.dependence;
        description = aParam.description;
        displayName = aParam.displayName;
        id = aParam.id;
        indentation = aParam.indentation;
        ishidden = aParam.ishidden;
        ismandatory = aParam.ismandatory;
        isUserBound = aParam.isUserBound;
        parameterCount = aParam.parameterCount;
        position = aParam.position;
        type = aParam.type;
        types = aParam.types;
        valueConstraint = aParam.valueConstraint;
        valueConstraintDescription = aParam.valueConstraintDescription;
        valueTemplate = aParam.valueTemplate;
        vdef = aParam.vdef;
        vlist = aParam.vlist;
    }
    
    ProgramDescription getParent()
    {
    	return parent;
    }
    
     void setParent(ProgramDescription prg)
    {
    	this.parent = prg;
    }
	
	/**
	 * Tells whether the supplied parameter is activated. This method is either overriden by subclasses, or it can be
	 * dynamically overriden and replaced by a method which determines the parameter's activated-state depending on
	 * its {@link #getDependence() dependencies} (this is the case when the prgs'descriptions are "reloaded" from XML,
	 * cf. {@link ResourceRepository#load(java.io.File)}).<br>
	 * @param param
	 *            the parameter to examine. Please note: while this is not checked at runtime, the
	 *            {@link Parameter#getDescription() parameter's description} should be the receiver. Code outside the
	 *            package will call {@link Parameter#isActivated()} instead.
	 * @return true if all dependencies are fulfilled.
	 */
    public Boolean isActivated(Parameter param)
    {
        return true;
    }
	
	/**
	 * Tells whether the supplied string represents a valid value for such a parameter. When overriding this method,
	 * subclasses that choose not to call super can call {@link #isaValidValue_wrtType(String)} directly.
	 * @see ParameterDescription#isaValidValue(String)
	 * @param value
	 *            the String representation of the value whose validity should be tested
	 * @return true if the value is valid
	 */
    public Boolean isaValidValue(String value)
    {
    	return isaValidValue_wrtType(value) && _isaValidValue(value);
    }
	
	/**
	 * Checks that the value is correct with respect to the type. For example if this parameter's type is integer, the
	 * method checks that the value is a parsable integer.
	 * @param value
	 *            the value to check
	 * @return true if the value is valid with respect to the parameter's type
	 * @see #getValueConstraintMessage()
	 */
    public boolean isaValidValue_wrtType(String value)
    {
	    switch (type)
    	{
    		case INT:
    			try
    			{
    				Integer.parseInt(value); // NOSONAR side-effect: exception raised
    			}
    			catch (NumberFormatException e)
    			{
    				return false;
    			}
    			break;
    		case FLOAT:
    			try
    			{
    				Float.parseFloat(value); // NOSONAR side-effect: exception raised
    			}
    			catch (NumberFormatException e)
    			{
    				return false;
    			}
    			break;
    		default:
    			/* true */
    			break;
    	}
	    return true;
    }
	
	/**
	 * Helper method for {@link #isaValidValue(String)}, which does nothing else than
	 * <code>isaValidValue_wrtType(value) && _isaValidValue(value);</code>. Subclasses can either override
	 * isaValidValue() directly, or they can just override this method to add some extra checks. This is the way it is
	 * done when the programs and their parameters are {@link ResourceRepository#dynamicallyReloadDescriptions()
	 * dynamically loaded from XML descriptions}. {@link ParameterDescription}'s implementation returns true.
	 * @param value
	 *            the value whose validity should be tested
	 * @return true is the value is valid.
	 */ 
    protected Boolean _isaValidValue(String value)
    {
    	return true;
    }
    
    /**
	 * Initializes a {@link ParameterDescription} object from a XML element.
	 * @param xml
	 *            the xml element representing the object
	 * @throws InvalidXMLException
	 *             if the supplied XML element is not a valid representation for a ParameterDescription object
	 * @see #toXML()
	 */
    public void fromXML(Element xml) throws InvalidXMLException
    {
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.PARAMETER);
        
        if ( !xml.getName().equalsIgnoreCase("parameter") )
            throw xml_exc.setMsg("root element should be <parameter/>");
        
        /* id */
        id = xml.getAttributeValue("id");
        if (id == null)
            throw xml_exc.setMsg("id cannot be null");
        xml_exc.setName(id);
        
        /* type and vdef */
        String _type=null, _vdef=null;
        try {
        	_type = xml.getAttributeValue("type", "");
        	_type = _type.toUpperCase(Locale.ENGLISH);
        	_type = _type.replace('-', '_'); // for input-directory e.g.
        	
            /* vdef */
            _vdef = xml.getChildText("vdef");
            setTypeAndVdef(ParameterType.valueOf(_type), _vdef); // does not raise NullPtrExc, just IllegalArgExc
        }
        catch (IllegalArgumentException e) {
            throw xml_exc.setMsg("Invalid parameter type ("+_type+") and/or vdef ("+_vdef+"): "+e.toString());
        }
        

        /* ismandatory */
        ismandatory = Facade_xml.boolean_for(xml.getAttributeValue("ismandatory"));

        /* ishidden */
        ishidden = Facade_xml.boolean_for(xml.getAttributeValue("ishidden"));

        /* ishidden */
        isUserBound = Facade_xml.boolean_for(xml.getAttributeValue("is-user-bound"));

        /* position */
        String pos_str = xml.getChildText("position");
        
        try {
            position = Integer.valueOf(pos_str);
        }
        catch (NumberFormatException e) {
            throw xml_exc.setMsg("position is not an integer ("+pos_str+")");
        }

        /* description */
        description = xml.getChildText("description");
        
        /* displayName: <name/> */
        displayName = xml.getChildText("name"); // cannot be null according to the DTD

        /* indentation: <indent/> */
        String indent_str = xml.getChildText("indent");
        try {
            indentation = indent_str==null ? 0 : Integer.valueOf(indent_str);
            if (indentation<0)
            	throw new NumberFormatException("not a positive number");
        }
        catch (NumberFormatException e) {
            throw xml_exc.setMsg("indentation is not an integer ("+indent_str+")");
        }


        /* code */
        Element code_xml = xml.getChild("code");
        if ( code_xml != null ) {
            valueTemplate = code_xml.getText();
            valueTemplateTarget = code_xml.getAttributeValue("target");
            if ( valueTemplateTarget != null )
            	valueTemplateTarget = valueTemplateTarget.toUpperCase(Locale.ENGLISH); // null if empty, fine
        }
        
        dependence = xml.getChildText("dep");

        Element constraint = xml.getChild("constraint");
        valueConstraint ="";
        valueConstraintDescription = "";
        if (constraint!=null)
        {
        	valueConstraint = constraint.getChildText("code");
        	valueConstraintDescription = constraint.getChildText("description");
        }
        
        /* types */ /* TODO: check that this is ONLY supplied for inputs/outputs */
        types = null; /* reset the types */
        
        List <Element> types_elements;
        try { 
            final XPath xpath = XPath.newInstance("./types/type");
            types_elements = xpath.selectNodes(xml);
        }
        catch (JDOMException e) {
            String msg = "Unable to select nodes /types/type";
            if ( e.getMessage() != null )
                msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        if ( types_elements.size() > 0 )
            types = new ArrayList<IOType>();

        for (Object type_xml: types_elements) {
            String type_str = ((Element)type_xml).getText().toUpperCase(Locale.ENGLISH);
            try { types.add( IOType.valueOf(type_str) ); }
            catch (IllegalArgumentException e) {
                throw xml_exc.setMsg(e.toString());
            }
        }

        /* vlist */ /* TODO: check that this is ONLY supplied for ENUM */
        vlist = null; /* reset vlist */
        
        List <Element> vlist_items;
        
        try {
            final XPath xpath = XPath.newInstance("./vlist/item");
            vlist_items = xpath.selectNodes(xml);
        }
        catch (JDOMException e) {
            String msg="Unable to select nodes /vlist/item"; //CHECK: does not fail when empty, right?
            if ( e.getMessage() != null )
                msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        if ( vlist_items.size() > 0 )
            vlist = new ArrayList<Item>();
        
        // TODO lorsqu'on acceptera les LIST, il y aura un attribut '' pour chaque item
        boolean found_vdef = false;
        for (Object oitem: vlist_items) {
            Element item = (Element) oitem;
            Item current_item = new Item();
            current_item.description = item.getChildText("description");
            current_item.id = item.getAttributeValue("id");

            if ( current_item.id.equals(vdef) ) {
                current_item.isDefault = true;
                found_vdef = true;
            }

            Element item_code = item.getChild("code");
            if ( item_code != null ) {
                current_item.valueTemplate = item_code.getText();
                String item_target = item_code.getAttributeValue("target");
                if ( item_target != null )
                	item_target = item_target.toUpperCase(Locale.ENGLISH);
                current_item.valueTemplateTarget = item_target;
                
                current_item.isFormatter = Facade_xml.boolean_for(item_code.getAttributeValue("pattern"));
            }
            vlist.add( current_item );
        }

        if ( ! found_vdef && vlist_items.size() > 0 )
            throw xml_exc.setMsg("Unable to find any items for vdef=="+vdef);
    }

    /**
     * Builds an XML element representing the object
     * @return the XML element representing the object
     * @see #fromXML(Element)
     */
    public Element toXML()
    {
        Element param = new Element("parameter");
        
        param.setAttribute("id", id);
        param.setAttribute("type", type.toString());
        param.setAttribute("ismandatory", ismandatory ? "1" : "0");

        param.setAttribute("is-user-bound", isUserBound ? "1" : "0");

        param.addContent(new Element("indent").setText(indentation+""));

        param.addContent(new Element("description").setText(position+""));
        param.addContent(new Element("code")
                .setText(valueTemplate));
        param.addContent(new Element("position").setText(position+""));
        if ( types.size()>0 )
        {
            final Element _vlist = new Element("types");
            for (IOType ptype: types)
                _vlist.addContent(new Element("type").setText(ptype.toString()));
            param.addContent(_vlist);
        }
        
        if ( vlist.size()>0 )
        {
            Element xml_vlist = new Element("vlist");
            for (Item current_item: vlist) {
                Element xml_item = new Element("item").setAttribute("id", current_item.id);

                xml_item.addContent(new Element("description").setText(current_item.description));
                
                if ( current_item.valueTemplate != null )
                    xml_item.addContent(new Element("code")
                            .setText(current_item.valueTemplate)
                            .setAttribute("pattern", current_item.isFormatter?"1":"0"));
            }
            param.addContent(xml_vlist);
        }

        param.addContent(new Element("vdef").setText(position+""));

        return param;
    }
    
    /* Getters and setters */
    /**
     * @return the constraint for the receiver
     */
    public String getValueConstraint() {
        return valueConstraint;
    }

    /**
     * @return the constraint for the receiver
     */
    public String getValueConstraintDescription() {
        return valueConstraintDescription;
    }

    /**
     * Returns the message to display when the value is {@link #isaValidValue(String) invalid}.
     * 
     * @return either the {@link #getValueConstraintDescription() description of the constraint}, or a default message
     *         if the latter is null or empty.
     */
    public String getValueConstraintMessage() {
        if ( valueConstraintDescription != null && ! "".equals(valueConstraintDescription) )
            return valueConstraintDescription;
        switch (type)
        {
            case FLOAT:
                return I18N.s("UI.dialog.prg_properties.invalid_float_value");
            case INT:
                return I18N.s("UI.dialog.prg_properties.invalid_integer_value");
            default:
                return I18N.s("UI.dialog.prg_properties.invalid_value");
        }
    }

    /**
	 * Parses the value constraint and transforms it into a valid java boolean expression. The built expression
	 * determines whether a given value is valid wrt. the constraint; this value is named <code>value</code> in the
	 * built expression.
	 * @param valueParameterName
	 *            the name of the variable containing the String value that is tested by the returned expression
	 * @return the equivalent java boolean expression
	 */
	public String getValueConstraintAsBooleanExpression(String valueParameterName) {
		if (valueConstraint==null || "".equals(valueConstraint))
			return "true";
		
		Formatter fmt = new Formatter();
		String c = valueConstraint; //"%%%d %d %%%d";
		c = c.replaceAll("([^%])%%", "$1DOUBLE___PERCENT")
		     .replaceAll("%d", "Integer.parseInt(%1\\$s)")
		     .replaceAll("%s", "%1\\$s")
		     .replaceAll("%f", "Double.parseDouble(%1\\$s)")
		     .replaceAll("%g", "Double.parseDouble(%1\\$s)")
		     .replaceAll("DOUBLE___PERCENT", "%%");
		try
		{
		fmt.format(c, valueParameterName);
		return fmt.toString();
		}
		catch(Exception e)
		{
			return "";
		}
		finally
		{
			fmt.close();
		}
	}

	/**
     * @return the dependencies for the receiver
     */
    public String getDependence() {
        return dependence;
    }

    /**
     * Sets the dependencies for this parameter
     * @param dependence the expression of the parameter's dependencies
     */
    public void setDependence(String dependence) {
        this.dependence = dependence;
    }
    
    /**
	 * Parses the dependence and transforms it into a valid java boolean expression. The built expression determines
	 * whether a given {@link Parameter} object has its dependencies fulfilled; this object is named
	 * <code>parameterName</code> is the built expression.
	 * @param parameterName
	 *            the name of the variable containing the {@link Parameter} object that is tested by the returned
	 *            expression
	 * @return the equivalent java boolean expression
	 */
    public String getDependenceAsBooleanExpression(String parameterName) {
        String _code = "";

        if (dependence==null || dependence.equals("") || dependence.equals("[]") || dependence.equals("()"))
            return "true";
    
        String quotedParameterName = Matcher.quoteReplacement(parameterName);

        _code = new String(dependence);

        _code = _code
        	.replaceAll("\\s*([^:(]+):type *= *\"([^)\"]+)\"",
        			"("
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\")"+".getItem().getId().equals(\"$2\")"
        			+" && "
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\").isActivated()"
        			+")")
        	.replaceAll("\\s*([^:(]+):type *!= *\"([^)\"]+)\"",
        			"(!("
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\")"+".getItem().getId().equals(\"$2\"))"
        			+" && "
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\").isActivated()"
        			+")")
        	.replaceAll("\\s*([^:(]+):value *= *\"([^)\"]+)\"",
        			"("
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\")"+".getData().equals(\"$2\")"
        			+" && "
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\").isActivated()"
        			+")")
        	.replaceAll("\\s*([^:(]+):value *!= *\"([^)\"]+)\"",
        			"(!("
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\")"+".getData().equals(\"$2\"))"
        			+" && "
        			+quotedParameterName+".getProgram().getParameterWithID(\"$1\").isActivated()"
        			+")")
        	.replace("AND", "&&")
        	.replace("OR", "||")
        	.replace("NOT", "!");
        
        Pattern p = Pattern.compile("\\s*([^:(]+):value *([><=]+) *([^)]+)");
        Matcher m = p.matcher(_code);
        while (m.find())
        {
        	String ref_param_id = m.group(1);
        	String operator = m.group(2);
        	String value = m.group(3);
        	ParameterDescription ref_paramDesc = getParent().getParameterWithId(ref_param_id);
        	String castExpression = ref_paramDesc.castExpressionForValue(quotedParameterName+".getProgram().getParameterWithID(\"$1\")"+".getData()");

        	_code = m.replaceFirst("("
               			+ castExpression+operator+value
            			+" && "
            			+quotedParameterName+".getProgram().getParameterWithID(\"$1\").isActivated()"
            			+")");
            m = p.matcher(_code);
        }
        return _code;
    }

    public String castExpressionForValue(String data)
    {
    	switch (type)
    	{
			case BOOLEAN:
				return "(\"1\".equals("+data+") ? true: false)";
			case CODE:
			case COMMAND:
				return null;
			case FLOAT:
				return "Double.parseDouble("+data+")";
			case INT:
				return "Integer.parseInt("+data+")";
			case ENUM:
			case INPUT:
			case INPUT_DIRECTORY:
			case OUTPUT:
			case OUTPUT_DIRECTORY:
			case STRING:
			case TEXT:
				default:
				return data;
    	}
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the value template
     */
    public String getValueTemplate() {
        return valueTemplate;
    }

    /**
     * @param valueTemplate the value template
     */
    public void setValueTemplate(String valueTemplate) {
        this.valueTemplate = valueTemplate;
    }

    /**
     * Returns the valueTemplateTarget associated to this parameter.
     * @return the target for the value template. It is either {@code null} or a non-empty string.
     */
    public String getValueTemplateTarget() {
    	if ("".equals(valueTemplateTarget))
            valueTemplateTarget = null; // normalize
        return valueTemplateTarget;
    }

    /**
     * Sets the valueTemplateTarget associate to this parameter
     * @param valueTemplate the target for the value template. An empty string is converted to {@code null}
     */
    public void setValueTemplateTarget(String valueTemplateTarget) {
    	if ("".equals(valueTemplateTarget))
    		this.valueTemplateTarget = null;
    	this.valueTemplateTarget = valueTemplateTarget;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName()
    {
    	return displayName;
    }

	/**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName)
    {
    	this.displayName = displayName;
    }

	/**
     * @return the indentation
     */
    public int getIndentation()
    {
    	return indentation;
    }

	/**
     * @param indentation the indentation to set
     */
    public void setIndentation(int indentation)
    {
    	this.indentation = indentation;
    }

	/**
     * @return the ishidden
     */
    public boolean isHidden() {
        return ishidden;
    }

    /**
     * @param ishidden the ishidden to set
     */
    public void setIsHidden(boolean ishidden) {
        this.ishidden = ishidden;
    }

    /**
     * @return the ismandatory
     */
    public boolean isMandatory() {
        return ismandatory;
    }

    /**
     * @param ismandatory the ismandatory to set
     */
    public void setIsMandatory(boolean ismandatory) {
        this.ismandatory = ismandatory;
    }

    /**
     * Tells whether the values taken by a parameter is user-bound, i.e. should not be made available
     * to others.
     * @return {@code true} if the value of such parameters should be bound to a given user
     */
    public boolean isUserBound()
    {
    	return isUserBound;
    }

	/**
     * @param isUserBound the do_not_export to set
     */
    public void setIsUserBound(boolean isUserBound)
    {
    	this.isUserBound = isUserBound;
    }

	public int getParameterCount()
    {
    	return parameterCount;
    }
    
    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * @return the type
     */
    public ParameterType getType() {
        return type;
    }

    /**
     * Sets the type of the parameter.
     * @param type the type to set. It cannot be {@code null}.
     * @throws NullPointerException if {@code type} is {@code null}.
     */
    protected void setType(ParameterType type) {
        this.type = type;
    }

    /**
     * @return the types
     */
    public List<IOType> getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(List<IOType> types) {
        this.types = types;
    }

    /**
     * @return the vdef
     */
    public String getVdef() {
        return vdef;
    }
	
    /**
	 * Sets the receiver's type and default value.
	 * 
	 * @param type
	 *            the parameter's type
	 * @param vdef
	 *            the parameter's default value
	 * @throws NullPointerException
	 *             if {@code type} is {@code null}
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>when the {@code vdef} is {@code null} or empty, and the {@code type} represents an
	 *             {@link ParameterType#isOutput() output}.
	 *             <li>when the {@code type} is {@link ParameterType#BOOLEAN boolean}, and the vdef is neither "true"
	 *             nor "false" nor "0" nor "1".
	 *             </ul>
	 */
	public void setTypeAndVdef(ParameterType type, String vdef) throws NullPointerException, IllegalArgumentException
    {
    	if (type == null)
    		throw new NullPointerException("Illegal null type");
    	if ( type.isOutput() && (vdef==null || "".equals(vdef)) )
    		throw new IllegalArgumentException("vdef cannot be null or empty in an output parameter");
    	switch(type)
    	{
    		case BOOLEAN:
    			if (vdef == null)
    				this.vdef = "0";
    			else if (vdef.equals("0") || vdef.equals("false"))
    				this.vdef = "0";
    			else if (vdef.equals("1") || vdef.equals("true"))
    				this.vdef = "1";
    			else
    				throw new IllegalArgumentException("Invalid value for a boolean default value: "+vdef);
    			this.type = type;
    			break;
    		default:
                /* TODO: check vdef definition (int, float, etc.) */
    			this.type = type;
    			this.vdef = vdef;
    	}
    }
	
    /** Returns the items registered in the vlist, if any.  If the receiver's
     * {@link #getType() type} is not {@link ParameterType#ENUM}, returned 
     * value is null.
     * @return null if inappropriate. If non-null, the returned array's length
     * is greater or equal to 2 (two).
     */
    public Item [] getItems() {
    	if ( type != ParameterType.ENUM )
    		return null;
        return vlist.toArray(new Item[vlist.size()]);
    }

    public Item getItemWithID(String anID) {
        for (Item item: vlist)
            if ( item.id.equals(anID) )
                return item;
        return null;
    }
    
    /** Return the item in {@link #getItems()} */
    public Item defaultItem() {
        for (Item item: vlist)
            if ( item.isDefault() )
                return item;
        return null;
    }

    /**
	 * Internally used (by {@link Parameter}) to check that it gets an item compatible with its description
	 * @return true if this is a {@link ParameterType#ENUM} parameter and if the item is one of the declared
	 *         {@link #getItems() items}
	 * @see Parameter#setItem(eu.telecom_bretagne.praxis.core.resource.ParameterDescription.Item)
	 */
    public boolean hasItem(Item item) {
        return (type == ParameterType.ENUM) && (vlist!=null) && (vlist.contains(item));
    }
    
    public boolean isCompatibleWith(ParameterDescription prgDescription) {
        if ( ! ( type.isInput() || type.isOutput())
            || (type.isInput()  && (!prgDescription.type.isOutput()))
            || (type.isOutput() && (!prgDescription.type.isInput())) )
            throw new IllegalArgumentException("Compatibility can be computed for input or output parameters only");
        return IOType.areCompatible(types, prgDescription.types);
    }

    /* Serializable interface */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
    {
    	in.defaultReadObject();
    	/* now correct IOTypes: they are considered as singleton, so we iterate on the list and replace every IOType
    	 * with the corresponding one returned by IOType.from() */
    	ArrayList<IOType> types_fixed = new ArrayList<IOType>();
    	if (types==null)
    		return;

    	for (IOType type_fixed: types)
    	{
    		if (type_fixed!=null)
    		{
    			type_fixed=IOType.from(type_fixed.name, type_fixed.colorName, type_fixed.isUniversal);
    		}
    		types_fixed.add(type_fixed);
    	}
    	//types.clear(); //will throw java.lang.UnsupportedOperationException on java.util.AbstractList
    	types = types_fixed;
    	
    	// reinit the marker, so that the callbacks can be retrieved, as expected
    	isActivated_method = marker_method;
    	isaValidValue_method = marker_method;
    	// WARNING: parent is also null, the ProgramDescription should set it.
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException
    {
    	out.defaultWriteObject();
    }
    
    
    
}
