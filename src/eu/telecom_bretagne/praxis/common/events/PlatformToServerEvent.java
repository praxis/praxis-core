/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import eu.telecom_bretagne.praxis.core.execution.Result;

/**
 * The data stored in this object depends on its type:
 * <ul>
 * <li><code>username</code> and <code>workflow</code> are currently NOT set
 * <li>CONFIGURATION:
 *     <ul>
 *     <li>{@link Event#data}: the description of resources  (String)
 *     <li>{@link Event#file_conveyor}: not used
 *     </ul>
 * <li>END_OF_EXECUTION:
 *     <ul>
 *     <li>{@link Event#data}: a {@link Result} object
 *     <li>{@link Event#file_conveyor}: a zip file, containing the results (if any)
 *     </ul>
 * <li>LOGOUT: unused for the moment being
 *     <ul>
 *     <li>{@link Event#data}: n/a
 *     <li>{@link Event#file_conveyor}: n/a
 *     </ul>
 */
public class PlatformToServerEvent extends Event
{
	static final long serialVersionUID = -1092690438277504701L;
	
	public interface PlatformToServerEventListener
	extends EventListener
	{
		public void platformSendsConfiguration(Event event);
		public void platformSendsResourcesAvailabilityUpdates(Event event);
		public void platformSendsExecutionProgress(Event event);
		public void platformSendsResults(Event event);
		public void platformLogout(Event event);
	}
	
	public static enum Type { CONFIGURATION, AVAILABLE_RESOURCES, EXECUTION_PROGRESS, END_OF_EXECUTION, LOGOUT }
	
	Type type;
	
	public PlatformToServerEvent(Type type)
	{
		super("no source");
		this.type = type;
	}
	
	@Override
	public void dispatch_to(EventListener listener)
	throws IncompatibleListenerException
	{
		PlatformToServerEventListener l;
		try {
			l = (PlatformToServerEventListener) listener;
		}
		catch (ClassCastException e) {
			throw new IncompatibleListenerException(listener, this);
		}
		switch (type)
		{
			case CONFIGURATION:       l.platformSendsConfiguration(this); break;
			case AVAILABLE_RESOURCES: l.platformSendsResourcesAvailabilityUpdates(this); break;
			case EXECUTION_PROGRESS:  l.platformSendsExecutionProgress(this); break;
			case END_OF_EXECUTION:    l.platformSendsResults(this); break;
			case LOGOUT:              l.platformLogout(this); break;
		}
	}
	
	@Override
	public String toString()
	{
		return "<PlatformToServer type:"+type.toString()+">";
	}
}
