/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.EventObject;

import eu.telecom_bretagne.praxis.common.Log;


/**
 * An event is essentially a message exchanged between two elements within
 * praxis (namely: the clients, the frontend and the platforms).
 * An event can be viewed as a command (such as a client requesting the
 * execution of a workflow) or an information (e.g. when a platform sends to
 * the frontend the result of an execution).  Many events also need to
 * convey a possibly huge amount of informations, for example, again: the
 * set of files produced by the execution of a program on the platform-side.
 * <br>
 * Events object are specially designed so that it is easy to transport them
 * along with any set of files: simply zip these files together, make
 * {@link #file_conveyor} point to them, and call
 * {@link #writeTo(OutputStream)}.  On the other side, it is as simple as
 * calling {@link #readFrom(InputStream)}.
 * 
 * An event is sent through a {@link CommunicationFacade#send(Event) Communication Facade}.  After it has been sent, it
 * should not be modified; please see 
 * 
 * @author Sébastien Bigaret
 *
 */
public abstract class Event extends EventObject implements Serializable, Cloneable
{
	static final long serialVersionUID = -6706015821514443305L;
	
	static public class IncompatibleListenerException extends Exception
	{
        private static final long serialVersionUID = -7125862362536581467L;
		public EventListener listener;
		public Event event;
		public IncompatibleListenerException(EventListener listener, Event event)
		{
			this.listener=listener; this.event = event;
		}
	}
	
	public Serializable data;
	
	public File file_conveyor;
	
	/**
	 * Indicates whether the event should be forwarded to other servers connected using the 
	 * Client Facade
	 */
	public boolean forward = false;

	public transient CommunicationFacade.EventHandlingCallback callback = null;
	
    protected Event(Object source)
	{
		super(source);
	}
	
	/**
	 * Responsible for calling the methods of the listener, corresponding
	 * to the supplied event.
	 * Note that an event will only dispatch the message to a compatible
	 * listener (documented as such in their respective documentation); when
	 * the listener is not compatible, an exception is raised
	 * @param listener
	 * @throws IncompatibleListenerException when the listener is not
	 * compatible
	 */
	public abstract void dispatch_to(EventListener listener)
	throws IncompatibleListenerException;
	
	/**
	 * Writes onto the stream this object, serialized, along with the size and
	 * content of its {@link #file_conveyor} if any.
	 * Transmitted data must be read using {@link #readFrom(InputStream)}.
	 * @param out the output stream
	 * @throws IOException
	 */
	public void writeTo(OutputStream out) throws IOException
	{
		/* we rely on the default behaviour for serializable, just adding the
		 * file to be transmitted, if any
		 */
		ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(out));
		oos.flush();
		oos.writeObject(this);
		if ( file_conveyor == null )
		{
			oos.writeLong(-1);
			oos.flush();
			return;
		}
		long file_size = file_conveyor.length();
		oos.writeLong(file_size);
		oos.flush();
		
		InputStream fis = new BufferedInputStream(new FileInputStream(file_conveyor));
		byte[] buf = new byte[1024];
		int numRead = 0;
		
		// Note: do not, EVER, write on the OutputStream that lies under an ObjectOutputSTream,
		// or the next time the other ends tries to create a ObjectInputSTream() on that
		// stream, it will fail badly with a CorruptedStreamException
		while ((numRead = fis.read(buf)) >= 0) {
			oos.write(buf, 0, numRead);                
		}
		// however, it is still possible to use the OOS.write()/read() methods
		// which remain, in any case, much more efficient than writeInt()!
		//        for (long count=0; count<file_size; count++)
		//            oos.writeInt(fis.read());
		
		fis.close();
		oos.flush();
	}
	
	/**
	 * Creates and returns the transmitted Event object, as well as the file
	 * that was transmitted, if any.
	 * {@link #writeTo(OutputStream)}
	 * @param in the input stream to be read
	 * @return the transmitted Event object; its <code>file_conveyor</code>
	 * field points to the file that was transmitted along with the object
	 * (or it is null if no file was transmitted)
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Event readFrom(InputStream in)
	throws IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(in));
		
		Event evt = (Event)ois.readObject();
		long size_of_file = ois.readLong();
		if ( size_of_file != -1 )
		{
			evt.file_conveyor = File.createTempFile("bs_", ".tpf");// transmitted praxis file
			OutputStream fos = new BufferedOutputStream(new FileOutputStream(evt.file_conveyor));
			
			// for (long count=0; count<size_of_file; count++)
			// 	fos.write(ois.readInt());
			for (long count=0; count<size_of_file; count++)
				fos.write(ois.read());
			fos.close();
		}
		Log.log.finest(()->"Received event "+evt+" w/ file "+evt.file_conveyor+", size:"+size_of_file);
		return evt;
	}
	
	@Override
	public Event clone()
	{
		Event clone = null;
        try
        {
	        clone = (Event) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
        	/* explicitly ignore, it won't happen */
        }
		return clone;
	}
}
