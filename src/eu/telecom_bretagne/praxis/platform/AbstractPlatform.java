/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.platform;

import static eu.telecom_bretagne.praxis.common.events.PlatformToServerEvent.Type.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.jdom.Document;
import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.XMLConstants;
import eu.telecom_bretagne.praxis.common.Utile.ResourceAvailability;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.common.events.PlatformToServerEvent;
import eu.telecom_bretagne.praxis.common.events.ServerToPlatformEvent.ServerToPlatformEventListener;
import eu.telecom_bretagne.praxis.core.execution.Result;

/**
 */
public abstract class AbstractPlatform implements ServerToPlatformEventListener
{
	protected CommunicationFacade cnx;
	protected Document configuration;
	protected Map<String,ResourceAvailability> resources_ids;
	
	/**
	 * Reads and sets the configuration file and sets the OS type in the xml. This is called by the constructor.
	 * @param file
	 *            the (xml) configuration file to read and validate
	 * @return the XML document if the file was successfully read and if the determined OS is supported, {@code null}
	 *         otherwise.
	 * @see eu.telecom_bretagne.praxis.common.Environment#getPlatformOS()
	 */
	protected static Document initConfiguration(final String file)
	{
		Document doc = null;
		try (InputStream in = FileResources.inputStreamForResource(file/*, false*/)) // config. remains in plain text, always
		{
			doc = Facade_xml.read(in, true);
		}
		catch (Exception e) {
			Log.log.log(Level.SEVERE,
			            "Error while reading the configuration file "+file, e);
			return null;
		}
		return initConfiguration(doc);
	}
	
	/**
	 * Sets the configuration file and sets the OS type in the xml. This is called by the constructor.
	 * @param file
	 *            the (xml) configuration file to read and validate
	 * @return the XML document if the file was successfully read and if the determined OS is supported, {@code null}
	 *         otherwise.
	 * @see eu.telecom_bretagne.praxis.common.Environment#getPlatformOS()
	 */
	protected static Document initConfiguration(final Document configuration)
	{
		final String OS = eu.telecom_bretagne.praxis.common.Environment.getPlatformOS();
		if ( !"UNIX".equals(OS) && !"WINDOWS".equals(OS) )
		{
            Log.log.severe(() -> "Unsupported OS: " + ( OS != null ? OS : "<null>" ));
   			return null;
		}
		Element root = configuration.getRootElement();
		root.setAttribute(XMLConstants.PLATFORM_OS_ATTR_TAG, OS);
		return configuration;
	}
	
	/**
	 * @param cnx
	 *            an established connection to the server
	 * @param configuration
	 *            the configuration for this platform
	 * @throws RuntimeException
	 *             if the underlying OS is not supported, or if the configuration is invalid. When an exception is
	 *             raised, the supplied connection is automatically {@link CommunicationFacade#disconnect()
	 *             disconnected}.
	 */
	public AbstractPlatform(CommunicationFacade cnx, Document configuration)
	{
		this.configuration = initConfiguration(configuration);
		if (this.configuration==null)
		{
			Log.log.severe("Invalid configuration XML document");
			cnx.disconnect();
			throw new RuntimeException("Invalid configuration XML document");
		}
		finalizeInitialization(cnx);
	}

	protected void finalizeInitialization(CommunicationFacade communicationFacade)
	{
		initResourcesAvailability();
		this.cnx = communicationFacade;
		cnx.setName("Platform");
		cnx.addListener(this);
		sendDescription(configuration);
	}

	String[] resourcesIDs()
	{
		return resources_ids.keySet().toArray(new String[0]);
	}
	
	public synchronized void updateStatus(String prg_id, boolean available)
	{
		ResourceAvailability wasAvailable=resources_ids.get(prg_id);
		if (wasAvailable==null)
		{
			// CHECK? should not happen
			Log.log.severe(()->"Dunno anything about "+prg_id);
			return;
		}
		if ( (  available && wasAvailable.equals(ResourceAvailability.NOT_AVAILABLE) ) ||
			 ( !available && wasAvailable.equals(ResourceAvailability.AVAILABLE) ) )
		{
			resources_ids.put(prg_id, available?ResourceAvailability.AVAILABLE:ResourceAvailability.NOT_AVAILABLE);
			sendAvailableResources();
		}
	}

	/**
	 * should be called <b>after</b> {@link #initConfiguration(String)}
	 */
	protected void initResourcesAvailability()
	{
		List<?> declaredProgs = configuration.getRootElement().getChild(XMLConstants.PLAT_PROGRAMS_TAG).getChildren(XMLConstants.PLAT_PROGRAM_TAG);
		resources_ids = new HashMap<String, ResourceAvailability>();
		for(Iterator<?> prg = declaredProgs.iterator(); prg.hasNext(); )
        {
			resources_ids.put( ((Element)prg.next()).getAttributeValue("id"),
			                   ResourceAvailability.AVAILABLE );
			/*
			String provider_name_version[] = ((Element)prg.next()).getText().split("___", 3);
            String provider = provider_name_version[0];
            String name = provider_name_version[1];
			*/
        }
	}

	/**
     * 
     */
	// Keep EnumMap here: we need a Serializable object in sendAvailableResources() & sendDescription()
    protected EnumMap<ResourceAvailability, List<String>> getAvailabilityMap()
    {
		EnumMap<ResourceAvailability, List<String>> map = new EnumMap<>(ResourceAvailability.class);
	    ArrayList<String> avail = new ArrayList<String>();
		ArrayList<String> not_avail = new ArrayList<String>();
		for (String prg: resources_ids.keySet().toArray(new String[0]))
		{
			if (resources_ids.get(prg).equals(ResourceAvailability.AVAILABLE))
				avail.add(prg);
			else
				not_avail.add(prg);
		}
		
		map.put(ResourceAvailability.AVAILABLE,     avail);
		map.put(ResourceAvailability.NOT_AVAILABLE, not_avail);
		return map;
    }
	
	/* Messages sent to server */
	/** Sends our description to the server */
	public void sendDescription(Document description)
	{
		PlatformToServerEvent event = new PlatformToServerEvent(CONFIGURATION);
		Document document=(Document)description.clone();
		document.getRootElement().removeChild(XMLConstants.PLAT_PROGRAMS_TAG);
		event.data = new Object[] { document, getAvailabilityMap() };
		cnx.send(event);
	}

	public void sendAvailableResources()
	{
		PlatformToServerEvent event = new PlatformToServerEvent(AVAILABLE_RESOURCES);
		event.data = getAvailabilityMap();
		cnx.send(event);
	}
	
	public void sendProgress(Result result)
	{
		sendResult(EXECUTION_PROGRESS, result);
	}

	public void sendResults(Result result)
	{
		sendResult(END_OF_EXECUTION, result);
	}

	private void sendResult(PlatformToServerEvent.Type eventType, Result result)
	{
		PlatformToServerEvent event = new PlatformToServerEvent(eventType);
		event.data = result;
		event.file_conveyor=result.zipFile();
		result.readyToSerialize();
		cnx.send(event);
	}
	
	/* **** ServerToPlatformEventListener **** */
	public abstract void serverCancelsExecution(Event event);
	
	public abstract void serverRequestsExecution(Event event);
	
	public void serverRequestsTermination(Event event) {
		Log.log.info(()->"Server asked for termination, reason: "+(String)event.data+". Exiting");
		cnx.disconnect();
	}
	
	public void serverSentMessage(Event event)
	{
		Log.log.info(()->"Received message: "+(String)event.data+" (NB: received message are currently ignored)");
		if ("LOGIN_ACK".equals(event.data))
			sendAvailableResources();
	}
	
	public void disconnected(Exception exception)
	{
		/* Oops, the server has disappeared! PLatform is about to terminate automatically on disconnection */
		String msg = "Lost connection to the server";
		if ( exception != null )
			Log.log.log(Level.INFO, msg, exception);
		else
			Log.log.log(Level.INFO, msg);
		
	}

}
