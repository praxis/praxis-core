/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.core.execution;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import eu.telecom_bretagne.praxis.core.execution.ExecutionSet.ProgramInfo;
import eu.telecom_bretagne.praxis.core.workflow.Program;

/**
 * @author Sébastien Bigaret
 *
 */
public class ExecutionSetTest
{   
	@Test
	public void addToSet()
	{
		ExecutionSet s = new ExecutionSet();
		Program p1 = new Program();
		assertTrue(s.add(p1));
		assertEquals(s, p1.getExecutionSet());
		assertEquals(1, s.size());
		assertTrue(s.contains(p1));
		assertFalse(s.add(p1));
		assertTrue(s.getInfoForProgram(p1)!=null);
	}
	
	@Test
	public void removeFromSet()
	{
		ExecutionSet s1 = new ExecutionSet(),
		             s2 = new ExecutionSet();
		Program a1 = new Program();
		assertFalse(s1.remove(a1));
		s1.add(a1);
		assertFalse(s2.remove(a1));
		assertEquals(s1, a1.getExecutionSet());

		assertTrue(s1.remove(a1));
		assertTrue(s1.isEmpty());
		assertNull(a1.getExecutionSet());
		assertNull(s1.getInfoForProgram(a1));
	}

	@Test
	public void reassignProgram()
	{
		ExecutionSet s1 = new ExecutionSet(), s2 = new ExecutionSet();
		Program a1 = new Program();
		s1.add(a1);
		ProgramInfo info_s1 = s1.getInfoForProgram(a1);
		s2.add(a1);
		ProgramInfo info_s2 = s2.getInfoForProgram(a1);
		assertTrue(s1.isEmpty());
		assertEquals(1, s2.size());
		assertTrue(s2.contains(a1));
		assertEquals(s2, a1.getExecutionSet());
		assertEquals(info_s1, info_s2);
		assertNull(s1.getInfoForProgram(a1));
	}
	
	@Test
	public void addAll()
	{
		ExecutionSet s1 = new ExecutionSet();
		ExecutionSet s2 = new ExecutionSet();
		Program a1 = new Program();
		Program a2 = new Program();
		Program a3 = new Program();
		Program b1 = new Program();
		s1.addAll(Arrays.asList(new Program[]{a1, a2, a3}));
		assertEquals(s1, a1.getExecutionSet());
		assertEquals(s1, a2.getExecutionSet());
		assertEquals(3, s1.size());
		assertTrue(s1.contains(a1));
		assertTrue(s1.contains(a2));

		ProgramInfo info_a1 = s1.getInfoForProgram(a1);
		ProgramInfo info_a2 = s1.getInfoForProgram(a2);
		ProgramInfo info_a3 = s1.getInfoForProgram(a3);
		assertTrue(info_a1!=null);
		assertTrue(info_a2!=null);
		assertTrue(info_a3!=null);
		
		// reassignAll
		s2.add(b1);
		s2.addAll(Arrays.asList(new Program[]{a1, a3}));
		// -> s1 = { a2 }
		assertEquals(s1, a2.getExecutionSet());
		assertEquals(1, s1.size());
		assertTrue(s1.contains(a2));
		// and s2 = { b2, a1, a3 }
		assertEquals(s2, a1.getExecutionSet());
		assertEquals(s2, a3.getExecutionSet());
		assertEquals(3, s2.size());
		assertTrue(s2.contains(b1));
		assertTrue(s2.contains(a1));
		assertTrue(s2.contains(a3));
		assertFalse(s2.contains(a2));

		assertNull("No more info for a1 in s1", s1.getInfoForProgram(a1));
		assertNull("No more info for a3 in s1", s1.getInfoForProgram(a3));
		assertEquals("info for a2 should be untouched", info_a2, s1.getInfoForProgram(a2));
		assertEquals("Info should have been transmitted", info_a1, s2.getInfoForProgram(a1));
		assertEquals("Info should have been transmitted", info_a3, s2.getInfoForProgram(a3));
	}
	
	@Test
	public void removeAll()
	{
		ExecutionSet s1 = new ExecutionSet();
		Program a1 = new Program();
		Program a2 = new Program();
		Program a3 = new Program();

		s1.addAll(Arrays.asList(new Program[]{a1, a2, a3}));

		// trying removalAll() with a collection smaller, then bigger of the target set
		// see comments in ExecutionSet.removeAll() for details
		assertTrue(s1.removeAll(Arrays.asList(new Program[]{ a1, a3 }))); // remove(smaller set)
		
		assertEquals(s1, a2.getExecutionSet());
		assertNull(a1.getExecutionSet());
		assertNull(a3.getExecutionSet());
		assertNull("s1 has no more info for a1", s1.getInfoForProgram(a1));
		assertNull("s1 has no more info for a3", s1.getInfoForProgram(a3));

		assertFalse(s1.removeAll(Arrays.asList(new Program[]{ a1, a3, a1 })));

		assertTrue(s1.removeAll(Arrays.asList(new Program[]{ a2, a1, a3, a2, a2 }))); // remove bigger set

		assertTrue(s1.isEmpty());
		assertNull(a2.getExecutionSet());

		assertNull("s1 has no more info for a2", s1.getInfoForProgram(a2));
	}

	@Test
	public void clear()
	{
		ExecutionSet s = new ExecutionSet();
		Program a1 = new Program();

		s.add(a1);
		s.clear();
		assertTrue(s.isEmpty());
		assertNull(a1.getExecutionSet());
		assertNull("set has no more info for a1", s.getInfoForProgram(a1));
	}
	
/*	@Test
	public void predecessors()
	{
		ExecutionSet s = new ExecutionSet();
		Program a1 = new Program();
		Program a2 = new Program();
		Program a3 = new Program();

		s.addAll(Arrays.asList(new Program[]{a2, a3}));
		ActivityContext.addPredecessorTo(a1, a2);
		ActivityContext.addPredecessorTo(a2, a3);

		Program[] predecessors = s.predecessors();
		assertEquals(1, predecessors.length);
		assertEquals(a1, predecessors[0]);
	}
*/}
