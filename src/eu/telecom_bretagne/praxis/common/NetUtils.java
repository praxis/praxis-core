/**
 * 
 */
package eu.telecom_bretagne.praxis.common;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Sébastien Bigaret
 */
public class NetUtils
{
    static private final String                       CLASS_NAME   = NetUtils.class.getName();

    /**
     * This map caches the result computed by {@link #isLocal(String)} and {@link #isLocal(String, boolean)}.
     */
    static private ConcurrentHashMap<String, Boolean> isLocalCache = new ConcurrentHashMap<>();


    /**
     * Stores {@link InetAddress#getLocalHost()}: when a machine has a misconfigured DNS, one call can take several
     * seconds. At the time of writing (September 2017), this happens on some Mac OS X.
     * 
     * @see <a href=
     *      "https://stackoverflow.com/questions/33289695/inetaddress-getlocalhost-slow-to-run-30-seconds/41847289#41847289">
     *      InetAddress.getLocalHost() slow to run (30+ seconds)</a>
     * @see <a href="https://github.com/thoeni/inetTester">inetTester, by Antonio Troina</a>
     */
    static private InetAddress   localhostInetAddress;

    /**
     * Same as {@link #localhostInetAddress}, but for {@code InetAddress.getAllByName("localhost")}.
     */
    static private InetAddress[] allLocalhostInetAddress;

    static
    {
        try
        {
            localhostInetAddress = InetAddress.getLocalHost();
        }
        catch (UnknownHostException e)
        {
            localhostInetAddress = null;
        }
        try
        {
            allLocalhostInetAddress = InetAddress.getAllByName("localhost");
        }
        catch (UnknownHostException e)
        {
            allLocalhostInetAddress = new InetAddress[] {};
        }
    }

    /**
     * The real logic behind {@link #isLocal(String)} and {@link #isLocal(String, boolean)}. Exceptions like
     * {@link UnknownHostException} or {@link SocketException} are ignored (and the return value is false).
     * 
     * @param host
     *            the host name or its IP address.
     * @return {@code true} if the supplied host corresponds to the local machine.
     */
    static private boolean _isLocal(final String host)
    {
        if ( host == null || host.startsWith("127.") || "localhost".equals(host)
             || "::1".equals(host) || "0:0:0:0:0:0:0:1".equals(host) )
            return true;
        // from this point on, isLocal cannot be null
        InetAddress hostAddr = null;
        try
        {
            hostAddr = InetAddress.getByName(host);
        }
        catch (UnknownHostException e)
        {
            return false; /* We cannot do anything if we can't get the InetAdress */
        }

        /* loopback or any-local: this is local */
        if ( hostAddr.isLoopbackAddress() || hostAddr.isAnyLocalAddress() )
            return true;

        /* Does it match the localhost? */
        if ( hostAddr.equals(localhostInetAddress) )
        {
            Log.log.finest("This is InetAddress.getLocalHost()");
            return true;
        }

        /* If we can find an interface for this hostAddr, it is local */
        try
        {
            final NetworkInterface localInterface = NetworkInterface.getByInetAddress(hostAddr);
            if ( localInterface != null )
            {
                Log.log.finest("Found a local interface");
                return true;
            }
        }
        catch (SocketException e)
        { /* it could not be determined: let's continue */ }

        /* last try: compare to every InetAddress associated to the hostname "localhost" */
        for (InetAddress local: allLocalhostInetAddress)
        {
            Log.log.finest(() -> "Testing localhost: " + local);
            if ( local.equals(hostAddr) )
            {
                Log.log.finest(() -> "Found localhost: " + local);
                return true;
            }
        }
        return false;
    }

    /**
     * Determines whether a given host (either a host name or an IP address) corresponds to the local host. Equivalent
     * to {@code isLocal(host, false)}.<br/>
     * 
     * @param host
     *            a host name, or an IP address. If {@code null}, returns {@code true} (same semantics as in
     *            {@link java.net.Socket#Socket(String, int)}).
     * @return {@code true} if the host was found to be the local machine. When returning {@code false}, the method only
     *         indicates that the supplied {@code host} name could not be determined as the local host: it does
     *         <b>NOT</b> mean that the {@code host} is a valid host name or IP.
     */
    static public boolean isLocal(final String host)
    {
        return isLocal(host, false);
    }

    /**
     * Determines whether a given host (either a host name or an IP address) corresponds to the local host.
     * 
     * @param host
     *            a host name, or an IP address. If {@code null}, returns {@code true} (same semantics as in
     *            {@link java.net.Socket#Socket(String, int)}).
     * @param cached
     *            If {@code true}, and if the method has already been called for the same host, the returned value is
     *            the one that was computed on the previous call. If {@code false}, the cache value for this
     *            {@code host} is invalidated and re-computed.
     * @return {@code true} if the host was found to be the local machine.<br />
     *         When returning {@code false}, the method only indicates that the supplied {@code host} name could not be
     *         determined as the local host: it does <b>NOT</b> mean that the {@code host} is a valid host name or IP.
     */
    static public boolean isLocal(final String host, final boolean cached)
    {
        Boolean isLocal = null;
        Log.log.entering(CLASS_NAME, "isLocal", new Object[] { host, cached });
        if ( host == null )
            isLocal = true;
        else
            isLocal = cached ? isLocalCache.get(host) : null;

        if ( isLocal == null )
        {
            isLocal = _isLocal(host);
            isLocalCache.put(host, isLocal);
        }
        Log.log.exiting(CLASS_NAME, "isLocal", isLocal);
        return isLocal;
    }

}
