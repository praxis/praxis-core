/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.common;

import java.io.IOException;
import java.rmi.server.RMISocketFactory;
import java.util.Vector;

import sun.rmi.transport.proxy.RMIHttpToCGISocketFactory;/*FIX sun.rmi*/
import sun.rmi.transport.proxy.RMIHttpToPortSocketFactory;/*FIX sun.rmi*/

/**
 * This RMISocketfactory alters the behaviour of the default strategies used by RMI to connect to the server.
 * It permits:
 * <ul>
 * <li> to use a direct connection to the localhost, while forcing the behaviour to other hosts using another
 * factory (such as RMIHttpToCGISocketFactory),</li>
 * <li> to change the order in which the different stregies are used after direct connection fails; system's
 * property {@code rmi.factories} determines this order.</li>
 * </ul>
 * @author Sébastien Bigaret
 */
class RMIMasterSocketFactory
    extends sun.rmi.transport.proxy.RMIMasterSocketFactory/*FIX sun.rmi*/
{
	private final RMISocketFactory factory;

	/** Equivalent to {@code RMIMasterSocketFactory(null)} */
	public RMIMasterSocketFactory()
	{
		this(null);
	}
	
	/**
	 * Initializes the factory.<br/>
	 * If parameter {@code factory} is {@code null}, super's {@code altFactoryList} field is reinitialized using
	 * system's property {@code rmi.factories}.  The latter is a comma-separated list of values being either:
	 * <ul>
	 * <li>{@code http-to-port} corresponding to the factory RMIHttpToPortSocketFactory</li>
	 * <li>{@code http-to-cgi} corresponding to the factory RMIHttpToCGISocketFactory</li>
	 * </ul>
	 * The factories will be tried by {@link #createSocket(String, int)} in the order specified in
	 * {@code rmi.factories}; for example a value of {@code "http-to-cgi"} skips the http-to-port strategy and makes
	 * {@code createSocket()} try the http-to-cgi strategy only.
	 * @param factory
	 *            The factory to use for connection to any host except localhost
	 *            (see {@link #createSocket(String, int)}. it may be {@code null}.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RMIMasterSocketFactory(RMISocketFactory factory)
	{
		super();
		this.factory = factory;
		Log.log.finest(()->"Called with factory: "+(factory==null?"null":factory));
		if (factory != null)
			return; // no need to do anything, we won't use super.createSocket().
		
		final String factories = System.getProperty("rmi.factories");
		if (factories==null || factories.equals(""))
			return;

		// this should not be tested 
		this.altFactoryList = new Vector();
		for (String factoryName: factories.split(","))
		{
			if ("http-to-port".equals(factoryName.trim()))
				this.altFactoryList.add(new RMIHttpToPortSocketFactory());
			if ("http-to-cgi".equals(factoryName.trim()))
				this.altFactoryList.add(new RMIHttpToCGISocketFactory());
		}
	}

	/* documentation largely taken from java.rmi.server.RMISocketFactory */
	/**
	 * Create a server socket on the specified port (port 0 indicates an anonymous port).
	 * If {@code host} is {@code "localhost"} or starts with {@code "127."}, a {@link java.net.Socket} is returned
	 * (direct connection).  If a factory was supplied when object was built, it is used.  Otherwise, the method
	 * calls super's implementation.
	 * @param port
	 *            the port number
	 * @return the server socket on the specified port
	 * @exception IOException
	 *                if an I/O error occurs during server socket creation
	 */
	public java.net.Socket createSocket(final String host, final int port) throws java.io.IOException
	{
	    final String hostname = (host==null?"<null>":host);
		Log.log.finer(()->"Looking for "+hostname+":"+port);

		final boolean isLocal = NetUtils.isLocal(host, true);
		if (isLocal)
		{
			Log.log.finest(()->"direct connection to localhost: "+hostname+":"+port);
			return new java.net.Socket(host, port);
		}
		if (factory != null)
		{
			Log.log.finest(()->"Using stored factory "+factory+" for "+hostname+":"+port);
			return factory.createSocket(host, port);
		}
		Log.log.finest(()->"Calling super.createSocket <- "+hostname+":"+port);
		java.net.Socket s = super.createSocket(host, port);
		Log.log.finest(()->"super.createSocket returned -> "+( (s!=null)?s.getClass():"<null>") );
		return s;
	}
}
