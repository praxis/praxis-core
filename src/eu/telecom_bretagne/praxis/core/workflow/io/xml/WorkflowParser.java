/**
 * 
 */
package eu.telecom_bretagne.praxis.core.workflow.io.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisXMLResolver;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput;

/**
 * @author Sébastien Bigaret
 */
public abstract class WorkflowParser
{
    static final String WORKFLOW = "workflow";

    static final String PROGRAMS = "programs";

    static final String INPUTS = "inputs";

    /**
	 * Builds a new workflow by reading a file
	 * 
	 * @param filename
	 *            the path of the xml file to read
	 * @param xmlWarnings
	 *            the object is which warnings intended for the user should be stored.
	 * @return the new workflow
	 */
    public static Workflow create(String filename, Workflow.XMLWarnings xmlWarnings)
    throws InvalidXMLException, IOException, XMLStreamException
    {
        return create(new File(filename), xmlWarnings);
    }

    /**
	 * Builds a new workflow by reading an XML file.
	 * 
	 * @param file
	 *            the file containing the workflow
	 * @param xmlWarnings
	 *            the object is which warnings intended for the user should be stored.
	 * @return the new workflow
	 */
    public static Workflow create(File file, Workflow.XMLWarnings xmlWarnings)
    throws InvalidXMLException, IOException, XMLStreamException
    {
        if ( ! (file.exists() && file.isFile() && file.canRead()) )
            throw new IllegalArgumentException("invalid workflow file");

        Workflow workflow = null;

        // First create a new XMLInputFactory
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        inputFactory.setXMLResolver(new PraxisXMLResolver());
        // Setup a new eventReader
        InputStream in = new FileInputStream(file);
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

        // Read the XML document
        if ( ! eventReader.hasNext() )
        {
            eventReader.close();
            in.close();
            return null;
        }
        
        while (eventReader.hasNext())
        {
            XMLEvent event = eventReader.nextEvent();
            
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();

                // If we have a item element we create a new item
                if ( WORKFLOW.equals(startElement.getName().getLocalPart()) )
                {
                    workflow = create(eventReader, xmlWarnings);
                }
            }
            
        }
        eventReader.close();
        in.close();
        return workflow;
    }

    /**
     * Rebuilds the id stored in the string and stores it in {@link #id}.
     * @param id_str the string representation of the ID
     * @return the WorkflowID encoded in {@code id_str}
     */
    protected static WorkflowID buildID(String id_str)
    {
        WorkflowID workflowID = null;
        if (id_str==null)
            return new WorkflowID();

        try
        {
            workflowID = new WorkflowID(id_str);
        }
        catch (IOException | ClassNotFoundException e) { Log.log.log(Level.WARNING, "Unable to read the id", e); }
        
        if (workflowID == null) // TODO flag this, so that the GUI can report it
            workflowID = new WorkflowID();
        return workflowID;
    }

    static Workflow create(XMLEventReader eventReader, Workflow.XMLWarnings xmlWarnings)
    throws XMLStreamException, InvalidXMLException
    {
        Workflow workflow = null;
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW);
        final String INVALID_WORKFLOW = "Incorrect workflow: at least a name is required";

        while ( eventReader.hasNext() )
        {
            // id?,name,user,annotation?,inputs+, programs
            XMLEvent event = eventReader.nextEvent();
            if (event.isEndElement())
                if (WORKFLOW.equals(event.asEndElement().getName().getLocalPart()))
                    break;

            if (!event.isStartElement())
                continue;
            
            final StartElement startElement = event.asStartElement();
            switch (startElement.getName().getLocalPart())
            {
            case "id":
                event = eventReader.nextEvent();
                final String wfID_str = event.asCharacters().getData();

                workflow = new Workflow(buildID(wfID_str));
                xml_exc.setName(workflow.id().name());
                break;

            case "name":
                // the workflowID contains the name and has already been retrieved, yes
                // But if the name has been changed in the XML, it's because the user has changed it.
                // Praxis itself does not store the workflow's name but in the workflowID.
                // So, if the user intended to change the workflow's name, we are okay.
                // (this may happen, for example, when the user is directly manipulating the file in the workspace,
                // to copy/paste a workflow for example (emptying the workflowID and setting a different name in
                // that case).
                event = eventReader.nextEvent();
                final String name = event.asCharacters().getData();
                // the id is optional: if there was no id, the workflow has not yet been created
                if (workflow == null)
                    workflow = new Workflow(name); // TODO flag this, so that the GUI can report it
                else
                    workflow.setName(name);
                xml_exc.setName(name);
                break;
            
            case "user":
                if ( workflow == null ) throw new InvalidXMLException(INVALID_WORKFLOW);
                workflow.setUser(Utils.getPCDATA(startElement, eventReader));
                break;

            case "annotation":
                if ( workflow == null ) throw new InvalidXMLException(INVALID_WORKFLOW);
                workflow.setAnnotation(Utils.getPCDATA(startElement, eventReader));
                break;

            case "inputs":
                if ( workflow == null ) throw new InvalidXMLException(INVALID_WORKFLOW);
                handleInputs(workflow, eventReader, xmlWarnings);
                break;

            case "programs":
                if ( workflow == null ) throw new InvalidXMLException(INVALID_WORKFLOW);
                handlePrograms(workflow, eventReader, xmlWarnings);
                break;
            }
        }

        if ( workflow == null ) throw new InvalidXMLException(INVALID_WORKFLOW);

        /* Now that all programs have been loaded, we need to connect the
         * programs: this is done by connecting input and output parameters.
         */
        for (Program program: workflow.getPrograms())
        {
            for (final Parameter input_param: program.getInputParameters())
            {
                if ( input_param.getInput() != null )
                    /* this is an input parameter already connected to a WorkflowInput */
                    continue;
                if ( input_param.getData()==null || "".equals(input_param.getData()) )
                    /* CHECK input.getData() may be null as well, when the loaded prg. was saved w/ a description which did not contain this input */
                    /* not set; in this case, input.io defaults to null, perfect */
                    continue;

                // Now get the corresponding output Parameter which is connected to the input_param
                final String input_xml_id = input_param.getData(); // formatted as: "program_id.parameter_id"
                Parameter output_param = null;
                searchOutputParameter:
                {
                    final int idx = input_xml_id.lastIndexOf(".");
                    if ( idx ==-1 )
                        break searchOutputParameter;
                    final Program input_prg = workflow.getProgramWithId(input_xml_id.substring(0, idx));
                    if ( input_prg == null )
                        break searchOutputParameter;
                    output_param = input_prg.getParameterWithID(input_xml_id.substring(idx+1));
                }
                if ( output_param == null )
                {
                    String input_prg="??", input_name="??";
                    try {
                        input_prg  = input_xml_id.split("\\.")[0];
                        input_name = input_xml_id.split("\\.")[1];
                    }
                    catch (IndexOutOfBoundsException exc) { /* explicitly ignore */ }

                    xmlWarnings.add(Workflow.XMLWarnings.PRG_HAS_INVALID_LINK,
                                    new String[] { program.getProgramID(), input_prg, input_name });
                    continue;
                }
                if (!input_param.isActivated() || !output_param.isActivated())
                {
                    xmlWarnings.add(Workflow.XMLWarnings.PRG_LINK_IS_INACTIVE,
                                    new String[] { output_param.getProgram().getProgramID(),
                                                   output_param.getDescription().getDisplayName(),
                                                   input_param.getProgram().getProgramID(),
                                                   input_param.getDescription().getDisplayName(),
                    });
                    continue;
                }
                output_param.addOutput(input_param);
            }
        }

        /*
         * check WorkflowInputs: if, due to changes in the description, some are
         * connected to a non-activated input, the link must be removed.
         */
        for (WorkflowInput wfInput: workflow.getInputs())
        {
            for (Parameter p: wfInput.outputs()) // we need a copy
            {
                if ( ! p.isActivated() )
                    p.setInput(null); // TODO add Workflow.XMLWarnings
            }
        }

        return workflow;
    }

    protected static void handleInputs(Workflow workflow, XMLEventReader eventReader,
                                       Workflow.XMLWarnings warnings) throws InvalidXMLException
    {
        while ( eventReader.hasNext() )
        {
            final XMLEvent event;
            try
            {
                event = eventReader.nextEvent();
            }
            catch (XMLStreamException ise)
            {
                throw new InvalidXMLException(ELEMENT_TYPE.WORKFLOW, workflow.getName(),
                                              "problem when reading inputs"+ise.getMessage());
            }
            if ( event.isEndElement() )
                if ( INPUTS.equals(event.asEndElement().getName().getLocalPart()) )
                    break;

            if ( !event.isStartElement() )
                continue;

            final StartElement startElement = event.asStartElement();

            if (WorkflowInputParser.INPUT.equals(startElement.getName().getLocalPart()))
            {
                // the input's id is not stored in the WorkflowID, but in the map
                final String input_id;

                final Attribute _id = startElement.getAttributeByName(new QName("id"));
                if ( _id == null )
                	throw new InvalidXMLException(ELEMENT_TYPE.WORKFLOW).setMsg("Invalid id for input: (missing attribute)");
                input_id = _id.getValue();

                if ( workflow.getInputWithId(input_id) != null )
                {
                    throw new InvalidXMLException(ELEMENT_TYPE.WORKFLOW, workflow.getName(),
                                                  "Duplicate input id:" + input_id);
                }

                WorkflowInput input = WorkflowInputParser.fromXML(workflow, startElement, eventReader, warnings);
                workflow.addNewInput(input_id, input);
                // program.setWorkflow(workflow); // already done in Workflow#addProgram()
            }
        }
    }

    protected static void handlePrograms(Workflow workflow, XMLEventReader eventReader,
                                         Workflow.XMLWarnings warnings) throws InvalidXMLException
    {
        while ( eventReader.hasNext() )
        {
            final XMLEvent event;
            try
            {
                event = eventReader.nextEvent();
            }
            catch (XMLStreamException ise)
            {
                throw new InvalidXMLException(ELEMENT_TYPE.WORKFLOW, workflow.getName(),
                                              "problem when reading inputs"+ise.getMessage());

            }
            if ( event.isEndElement() && PROGRAMS.equals(event.asEndElement().getName().getLocalPart()) )
                break;

            if ( !event.isStartElement() )
                continue;

            StartElement startElement = event.asStartElement();

            if ( ProgramParser.PROGRAM.equals(startElement.getName().getLocalPart()) )
            {
                /* Program program = */ ProgramParser.fromXML(workflow, startElement, eventReader, warnings);
                // the program is added to the workflow, when this method returns.
                // program.setWorkflow(workflow); // already done in Workflow#addProgram()
            }
        }

    }

    /**
     * Simple test for Workflow read/write
     * @param args (ignored)
     */
    public static void main(String[] args) throws Throwable
    {
        Workflow.XMLWarnings warnings = new Workflow.XMLWarnings();

        //String tst="/home/big/diviz_workspaces/diviz_workspace_all/PyXMCDA-csvToXMCDA-performanceTable/current/PyXMCDA-csvToXMCDA-performanceTable.dvz";
        String tst = "/home/big/diviz_workspaces/diviz_workspace_all/test-bigMixKappalab/current/test-bigMixKappalab.dvz";
        Workflow workflow = create(tst, warnings);
        workflow.save(new File("/tmp/test.dvz"), true);
        System.exit(0);
    }

}
