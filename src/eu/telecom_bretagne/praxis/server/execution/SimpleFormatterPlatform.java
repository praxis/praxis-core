/* (c) Sébastien Bigaret */
package eu.telecom_bretagne.praxis.server.execution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.ParameterType;
import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription.CannotExecuteException;




/**
 * @author Sébastien Bigaret
 */
public abstract class SimpleFormatterPlatform
    extends ExecutionEngine
{
	private static final long serialVersionUID = -3142931935735598166L;

	/**
	 * Maps every {@link Program#getProgramID() program's id} to the corresponding script.
	 * @see #prepareExecution(Activity, ExecutionEngineConfiguration)
	 * @implementation For the moment being, the whole ExecutionEngine only executes a single program, the one pointed
	 *                 to by the supplied activity (see
	 *                 {@link #prepareExecution(Activity, ExecutionEngineConfiguration)}, so this map gets only one
	 *                 key for the moment. However, we plan to be able to handle a set of activities (or a Process) in
	 *                 a near future, that's why we keep it as-is.
	 */
	protected LinkedHashMap<String, Map<Enum<?>,List<String>>> scripts_for_prg;
	
	/**
	 * It contains, for every {@link Program#getProgramID() program's id}, a dictionary mapping the id of each
	 * program's output parameter to an array of two files containing:
	 * <ul>
	 * <li>1st: the path of the file produced after the execution completes, associated to the output parameter,</li>
	 * <li>2nd: the path of the file after it is {@link #renameOutputFiles(String, File, Result) renamed}.</li>
	 * </ul>.
	 * Paths are stored as File to guarantee the abstract representation of path names: they are built on the server
	 * while the renaming itself happens on the platform.
	 * @see #prepareExecution(Activity, ExecutionEngineConfiguration)
	 * @see #identifyOutputFiles(Program)
	 * @see #renameOutputFiles(String, File, Result)
	 */
	protected HashMap<String, HashMap<String, File[]>> output_files_for_prg;
	
	/**
	 * It contains, for every {@link Program#getProgramID() program's id}, a dictionary mapping the input filename
	 * for each program's input to the default ones, i.e. the ones that are expected by the program when it starts its
	 * execution (they are multiple default input files when the same file is connected to different inputs of the
	 * same program).
	 * @see #prepareExecution(Activity, ExecutionEngineConfiguration)
	 * @see #identifyInputFiles(Program)
	 * @see #prepareInputFiles(String, File, Result)
	 */
	protected HashMap<String, HashMap<File,ArrayList<String>>> input_files_for_prg;
	
	/**
	 * 
	 */
	protected SimpleFormatterPlatform(ExecutionEngineConfiguration configuration, WorkflowID workflowID, ExecutionID executionID)
	{
		super(configuration, workflowID, executionID);
		scripts_for_prg = new LinkedHashMap<String, Map<Enum<?>, List<String>>>();
		input_files_for_prg  = new HashMap<String, HashMap<File, ArrayList<String>>>();
		output_files_for_prg = new HashMap<String, HashMap<String, File[]>>();
	}
	
	protected void addCodeToTarget(String prgID, Enum<?> target, String code)
	{
		addCodeToTarget(prgID, target, code, -1);
	}
	protected void insertCodeToTarget(String prgID, Enum<?> target, String code)
	{
		addCodeToTarget(prgID, target, code, 0);
	}
	
	protected void addCodeToTarget(String prgID, Enum<?> target, String code, int position)
	{
		if (target==null)
			target=defaultTarget();
		
		Map<Enum<?>, List<String>> targets_code = scripts_for_prg.get(prgID);
		if (targets_code==null)
		{
			targets_code=new HashMap<Enum<?>, List<String>>();
			scripts_for_prg.put(prgID, targets_code);
		}
		List<String> target_code = targets_code.get(target);
		if (target_code==null)
		{
			target_code = new ArrayList<String>();
			targets_code.put(target, target_code);
		}
		if (position<0)
			target_code.add(code);
		else
			target_code.add(position, code);
	}
	
	protected String[] codesForTarget(String prgID, String target)
	{
		Map<Enum<?>, List<String>> targets_code = scripts_for_prg.get(prgID);
		if (targets_code==null)
			return new String[]{};
		List<String> codes = targets_code.get(target);
		return codes.toArray(new String[codes.size()]);
	}
	
	/**
	 * Processes the supplied program and produces the {@link #scripts_for_prg scripts} as well as the {@link #output_files_for_prg map of output files}.
	 * @param activity
	 * @param aConfiguration
	 */
	@Override
	public void prepareExecution(Activity activity, ExecutionEngineConfiguration aConfiguration)
	throws CannotExecuteException
	{
		super.prepareExecution(activity, aConfiguration);
		generateScript(activity);
	}
	
	/**
	 * Generates the shell scripts for all programs in <code>program</code>.<br>
	 * Generated scripts are stored in {@link #scripts_for_prg}.<br>
	 * Key-value pairs mapping default filenames to the requested one are stored in {@link #output_files_for_prg}
	 * @param activity
	 * @implementation Warning: there is a single instance per platform declared in the server. It means that, the
	 *                 execution of two different & independent branches of a workflow triggers the call of
	 *                 generateScript() **concurrently**, in two different threads. As a result, neither
	 *                 generateScript() nor methods it calls should, IN ANY WAY, rely on some state stored within the
	 *                 instance. Here for example, the variable 'result' declared here in generateScript() CANNOT
	 *                 be declared as an instance variable.
	 */
	protected void generateScript(final Activity activity)
	{
		identifyInputFiles(activity); // should be done before the script is generated: getCode_output() relies on it
		identifyOutputFiles(activity);
		for (Program program: activity.getExecutionSet().programsSortedForExecution())
		{
			generateScript(program);
		}
		generateScript_justProcessed(activity);
	}

	protected void generateScript(final Program program)
	{
		/*
		 * Those two are needed after all parameters have been processed, when time comes where the output files must
		 * get their new names, see getLigneCodeScriptType()
		 */
		final String prgID = program.getProgramID();
		final ParameterDescription[] parametersDesc = program.getDescription().getParametersSortedByPosition();
		ParameterType type;

		for (ParameterDescription paramDesc: parametersDesc)
		{
			Parameter param = program.getParameterForDescription(paramDesc);
			type = paramDesc.getType();
			if (type.equals(ParameterType.CODE))
				if (param==null)
					param=new Parameter(paramDesc, program);
			
			String code = getCode(paramDesc, param);
			if (code != null)
				addCodeToTarget(prgID, getTarget(paramDesc, param), code);
		}
		//scripts_for_prg.put(program.getProgramID(), result);
	}
	
	/**
	 * Analyses the input parameters for the supplied program, and adds to the corresponding map in
	 * {@link #input_files_for_prg} key-value pairs, one for each {@link Parameter#isActivated() activated input},
	 * mapping its actual value to the default value (<code>&lt;vdef/&gt;</code>).
	 * @param activity
	 *            the activity whose outputs are analysed
	 */
	private void identifyInputFiles(final Activity activity)
	{
		for (Program program: activity.getExecutionSet())
		{
			identifyInputFiles(program);
		}
	}
	
	private void identifyInputFiles(final Program program)
	{
		final String prg_id = program.getProgramID();
		HashMap<File, ArrayList<String>> map = input_files_for_prg.get(prg_id);
		if (map==null)
		{
			map = new HashMap<File, ArrayList<String>>();
			input_files_for_prg.put(prg_id, map);
		}
		
		for (Parameter input: program.getInputParameters())
		{
			if (!input.isActivated())
				continue;
			File infile;
			if (input.getInput() != null)
			{
				infile = new File(input.getInput().filepath());
			}
			else
			{
				Parameter p = input.getPrgInput();
				infile = new File(p.getProgram().getProgramID(), p.getDescription().getVdef());
			}
			ArrayList<String> infile_corresponding_filename = map.get(infile);
			if (infile_corresponding_filename==null)
			{
				infile_corresponding_filename = new ArrayList<String>();
				map.put(infile, infile_corresponding_filename);
			}

			final String vdef = input.getDescription().getVdef(); 

			if (vdef!=null && !vdef.equals(""))
			{
				infile_corresponding_filename.add(String.format(vdef, infile));

				// we can modify the parameter: each activity received its own copy, there is no risk of overriding
				// anything outside the activity's scope (see WorkflowPlanner.plan())
				// see also getCode_input() which relies on this being set
				input.setData(vdef);
			}
			else
				input.setData(infile.getPath());
		}
	}
	
	/**
	 * Copies all the input files of a program to the execution directory, under the name the program expects to find.<br/>
	 * When some input files could not be found, or if (some of) them could not be copied, the {@code prgResult}
	 * object is updated as follows:
	 * <ul>
	 * <li>its status is set to {@link Result.Status#ERROR}</li>
	 * <li>the status for program ID {@code ID} is set to {@link ProgramResult.ProgramStatus#ERROR},</li>
	 * <li>a new key is created in the informational part of the result, named "files_not_found", containing a
	 * comma-separated list of missing files</li>
	 * </ul>
	 * @param prgID
	 *            the ID of the program whose input files should be prepared
	 * @param executionDirectory
	 *            the execution directory
	 * @param prgResult
	 * @return the list of the files created by this method
	 */
	protected File[] prepareInputFiles(String prgID, File executionDirectory, Result prgResult)
	{
		HashMap <File,ArrayList<String>> prg_input_files = input_files_for_prg.get(prgID);
		if (prg_input_files==null)
			return new File[]{};
		ArrayList <File> newFiles = new ArrayList<File>();
		
		for (Map.Entry<File, ArrayList<String>> prg_input_file: prg_input_files.entrySet())
		{
			// TODO: s'assurer qu'on n'a pas a->b et b->c auquel cas on écraserait b...
			// Note: some programs creates empty output files when they starts, so they may exist even if the
			// process failed
			File filename = prg_input_file.getKey();
			File src_file = new File(executionDirectory, filename.getPath());
			
			// iterate on the destination files, and copy the src into each dest. files
			for (String dest_file_name: prg_input_file.getValue())
			{
				File dst_file = new File(executionDirectory, dest_file_name);
				if (!src_file.exists())
				{
					prgResult.addInfoForPrg(prgID, "files_not_found", src_file+",");
					prgResult.setStatus(Result.Status.ERROR);
					continue;
				}
				try
				{
					if (src_file.equals(dst_file))
						continue;
					// deepCopyFile(): we can receive files or directories here
					Utile.deepCopyFile(src_file, dst_file);
					newFiles.add(dst_file);
				}
				catch (IOException ioe)
				{
					prgResult.setStatus(Result.Status.ERROR);
					continue; // so that all missing files, if any, are added to "files_not_found"
				}
			}
			// Do not delete the original files, it's not our job, plus
			// they may be needed as input files for another program being executed in the same batch
		}
		if (Result.Status.ERROR.equals(prgResult.getStatus()))
		{
			prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.ERROR); // CHECK 
			// CHECK this should probably be done in exec., bypassing the real exec.
			// CHECK in this case, check the above code as well, it's no use keeping copying the files when a error is detected
			// or maybe it's worth it, if we want to try the execution nevertheless...
			// -> We should probably use status WARNING instead of ERROR then?
			result.mergeWith(prgResult);
			prgResult.dumpContent(executionDirectory);
		}
		return newFiles.toArray(new File[newFiles.size()]);
	}
	
	/**
	 * Analyses the output parameters for the supplied program, and adds to the corresponding map in
	 * {@link #output_files_for_prg} key-value pairs, one for each {@link Parameter#isActivated() activated output},
	 * mapping the default value (<code>&lt;vdef/&gt;</code>) to its actual value in the script.
	 * @param activity
	 *            the activity whose outputs are analysed
	 */
	private void identifyOutputFiles(Activity activity)
	{
		for (Program program: activity.getExecutionSet())
			identifyOutputFiles(program);
	}

	private void identifyOutputFiles(Program program)
		{
		final String prg_id = program.getProgramID();
		HashMap<String, File[]> map = output_files_for_prg.get(prg_id);
		if (map==null)
		{
			map = new HashMap<String, File[]>();
			output_files_for_prg.put(prg_id, map);
		}
		
		for (Parameter output: program.getOutputParameters())
		{
			if (!output.isActivated())
				continue;
			final String vdef = output.getDescription().getVdef();
			if (vdef!=null && !vdef.equals(""))
				map.put(output.getDescription().getId(), new File[] {new File(vdef), new File(program.getProgramID(), vdef)});
		}
	}
	
	/**
	 * Renames the output files to the expected names, after the execution is finished.
	 * @param prgID the ID of the program whose output file should be renamed
	 * @param executionDirectory the directory where the execution took place
	 * @param prgResult the program's result object. if something goes wrong (i.e. an output file cannot be renamed, or
	 * it cannot be found), its status will be changed to {@link Result.Status#ERROR}.
	 */
	protected void renameOutputFiles(String prgID, File executionDirectory, Result prgResult)
	{
		HashMap <String,File[]> prg_output_files = output_files_for_prg.get(prgID);
		if (prg_output_files==null)
			return;
		for (Map.Entry<String, File[]> prg_output_file: prg_output_files.entrySet())
		{
			final String paramID = prg_output_file.getKey();
			final String dstFileName = prg_output_file.getValue()[1].getPath();
			boolean error = false;
			// TODO: s'assurer qu'on n'a pas a->b et b->c auquel cas on écraserait b...
			String filename = prg_output_file.getValue()[0].getPath();
			File src_file = new File(executionDirectory, filename);
			File dst_file = new File(executionDirectory, dstFileName);
			if (!src_file.exists())
			{
				prgResult.addInfoForPrg(prgID, "files_not_found", src_file+",");
				error = true;
			}
			else if (!Utile.renameFile(src_file, dst_file))
			{
				Log.log.warning(()->"could not rename files "+src_file+" to "+dst_file);
				error = true;
			}
			else
			{
				prgResult.getProgramInfo(prgID, true).setOutput(paramID, new File(dstFileName));
			}
			if (error && Result.Status.OK.equals(prgResult.getStatus()))
			{
				prgResult.setStatus(Result.Status.ERROR);
				prgResult.setExecStatus(prgID, ProgramResult.ProgramStatus.ERROR); // CHECK 
			}
		}
	}
	
	/**
	 * Called by {@link #generateScript(Activity)} for each {@link Activity} that it processes, <b>after</b>
	 * having processed the supplied program: this means that the generated scripts and information about its output
	 * files can be found, respectively, in {@link #scripts_for_prg} and {@link #output_files_for_prg}. <br>
	 * @param activity
	 * */
	/* OUT OF DATE comment, TODO update me
	 * Subclasses may implement it to add their own processing during this step; note that this is just a convenience
	 * offered to subclasses, since subclasses may as well override {@link #generateScript(Resource)}, and iterate on
	 * the {@link Resource#getAllConcreteResources() concrete resources} after calling super implementation.
	 */
	protected void generateScript_justProcessed(Activity activity) {}
	
	protected Enum<?> getTarget(ParameterDescription description, Parameter parameter)
	{
		if (description.getType().equals(ParameterType.ENUM))
			return getTargetForName(parameter.getItem().getValueTemplateTarget());
		return getTargetForName(description.getValueTemplateTarget());
	}
	
	/**
	 * @param description
	 * @param parameter
	 * @return the corresponding code, or <code>null</code> if no code should be issued (parameter is not activated)
	 */
	protected String getCode(ParameterDescription description, Parameter parameter)
	{
		final ParameterType type = description.getType(); 
		
		if (type.equals(ParameterType.COMMAND))
			return getCode_commandOrCode(description);
		
		if (parameter==null /* CHECK a null param. should not happen, but still, should we ignore it if it happens? */
				|| !parameter.isActivated())
			return null;
		
		if (type.equals(ParameterType.CODE) || type.equals(ParameterType.COMMAND))
			return getCode_commandOrCode(description);
		
		switch (parameter.getDescription().getType())
		{
			case BOOLEAN:
				return getCode_boolean(parameter);
			case CODE:
				//case COMMAND: // NO: no parameter is created for COMMAND, since a COMMAND has no dependency there is no need to
				return getCode_commandOrCode(description);
			case ENUM:
				return getCode_enumeration(parameter);
			case INPUT:
			case INPUT_DIRECTORY:
				return getCode_input(parameter);
			case INT:
			case FLOAT:
			case STRING:
			case TEXT:
				return getCode_IntFloatString(parameter);
			case OUTPUT:
			case OUTPUT_DIRECTORY:
				return getCode_output(parameter);
			default:
				assert false; return "";
		}
	}
	/**
	 * Generate the code corresponding to a parameter w/ type: "boolean"
	 * @param param
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            formatter that is used to store the corresponding code
	 */
	protected String getCode_boolean(Parameter param)
	{
		assert param.getDescription().getType().equals(ParameterType.BOOLEAN);
		String vdef = param.getDescription().getVdef();
		if ( vdef.trim().equals("")
				|| (vdef.equals("0") && param.getData().equals("1"))
				|| (vdef.equals("1") && param.getData().equals("0")))
			return String.format(param.getDescription().getValueTemplate(),
			                     param.getData().equals("1")?"true":"false");
		return "";
	}
	
	/**
	 * Generate the code corresponding to a parameter w/ type: "command" or "code"
	 * @param paramDesc
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            the formatter that is used to store the corresponding code, or the empty string if
	 *            <code>param</code>'s type is not {@link ParameterType#COMMAND} or {@link ParameterType#CODE}.
	 */
	protected String getCode_commandOrCode(ParameterDescription paramDesc)
	{
		assert paramDesc.getType().equals(ParameterType.COMMAND) || paramDesc.getType().equals(ParameterType.CODE);
		return String.format(paramDesc.getValueTemplate());
	}
	
	/**
	 * Generate the code corresponding to an input parameter.
	 * @param param
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            the formatter that is used to store the corresponding code, or the empty string if
	 *            <code>param</code>'s type is not an {@link ParameterType#isInput() input}.
	 */
	protected String getCode_input(Parameter param)
	{
		assert param.isInput();
		if (param.getInput() != null)
			return String.format(param.getDescription().getValueTemplate(), param.getInput().filepath());
		// the original param.getData() (equals to Parameter.output_xml_id()) is replaced by the output file name
		// in identifyInputFiles()
		return String.format(param.getDescription().getValueTemplate(), param.getData());
	}
	
	/**
	 * Generate the code corresponding to an output parameter.
	 * @param param
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            the formatter that is used to store the corresponding code, or the empty string if
	 *            <code>param</code>'s type is not an {@link ParameterType#isOutput() output}.
	 */
	protected String getCode_output(Parameter param)
	{
		assert param.isOutput();
		return String.format(param.getDescription().getValueTemplate(), param.getDescription().getVdef());
	}
	
	/**
	 * Generate the code corresponding to a parameter w/ type: "int", "string" or "float"
	 * @param param
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            the formatter that is used to store the corresponding code, or the empty string if
	 *            <code>param</code>'s type is not {@link ParameterType#INT}, {@link ParameterType#STRING} or
	 *            {@link ParameterType#FLOAT}.
	 */
	protected String getCode_IntFloatString(Parameter param)
	{
		assert ((param.getDescription().getType().equals(ParameterType.INT))
		        || (param.getDescription().getType().equals(ParameterType.STRING))
		        || (param.getDescription().getType().equals(ParameterType.TEXT))
		        || (param.getDescription().getType().equals(ParameterType.FLOAT)));
		return String.format(param.getDescription().getValueTemplate(), param.getData(), param.getData());
	}
	
	/**
	 * Generate the code corresponding to a parameter w/ type: {@link ParameterType#ENUM enumeration}.
	 * @param param
	 *            the parameter for which code should be generated
	 * @param formatter
	 *            the formatter that is used to store the corresponding code, or the empty string if
	 *            <code>param</code> is not an enumeration.
	 */
	protected String getCode_enumeration(Parameter param)
	{
		assert param.getDescription().getType().equals(ParameterType.ENUM);
		return String.format(param.getItem().getValueTemplate(),param.getItem().getDescription());
	}
	
	
}
