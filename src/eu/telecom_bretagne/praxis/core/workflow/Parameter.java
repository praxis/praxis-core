/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.workflow;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.resource.IOType;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.Item;
import eu.telecom_bretagne.praxis.core.resource.ParameterDescription.ParameterType;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;


/**
 * Parameter: a parameter, within a Program, within a given workflow
 * @author Sébastien Bigaret
 *
 */
public class Parameter implements Cloneable, PropertyChangeListener, java.io.Serializable
{

	private static final long serialVersionUID = -7236633502138745978L;
	public static final String INPUT_PROPERTY = "input";
	public static final String OUTPUT_PROPERTY = "outputs";
	public static final String PRG_INPUT_PROPERTY = "prgInput";

	public boolean isInput() {
		return getDescription().getType().isInput();
	}

	public boolean isOutput() {
		return getDescription().getType().isOutput();
	}

	public List<IOType> types() {
		return getDescription().getTypes();
	}



	protected static final String PARAM_ID   = "idref";
	public    static final String INFO       = "info";
	public    static final String INFO_NAME  = "name";
	public    static final String INFO_VALUE = "value";

	/** Id pointing to the corresponding ParameterDescription */
	protected String ref_id;

	/** The enclosing Program */
	protected Program program;

	/** Value for the parameter, only for {@link ParameterType#INT},
	 * {@link ParameterType#FLOAT}, {@link ParameterType#STRING}, {@link ParameterType#TEXT},
	 * and {@link ParameterType#BOOLEAN} parameters */
	String data = null;

	/** Used only by {@link ParameterType#ENUM} parameters */
	Item item;

	/** Used only by {@link ParameterType#isInput() input} parameters
	 * @see #io
	 */
	WorkflowInput input;

	/** Used only by {@link ParameterType#isInput() input} and 
	 * {@link ParameterType#isOutput() output} parameters.
	 * 
	 * Input parameters are bound either to an output parameter (in that case,
	 * this attribute is set), or to a file/db in the workflow (and in this
	 * case, the attribute {@link #input} is set and this attribute is
	 * <tt>null</tt>).
	 * 
	 * Output parameters set: {@link #data}, with the file name, and
	 * possibly {@link #io} when linked to an
	 * {@link ParameterType#isInput() input parameter}
	 * 
	 * @see #input
	 */
	protected Parameter prg_input; // single for input
	protected List <Parameter> outputs = new ArrayList<Parameter>(); // multiple outputs possible

	/**
	 * PropertyChangeSupport à la beans
	 */
	protected PropertyChangeSupport propChgSupport = new PropertyChangeSupport(this);

	/**
	 * @throws InvalidXMLException
	 *             if the object can not be built from the informations included in the supplied XML node
	 * @throws NullPointerException
	 *             if parameter parent_prg is {@code null}.
	 */
	public Parameter(Element xml, Program parent_prg, XMLWarnings warnings)
	throws InvalidXMLException
	{
		if ( parent_prg == null )
			throw new NullPointerException("Parameter parent_prg cannot be null");

		InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.PARAMETER);

		ref_id = xml.getAttributeValue(PARAM_ID);
		if (ref_id == null)
			throw xml_exc;
		xml_exc.setName(ref_id);
		program = parent_prg;
		Element info = xml.getChild(INFO);

		if ( getDescription() == null )
		{
			if (info!=null) /* if null, the parameter was not activated, so it is not use to report it */
				warnings.add(Workflow.XMLWarnings.PRG_DECLARES_INVALID_PARAM,
				             new String[] { parent_prg.getDescription().fullName(),
				                            info.getAttributeValue(INFO_NAME),
				                            info.getAttributeValue(INFO_VALUE), });
			throw xml_exc.setMsg("Unable to find a valid description for parameter").setIsFatal(false);
		}

		switch (getDescription().getType()) {
			case FLOAT:
			case INT:
			case STRING:
			case TEXT:
				data = xml.getChildText("data");
				// TODO: check FLOAT, INT, peut-être pour tout le monde d'ailleurs (boolean, etc.)
				break;
			case BOOLEAN:
				data = xml.getChildText("data");
				/* the rest of the framework expects "0" or "1" in this.data
				   Until this changes, let's accept true/false as well */
				if ( "true".equals(data) )
					data = "1";
				else if ( "false".equals(data))
					data = "0";

				if ( !data.equals("0") && !data.equals("1") )
					throw xml_exc.setMsg("Invalid value for boolean parameter: "+data);
				break;
			case ENUM:
				item = getDescription().getItemWithID(xml.getChildText("data"));
				if ( item == null )
				{
					if (info!=null) /* if null, the param. was not activated, no need to report */
						warnings.add(Workflow.XMLWarnings.PRG_PARAM_HAS_INVALID_VALUE,
						             new String[] { parent_prg.getDescription().getPrgName(),
						                            info.getAttributeValue(INFO_NAME),
						                            info.getAttributeValue(INFO_VALUE),
						                            getDescription().defaultItem().getDescription()});
					throw xml_exc.setMsg("default selection not found in vlist").setIsFatal(false);
				}
				break;
			case OUTPUT:
			case OUTPUT_DIRECTORY:
				data = xml.getChildText("data");
				break;
			case INPUT:
			case INPUT_DIRECTORY:
				String input_id = xml.getChild("data").getAttributeValue("input_id");
				if ( input_id != null ) {
					setInput(parent_prg.workflow.getInputWithId(input_id));
					if ( input == null )
						throw xml_exc.setMsg("Invalid input_id: "+input_id+": not registered within the workflow");
				}
				else
					data = xml.getChildText("data");
				break;
			case CODE:
			case COMMAND:
				break;
		}
	}

	/**
	 * Builds a new parameter.
	 * 
	 * @param description
	 *            the corresponding description
	 * @param parent_prg
	 *            the program this parameter belongs to
	 * @throws NullPointerException
	 *             if parameter parent_prg is {@code null}.
	 */
	public Parameter(ParameterDescription description, Program parent_prg)
	{
		if ( parent_prg == null )
			throw new NullPointerException("Parameter parent_prg cannot be null");
		program = parent_prg;
		init(description);
	}

	protected void init(ParameterDescription description)
	{
		ref_id = description.getId();
		switch (description.getType()) {
			case COMMAND: // eclipse bug: import xxx.* needed for enum as case label, cf.https://bugs.eclipse.org/bugs/show_bug.cgi?id=77151
				throw new IllegalArgumentException("ParameterDescription cant be a COMMAND");
			case FLOAT:
			case INT:
				data = description.getVdef();
				break;
			case BOOLEAN:
				data = description.getVdef(); /* "0" or "1" */
				break;
			case STRING:
			case TEXT:
				data = description.getVdef();
				break;
			case ENUM:
				item = description.defaultItem();
				break;
			case INPUT:
			case INPUT_DIRECTORY:
				break;
			case OUTPUT:
			case OUTPUT_DIRECTORY:
				break;
			case CODE:
				break;
			default:
				eu.telecom_bretagne.praxis.common.Utile.unimplemented();
		}
	}

	/** */
	public Element toXML()
	{
		Element param_xml = new Element("parameter");

		param_xml.setAttribute(PARAM_ID, ref_id);

		Element data_xml = new Element("data");

		ParameterDescription param_desc = getDescription();
		String param_desc_displayName=param_desc.getDisplayName();
		if (param_desc_displayName==null)
			param_desc_displayName="";

		if ( isInput() && input != null)
			data_xml.setAttribute("input_id", ""+program.workflow.getIdForInput(input));
		else if ( isInput() )
			data_xml.setText( prg_input != null ? prg_input.output_xml_id() : "" );
		else if ( isOutput() )
			data_xml.setText(output_xml_id());
		else if ( param_desc.getType().equals(ParameterType.ENUM) )
		{
			data_xml.setText(item.getId());
			if (isActivated())
			{
				Element info_xml = new Element(INFO);
				info_xml.setAttribute(INFO_NAME, param_desc_displayName);
				info_xml.setAttribute(INFO_VALUE, item.getDescription()!=null?item.getDescription():"");//setAttribute raises on null value
				param_xml.addContent(info_xml);
			}
		}
		else
		{
			data_xml.setText(data);
			if (isActivated())
			{
				Element info_xml = new Element(INFO);
				info_xml.setAttribute(INFO_NAME, param_desc_displayName);
				info_xml.setAttribute(INFO_VALUE, data!=null?data:"");
				param_xml.addContent(info_xml);
			}
		}
		param_xml.addContent(data_xml);
		return param_xml;
	}

	/**
	 * Returns the 'id' used in the xml representation of an output parameter:
	 * 
	 * @return a string made w/ the concatenation of the program's id, the character point <tt>"."</tt> and the
	 *         receiver's ref_id; it is used in {@link #toXML()}
	 * @throws IllegalStateException
	 *             if the receiver is not an {@link #isOutput() output parameter}.
	 */ 
	public String output_xml_id() { // TODO make it visibility-package again (same for workflow.getIdForProgram())
		if ( ! isOutput() )
			throw new IllegalStateException("receiver is not an output");
		return program.workflow.getIdForProgram(program)+"."+ref_id;
	}

	/** Returns the corresponding ParameterDescription
	 * 
	 * @return the corresponding ParameterDescription
	 */
	public ParameterDescription getDescription()
	{
		return  program.getDescription().getParameterWithId(this.ref_id);
	}

	/**
	 * Evaluates the dependencies of the receiver and, given the values of
	 * the other parameters in the {@link #getProgram() receiver's program},
	 * tells whether the parameter is activated.
	 * @return true if all the parameter's dependencies are fulfilled.
	 * @throws UnsupportedOperationException in case the code failed.
	 * @see ParameterDescription#getDependence()
	 */
	public boolean isActivated() throws UnsupportedOperationException {
		try {
			return getDescription().isActivated(this);
		}
		catch (Exception e) {
			String prg_param = getProgram().getDescription().id()+":"+getDescription().getId();
			Log.log.log(Level.SEVERE, 
			            "Serious: isActivated failed for prg:param "+prg_param, e);
			throw new UnsupportedOperationException("isActivated() raised on "+prg_param, e);
		}
	}

	/**
	 * Tells whether the value hold by this parameter is valid with respect to the parameter's description. Depending on
	 * the context, callers may be also interested in knowing whether the parameter is {@link #isActivated() activated}
	 * (this method does <b>not</b> check that).
	 * 
	 * @return {@code true} if the value is valid wrt. the parameter's description, {@code false} otherwise.
	 * @see ParameterDescription#isaValidValue(String)
	 */
	public boolean hasaValidValue()
	{
		return getDescription().isaValidValue(this.getData());
	}

	/* Getters */
	public String getData() {
		return data;
	}

	public void setData(String data) {
		propChgSupport.firePropertyChange("data", this.data, this.data = data);
	}

	/** Returns the selected item for a {@link ParameterType#ENUM} parameter.
	 * @return one of the receiver's {@link ParameterDescription#getItems() possible items}.
	 */ 
	public Item getItem() {
		return item;
	}

	/**
	 * Sets the selected item
	 * 
	 * @throws IllegalArgumentException
	 *             if the item is not compatible with the {@link ParameterDescription#hasItem(Item) parameter's
	 *             description}, or if the receiver is not a {@link ParameterType#ENUM} parameter
	 */
	public void setItem(Item item) {
		if ( getDescription().hasItem(item) )
			propChgSupport.firePropertyChange("item", this.item, this.item = item);
		else
			throw new IllegalArgumentException("parameter item is not valid");
	}

	/**
	 * @return Returns the program.
	 */
	public Program getProgram() {
		return program;
	}

	/** Tells whether an input parameter and an output parameter are compatible
	 * @see ParameterDescription#isCompatibleWith(ParameterDescription) the
	 * corresponding method in ParameterDescription
	 */
	public boolean isCompatibleWith(Parameter parameter) {
		return getDescription().isCompatibleWith(parameter.getDescription());
	}

	public WorkflowInput getInput() {
		return input;
	}

	public void setInput(WorkflowInput input) {
		if (this.input!=null)
		{
			this.input.removeOutput(this);
		}
		if (input!=null)
			input.addOutput(this);
		propChgSupport.firePropertyChange(INPUT_PROPERTY, this.input, this.input = input);
	}


	/**
	 * Returns the input parameter connected to this (output) parameter.
	 * @return the connected parameter, or null if it is not connected.
	 */
	public Parameter getPrgInput() {
		return prg_input;
	}

	/**
	 * Returns the output parameters connected to this (input) parameter.
	 * @return An array of the connected parameters.
	 */
	public Parameter[] getOutputs() {
		return outputs.toArray(new Parameter[outputs.size()]);
	}
	/*
	 * Connects an input parameter with an output parameter.
	 * When this method connects the receiver to the other end, it also takes
	 * care of:
	 * <ul>
	 * <li> disconnecting the other side of the receiver's former link, if any
	 * <li> disconnecting the other side of the link that the parameter
	 * <tt>io</tt> may already be involved in, if appropriate
	 * </ul>
	 * @param io the parameter with which the receiver. If <tt>null</tt> the
	 * receiver is disconnected.
	 * @throws IllegalStateException if the receiver is neither an
	 * {@link #isInput() input parameter} nor an
	 * {@link #isOutput() output parameter}.
	 * @throws IllegalArgumentException when trying to link two input (or two output) pamareters.
	 */
	/*
    public void connectTo(Parameter io) throws IllegalArgumentException, IllegalStateException
    {
        if ( !isInput() && !isOutput() )
            throw new IllegalStateException("Parameter is neither an input nor an output");

        if ( io==null ) { // disconnection
            this.prg_input = null;
            return;
        }

        if (  ( isInput() && !io.isOutput() )
                || (isOutput() && !io.isInput() ) )
            throw new IllegalArgumentException("trying to attach to inputs (or 2 outputs)");

        if ( this.io!=null && this.io.getIO()!=null )
            this.io.getIO().connectTo(null);
        
        this.io = io;
        if ( io.getIO() != null )
            io.getIO().connectTo(null);  
        io.io = this; 
    }
	 */
	/**
	 * Sets the destination parameter for this input parameter. Additionally, the method does the following:
	 * <ul>
	 * <li>if <code>this</code> parameter is already connected to an other parameter, the latter is updated so that
	 * <code>this</this> is removed from its registered outputs;</li>
	 * <li><code>this</code> is added to the (non-null) <code>output_parameter</code>'s registered outputs.</li>
	 * </ul>
	 * <b>Note for property change listeners</b>: the implementation guarantees that notifications are sent after
	 * every changes has been done. In other words, when a notification is sent about a parameter's connection, the
	 * parameter at the other side of the connection has already been updated.
	 * @param output_parameter
	 *            the output parameter to connect to this. A null value simply disconnects this parameter.
	 * @throws IllegalArgumentException
	 *             if <code>this</code> is not an input parameter, or if the supplied parameter is not an output
	 *             parameter
	 * @see #addOutput(Parameter)
	 * @see #removeOutput(Parameter)
	 */
	public void setPrgInput(Parameter output_parameter) {
		if ( !isInput() )
			throw new IllegalArgumentException("setPrgInput() only for input parameters");
		if ( output_parameter!=null && !output_parameter.isOutput() )
			throw new IllegalArgumentException("setPrgInput() param must be an output parameter");

		Parameter old_input = prg_input;
		/* disconnect any existing link */
		if ( prg_input != null )
		{
			prg_input.removeOutput_internal(this);
		}
		prg_input = output_parameter;
		if (output_parameter != null)
		{
			output_parameter.addOutput_internal(this);
		}
		propChgSupport.firePropertyChange(PRG_INPUT_PROPERTY, old_input, this.input);
	}

	/**
	 * Adds the supplied parameter from the list of this parameter's outputs. This method also takes care of calling
	 * {@link #setPrgInput(Parameter)} with <code>this</code> as its parameter.<br>
	 * <b>Note for property change listeners</b>: the implementation guarantees that notifications are sent after
	 * every changes has been done. In other words, when a notification is sent about a parameter's connection, the
	 * parameter at the other side of the connection has already been updated.
	 * @param input_parameter
	 *            the parameter to remove. It cannot be null.
	 * @throws IllegalArgumentException
	 *             if <code>this</code> is not an output parameter or if the supplied input parameter is not an input
	 *             parameter
	 * @throws NullPointerException
	 *             if input_parameter is null
	 * @see #setPrgInput(Parameter)
	 * @see #removeOutput(Parameter)
	 */
	public void addOutput(Parameter input_parameter)
	{
		if ( !isOutput() )
			throw new IllegalArgumentException("addOutput() only for output parameters");
		if ( !input_parameter.isInput() ) // NullPointerExc. if null
			throw new IllegalArgumentException("addOutput() param must be an input parameter");

		input_parameter.setPrgInput(this);
	}

	/**
	 * Internally used to simply add the supplied input parameter to the outputs of this object; no extra operation is
	 * done, except that property change listeners are notified.<br>
	 * Adding an input parameter that is already registered in the outputs of this object is silently ignored; in this
	 * case, no notification is sent to the property change listeners. <br>
	 * Neither the receiver nor the parameter are checked; {@link #addOutput(Parameter)} does that.
	 * @param input_parameter
	 *            the parameter to add to the receiver's output.
	 */
	protected void addOutput_internal(Parameter input_parameter)
	{
		if ( !outputs.contains(input_parameter) )
		{
			outputs.add(input_parameter);
			propChgSupport.firePropertyChange(OUTPUT_PROPERTY, null, getOutputs());
		}
	}

	/**
	 * Removes the supplied parameter from the list of this parameter's outputs. This method also takes care of
	 * resetting the input parameter by calling {@link #setPrgInput(Parameter)} with a null value.<br>
	 * <b>Note for property change listeners</b>: the implementation guarantees that notifications are sent after
	 * every changes has been done. In other words, when a notification is sent about a parameter's connection, the
	 * parameter at the other side of the connection has already been updated.
	 * @param input_parameter
	 *            the parameter to remove. It cannot be null.
	 * @throws IllegalArgumentException
	 *             if this is not an output or if the supplied input parameter is not an input parameter
	 * @throws NullPointerException
	 *             if input_parameter is null
	 * @see #setPrgInput(Parameter)
	 * @see #addOutput(Parameter)
	 */
	public void removeOutput(Parameter input_parameter)
	{
		if ( !isOutput() )
			throw new IllegalArgumentException("addOutput() only for output parameters");
		if ( !input_parameter.isInput() ) // NullPointerExc. if null
			throw new IllegalArgumentException("removeOutput() param must be an input parameter");
		
		input_parameter.setPrgInput(null);
	}

	/**
	 * Internally used to simply remove the supplied input parameter from this' outputs; no extra operation is done,
	 * except that property change listeners are notified.<br>
	 * Removing an input parameter that is not registered in the outputs of this object is silently ignored; in this
	 * case, no notification is sent to the property change listeners. <br>
	 * Neither the receiver nor the parameter are checked; {@link #removeOutput(Parameter)} does that.
	 * @param input_parameter
	 *            the input parameter to remove.
	 */
	protected void removeOutput_internal(Parameter input_parameter)
	{
		if ( outputs.remove(input_parameter) )
			propChgSupport.firePropertyChange(OUTPUT_PROPERTY, null, getOutputs());
	}

	/**
	 * Sets this program's {@link #getInput() "workflow input"} to <code>null</code>, and removes any
	 * {@link #getPrgInput() input connections} and {@link #getOutputs() output connections} this parameter may model.<br>
	 * Apart from these elements (the so-called links) that are reset, the parameter is left as-is and keeps its other
	 * settings ( {@link #getData()}, {@link #getItem()}).
	 * @see Program#invalidate()
	 */
	public void invalidate() {
		// do NOT reset either data or item
		setInput(null);
		if ( prg_input != null )
			prg_input.removeOutput(this);
		List <Parameter>outputs_copy = new ArrayList<Parameter>(outputs);
		for (Parameter p: outputs_copy) // we need a copy here: p.setPrgInput() modifies this.outputs
			p.setPrgInput(null);
	}

	/**
	 * TODO: add big fat warning, do not use, for GUI only
	 * 
	 * @param prg
	 *            the program to which the shallow copy should be attached. It must be non-null.
	 * @throws NullPointerException
	 *             if parameter prg is {@code null}.
	 */
	public Parameter shallowCopyFor(Program prg) {
		if ( prg == null )
			throw new NullPointerException("parameter prg cannot be null");
		Parameter p = null;
		try {
			p = (Parameter) this.clone();
		} catch (CloneNotSupportedException e) { /* ignored, won't happen */ }
		p.program = prg;
		return p;
	}

	public String toString()
	{
		return "[Parameter ref_id:"+ref_id+"]";
	}

	/* PropertyChangeSupport listeners & */

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propChgSupport.addPropertyChangeListener(listener);
	}

	public PropertyChangeSupport getListeners() { 
		return propChgSupport;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propChgSupport.removePropertyChangeListener(listener);
	}
	@Override
	public void propertyChange(PropertyChangeEvent evt)
	{
		evt.getPropertyName();
	}

	private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		if (input != null)
			input.addOutput(this);
	}
}
