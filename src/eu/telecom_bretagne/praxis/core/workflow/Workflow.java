/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.workflow;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.xpath.XPath;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.I18N;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.XMLConstants;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowInput.INPUT_TYPE;


/**
 * @author Sébastien Bigaret
 *
 */
public class Workflow implements java.lang.Cloneable, java.io.Serializable
{
	private static final long serialVersionUID = -5907539065298108120L;

	public static class XMLWarnings
	{
		/**
		 * A simple class storing a key and its arguments; there are used to store internationalized messages and
		 * their arguments.
		 * @author Sébastien Bigaret
		 */
		private static class I18NCouple
		{
			/** The I18N key */
			String i18nKey;
			/** The arguments used in the internationalized string pointed to by the I18N key */
			String[] args;
			
			/**
			 * Builds a new I18NCouple with the supplied key and arguments
			 * @param i18nKey the key to register.
			 * @param args
			 *            the arguments for building the internationalized string pointed to by the key. It may be
			 *            null or empty.
			 */
			I18NCouple(String i18nKey, String[] args)
			{
				this.i18nKey = i18nKey;
				this.args = args;
			}
			/**
			 * Returns the internationalized string pointed to by the i18nKey, using #args as its arguments.
			 * @see eu.telecom_bretagne.praxis.common.I18N#s(String key, Object args)
			 */
			@Override
			public String toString()
			{
				return I18N.s(i18nKey, args);
			}
		}
		/** Internationalised string's parameters: the missing description's id */
		public static final String PRG_DESC_DOES_NOT_EXIST_1="wf.xmlwarnings.prg_desc_does_not_exists";
		/** Internationalised string's parameters: the substituted description's id, and the substitution description's id */
		public static final String PRG_DESC_REPLACED_BY_ALTERNATIVE="wf.xmlwarnings.prg_desc_replace_by_alternative";
		/** Three params: prg's name, param's name, param's value */
		public static final String PRG_DECLARES_INVALID_PARAM="wf.xmlwarnings.prg_declares_invalid_param";
		/** Params: prg's name, param's name, param's value, param's default value */
		public static final String PRG_PARAM_HAS_INVALID_VALUE="wf.xmlwarnings.prg_param_has_invalid_value";
		/** Params: prg's id in the workflow, the source prg's id and the source input parameter's name */
		public static final String PRG_HAS_INVALID_LINK="wf.xmlwarnings.prg_has_invalid_link";
		/** Params: output-prg's id, output-param's name, input-prg's id, input-param's name */
		public static final String PRG_LINK_IS_INACTIVE="wf.xmlwarnings.prg_link_is_inactive";
		/** Params: prg's id, param.desxcription's id and TYPE */
		public static final String PRG_CONTAINS_USELESS_PARAMETER="wf.xmlwarnings.prg_contains_useless_parameter";

		private ArrayList<I18NCouple> warnings = new ArrayList<I18NCouple>();
		/**
		 * Adds a new warning.
		 * @param warning a string describing the warning, with placeholders for the params
		 * @param params the parameters to insert into the warning's placeholders
		 */
		public void add(String warning, String[] params)
		{
			warnings.add(new I18NCouple(warning, params));
		}
		public boolean isEmpty()
		{
			return warnings.isEmpty();
		}
		public String htmlDetails()
		{
			if (warnings.isEmpty())
				return "";
			StringBuilder buf = new StringBuilder();
			buf.append("<html><ul>\n");
			for (I18NCouple c: warnings)
				buf.append("<li>").append(c).append("</li>\n");
			buf.append("</ul></html>");
			return buf.toString();
		}
		public String details()
		{
			if (warnings.isEmpty())
				return "";
			StringBuilder buf = new StringBuilder();
			for (I18NCouple c: warnings)
				buf.append(c).append("\n");
			return buf.toString();
		}
	}
    protected static final String WORKFLOW_ROOT_NODE = "workflow";

	/**
	 * 
	 */
	public static final String WORKFLOW_NAME_IN_ZIP = "zipped_workflow";
	
    /** The pattern object used by {@link #isaValidName(String)} */
    public static final Pattern validNamePattern = Pattern.compile("[-a-zA-Z0-9._ ]+");

    /**
     * Checks whether this is a valid name for a workspace. A valid name is
     * exclusively made up of alpha-numeric characters plus: <tt>.</tt> (point)
     * and <tt>_</tt> (underscore).
     * @param name the name to check
     * @return <code>true</code> if it is valid, <code>false</code> otherwise.
     */
    public static boolean isaValidName(String name)
    {
        return validNamePattern.matcher(name).matches();
    }
    
	/**
	 * Stores the workflow inputs in a map whose keys are unique ids in the workflow. When automatically added by the
	 * framework, ids are simply integers; however, when manipulating workflows outside the praxis framework it is
	 * handy to be able to use strings instead of integers only (e.g. in order to make ids more explicit, or refering
	 * to an other external source, etc.)
	 */
    protected LinkedHashMap<String, WorkflowInput> inputs;
    
    protected String user; /* TODO: remove?? */

    protected WorkflowID id;

    /** An annotation attached to the workflow, supplied by the designer */
    protected String annotation;
    
    /** A {@link LinkedHashMap} guarantees that we will always iterate
     * on programs the same way, i.e. in the order of insertion.  This makes
     * it possible to keep the same relative order in which the program are
     * inserted when imported or pasted: in particular, pasting 'seqboot-1' and
     * 'seqboot-2', for example, will always result in two programs 'seqboot-x'
     * and 'seqboot-y', respectively, with x<y. */
    protected LinkedHashMap<String,Program> programs; /* maps prg.id to prg */

    protected boolean executed = false;

	/** 
	 * The PropertyChangeSupport that is used to notify listeners that the object has changed
	 */
    protected PropertyChangeSupport propChgSupport = new PropertyChangeSupport(this);

	public transient File currentDirectory;
	
	/**
	 * Checks that the document is confirm to a DTD acceptable for building a workflow
	 * @param xml
	 *            the XML document to check
	 * @return the DTD's public ID
	 * @throws InvalidXMLException
	 *             if the document has no DTD, or if it not valid for a workflow.
	 */
	public static String checkDTD(Document xml) throws InvalidXMLException
	{
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW);
        
        if (xml.getDocType()==null)
    		throw xml_exc.setMsg("Unable to find a doc.type");
        String wf_publicID = xml.getDocType().getPublicID();
        if ( ! (   XMLConstants.WORKFLOW_DTD_PUBID.equals(wf_publicID)
        		|| XMLConstants.WORKFLOW_DTD_PUBID_v2.equals(wf_publicID)))
        	throw xml_exc;

        if ( ! (   XMLConstants.WORKFLOW_DTD_PUBID.equals(wf_publicID)
        		|| XMLConstants.WORKFLOW_DTD_PUBID_v2.equals(wf_publicID) ) )
        	throw xml_exc.setMsg("DTD: Unknown docType: "+("".equals(wf_publicID)?"<empty>":wf_publicID));
        return wf_publicID;
	}
	
	/**
	 * Reads the XML file containing a workflow, and returns the XML document. This methods checks not only that the
	 * document conforms to the DTD it declares, but also that this DTD is a valid DTD for representing a workflow.
	 * The workflow itself is <b>not</b> built, so there is no absolute guarantee that the workflow can be
	 * successfully built until one of its constructors is called. Nevertheless, it is absolutely sufficient to
	 * determine whether a file contains a potential workflow.
	 * @param workflowFile
	 *            the file to check
	 * @throws InvalidXMLException
	 *             if the file is not a potential workflow. This can happen for various reasons, such as: the file
	 *             does not exist, it is not readable, it is not an XML file, or the xml does not conform to the
	 *             expected DTD.
	 */
	public static Document readWorkflowFile(File workflowFile) throws InvalidXMLException
	{
    	Document xml_doc = Facade_xml.read(workflowFile, true);
    	checkDTD(xml_doc); // result is not important, but it should be valid
        return xml_doc;
	}
	
    public Workflow(WorkflowID workflowID)
    {
        this.id = workflowID;
        programs = new LinkedHashMap<String, Program>();
        inputs = new LinkedHashMap<String, WorkflowInput>();
    }

	public Workflow(String name)
    {
        this.id = new WorkflowID();
        this.id.setName(name);
        programs = new LinkedHashMap<String, Program>();
        inputs = new LinkedHashMap<String, WorkflowInput>();
    }
    
    public Workflow(File xmlFile, XMLWarnings warnings) throws InvalidXMLException
    {
    	this(readWorkflowFile(xmlFile), warnings);
    	currentDirectory = xmlFile.getParentFile();
    }
    
    public Workflow(File xmlFile, XMLWarnings warnings, boolean executed) throws InvalidXMLException
    {
    	this(readWorkflowFile(xmlFile), warnings, executed);
    	currentDirectory = xmlFile.getParentFile();
    }

    public Workflow(InputStream xml_stream, XMLWarnings warnings) throws InvalidXMLException
    {
        this(Facade_xml.read(xml_stream, true), warnings);
    }
    
    public Workflow(InputStream xml_stream, XMLWarnings warnings, boolean executed) throws InvalidXMLException
    {
        this(Facade_xml.read(xml_stream, true), warnings, executed);
    }

    protected Workflow(Document xml, XMLWarnings warnings) throws InvalidXMLException
    {
        fromXML(xml, warnings);
    }

    protected Workflow(Document xml, XMLWarnings warnings, boolean executed) throws InvalidXMLException
    {
    	this.executed = executed;
    	fromXML(xml, warnings);
    }
    
    public WorkflowID id()
    {
        return id;
    }
    
    public WorkflowInput addNewInput(int x, int y, String name, INPUT_TYPE type) {
        WorkflowInput input = new WorkflowInput(type, name, x, y);
        inputs.put(getNextInputID(), input);
        propChgSupport.firePropertyChange("inputs", null, getInputs());
        return input;
    }

    public WorkflowInput addNewInput(String input_id, WorkflowInput workflowInput) {
        if ( inputs.containsKey(input_id) )
            throw new IllegalStateException("Duplicate input id");
        inputs.put(input_id, workflowInput);
        return workflowInput;
    }

    public void deleteInput(WorkflowInput wf_input) {
        if ( !inputs.containsValue(wf_input) )
            throw new RuntimeException("error"); //TODO

        /* Reset any program's input parameter referencing this input */
        for (Program prg: programs.values())
            for (Parameter p: prg.getInputParameters())
                if ( p.input == wf_input )
                    p.input = null;
        
        wf_input.invalidate();
        inputs.remove(getIdForInput(wf_input));
        propChgSupport.firePropertyChange("inputs", null, getInputs());
    }
    
    /** Returns the program corresponding to the supplied id
     * @param prg_id a program id
     * @return {@code null} if no such program exists
     */
    public Program getProgramWithId(String prg_id) {
        return programs.get(prg_id);
    }
    
    protected Parameter getParameterWithXMLID(String xml_id) {
    	int idx = xml_id.lastIndexOf(".");
        if ( idx == -1 )
            return null;
        Program prg = getProgramWithId(xml_id.substring(0, idx));
        if ( prg == null )
            return null;
        return prg.getParameterWithID(xml_id.substring(idx+1));
    }
    
    /** Returns the input (file or database) corresponding to the supplied id.
     * Note that ids are internal stuff, only needed when storing and restoring
     * from xml files.
     * @param input_id an input id
     * @return {@code null} if no such input exists
     */
    public WorkflowInput getInputWithId(String input_id) { // TODO back to: package-visibility, not public
        return inputs.get(input_id);
    }
    
    /** returns the id associated to that input. This is only needed when 
     * saving a workflow into an xml file: when saving an input parameter
     * referencing a WorkflowInput, the corresponding id is needed.
     * Note that ids are internal stuff, only needed when storing and restoring
     * from xml files.
     * @param input the {@link WorkflowInput}
     * @return the corresponding id
     * @throws IllegalArgumentException if no such WorkflowInput object is referenced within this Workflow.
     */
    public String getIdForInput(WorkflowInput input) // TODO back to: package-visibility, not public
    throws IllegalArgumentException
    {
        for (String input_id: inputs.keySet())
            if ( inputs.get(input_id)==input )
                return input_id;
        throw new IllegalArgumentException("Could not find the request input"); // TODO CHECK why not null???
    }

    /** Returns the id associated to that program. This is only needed when 
     * saving a workflow into an xml file: when saving an input parameter
     * or an output parameter linking a program to another,
     * the corresponding id is needed so that the input/output id is built.
     * Note that ids are internal stuff, only needed when storing and restoring
     * from xml files.
     * @param program the {@link Program} for which the id is needed
     * @return the corresponding id
     * @throws IllegalArgumentException if no such Program object is referenced within this Workflow.
     */
    public String getIdForProgram(Program program)
    throws IllegalArgumentException
    {
        for (String prg_id: programs.keySet())
            if ( programs.get(prg_id)==program )
                return prg_id;
        throw new IllegalArgumentException("Could not find the requested program"); // TODO CHECK why not null???
    }

    /** Returns an available id for a new program.
     * Note that ids are internal stuff, only needed when storing and restoring
     * from xml files.
     */
    String getNextProgramID(String prg_desc_id) {
        
        String nextID = prg_desc_id+"-1";
        long idx = 1;
        while (programs.containsKey(nextID)) {
            idx++;
            nextID = prg_desc_id + "-" + idx;
        }
        return nextID;
    }

    /**
     * 
     * Note that ids are internal stuff, only needed when storing and restoring
     * from xml files.
     */
    String getNextInputID() {
        Long $nextID = new Long(0);
        while (inputs.containsKey((++$nextID).toString()));
        return $nextID.toString();
    }
    
    public Program createAndAddNewProgram(ProgramDescription prg_desc) {
        final String prg_id = getNextProgramID(prg_desc.getPrgName());
        Program prg = new Program(prg_desc, this);
        programs.put(prg_id, prg);
        propChgSupport.firePropertyChange("programs", null, getPrograms());
        return prg;
    }
     
    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    public Workflow clone() {
        Workflow clone;
        try {
            clone = new Workflow(toXMLDocument(true), new XMLWarnings());
        } catch (InvalidXMLException e) {
            // Sounds impossible. If this happens, there's something wrong in the implementation itself!
            Log.log.log(Level.SEVERE, "Ooops, unable to clone() ?!!", e);
            return null;
        }
        // when cloned inside the GUI, we should take care of the wf input's copiedFromResults status
        // (see comments for this field for details)
        for (WorkflowInput input: this.getInputs())
            clone.getInputWithId(this.getIdForInput(input)).copiedFromResults = input.copiedFromResults;
        return clone;
    }
	
	/**
	 * Adds a program with the supplied id. This is <b>not</b> the way a program is normally added to a workflow,
	 * callers should consider using the recommended method {@link #createAndAddNewProgram(ProgramDescription)}. <br>
	 * However, this method comes in handy in situations where it is important to ensure that a previously removed
	 * program can be reinserted with the very same id. Please remember however that this will not restore the workflow
	 * in the same exact state: since removing a program {@link Program#invalidate() invalidates it}, all of its
	 * connections have been reset, if any.
	 * @param programID the ID to assign to the program in this workflow
	 * @param program the program to add to this workflow
	 * @throws IllegalArgumentException if the supplied programID is already assigned to a program in this workflow
	 */
    public void addProgram(String programID, Program program)
    {
    	if (getProgramWithId(programID)!=null)
    		throw new IllegalArgumentException("Program ID: "+programID+" is already assigned within this workflow");
    	program.setWorkflow(this);
    	programs.put(programID, program);
        propChgSupport.firePropertyChange("programs", null, getPrograms());
    }
	
	/**
	 * Removes a program from this workflow. Callers should be warned that the program is {@link Program#invalidate()
	 * invalidated} before being removed; it means that conenctions are dropped, and as a consequence other programs
	 * are changed
	 * @param prg the program to remove.
	 */
	public void deleteProgram(Program prg) {
		if ( !programs.containsValue(prg) )
			throw new RuntimeException("error"); //todo
		prg.invalidate();
		programs.remove(getIdForProgram(prg));
		// Now notify listeners that "an arbitrary set of if its properties have changed" (quoting
		// java.beans.PropertyChangeEvent documentation)
		propChgSupport.firePropertyChange(null, null, null);
	}

	/**
	 * Prepares a zip file containing all the files referenced by the workflow,
	 * and clones the receiver Specifically, it contains all the files that are
	 * referenced by the workflow.
	 * 
	 * @param zip
	 *            path of the zip file to build
	 * @param executionDirectory
	 *            path of the execution directory, in which the files referenced
	 *            by the workflow are copied.
	 * @return the workflow as it will be executed
	 * @throws IOException
	 *             if the workflow is not suitable for execution. In this case,
	 *             the zip file is deleted.
	 */
    public Workflow prepareWorkflowForExecution(File zip, File executionDirectory)
    throws IOException
    {
    	return exportWorkflowWithInput(zip, null, executionDirectory, null, true, true);
    }
    
    /**
     * Rebuilds the id stored in the string and stores it in {@link #id}.
     * @param id_str the string representation of the ID
     */
    protected void buildID(String id_str)
    {
        if (id_str==null) // TODO CHANGE DTD: id should not be null
        {
            id = new WorkflowID();
            return;
        }
        WorkflowID new_id = null;

        try
        {
            new_id = new WorkflowID(id_str);
        }
        catch (IOException | ClassNotFoundException e) { Log.log.log(Level.WARNING, "Unable to read the id", e); }
        
        if (new_id == null)
            id = new WorkflowID(); // TODO flag this, so that the GUI can report it
        else
            id = new_id;
    }
    
    /**
     * Reads the provided xml element and builds the corresponding Workflow.
     * During this operation, the receiver is completely reinitialized.
     * @param xml_doc
     * @param warnings may be null
     * @throws InvalidXMLException TODO pointer to DTD
     * @see #toXML()
     */
    protected void fromXML(Document xml_doc, XMLWarnings warnings) throws InvalidXMLException
    {
        Element xml_node = xml_doc.getRootElement();
        @SuppressWarnings("rawtypes") // because of org.jdom
        List xml_files, xml_prgs;
        InvalidXMLException xml_exc = new InvalidXMLException(ELEMENT_TYPE.WORKFLOW);
        
        if ( warnings==null )
        	warnings = new XMLWarnings(); // and yes, it will be discarded at the end
        
        // If the XML comes from a file, it has already been validated wrt its DTD.
        // We still need to know which version of the XML workflow we're loading
        String wf_publicID = checkDTD(xml_doc);
        int version=-1;
        if (XMLConstants.WORKFLOW_DTD_PUBID.equals(wf_publicID))
        	version = 3;
        else if (XMLConstants.WORKFLOW_DTD_PUBID_v2.equals(wf_publicID))
        	version = 2;
        
        // version 2 and 3 are basically the same, except for the root element
        switch (version)
        {
        	case 2:
                if ( !xml_node.getName().equalsIgnoreCase("scenario") )
                    throw xml_exc.setMsg("root element is not <scenario/>");
                break;
        	case 3:
                if ( version==3 && !xml_node.getName().equalsIgnoreCase(WORKFLOW_ROOT_NODE) )
                    throw xml_exc.setMsg("root element is not <"+WORKFLOW_ROOT_NODE+"/>");
                break;
        	default:
        		throw xml_exc.setMsg("DTD: Unknown docType: "+wf_publicID);
        }

        buildID(xml_node.getChildText("id"));
        user = xml_node.getChildText("user");
        annotation = xml_node.getChildText("annotation");
        String name = xml_node.getChildText("name"); // COMPATIBILITY
        if (name != null)
        	id.setName(name);
        
        xml_exc.setName(id.name());
        
        /* files */
        try {
            final XPath xpath = XPath.newInstance("./inputs/input");
            xml_files = xpath.selectNodes(xml_node);
        }
        catch (org.jdom.JDOMException e) {
            String msg = "unable to select <input/> nodes";
            if ( e.getMessage() != null )
            	msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        /* handle file */ /* TODO: handle Databases */
        inputs = new LinkedHashMap<String, WorkflowInput>();
        for (Object xml_file: xml_files)
        {
            WorkflowInput f;
            final String input_id = ((Element)xml_file).getAttributeValue("id");

            if ( input_id == null || input_id.equals("") )
                throw xml_exc.setMsg("Invalid id for input (omitted or empty)");
            if ( inputs.containsKey(input_id) )
                throw xml_exc.setMsg("Duplicate input id:"+input_id);
            ((Element)xml_file).removeAttribute("id");
            
            try {
                f = WorkflowInput.buildFromXML((Element)xml_file);
            }
            catch (InvalidXMLException e) {
                throw xml_exc.setMsg("Couldn't read input: "+e.toString());
            }
            inputs.put(input_id, f);
        }

        /* programs */
        try {
            final XPath xpath = XPath.newInstance("./programs/program");
            xml_prgs = xpath.selectNodes(xml_node);
        }
        catch (org.jdom.JDOMException e) {
            String msg = "unable to select <program/> nodes";
            if ( e.getMessage() != null )
                msg += " ("+e.getMessage()+")";
            throw xml_exc.setMsg(msg);
        }

        programs = new LinkedHashMap <String, Program>();
        for (Object xml_prg: xml_prgs)
        {
            String prg_id = ((Element)xml_prg).getAttributeValue("id");

            if ( programs.containsKey(prg_id) )
                throw xml_exc.setMsg("Duplicate prg id:"+prg_id);
            ((Element)xml_prg).removeAttribute("id");

            try
            {
            	Program prg = new Program((Element)xml_prg, this, warnings);
            	programs.put(prg_id, prg);
            }
            catch (InvalidXMLException exc)
            {
            	if (exc.isFatal())
            		throw exc;
            }
        }

        /* Now that all programs have been loaded, we need to connect the
         * programs: this is done by connecting input and output parameters.
         */
        for (Program program: programs.values()) {
            for (Parameter input: program.getInputParameters()) {
                if ( input.getInput() != null )
                    /* this is an input parameter already connected to a WorkflowInput */
                    continue;
                if ( input.getData()==null || "".equals(input.getData()) )
                	/* CHECK input.getData() may be null as well, when the loaded prg. was saved w/ a description which did not contain this input */
                    /* not set; in this case, input.io defaults to null, perfect */
                    continue;
                
                Parameter output = getParameterWithXMLID(input.getData());
                if ( output == null )
                {
                	String input_prg="??",input_name="??";
                	try {
                		input_prg  = input.getData().split("\\.")[0];
                		input_name = input.getData().split("\\.")[1];
                	}
                	catch (IndexOutOfBoundsException exc) { /* explicitly ignore */ }
                	
        		warnings.add(Workflow.XMLWarnings.PRG_HAS_INVALID_LINK,
        		             new String[] { program.getProgramID(),
        		                            input_prg, input_name });
        		continue;
                }
                if (!input.isActivated() || !output.isActivated())
                {
        			warnings.add(Workflow.XMLWarnings.PRG_LINK_IS_INACTIVE,
        			             new String[] { output.getProgram().getProgramID(),
        			                            output.getDescription().getDisplayName(),
        			                            input.getProgram().getProgramID(),
        			                            input.getDescription().getDisplayName(),
        			                            });
        			continue;
                }
                output.addOutput(input);
            }
        }
        /*
         * check WorkflowInputs: if, due to changes in the description, some are
         * connected to a non-activated input, the link must be removed.
         */
        for (WorkflowInput input: inputs.values())
        {
            for (Parameter p: input.outputs()) // we need a copy
            {
                if ( ! p.isActivated() )
                    p.setInput(null); // TODO add Workflow.XMLWarnings
            }
        }
    }
	
	/**
	 * Returns the workflow serialized as XML, in a {@link Element}.
	 * @return the serialized workflow
	 * @see Workflow#toXMLDocument()
	 * @implementation This is not part of the public API: since there exist workflows that have been saved using an
	 *                 older DTD, {@link #fromXML(Document, XMLWarnings)} needs to examine the doc. type to determine
	 *                 which version of the DTD an xml-serialized workflow used. Hence, the public API only exposes
	 *                 {@link #toXMLDocument()} which carries the appropriate information.
	 */
    protected Element toXML(boolean includeUserBoundParameters)
    {
        Element xml_wf = new Element(WORKFLOW_ROOT_NODE);

        xml_wf.addContent(new Element("id").setText(id.dumpID()));
        xml_wf.addContent(new Element("name").setText(id.name()));
        xml_wf.addContent(new Element("user").setText(user));
        if (annotation!=null)
        	xml_wf.addContent(new Element("annotation").setText(annotation));
        
        /* inputs */
        Element xml_inputs = new Element("inputs");
        xml_wf.addContent(xml_inputs);
        
        for (String input_id: inputs.keySet()) {
            Element xml_input = inputs.get(input_id).toXML();
            xml_input.setAttribute("id", input_id);
            xml_inputs.addContent(xml_input);
        }
        
        /* programs */
        Element xml_prgs = new Element("programs");
        xml_wf.addContent(xml_prgs);
        for (String prg_id: programs.keySet()) {
            Element xml_prg = programs.get(prg_id).toXML(includeUserBoundParameters);
            xml_prg.setAttribute("id", prg_id);
            xml_prgs.addContent(xml_prg);
        }
        
        return xml_wf;
    }

	/**
	 * Returns the workflow serialized as XML, in a {@link Document}
	 * 
	 * @param includeUserBoundParameters
	 *            Indicates whether parameters that are marked as
	 *            {@link eu.telecom_bretagne.praxis.core.resource.ParameterDescription#isUserBound() "userBound"} should
	 *            be included.
	 * @return the serialized workflow.
	 */
    public Document toXMLDocument(boolean includeUserBoundParameters)
    {
	    return new Document(toXML(includeUserBoundParameters), 
                new DocType(WORKFLOW_ROOT_NODE, XMLConstants.WORKFLOW_DTD_PUBID, XMLConstants.WORKFLOW_DTD_SYSID));
    }
    
    public void save(File file, boolean includeUserBoundParameters) throws IOException
    {
        Facade_xml.write(toXMLDocument(includeUserBoundParameters), file);
    }

    public void save(OutputStream stream, boolean includeUserBoundParameters) throws IOException
    {
        Facade_xml.write(toXMLDocument(includeUserBoundParameters), stream);
    }
    
    /**
     * Save the workflow in a zipped file, along with all its (valid) input files.
     * The zip contains the workflow, named workflow.<ext>, and its input files
     * @param zipFile the file into which the workflow and its inputs are zipped.
     * @throws IOException in case any error occurs when writing the zip. When this happens, the
     * zipFile, is created, is deleted before the exception is raised.
     */
    public void saveWithInput(File zipFile, boolean includeUserBoundParameters) throws IOException
    {
        /* void */exportWorkflowWithInput(zipFile, getName(), null,
                                          WORKFLOW_NAME_IN_ZIP + Configuration.get("file_extension"),
                                          false, includeUserBoundParameters);
    }
	
	/**
	 * Exports a workflow with its input files into a zip. Depending on the parameters, the zip built here:
	 * <ul>
	 * <li>can contain the workflow itself,
	 * <li>can put everything at the top-level, or within a specified directory the workflow itself
	 * 
	 * @param zipFile
	 *            the zip file to be created
	 * @param zipDirectory
	 *            the root directory in which all files will be put in the zip. A {@code null} value is equivalent to
	 *            the empty string.
	 * @param copyToDirectory
	 *            if non null, all files will also be copied within this directory (i.e. input files along with the
	 *            workflow)
	 * @param workflow_filenameInZip
	 *            if non-null, the workflow is also put in the zip with the provided filename. If {@code null}, the
	 *            workflow is not put into the zip.
	 * @param abortOnErrors
	 *            if {@code true}, any error happening when trying to add an input file is fatal. Otherwise, the zip is
	 *            build and ignores input files that cannot be found or read. NBote that this does not avoid
	 *            {@code IOException} at all: if the zip could not be written, the exception is always thrown.
	 * @param includeUserBoundParameters
	 *            Indicates whether parameters that are marked as
	 *            {@link eu.telecom_bretagne.praxis.core.resource.ParameterDescription#isUserBound() "userBound"} should
	 *            be included.
	 * @return the workflow that was saved. Note that this workflow is <b>not</b> the same as {@code this}, it is a
	 *         clone of this object where input files are adapted so that they refer to the same filenames as those
	 *         stored in the zip, relative to the zip's root directory.
	 * @throws IOException
	 *             in case any error occurs when writing the zip. When this happens, the zipFile, is created, is deleted
	 *             before the exception is raised.
	 */
	public Workflow exportWorkflowWithInput(File zipFile, String zipDirectory, File copyToDirectory,
	                                        String workflow_filenameInZip, boolean abortOnErrors,
	                                        boolean includeUserBoundParameters) throws IOException
	{
		List<String> zippedInputFiles = new ArrayList<String>();
		// we'll work on a copy
		Workflow clone = clone();
		
		if (zipDirectory == null)
			zipDirectory = "";
		else
			// zip_addEntry normalizes path when building zips, however we're using it directly when inserting
			// the workflow: use '/' as it should be in zip (cf. Utile.zip_addEntry())
			if (!zipDirectory.endsWith("/"))
				zipDirectory += '/';
		
		try ( ZipOutputStream zip_output = new ZipOutputStream(new FileOutputStream(zipFile)) ) // the zip to build
		{
			/* 
			 * Iterate on the WorkflowInputs and for each:
			 * - copy the WFInput's files/dirs. into the execution directory
			 * - modify the corresponding input in the cloned workflow so that it points to the copied file/dir.
			 */
			
			for (String input_id: clone.inputs.keySet())
			{
				WorkflowInput input = clone.getInputWithId(input_id);
				if (input.isOfType(WorkflowInput.INPUT_TYPE.DATABASE))
					eu.telecom_bretagne.praxis.common.Utile.unimplemented("Unsupported input type: DATABASE");

				// we'll copy the files and make the WFInputs in the cloned workflow point to them
				List<String> clonedInputFiles = new ArrayList<String>();
				
				for (String path: input.filepaths())
				{
					File originalFile = new File(path);
					
					if (!originalFile.isAbsolute() && currentDirectory != null)
					{
						// path relative to the workflow's file
						originalFile = new File(currentDirectory, path);
					}
					
					String destinationFilename = originalFile.getName();
					// check if we already have a file with the same name. If true, modify the name so that there
					// is no filename collision anymore
					int idx = 0;
					String ext = "";
					while (zippedInputFiles.contains(destinationFilename + ext))
					{
						idx += 1;
						ext = "-" + idx;
					}
					destinationFilename += ext;
					
					
					// Check that the file can be found and read
					if (!originalFile.exists())
					{
						if (abortOnErrors)
							throw new IOException(I18N.s("UI.ctrlwf.run.file_does_not_exist", originalFile
							                             .getAbsolutePath()));
					}
					else
						if (!originalFile.canRead())
						{
							if (abortOnErrors)
								throw new IOException(I18N.s("UI.ctrlwf.run.file_unreadable", originalFile
								                             .getAbsolutePath()));
							return null;
						}
					
					try
					{
						Utile.zip_addEntry(zip_output, originalFile, zipDirectory + destinationFilename,
						               originalFile.getName());
					}
					catch (FileNotFoundException e)
					{ // thrown by Utile.zipFiles(), by FileOutputStream
						if (abortOnErrors)
						{
							throw new IOException(I18N.s("UI.ctrlwf.run.zip_failed_missing_file", e
							                                       .toString()), e);// TODO custom err. window
						}
						// invalid file: keep its original name
						clonedInputFiles.add(new File(path).getName());
						continue;
					}
					catch (IOException e)
					{
						if (abortOnErrors)
						{
							throw new IOException(I18N.s("UI.ctrlwf.run.zip_failed", e.toString()), e);
						}
						// invalid file: keep its original name
						clonedInputFiles.add(new File(path).getName());
						continue;
					}
					
					clonedInputFiles.add(destinationFilename);
					zippedInputFiles.add(destinationFilename);
					
					// Let's copy the files
					if (copyToDirectory != null)
					{
						if (input.isOfType(INPUT_TYPE.FILE))
							Utile.copyFile(new File(path), new File(copyToDirectory, destinationFilename));
						if (input.isOfType(INPUT_TYPE.DIRECTORY))
						{
							new File(copyToDirectory, destinationFilename).mkdir();
							Utile.deepCopyFile(new File(path), new File(copyToDirectory, destinationFilename));
						}
					}
				}
				input.setContent(clonedInputFiles);
			}
			
			if (copyToDirectory != null)
			{
				// ? copy CLONE workflow?
			}
			if (workflow_filenameInZip != null)
			{
				// Now that the cloned workflow's inputs are adapted, save it
				zip_output.putNextEntry(new ZipEntry(zipDirectory + workflow_filenameInZip));
				clone.save(zip_output, includeUserBoundParameters);
				zip_output.close();
			}
		}
		catch (IOException exc)
		{
			zipFile.delete();
			throw exc;
		}
		return clone;
	}

    public void importWorkflow(Workflow workflow)
    {
        for (WorkflowInput input: workflow.getInputs())
            inputs.put(getNextInputID(), input);

        for (Program prg: workflow.getPrograms()) {
            prg.setWorkflow(this);
            programs.put(getNextProgramID(prg.getDescription().getPrgName()), prg);
        }            
    }

    /* Getters and setters */
    public boolean wasExecuted()
    {
    	return executed;
    }

    public void setExecuted(boolean flag)
    {
    	this.executed = flag;
    }

    public WorkflowInput[] getInputs() {
        return (inputs.values()).toArray(new WorkflowInput[inputs.size()]);
    }
    
    public String getName() {
        return id.name();
    }

    public void setName(String name) {
        id.setName(name);
    }

    /** Returns all the workflow's programs */
    public Program[] getPrograms() {
        return programs.values().toArray(new Program[programs.size()]);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

	/** Returns the annotation attached to this object; this is a field left to the user's disposal
	 * @return the optional annotation, it may be null
	 */
	public String getAnnotation()
	{
		return annotation;
	}

	/**
	 * Sets the optional annotation for this object; this is a field left to the user's disposal
	 * @param annotation
	 *            the new annotation; it may be null or empty
	 */
	public void setAnnotation(String annotation)
	{
		if(annotation.equals(""))
			this.annotation = null;
		else
			this.annotation = annotation;
	}
	
	/**
	 * Tells whether the workflow can be executed. A workflow is ready to be executed if:
	 * <ul>
	 * <li>all its programs are {@link Program#isReadyForExecution() ready for execution},
	 * <li>and all its <b>connected</b> inputs have a {@link WorkflowInput#hasaValidValue() valid value}.
	 * </ul>
	 * 
	 * @return {@code true} if the workflow is ready for execution, {@code false} otherwise.
	 */
	public boolean isReadyForExecution()
	{
		for (Program prg: getPrograms())
			if ( !prg.isReadyForExecution() )
				return false;
		for (WorkflowInput input: getInputs())
			if ( input.outputs().length!=0 && (!input.hasaValidValue()) )
				return false;
		return true;
	}

	/**
	 * Returns the workflow's {@link #getName() name}.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getName();
	}
	
	/* property change listeners */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    	propChgSupport.addPropertyChangeListener(listener);
    }
    
    public PropertyChangeListener[] getListeners() { 
    	return propChgSupport.getPropertyChangeListeners();
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    	propChgSupport.removePropertyChangeListener(listener);
    }
}
