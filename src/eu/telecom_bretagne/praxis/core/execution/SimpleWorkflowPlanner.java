/* License: please refer to the file README.license located at the root directory of the project */
/**
 * 
 */
package eu.telecom_bretagne.praxis.core.execution;

import java.util.ArrayList;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.core.execution.ExecutionSet.ProgramInfo;
import eu.telecom_bretagne.praxis.core.workflow.Program;

/**
 * @author Sébastien Bigaret
 *
 */
public class SimpleWorkflowPlanner
    extends WorkflowPlanner
{
	public SimpleWorkflowPlanner()
	{
		super();
	}

	public void plan(Process process)
    {
    	/*
    	 * During the planification, we consider that an activity with a non-null ExecutionSet has been assigned;
    	 * if the assigned platform is null, it means that no ressources were available at the time the
    	 * assignment was made.
    	 */
    	this.prepare(process);
    	ArrayList <Activity> todo = new ArrayList<Activity>(process.activities);
    
    	ArrayList <Activity> rootActivities = rootActivities(process);

    	for (Activity activity: process.activities)
    	{
    		assignPlatform(activity);
    	}
    	for (Activity activity: rootActivities)
    	{
    	    activity.getExecutionSet().setTag(0L);
    		todo.remove(activity);
    	}

    	/*
    	 * - root activities are tagged with 0
    	 * - an activity's tag is the maximum tag of its predecessors.
    	 */
    	if ( ! Configuration.getBoolean("simpleWorkflowPlanner_aggregate_activities") )
    		return;
    	
    	long max_tag = tagAllActivities(todo);
    	// at this point, todo is empty, let's nullify it
    	todo = null;
    	
    	for (Long tag=1L; tag<=max_tag; tag++)
    	{
    		final ArrayList<Activity> activities = new ArrayList<Activity>(process.activities);
    		for (Activity a: activities) // decideExecSet() may modify the list
    		{
    			if (a.getContainer()==null) // activity has been merged into an other one and is not in the process anymore
    				continue;
    			if (a.getExecutionSet().getTag().equals(tag))
    				continue;
    			decideExecSet(a);
    		}
    	}
    }

	/**
	 * 
	 * @param todo
	 * @return the maximum tag assigned to an activity
	 */
    @SuppressWarnings("unchecked")
    private long tagAllActivities(ArrayList <Activity> todo)
	{
    	long max = 0;
		// This is not very efficient, but workflows are not huge enough for us to care about performance penalties here
		while ( ! todo.isEmpty() )
		{
			for (Activity a: (ArrayList<Activity>)todo.clone())
			{
				Long tag = tagActivity(a);
				if ( tag != null)
				{
					todo.remove(a);
					if (tag > max)
						max = tag;
				}
			}
		}
		return max;
	}

    /**
     * 
     * @param activity
     * @return the assigned tag, or {@code null} if no tag could be assigned
     */
    private Long tagActivity(Activity activity)
	{
		long tag = -1;
		for (Activity p: activity.getPredecessors())
		{
			Long pred_tag = (Long) (p.getExecutionSet().getTag());
			if ( pred_tag == null ) // a predecessor is not tagged yet
				return null;
			if ( pred_tag > tag)
				tag = pred_tag;
		}
		tag = tag+1;
		activity.getExecutionSet().setTag(tag);
		return tag;
	}
	
	private void decideExecSet(Activity activity)
	{
		boolean mergeAll = true;
		java.util.Set <Activity> predecessors = activity.getPredecessors();
		for (Activity predecessor: predecessors)
		{
			if (predecessor.getPlatform() != activity.getPlatform())
				mergeAll = false;
		}
		
		final Process process = activity.getContainer();
		if (!mergeAll)
			return;

		// determine the max sequence in predecessors
		int max = 0;
		for  (Activity predecessor: predecessors)
		{
			final ExecutionSet pred_set = predecessor.getExecutionSet();
			for (Program p: pred_set)
			{
				final ProgramInfo pred_info = pred_set.getInfoForProgram(p);
				if ( pred_info.sequence > max )
					max = pred_info.sequence;
			}
		}
		// add (max+1) to all our sequence
		final ExecutionSet activity_set = activity.getExecutionSet();
		for (Program p: activity_set)
		{
			activity_set.getInfoForProgram(p).sequence += max+1;
		}
		
		// Finally, assign all predecessors to the same platform as activity: merge them
		// The resulting activity gets the smallest tag value of all the merged activities.
		Long tag = (Long) activity.getExecutionSet().getTag();
		for (Activity predecessor: predecessors)
		{
			final Long pred_tag = (Long) predecessor.getExecutionSet().getTag();
			activity = process.merge(activity, predecessor);
			if (pred_tag < tag)
				tag = pred_tag;
			activity.getExecutionSet().setTag(tag);
		}
	}
	
}
