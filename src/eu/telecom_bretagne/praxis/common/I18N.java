/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * Handles and builds the internationalized message used in praxis and in praxis' projects.
 * @author Sébastien Bigaret
 */
public class I18N
{
	private static ResourceBundle praxis;

	private static ResourceBundle applicationSpecific;

	private static final MessageFormat msg_fmt = new MessageFormat("");
	
	/** This class name */
	private static final String CLASS = PraxisPreferences.class.getName();

	static
	{
		try
		{
			setLocale(Locale.getDefault());
		}
		catch (Throwable t)
		{
			// Log it and try to continue nevertheless
			Log.log.log(Level.SEVERE, "Caught an exception in the static initializer", t);
		}
	}

	public static void setLocale(final Locale locale)
	{
		Log.log.entering(CLASS, "setLocale()", locale);
		// This is the locale to use in the formatter in case we cannot find the corresponding praxis bundle,
		// or the app.specific bundle using the locale passed as parameter.
		Locale msg_fmt_locale = locale;
		try
		{
			praxis = ResourceBundle.getBundle("data/i18n/praxis", locale);
		}
		catch (NullPointerException | MissingResourceException e)
		{
			Log.log.info(MessageFormat.format("Could not find bundle for praxis and locale: {0}, defaulting to English", locale));
			praxis = ResourceBundle.getBundle("data/i18n/praxis", new Locale("en"));
			msg_fmt_locale = new Locale("en");
		}
		String appSpecific = Configuration.get("i18n", "");

		if (!"".equals(appSpecific))
		{
			// try to find the requested one.
			// If it fails: defaults to English
			// And if it fails again: pretend the app. specific bundle was not set
			try
			{
				applicationSpecific = ResourceBundle.getBundle(appSpecific, locale);
			}
			catch (NullPointerException | MissingResourceException e)
			{
				Log.log.info(MessageFormat.format("Could not find the bundle for app. specific locale {0}, defaulting to English", locale));
				try
				{
					applicationSpecific = ResourceBundle.getBundle(appSpecific, new Locale("en"));
				}
				catch (NullPointerException | MissingResourceException e2)
				{
					Log.log.info("Could not find the app. English bundle: ignoring app.specific locale");
					applicationSpecific = null;
				}
			}
		}
		else
			applicationSpecific = null;
		Log.log.log(java.util.logging.Level.INFO, MessageFormat.format("Setting formatter''s locale: {0}", msg_fmt_locale));
		synchronized(msg_fmt) { msg_fmt.setLocale(msg_fmt_locale); }
		Log.log.exiting(CLASS, "setLocale()");
	}

	public static Locale getLocale()
	{
		synchronized(msg_fmt) { return msg_fmt.getLocale(); }
	}

	/**
	 * Returns the internationalized message corresponding to the supplied key.<br>
	 * The method simply returns the value computed by
	 * 
	 * <pre>
	 * s(key, new Object[]{}})
	 * </pre>
	 * 
	 * see {@link #s(String, Object)} for details.
	 * @param key
	 *            a key to be found in the I18N resources.
	 * @return the corresponding internationalized message.
	 */
	static public String s(String key)
	{
		return s(key, new Object[] {});
	}

	/**
	 * Builds the internationalized string pointed to by the key, using the supplied arguments. If the key is not
	 * found in the resources, the returned string is directly built from the key and its arguments using a
	 * {@link MessageFormat} object.
	 * @param key
	 *            a key to be found in the I18N resources
	 * @param args
	 *            the arguments used to built the message. It may be empty or null.
	 * @return the corresponding internationalized string if the key is found in the I18N resources. If not, the
	 *         string is directly built by a MessageFormat using the key and the arguments.
	 */
	static public String s(String key, Object args)
	{
		String fmt = key;
		try
		{
			if (applicationSpecific != null)
			{
				try
				{
					fmt = applicationSpecific.getString(key);
				}
				catch (java.util.MissingResourceException e)
				{
					fmt = praxis.getString(key);
				}
			}
			else
				fmt = praxis.getString(key);
		}
		catch (java.util.MissingResourceException e)
		{
			Log.log.severe("I18N: Unable to find resources for key: " + key);
		}
		catch (IllegalArgumentException e)
		{
			Log.log.log(Level.SEVERE, "I18N: could format " + fmt, e);
		}
		synchronized (msg_fmt)
		{
			msg_fmt.applyPattern(fmt);
			return msg_fmt.format(args);
		}
	}

	/**
	 * Returns the internationalized message corresponding to the supplied key.<br>
	 * The method simply returns the value computed by
	 * 
	 * <pre>
	 * s(key, new Object[] s})
	 * </pre>
	 * 
	 * see {@link #s(String, Object)} for details.
	 * @param key
	 *            a key to be found in the I18N resources.
	 * @param s
	 *            the argument used in the I18N string pointed to by the key.
	 * @return the corresponding internationalized message.
	 */
	static public String s(String key, String s)
	{
		return s(key, new Object[] { s });
	}
}
