/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import static eu.telecom_bretagne.praxis.core.execution.EventAudit.*;
import static eu.telecom_bretagne.praxis.core.execution.EventAudit.EventType.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.telecom_bretagne.praxis.core.workflow.Parameter;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;


public class Activity
    extends ExecutionObject
{
    private static final long serialVersionUID = -6948446753156668537L;

	private Process container;
	
	private Result result = null;
	private ExecutionSet set;

	/** used by {@link #nextKey()} to deliver a unique key */
	private static Long seq = 0L;

	/**
	 * Delivers a unique key per call, made after the current time and a unique key.
	 * @return a key guaranteed to be unique for every call.
	 */
	private static synchronized String nextKey()
	{
		// Unicity of the key implies that this method is not called mroe than ~2xLong.MAX_VALUE per milliseconds.
		seq++;
		if ( seq == Long.MAX_VALUE )
			seq = Long.MIN_VALUE;
		return new java.text.SimpleDateFormat("yyyyMMdd-HHmmssSSS_").format(java.util.Calendar.getInstance().getTime())
		       + Long.toString(seq, 16);
	}
	
	/**
	 * 
	 * @param process
	 */
	public Activity(Process process)
	{
		super(nextKey(), "nameTODO");
		this.container = process;
		setContext(new ActivityContext());
		addEvent(new DataEventAudit(ContextChanged, this));
		setExecutionSet(new ExecutionSet());
	}

	/** Used for testing only */
	/* package */ Activity(java.io.Serializable key, String name)
	{
		super(key, name);
		setContext(new ActivityContext());
		setExecutionSet(new ExecutionSet());
	}

	/**
	 * @param event
	 */
	@Override
	protected void addEvent(EventAudit event)
	{
		getContainer().addEvent(event);
	}
	
	/**
	 * Returns the process this activity depends on.
	 * @return the process on which
	 * @category getter
	 */
	public Process getContainer()
	{
		return container;
	}

	/**
	 * Returns the context associated to this activity
	 * @return the associated context
	 * @category getter
	 */
	@Override
	public ActivityContext getContext()
	{
		return (ActivityContext) super.getContext();
	}
	
	/**
	 * Returns the ExecutionSet this activity belongs to. Execution sets are built when the execution is planned.<br/>
	 * Activities are assigned to or removed from an execution set by the sets themselves, see
	 * {@link ExecutionSet#add(Activity)} and {@link ExecutionSet#remove(Object)}.
	 * @return the execution set this activity belongs to
	 */
	public ExecutionSet getExecutionSet()
	{
		return set;
	}
	
	/**
	 * Returns the key attached to this object
	 * @return the key associated to this object
	 * @category getter
	 */
	@Override
	public String getKey()
	{
		return (String) super.getKey();
	}

	/**
	 * Returns the platform responsible for executing the activity. This is a shortcut for {@code
	 * getExecutionSet().getPlatform()} except if the activity has no execution set, in which case it returns {@code
	 * null}.
	 * @return the platform that is assigned for executing the activity
	 */
	public PlatformDescription getPlatform()
	{
		if ( getExecutionSet() == null )
			return null;
		return getExecutionSet().getPlatform();
	}
	
	/**
	 * Returns the result associated to this activity
	 * @return the associated result
	 * @category getter
	 */
	public Result getResult()
	{
		return this.result;
	}

	void setContainer(Process aProcess)
	{
		this.container = aProcess;
	}

	/**
	 * Sets the context associated to this activity
	 * @param context
	 *            the context to be associated with this activity
	 * @throws IllegalArgumentException
	 *             if the supplied context is not an {@link ActivityContext} object
	 * @category setter
	 */
	@Override
	void setContext(Context context) // package: for tests
	{
		if ( ! (context instanceof ActivityContext) )
        {
			throw new IllegalArgumentException();
        }
		super.setContext(context);
	}
	
	private void setExecutionSet(ExecutionSet aSet)
	{
		this.set = aSet;
	}
	
	/**
	 * Sets the result associated to this activity.<br>
	 * Note that setting a result involves setting the status with the result's status.
	 * @param result
	 *            the result to associate with this activity
	 * @category setter
	 */
	public void setResult(Result result)
	{
		this.result = result;
		setState(result.getStatus());
	}

	public Set<Activity> getPredecessors()
	{
		Set<Activity> predecessors = new HashSet<Activity>();
		for (Program prg: this.getExecutionSet())
		{
			List<Parameter> inputs = prg.getInputParameters();
			for (Parameter input: inputs)
			{
				if (input.getPrgInput()!=null)
				{
					predecessors.add(container.activityContaining(input.getPrgInput().getProgram()));
				}
			}
			
		}
		predecessors.remove(this);
		return predecessors;
	}
	
	public boolean hasDisconnectedInputFiles()
	{
		for (Program prg: set)
			if (prg.getDisconnectedInputFiles().size()!=0)
				return true;
		return false;
	}
	
	public boolean hasInvalidInputFiles()
	{
		for (Program prg: set)
			for (Parameter param: prg.getInputParameters())				
				if (param.isActivated() && param.getInput()!=null && param.getInput().filepaths().length==0) //TODO DATABASE etc.
				// NB do not use param.getInput().hasaValidValue(): File objects are relative at this point
				// we just check at an early point that workflow inputs are not empty, checking that files exist
				// happens later (cf. PlatformDescription.execute())
					return true;
		return false;
	}

	@Override
	public String toString()
	{
		StringBuilder repr=new StringBuilder("[Activity: prgs:");
		if (set==null)
			repr.append("<null set>");
		else
		{
			repr.append("{");
			for (Program program: set)
				repr.append((program!=null)?program.getProgramID():"<unknown>").append(",");
			repr.append("} ");
		}
		repr.append(super.toString());
		repr.append(" result:" + ( (result!=null)?result:"null" ));
		repr.append("]");
		return repr.toString();
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException
	{
		if (result!=null)
			result.readyToSerialize();
		out.defaultWriteObject();
	}
}
