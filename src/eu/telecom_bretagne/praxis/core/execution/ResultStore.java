/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.common.PraxisPreferences;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.workflow.WorkflowID;


/**
 * <code>
 * CREATE TABLE ScResult(scid VARCHAR, execID VARCHAR, status VARCHAR, result VARCHAR, zippedFilename VARCHAR)
 * </code> The ResultStore is used on the server-side; the client-side uses a different approach (see
 * {@link eu.telecom_bretagne.praxis.client.StorageManager})
 * @implementation The (Prepared)Statement.close() statement present in all methods are mandatory, or the SQLiteJDBC
 *                 driver can start failing on rollback() & commit() with the error
 *                 "SQL logic error or missing database".
 * @author Sébastien Bigaret
 */
public class ResultStore
{
	public static final String RESULT_db_schema      = "CREATE TABLE ScResult(scid VARCHAR, execID VARCHAR, status VARCHAR, date VARCHAR, result VARCHAR, zippedFilename VARCHAR, user VARCHAR NOT NULL)";

	public static final String RESULT                = "ScResult";

	public static final String RESULT_scid           = "scID";

	public static final String RESULT_execid         = "execID";

	public static final String RESULT_status         = "status";

	public static final String RESULT_date           = "date";

	public static final String RESULT_result         = "result";

	public static final String RESULT_user           = "user";

	public static final String RESULT_zippedFilename = "zippedFilename";

	public static final String RESULT_full           = RESULT + "(" + RESULT_scid + "," + RESULT_execid + ","
	                                                   + RESULT_status + "," + RESULT_date + "," + RESULT_result + ","
	                                                   + RESULT_zippedFilename + "," + RESULT_user + ")";

	/**
	 * The date format used to insert date objects into the DB and to read them back.
	 * IMPORTANT this object is inherently unsafe for multithreaded use, access must be protected with synchronized
	 * blocks.
	 * @see ResultStore#dateToString(Date)
	 * @see ResultStore#stringToDate(String)
	 */
	private static transient final DateFormat RESULT_date_fmt = new SimpleDateFormat("yyyyMMdd_HHmmss.SSSZ");

	private Connection db_connection;

	/**
	 * Take care of deserializing the Result object.
	 *
	 * @param result_asHexStr the hexadecimal representation of the serialized object
	 * @return the {@link Result} object
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static Result deserializeResult(String result_asHexStr) throws IOException,
	        ClassNotFoundException
	{
		return (Result) Utile.deserializeObject(result_asHexStr);
	}

	public ResultStore() throws java.sql.SQLException
	{
		final String workspace = PraxisPreferences.get(PraxisPreferences.rootNode, "prefix");
		final String path = workspace + "/serverResultStore.bdb";
		boolean createDB = !new File(path).exists();
		try
		{
			Class.forName("org.sqlite.JDBC").newInstance();
		}
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
		{
			Log.log.log(Level.SEVERE, "Could not load or create sqlite JDBC driver", e);
		}
		db_connection = DriverManager.getConnection("jdbc:sqlite:/" + path);
		if (createDB)
		{
			db_connection.createStatement().execute(ResultStore.RESULT_db_schema);
		}
		try ( final Statement statement = db_connection.createStatement() )
		{
			statement.execute("PRAGMA synchronous = NORMAL;");
			statement.execute("PRAGMA journal_mode=WAL;");
		}
		catch(SQLException e)
		{
			Log.log.log(Level.WARNING, "Unable to set journal_mode to WAL w/ synchronous=normal", e);
		}
		db_connection.setAutoCommit(false);
		this.consistencyCheck();
	}

	/**
	 * Checks basic features in the DB, and removes elements that are invalid.
	 * See source code for details.
	 */
	private void consistencyCheck()
	{
		synchronized (db_connection)
		{
			final String select_all = "SELECT "+RESULT_scid + "," + RESULT_execid + "," +
				RESULT_status + "," + RESULT_date + "," + /* RESULT_result + "," + */
				RESULT_zippedFilename + "," + RESULT_user + " asc from "+RESULT;
			try (java.sql.Statement st = db_connection.createStatement();)
			{
				st.execute(select_all);
				try (ResultSet rs = st.getResultSet())
				{
				int incorrect_results=0;
				while (rs.next())
				{
					boolean deleteFlag = false;
					Status status = null;
					try
					{
						status = Status.valueOf(rs.getString(RESULT_status));
					}
					catch (IllegalArgumentException | NullPointerException e)
					{ /* if an exception is raised, status is null and this is handled below */ }

					final String filename = rs.getString(RESULT_zippedFilename);

					if (status==null)  // status should be valid
					{
						Log.log.warning("Detected result associated to an invalid status");
						deleteFlag = true;
					}
					else if ( status.isClosed() && filename!=null && ! new File(filename).exists() )
					{ // if a result is closed, the (zip) file must exist
						Log.log.warning("Detected a \"closed\" result associated to a non-existant file --deleting it. date:"+rs.getString(RESULT_date)+" status:"+rs.getString(RESULT_status));
						deleteFlag = true;
					}
					if (deleteFlag)
					{
						incorrect_results++;
						this.deleteResult(rs.getString(RESULT_scid), rs.getString(RESULT_execid));
					}
				}
				if (incorrect_results == 0)
					Log.log.info("Database is sane");
				else
					Log.log.info("Consistency check on database purged "+incorrect_results+" results");
				db_connection.commit();
				}
			}
			catch (SQLException | IllegalArgumentException e) { Log.log.log(Level.SEVERE, "", e); }
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
	}

	public void storeResult(Result result) // TODO: insert or update, as appropriate
	{
		eu.telecom_bretagne.praxis.common.Utile.unimplemented();
	}

	public boolean insertResult(Result result, String user)
	{
		synchronized (db_connection)
		{
		String insert_stmt = "INSERT INTO " + RESULT_full + " VALUES (?,?,?,?,?,?,?)";
		try (PreparedStatement st = db_connection.prepareStatement(insert_stmt))
		{
			st.setString(1, result.workflowID().dumpID());
			st.setString(2, result.executionID().dumpID());
			st.setString(3, result.status.name());
			st.setString(4, dateToString(result.getDate()));
			result.readyToSerialize();
			st.setString(5, Utile.serializeObject(result));
			st.setString(6, result.zipFile()!=null ? result.zipFile().getPath() : null);
			st.setString(7, user);
			st.execute();
			if ( st.getUpdateCount() !=1 )
			{
			    db_connection.rollback();
				return false;
			}
		    db_connection.commit();
		    return true;
		}
		catch (IOException | SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
		}
		try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		return false;
		}
	}

	static protected String dateToString(java.util.Date date)
	{
		synchronized (RESULT_date_fmt)
		{
			return RESULT_date_fmt.format(date);
		}
	}

	/**
	 *
	 * @param date_str
	 * @return the parsed Date, or null if it could not be read
	 */
	static public Date stringToDate(String date_str)
	{
		try
        {
			synchronized (RESULT_date_fmt)
			{
				return RESULT_date_fmt.parse(date_str);
			}
		}
        catch (ParseException e)
        {
        	// should not happen anyway, or DB is corrupted
        	Log.log.log(Level.SEVERE, "Cannot parse date: corrupted DB?", e);
        	return null;
        }
	}

	/**
	 * Update the Result object in the ResultStore's DB.
	 * @param result the updated result
	 * @return true if the operation was successful, false otherwise
	 */
	public boolean updateResult(Result result)
	{
		synchronized(db_connection)
		{
		String insert_stmt = "UPDATE " + RESULT + " SET status=?,date=?,result=?,zippedFilename=? WHERE " + RESULT_scid
		                     + "=? AND " + RESULT_execid + "=?";
		PreparedStatement st = null;
		try
		{
			st = db_connection.prepareStatement(insert_stmt);
			st.setString(1, result.status.name());
			st.setString(2, dateToString(result.getDate()));
			result.readyToSerialize();
			st.setString(3, Utile.serializeObject(result));
			st.setString(4, result.zipFile() != null ? result.zipFile().getPath() : "");
			st.setString(5, result.workflowID().dumpID());
			st.setString(6, result.executionID().dumpID());
			st.execute();
			if ( st.getUpdateCount() !=1 )
			{
			    db_connection.rollback();
				return false;
			}
		    db_connection.commit();
		    return true;
		}
		catch (IOException | SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
		}
		finally
		{
			try { if (st!=null) st.close(); } catch (SQLException e) { e.printStackTrace(); /* ignore */ }
		}
		try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		return false;
		}
	}

	/**
	 * The internal method fetching a result given a workflow ID, an execution ID and an optional userName. Methods
	 * {@link #getResult(WorkflowID, ExecutionID, String)} and {@link #getResult(WorkflowID, ExecutionID)} call this
	 * method; the first one is for external use, where we want to check that the user requesting a result is the one
	 * for which it is registered; thes econd one is for internal use, esp; for cases where the username is not known:
	 * the ExecutionLoop for example does not know for which user it works, and it does not need this information.
	 * @param workflowID
	 *            the workflow ID
	 * @param executionID
	 *            an execution ID
	 * @param username
	 *            the username with which the result was registered (optional, see comment above)
	 * @return the requested result, or null if it cannot be found.
	 */
	private Result _getResult(WorkflowID workflowID, ExecutionID executionID, String username)
	{
		synchronized(db_connection)
		{
		String select = "SELECT * FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND " + RESULT_execid + "=?";
		if (username!=null)
			select += " AND "+RESULT_user+"=?";
		try (PreparedStatement st = db_connection.prepareStatement(select))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(2, executionID.dumpID());
			if (username!=null)
				st.setString(3, username);
			st.execute();
			ResultSet rs = st.getResultSet();
			Result result = null;
			if (rs.next())
			{
				String result_asHexStr = rs.getString(RESULT_result);
				result = deserializeResult(result_asHexStr);
				/** file is NOT serialized, it should be restored */
				String filename = rs.getString(RESULT_zippedFilename);
				result.setZipFile((filename!=null && filename.length()>0) ? new File(filename):null);
			}
			rs.close();
			return result;
		}
		catch (SQLException | IOException | ClassNotFoundException e)
		{
			Log.log.log(Level.SEVERE, "", e);
			return null;
		}
        finally
        {
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
		}
	}

	/**
	 * Returns the result object registered with the supplied workflowID, executionID and user's name.
	 * @param workflowID
	 *            the workflow ID
	 * @param executionID
	 *            the execution ID
	 * @param username
	 *            the username with which the result was registered
	 * @return the requested result, or null if it cannot be found.
	 */
	public Result getResult(WorkflowID workflowID, ExecutionID executionID, String username)
	{
		return _getResult(workflowID, executionID, username);
	}

	/**
	 * Internal version of {@link #getResult(WorkflowID, ExecutionID, String)} when the username is unknown
	 * @see #_getResult(WorkflowID, ExecutionID, String) for details
	 * @param workflowID
	 *            the workflow ID
	 * @param executionID
	 *            the execution ID
	 * @return the requested result, or null if it cannot be found.
	 */
	protected Result getResult(WorkflowID workflowID, ExecutionID executionID)
	{
		return _getResult(workflowID, executionID, null);
	}

	public File getExecutionDirectoryFor(WorkflowID workflowID, ExecutionID executionID)
	{
		synchronized(db_connection)
		{
		String select = "SELECT * FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND " + RESULT_execid + "=?";
		try (PreparedStatement st = db_connection.prepareStatement(select))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(2, executionID.dumpID());
			st.execute();
			try (ResultSet rs = st.getResultSet() )
			{
				if (rs.next())
					return new File(rs.getString(RESULT_zippedFilename)).getParentFile();
				return null;
			}
		}
		catch (SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
			return null;
		}
		finally
		{
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
		}
	}

	/**
	 * Returns the results available for the supplied workflow and user. The returned results'
	 * {@link Result#zipFile()} is always null.
	 * 
	 * @param workflowID the workflow ID for which results
	 * @param username the username
	 * @return list of the available results. It may be null if the results could not be retrieved from the store.
	 */
	public ArrayList<Result> getResultsSummaryForWorkflow(WorkflowID workflowID, String username)
    {
		synchronized(db_connection)
		{
		String select = "SELECT * FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND "+RESULT_user+"=?";
		final ArrayList <Result> summary = new ArrayList<Result>();
		try (PreparedStatement st = db_connection.prepareStatement(select))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(2, username);
			st.execute();
			ResultSet rs = st.getResultSet();
			while (rs.next())
			{
				String result_asHexStr = rs.getString(RESULT_result);
				Result result;
				result = deserializeResult(result_asHexStr);
				// TODO should we cleanup Results (i.e. remove useless individuals prgs status & info.)?
				summary.add(result);
			}
			rs.close();
			return summary;
		}
		catch (SQLException | IOException | ClassNotFoundException e)
		{
			Log.log.log(Level.SEVERE, "", e);
			return null;
		}
		finally
		{
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
		}
    }

	/**
	 * Returns the results available for a given user. The returned results' {@link Result#zipFile()} are always null.
	 * 
	 * @param username the user name
	 * @return list of the available results. It may be null if the results could not be retrieved from the store.
	 */
	public ArrayList<Result> getResultsSummaryForUser(String username)
    {
		synchronized(db_connection)
		{
		String select = "SELECT * FROM " + RESULT + " where " + RESULT_user + "=?";
		final ArrayList<Result> summary = new ArrayList<Result>();
		try (PreparedStatement st = db_connection.prepareStatement(select))
		{
			st.setString(1, username);
			st.execute();
			ResultSet rs = st.getResultSet();
			while (rs.next())
			{
				String result_asHexStr = rs.getString(RESULT_result);
				Result result;
				try
				{
					result = deserializeResult(result_asHexStr);
				}
				catch (InvalidClassException | ClassNotFoundException e)
				{
					Log.log.log(Level.SEVERE, "", e);
					continue;
				}
				// TODO cleanup Results (removed useless individuals prgs status & info.)
				summary.add(result);
			}
			rs.close();
			return summary;
		}
		catch (SQLException | IOException e)
		{
			Log.log.log(Level.SEVERE, "", e);
			return null;
		}
		finally
		{
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
		}
    }

	/**
	 * Checks whether the ResultStore has some results for a given workflow
	 * @param workflowID
	 *            id of the workflow
	 * @param username
	 *            the owner's name (login)
	 * @return true if there are results available for this workflow and this user
	 */
	public boolean hasResults(WorkflowID workflowID, String username)
    {
		synchronized(db_connection)
		{
		String select = "SELECT COUNT(*) FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND "+RESULT_user+"=?";
		try (PreparedStatement st = db_connection.prepareStatement(select))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(2, username);
			st.execute();
			try (ResultSet rs = st.getResultSet())
			{
				rs.next();
				return rs.getInt(1) > 0;
			}
		}
		catch (SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
			return false;
		}
		finally
		{
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore CHECK */ }
		}
		}
    }

	/**
	 * Deletes all the results that are stored for a given workflow.
	 * @param workflowID
	 *            the workflow whose results should be deleted
	 * @param username
	 *            the owner's name (login)
	 * @return the number of results that have been deleted
	 */
	public int deleteAllResults(WorkflowID workflowID, String username)
	{
		synchronized(db_connection)
		{
		String stmt = "DELETE FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND "+RESULT_user+"=?";
		int deleted = 0;
		try (PreparedStatement st = db_connection.prepareStatement(stmt))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(2, username);
			deleted = st.executeUpdate();
			db_connection.commit();
			return deleted;
		}
		catch (SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
		}
		try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore */ }
		return 0;
		}
	}

	/**
	 * Deletes a result based on its composed PK.<br>
	 * It does not:
	 * <ul>
	 * <li>hold a lock on db_connection,
	 * <li>commit or rollback the connection.
	 * </ul>
	 * Hence, it is the responsability of the caller to take the appropriate actions.
	 * @param scID
	 *            the workflow ID
	 * @param execID
	 *            execution ID
	 * @throws SQLException
	 * @throws IllegalArgumentException
	 *             if less or more than one element is deleted
	 */
	private void deleteResult(String scID, String execID) throws SQLException, IllegalArgumentException
	{
		final String stmt = "DELETE FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND "+RESULT_execid+"=?";
		int deleted=0;

		try (PreparedStatement st = db_connection.prepareStatement(stmt))
		{
			st.setString(1, scID);
			st.setString(2, execID);
			deleted = st.executeUpdate();
			if (deleted != 1)
				throw new IllegalArgumentException("arguments must correspond to one and only one row");
		}
	}
	/**
	 * Deletes the supplied list of executions from the results' store.
	 * @param workflowID
	 * @param executionIDs
	 *            a list of results'ids. It should not be null.
	 * @param username
	 *            the owner's name (login)
	 * @return true if the operation was successful, false otherwise. When returning false, it is guaranteed that none
	 *         of the executions have been deleted.
	 */
	public boolean deleteResults(WorkflowID workflowID, List <ExecutionID> executionIDs, String username)
	{
		synchronized(db_connection)
		{
		final String stmt = "DELETE FROM " + RESULT + " WHERE " + RESULT_scid + "=? AND "+RESULT_execid+"=?" +
			" AND "+RESULT_user+"=?";
		int idx=0;
		File [] exec_dirs = new File[executionIDs.size()];

		try (PreparedStatement st = db_connection.prepareStatement(stmt))
		{
			st.setString(1, workflowID.dumpID());
			st.setString(3, username);
			for (ExecutionID executionID: executionIDs)
			{
				exec_dirs[idx] = getExecutionDirectoryFor(workflowID, executionID);

				st.setString(2, executionID.dumpID());
				int deleted = st.executeUpdate();
				if (deleted != 1)
				{
					db_connection.rollback();
					Log.log.fine(()->"deleting execution: 1!=deleted rows=="+deleted);
					return false;
				}
				idx++;
			}
			db_connection.commit();
			Log.log.fine(()->"Deleted "+executionIDs.size()+" items, changes committed");
			// last, delete all directories
			for (idx=0; idx<exec_dirs.length; idx++)
				Utile.deleteRecursively(exec_dirs[idx]);
			return true;
		}
		catch (SQLException e)
		{
			Log.log.log(Level.SEVERE, "", e);
		}
		finally // make sure we rollback on any exception, incl. RuntimeExceptions
		{
			try { db_connection.rollback(); } catch (SQLException e) { e.printStackTrace(); /* ignore */ }
		}
		return false;
		}
	}

	/**
	 * Delete a specific execution of the given workflow.
	 * @param workflowID
	 *            a workflow ID
	 * @param executionIDs
	 *            The id of the result that should be deleted. It should not be null.
	 * @param username
	 *            the owner's name (login)
	 * @return true if the operation was successful, false otherwise. When returning false, it is guaranteed that the
	 *         execution was not deleted.
	 */
	public boolean deleteResult(WorkflowID workflowID, ExecutionID executionID, String username)
	{
		return deleteResults(workflowID, Arrays.asList(executionID), username);
	}

}
