/* License: please refer to the file README.license located at the root directory of the project */
/*
 * Created on Apr 9, 2004
 *
 */
package eu.telecom_bretagne.praxis.common;

/**
 * @author Sébastien Bigaret
 *
 */
public class InvalidXMLException extends Exception {
	private static final long serialVersionUID = 2596435420268330701L;
	public enum ELEMENT_TYPE { WORKFLOW, WORKFLOW_INPUT, PROGRAM, PARAMETER, RESULT }
    private ELEMENT_TYPE element;
    private String name = null;
    private String msg = null; // TODO remove this, and use Throwable.getMessage() instead
    private boolean fatal = true;
    public InvalidXMLException() { super(); }
    public InvalidXMLException(String reason) { msg = reason; }
    public InvalidXMLException(ELEMENT_TYPE type) { this.element=type; }
    public InvalidXMLException(ELEMENT_TYPE type, String name, String msg)
    {
        this.element = type;
        this.name = name;
        this.msg = msg;
    }
    
    @Override
    public String toString()
    {
        if ( element != null )
            return super.toString()
                + String.format("[%s:%s] ", element, (name!=null ? name : "unspecified"))
                + (msg!=null ? "Reason: "+msg : "");

        return super.toString() + (msg!=null ? " Reason: "+msg : "");
    }
    /**
     * @return Returns the element.
     */
    public ELEMENT_TYPE getElement() {
        return element;
    }
    /**
     * @param element The element to set.
     * @return a reference to this instance.
     */
    public InvalidXMLException setElement(ELEMENT_TYPE element) {
        this.element = element;
        return this;
    }
    /**
     * 
     * @return true if the exception is unrecoverable
     */
    public boolean isFatal()
    {
    	return fatal;
    }
    /**
     * Indicates whether the exception is a fatal exception
     * @return a reference to this instance.
     */
    public InvalidXMLException setIsFatal(boolean fatal)
    {
    	this.fatal = fatal;
    	return this;
    }
    /**
     * @return Returns the msg. It may be {@code null}.
     */
    public String getMsg() {
        return msg;
    }
    /**
     * @param msg The msg to set. It may be {@code null}.
     * @return a reference to this instance.
     */
    public InvalidXMLException setMsg(String msg) {
        this.msg = msg;
        return this;
    }
    /**
     * @return Returns the name. It may be {@code null}.
     */
    public String getName() {
        return name;
    }
    /**
     * @param name The name to set. It may be {@code null}.
     * @return a reference to this instance.
     */
    public InvalidXMLException setName(String name) {
        this.name = name;
        return this;
    }
}
