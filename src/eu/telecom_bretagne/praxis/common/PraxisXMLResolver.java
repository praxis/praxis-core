/**
 * 
 */
package eu.telecom_bretagne.praxis.common;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamException;

import eu.telecom_bretagne.praxis.common.FileResources.ResourceNotFoundException;

/**
 * @author Sébastien Bigaret
 */
public class PraxisXMLResolver
    implements XMLResolver
{
    /**
     * a private cache mapping a Public ID to the corresponding {@link URL}
     */
    static private Map<String, URL> DTD_cache;

    static
    {
        try
        {
            DTD_cache = new HashMap<String, URL>();
            DTD_cache.put(XMLConstants.WORKFLOW_DTD_PUBID, FileResources.url(XMLConstants.WORKFLOW_DTD_FILE));
            DTD_cache.put(XMLConstants.WORKFLOW_DTD_PUBID_v2, FileResources.url(XMLConstants.WORKFLOW_DTD_FILE_v2));
        }
        catch (FileResources.ResourceNotFoundException exc)
        {
            Log.log.log(Level.SEVERE, "DTD not available: exiting", exc);
            System.exit(-1); // TODO: exit? or common.exit() somehow?
        }
    }

    /*
     * (non-Javadoc)
     * @see javax.xml.stream.XMLResolver#resolveEntity(java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public Object resolveEntity(String publicID, String systemID, String baseURI, String namespace)
    throws XMLStreamException
    {
        InputStream input_src = null;
        try
        {
            URL dtd = DTD_cache.get(publicID);
            if (dtd != null)
                input_src = FileResources.inputStreamForResource(dtd);
        }
        catch (ResourceNotFoundException e)
        {
            // should not happen: resources where previously checked in the static initializer
            e.printStackTrace();
        }
        return input_src;
    }

}
