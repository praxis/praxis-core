/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import eu.telecom_bretagne.praxis.common.events.Event.IncompatibleListenerException;



// TODO à déplacer

public abstract class CommunicationFacade
    extends Thread
{
	/**
	 * This interface is used by event senders willing to add some post-processing after the event has been sent. For
	 * example, a sender may use one to delete the event's file after it has been transmitted: <code>
	 * Event event = ...
	 * event.callback = new EventHandlingCallback() {
	 *                          public void eventSent(Event evt) { evt.file.delete(); }
	 *                      }
	 * </code>
	 * <b>IMPORTANT NOTE:</b> one should never rely on this mechanism to infer that an event has been successfully
	 * received on the other-side; the callback is triggered when the event has been sent (e.g. for clean-up purpose),
	 * with no assumptions whatsoever on the fact that it was successfully received.
	 * @author Sébastien Bigaret
	 */
	public static interface EventHandlingCallback
	{
		public void eventSent(Event event);
	}
	
	/**
	 * Builds a new connection.
	 * @param name
	 *            the name for the connection
	 * @param connection
	 *            the connection parameters.
	 * @return the connection
	 * @throws IOException
	 *             if the connection cannot be established
	 * @throws IllegalStateException
	 *             if {@code method} is invalid, or if the hostname or the port number are required and invalid.
	 */
	public static CommunicationFacade buildConnection(String name, String[] connection) throws IOException
	{
		if (connection==null || connection.length<3)
			throw new IllegalStateException("Invalid server specification");

		String method=connection[0];
		String hostname = connection[1];
		InetAddress address = null;
		int port = 0;

		if (!"socket".equals(method) && !"socket-ssl".equals(method) && !"sockets".equals(method)
				&& !"direct".equals(method) && !"rmi".equals(method))
			throw new IllegalStateException("Invalid server specification (unknown method: "+method+")");

		if (!method.equals("direct"))
		{
			try
			{
				address = InetAddress.getByName(hostname);
			}
			catch (UnknownHostException e)
			{
				throw new IllegalStateException("Invalid host specification (unknown host: "+hostname+")");
			}
		    try
		    {
		      port = Integer.parseInt(connection[2]);
		    }
		    catch (NumberFormatException nfe)
		    {
				throw new IllegalStateException("Invalid server specification (invalid port number: not a number)");
		    }
		}

		if (method.startsWith("rmi"))
		{
			boolean useHTTP = method.endsWith("http");
			// CHECK client had: cnx.setPriority(Thread.MIN_PRIORITY);
			return new RMICommunicationFacade(name+" (RMI cnx to "+hostname+":"+port+")", hostname, port, useHTTP);
		}
		else if ("socket".equals(method))
			return new SocketCommunicationFacade(name + "(socket)", address, port);
		else if ("socket-ssl".equals(method) || "sockets".equals(method))
			return new SSLSocketCommunicationFacade(name + " (socket-ssl)", address, port);
		else if ("direct".equals(method))
		{
			DirectCommunicationFacade cnx = new DirectCommunicationFacade(name + " (direct)");
			DirectCommunicationFacade.accept(cnx);
			return cnx;
		}
		else
			throw new IllegalArgumentException("Unknown protocol: "+method);

	}
	
	private List<EventListener> event_listeners = new ArrayList<EventListener>();

	private Semaphore event_listeners_modification_semaphore = new Semaphore(1, true);
	
	private boolean already_disconnected = false;

	/**
	 * Creates a new façade with the supplied name.
	 * @param name the name given to this façade, for example: "Client (using socket)"
	 */
	public CommunicationFacade(String name)
	{
		super(name);
	}
	
	/**
	 * Submits an event to be sent to the other side. Note that callers should not assert that the event has been sent
	 * when the method returns: depending on the concrete CommunicationFacade handling it, it may be sent immediately
	 * or after an undetermined delay. Moreover, there is no guarantee that the event will be successfully sent. <br>
	 * Callers willing to be notified when the event is actually sent should register their own
	 * {@link EventHandlingCallback callback}. <br>
	 * For the same reasons, callers are warned that neither an event nor its components should be modified after it
	 * has been sent and until the callback is called, or unexpected behaviour may happen.
	 * @param event
	 *            the event to be sent to the other side. It should not be null.
	 */
	public abstract void send(Event event);
	
	/**
	 * Called after an event has been successfully sent to the other-side. Subclasses <b>must</b> call this method
	 * after an event has been successfully sent; while this is usually done in the {@link #send(Event)} method, it
	 * may not be the case (for example, events may be queued waiting to be retrieved). We really mean "sent" here,
	 * not received.
	 * @param event
	 *            the event that has been sent
	 */
	protected void eventSent(Event event)
	{
		if ( event.callback != null )
			event.callback.eventSent(event);
	}
	/** The façade is said to be connected iff:
	 * <ul>
	 * <li> it has been successfully binded to the other side
	 * <li> the connection is valid
	 * </ul>
	 * In particular, subclasses must return false after {@link #disconnect()} has been called
	 * 
	 * @return true if the connection is alive
	 */
	public abstract boolean isConnected();

	/**
	 * Subclasses are expected to call this method in a dedicated thread upon reception of a new event.
	 */
	public void handle_event(Event event)
	{
		event_listeners_modification_semaphore.acquireUninterruptibly();
		ArrayList<EventListener> event_listeners_copy;
		try
		{
			event_listeners_copy = new ArrayList<EventListener>(event_listeners);
		}
		finally
		{
			event_listeners_modification_semaphore.release();
		}
		for (EventListener listener: event_listeners_copy)
			try
			{
				event.dispatch_to(listener);
			}
			catch (IncompatibleListenerException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // CHECK may raise?
	}
	
	/** Disconnects the façade from the other side. After this method has been called,
	 * {@link #isConnected()} should returns false.
	 * Implementors must make sure that this method can be safely called more than twice; after it is called,
	 * subsequent calls will silently return without doing anything.
	 */
	public void disconnect()
	{
		disconnect(null);
	}

	/**
	 * Subclasses must call this method
	 * @param reason
	 *            see {@link EventListener#disconnected(Exception)}. It may be null.
	 */
	protected synchronized void disconnect(Exception reason)
	{
		if ( already_disconnected )
			return;
		already_disconnected = true;
		synchronized(event_listeners)
		{
			for (EventListener listener: event_listeners)
				listener.disconnected(reason);
		}

	}
	
	/* listeners */
	/** Adds a listener */
	public void addListener(EventListener listener)
	{
		event_listeners_modification_semaphore.acquireUninterruptibly();
		try
		{
			event_listeners.add(listener);
		}
		finally
		{
			event_listeners_modification_semaphore.release();
		}
	}
	
	/** Removes a listener */
	public void removeListener(EventListener listener)
	{
		event_listeners_modification_semaphore.acquireUninterruptibly();
		try
		{
			event_listeners.remove(listener);
		}
		finally
		{
			event_listeners_modification_semaphore.release();
		}
	}
}
