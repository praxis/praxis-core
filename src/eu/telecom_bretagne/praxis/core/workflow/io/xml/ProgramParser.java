package eu.telecom_bretagne.praxis.core.workflow.io.xml;

import java.util.function.BiFunction;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.InvalidXMLException.ELEMENT_TYPE;
import eu.telecom_bretagne.praxis.core.resource.ProgramDescription;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.core.workflow.Workflow.XMLWarnings;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class ProgramParser
{
    static final String PROGRAMS = "programs";

    static final String PROGRAM = "program";

    static final String ANNOTATION = "annotation";

    static final String PARAMETER = "parameter";

    /**
	 * Builds a program from the informations stored in an XML structure.
	 * 
	 * @param workflow
	 *            The attached workflow. It must not be {@code null}. If the method is successful, this workflow is
	 *            <b>modified</b>: the return program is added to the workflow just before being returned.
	 * @param startElement
	 *            the root element {@code <program>} containing the informations
	 * @param eventReader
	 *            the event reader with which the parsing of the supplied workflow has begun
	 * @param warnings
	 *            the object is which warnings intended for the user should be stored.
	 * @return the built Program object
	 * @throws InvalidXMLException
	 *             if case the XML is not valid
	 */
    public static Program fromXML(Workflow workflow, final StartElement startElement, XMLEventReader eventReader,
                                  XMLWarnings warnings)
    throws InvalidXMLException
    {
        Program program;

        if (workflow == null )
            throw new NullPointerException("Parameter workflow cannot be null");

        InvalidXMLException xml_exc = new InvalidXMLException(InvalidXMLException.ELEMENT_TYPE.PROGRAM);

        if (! PROGRAM.equals(startElement.getName().getLocalPart()))
            throw xml_exc.setMsg("root element is not " + PROGRAM);

        String id = null;
        String prg_description_id = null;
        // info is not read, but written into xml; x & y are directly handled below;

        final BiFunction<StartElement, String, String> attributeValue =
            (element, aname) -> element.getAttributeByName(new QName(aname)).getValue();

        // attributes: id, x, y, idref: mandatory, info: optional
        prg_description_id = attributeValue.apply(startElement, "idref");
        if ( prg_description_id == null )
            throw xml_exc.setMsg("Attribute 'idref' is mandatory");
        xml_exc.setName(prg_description_id);

        id = attributeValue.apply(startElement, "id");
        if ( id == null )
            throw xml_exc.setMsg("Attribute 'id' is mandatory");
        if ( workflow.getProgramWithId(id) != null )
            throw new InvalidXMLException(ELEMENT_TYPE.WORKFLOW, workflow.getName(), "Duplicate prg id:" + id);

        
        // program description
        ProgramDescription program_description;
        handle_description:
        {
            final String prg_description_info = attributeValue.apply(startElement, "info");

            program = new Program(prg_description_id);
            program.setWorkflow(workflow);

            program_description = program.getDescription();
            if ( ( program_description==null || !program_description.isActive() ) && ! workflow.wasExecuted() )
            {
                program_description =
                    Program.delegate
                    .findAlternateProgramDescription(prg_description_id, prg_description_info, warnings, xml_exc);
            }

            if (program_description == null)
            {
                warnings.add(Workflow.XMLWarnings.PRG_DESC_DOES_NOT_EXIST_1, new String[] { prg_description_info });
                throw xml_exc.setMsg("Unable to find the corresponding prog's description ").setIsFatal(false);
            }
        }
        // Recreate the program with the updated description
        program = new Program(program_description, workflow);

        try // attribute 'x'
        {
            program.setX(Integer.valueOf(attributeValue.apply(startElement, "x")));
        }
        catch ( NullPointerException | NumberFormatException e)
        {
            throw xml_exc.setMsg("Coordinate x is not an integer: " + e.toString());
        }

        try // attribute 'y'
        {
            program.setY(Integer.valueOf(attributeValue.apply(startElement, "y")));
        }
        catch ( NullPointerException | NumberFormatException e)
        {
            throw xml_exc.setMsg("Coordinate y is not an integer: " + e.toString());
        }

        // annotations?, parameter+
        while ( eventReader.hasNext() )
        {
            XMLEvent event;
            try
            {
                event = eventReader.nextEvent();
            }
            catch (XMLStreamException e)
            {
                throw (InvalidXMLException)xml_exc.setMsg("Unable to read <program/> child nodes").initCause(e);
            }
            if ( event.isEndElement() && PROGRAM.equals(event.asEndElement().getName().getLocalPart()) )
                break;

            if ( !event.isStartElement() )
                continue;

            final StartElement element= event.asStartElement();
            if ( ANNOTATION.equals(element.getName().getLocalPart()) )
            {
                try
                {
                    program.setAnnotation(Utils.getPCDATA(element, eventReader));
                }
                catch (XMLStreamException e)
                {
                    throw (InvalidXMLException)xml_exc.setMsg("Unable to read <program/> annotation").initCause(e);
                }
                continue;
            }

            if ( PARAMETER.equals(element.getName().getLocalPart()) )
            {
                ParameterParser.fromXML(program, element, eventReader, warnings);
            }
        }
        workflow.addProgram(id, program);
        return program;
    }

}
