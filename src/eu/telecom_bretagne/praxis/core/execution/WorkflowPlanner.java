/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.core.workflow.Program;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.server.Serveur;
import eu.telecom_bretagne.praxis.server.execution.platform.PlatformDescription;



/**
 * Build an execution plan (namely: a {@link Process} along with its {@link Activity acivities} from a
 * {@link Workflow}.
 * @author Sébastien Bigaret
 */
public class WorkflowPlanner
{   
	public static WorkflowPlanner planner = new SimpleWorkflowPlanner();

	public WorkflowPlanner()
	{
		
	}

	/**
	 * Creates and registers within the process one activity per program in the corresponding workflow. The workflow
	 * is then examined and the activities' predecessors and successors are set according to the workflow to be
	 * executed.
	 * @param process
	 * @see ActivityContext#getPredecessors()
	 */
	protected void prepare(Process process)
	{
		// 1st, get the real Workflow
		Result r = Serveur.resultStore().getResult(process.getContext().workflowID,
		                                           process.getContext().executionID);

		Workflow workflow = null;
		
		// Important note: this is important that every activity gets it own, decorrelated Program
		// see comments in SimpleFormatterPlatform.identifyInputFiles()
		try
		{
			workflow =  new Workflow(new ByteArrayInputStream(r.getWorkflowXML()), null, false);
		}
		catch (InvalidXMLException e)
		{
			e.printStackTrace();
			// abort, but should not happen FIXME
		}

		if ( workflow.getPrograms().length == 0)
		{
			final StringBuilder log = new StringBuilder("Trying to executing a WF *WITHOUT* any programs in it");
			log.append(workflow.id().dumpID());
			Log.log.warning(() -> log.toString());
			// abort, but should not happen FIXME
			return;
		}
		
		// Create activities and register those who are linked with the other
		// 1. create activities
		for (Program program: workflow.getPrograms())
		{
			Activity activity = new Activity(process);
			process.addActivity(activity);
			activity.getExecutionSet().add(program);
		}

		// log details on the workflow being executed
		try
		{
			final StringBuilder log = new StringBuilder("Executing WF ");
			log.append(workflow.id().dumpID()).append(":");
			for (Program program: workflow.getPrograms())
			{
				log.append(" ").append(program.getDescription().id());
			}
			Log.log.info(() -> log.toString());
		}
		catch (Throwable t)
		{
			Log.log.log(java.util.logging.Level.WARNING, "Unable to log info on the execution", t);
		}
	}

	/**
	 * Plans the execution of the supplied process.<br/>
	 * This method simply creates one activity per program in the process' workflow, then it creates one execution set
	 * for each activity in the process.
	 * @param process
	 *            the process to be planned.
	 */
	public void plan(Process process)
	{
		this.prepare(process);

		for (Activity activity: process.activities)
		{
			assignPlatform(activity);
		}
	}

	/**
	 * After preparation, returning the activities that do not have any predecessors
	 * @param process
	 * @return
	 */
	protected static ArrayList<Activity> rootActivities(Process process) // TODO move to Process	
	{
		ArrayList<Activity> rootActivities = new ArrayList<Activity>();
		for (Activity activity: process.activities)
			if ( activity.getPredecessors().size() == 0 )
				rootActivities.add(activity);
		return rootActivities;
	}

	public static void assignPlatform(Activity activity)
	{
		for (PlatformDescription platform: Serveur.getPlatforms())
			if (isPlatformCompatibleWith(platform, activity))
			{
				activity.getExecutionSet().setPlatform(platform);
				break;
			}
	}

	public static boolean isPlatformCompatibleWith(PlatformDescription platform, Activity activity)
	{
		for (Program program: activity.getExecutionSet())
		{
			if (!platform.isAvailable(program.getDescription().id()))
				return false;
		}
		return true;
	}
}
