/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.core.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.jdom.Document;
import org.jdom.Element;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Facade_xml;
import eu.telecom_bretagne.praxis.common.FileResources;
import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.XMLConstants;


/**
 * Hierarchy of resources.
 * @author Sébastien Bigaret
 */
public class Hierarchy
{   
	protected static class Node
	{
		ArrayList<Node> children = new ArrayList<Node>();
		Node parent;
		String id;
		String default_child_id;
		
		Node(Element element)
        {
			this.id=element.getAttributeValue(XMLConstants.HRCHY_RESOURCE_ID_TAG);
	        default_child_id = element.getAttributeValue(XMLConstants.HRCHY_SERVICE_DEFAULT_TAG);
	        for (Object resource: element.getChildren(XMLConstants.HRCHY_RESOURCE_TAG))
        	{
        		Node child = new Node((Element)resource);
        		children.add(child);
        		child.parent = this;
            }
	        // TODO check that the default id is declared
        }
		void toString(StringBuilder str, int indent)
		{
			for (int i=0; i<indent; i++)
				str.append("  ");
			str.append(id);
			if (default_child_id!=null)
				str.append(" default:"+default_child_id);
			str.append("\n");
			for (Node child: children)
				child.toString(str, indent+1);
		}
	}
	protected ArrayList<Node> roots = new ArrayList<Node>();

	public Hierarchy(String filePath)
	{
		Element h = getProgramHierarchy(filePath);
		for (Object resource: h.getChildren(XMLConstants.HRCHY_RESOURCE_TAG))
		{
			Node root = new Node((Element)resource);
			roots.add(root);
		}
	}
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		for (Node root: roots)
			root.toString(str, 1);
		return str.toString();
	}


	protected static Element getProgramHierarchy(String filePath)
	{
		InputStream hrchy = null;
		Document hierarchyDoc;
		try
		{
			hrchy = FileResources.inputStreamForResource(filePath);
			hierarchyDoc = Facade_xml.read(hrchy, true);
		}
		catch (InvalidXMLException | FileResources.ResourceNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (hrchy!=null) try { hrchy.close();} catch (IOException e) { /* ignore */ }
		}
		if (hierarchyDoc == null)
			return null;
		
		return hierarchyDoc.detachRootElement();
	}
	
	public static void main(String[] args)
    {
		Hierarchy h = new Hierarchy(Configuration.get("server.allowed_resources"));
		System.out.println(h.toString());
    }
}
