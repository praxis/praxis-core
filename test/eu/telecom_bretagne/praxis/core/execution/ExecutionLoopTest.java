/* License: please refer to the file README.license located at the root directory of the project */
/** LICENSE */
package eu.telecom_bretagne.praxis.core.execution;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipFile;

import org.junit.*;

import eu.telecom_bretagne.praxis.common.InvalidXMLException;
import eu.telecom_bretagne.praxis.common.Launcher;
import eu.telecom_bretagne.praxis.core.execution.Activity;
import eu.telecom_bretagne.praxis.core.execution.ExecutionID;
import eu.telecom_bretagne.praxis.core.execution.ExecutionLoop;
import eu.telecom_bretagne.praxis.core.execution.Process;
import eu.telecom_bretagne.praxis.core.execution.Result;
import eu.telecom_bretagne.praxis.core.execution.EventAudit.EventType;
import eu.telecom_bretagne.praxis.core.execution.EventAudit.StateEventAudit;
import eu.telecom_bretagne.praxis.core.execution.Process.ProcessContext;
import eu.telecom_bretagne.praxis.core.execution.ProgramResult.ProgramStatus;
import eu.telecom_bretagne.praxis.core.execution.Result.Status;
import eu.telecom_bretagne.praxis.core.workflow.Workflow;
import eu.telecom_bretagne.praxis.server.Serveur;
import static org.junit.Assert.*;



public class ExecutionLoopTest
{
	@BeforeClass
    public static void oneTimeSetUp() throws Exception
    {
        System.setProperty("STANDALONE_EXEC", "true");        
        Launcher.main(new String[]{});
        // wait until the test platform is connected
        while (Serveur.getPlatforms().length==0)
        	Thread.sleep(50);
    }

    ProcessTest.TestRequester testRequester;
    ExecutionLoop execLoop = ExecutionLoop.executionLoop;
    
    protected Process initTestProcess(String name) throws InvalidXMLException, IOException
    {
        ProcessContext wf_context = ExecutionLoop.executionLoop.getContextTemplate();
        wf_context.setWorkingDirectory(new File("test_data", name));
        wf_context.resultsDirectory().mkdirs(); // TODO remove this
		Workflow wf = new Workflow(new File(wf_context.workingDirectory(),"workflow.xml"), null, false);
        wf_context.workflowID = wf.id();
        wf_context.executionID = new ExecutionID();        
        Result r = new Result(wf.id(), wf_context.executionID);
    	//r.setZipFile(new File(executionDirectory, "results.zip"));
    	r.setStatus(Result.Status.RUNNING);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		wf.save(baos, true);
		r.setWorkflowXML(baos.toByteArray());
    	Serveur.resultStore().insertResult(r, "test");
        Process process = ExecutionLoop.executionLoop.createProcess(wf_context.workflowID.name(),
                                                                    wf_context,
                                                                    testRequester, r);
        return process;
    }
    
    @Before
	public void SetUp()
	{
		ExecutionLoop.executionLoopThread.interrupt();
		testRequester = new ProcessTest.TestRequester();
		WorkflowPlanner.planner = new WorkflowPlanner(); // tests expects one prg per activity/exec.set
	}

    @After
    public void cleanUp()
    {
		execLoop.activities.clear();
		execLoop.processes.clear();
		execLoop.results.clear();
    }
    
	@Test
	public void pollEmptyQueues()
	{
		assertNull(execLoop.getNextSubmittedProcess());
		assertNull(execLoop.getNextActivity());
		assertNull(execLoop.getNextResult());
	}
	
	@Test
	public void submittedProcessIsaQueue() throws Exception
	{
		Process p1 = new Process(null, "p1");
		Process p2 = new Process(null, "p2");
		Process p3 = new Process(null, "p3");

		assertTrue(execLoop.submittedProcesses.isEmpty());
		execLoop.add(p1);
		execLoop.add(p2);
		execLoop.add(p3);
		
		assertEquals(execLoop.getNextSubmittedProcess(), p1);
		assertEquals(execLoop.getNextSubmittedProcess(), p2);
		assertEquals(execLoop.getNextSubmittedProcess(), p3);
	}
	
	@Test
	public void activityIsaQueue() throws Exception
	{
		Activity a1 = new Activity(null, "a1");
		Activity a2 = new Activity(null, "a2");
		Activity a3 = new Activity(null, "a3");

		execLoop.add(a1);
		execLoop.add(a2);
		execLoop.add(a3);
		
		assertEquals(execLoop.getNextActivity(), a1);
		assertEquals(execLoop.getNextActivity(), a2);
		assertEquals(execLoop.getNextActivity(), a3);
	}
	
	@Test
	public void processStarted() throws Exception
	{
		Process p1 = initTestProcess("test_cat1.bse");
		p1.setRequester(testRequester);
		execLoop.add(p1);
		execLoop.handleSubmittedProcesses();
		assertEquals(Status.RUNNING, p1.state);
	}

	/**
	 * Waits at most one second for final results in the exec. loop
	 * @param execLoop
	 * @return the number of final results stored in the execLoop
	 * @throws Exception
	 */
	protected static int waitForFinalResults(ExecutionLoop execLoop) throws Exception
	{
		int final_result_count = 0;
		Thread.sleep(1000);
		for (int i=0; i<10; i++)
		{
			  if (execLoop.results.isEmpty())
				  Thread.sleep(100);
			  else
			  {
				  for (Result r: execLoop.results)
					  if (r.status.isClosed())
						  final_result_count++;
				  if (final_result_count > 0)
					  break;
			  }
		}
		return final_result_count;
	}

	@Test
	/**
	 * test du wf cat-1: 1 programme "cat"
	 */
	public void test_cat1() throws Exception
	{
		Process process = initTestProcess("test_cat1.bse");
        execLoop.add(process);
        execLoop.handleSubmittedProcesses();
		Activity a = execLoop.activities.peek(); // do not remove it
		assertEquals(a.getExecutionSet().iterator().next().getProgramID(), "cat-1");
		assertNotNull("Activity should have been assigned", a.getPlatform());
		execLoop.handleActivities(); // should trigger the execution of the workflow
		java.util.Date start = process.result.getProgramInfo("cat-1").start;
		assertFalse(execLoop.activities.contains(a));
		assertEquals(Status.RUNNING, a.state);
		assertNotNull(start);

		// now wait for the result to come back
		int final_result_count = waitForFinalResults(execLoop);
		assertFalse("Waited one second but no result have been submitted", 0==final_result_count);
		// We may get intermediate results from the platform
		assertEquals("Got more than one final result?", 1, final_result_count);
		assertFalse(a.state.isClosed());
		assertFalse(process.isClosed());
		assertEquals(start, process.result.getProgramInfo("cat-1").start);
		assertNull(process.result.getProgramInfo("cat-1").end);
		execLoop.handleResults();
		assertEquals(start, process.result.getProgramInfo("cat-1").start);
		assertNotNull(process.result.getProgramInfo("cat-1").end);
		assertTrue(a.state.isClosed());
		assertTrue(process.isClosed());
		assertEquals(EventType.StateChanged, testRequester.lastEvent.eventType());
		assertEquals(process, testRequester.lastEvent.object());
		assertEquals(Status.OK, ((StateEventAudit)testRequester.lastEvent).newState);
	}
	
	@Test
	/**
	 * test du wf cat-1.missing input: 1 programme "cat", un fichier non connecté
	 */
	public void test_cat1_missing_infile() throws Exception
	{
		Process process = initTestProcess("test_cat1.missing_file.bse");
        execLoop.add(process);
        execLoop.handleSubmittedProcesses();
		Activity a = execLoop.activities.peek(); // do not remove it
		assertEquals(a.getExecutionSet().iterator().next().getProgramID(), "cat-1");
		assertNotNull("Activity should have been assigned", a.getPlatform());
		execLoop.handleActivities(); // should trigger the execution of the workflow
		assertFalse(execLoop.activities.contains(a));
		assertEquals(Status.RUNNING, a.state);

		assertFalse(execLoop.results.isEmpty());
		assertEquals("Got more than one result?", 1, execLoop.results.size());

		assertFalse(a.state.isClosed());
		assertFalse(process.isClosed());
		execLoop.handleResults();
		assertEquals(Status.ERROR, a.state);
		assertTrue(a.state.isClosed());
		assertTrue(process.isClosed());
		assertEquals(EventType.StateChanged, testRequester.lastEvent.eventType());
		assertEquals(process, testRequester.lastEvent.object());
		assertEquals(Status.ERROR, ((StateEventAudit)testRequester.lastEvent).newState);
	}
	@Test
	/**
	 * test du wf fail_missing-output_1: 1 prg. cat, 1 prg 'failure' dont une sortie manque
	 */
	public void fail_missingOutput_1() throws Exception
	{
		Process process = initTestProcess("fail_missing-output_1");
        execLoop.add(process);
        execLoop.handleSubmittedProcesses();
        for (int nb=0; nb<2; nb++)
        {
        	execLoop.handleActivities(); // should trigger the execution of the workflow
        	// now wait for the result to come back
        	int i;
        	for (i=0; i<10; i++)
        		if (execLoop.results.isEmpty())
        			Thread.sleep(100);
        	if (i==10)
        		assertFalse("Waited one second but no result have been submitted", execLoop.results.isEmpty());
        	execLoop.handleResults();
        }
        assertEquals(Status.ERROR, process.state);

		assertTrue(process.isClosed());
        
        assertEquals(process.activityContaining("cat-1").state, Status.OK);
        assertEquals(process.activityContaining("failure-1").state, Status.ERROR);
	}

	/**
	 * Test du wf fail_missing-output_1: 1 prg. cat, 1 prg 'failure' dont une sortie manque,
	 * chaîné avec: cat-2 sur la sortie présente, sleep-1 sur la sortie absente.
	 */
	@Test
	public void fail_missingOutput_2() throws Exception
	{
		Process process = initTestProcess("fail_missing-output_2");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();
        for (int nb=0; nb<2; nb++)
        {
        	execLoop.handleActivities(); // should trigger the execution of the workflow
        	// now wait for the result to come back
        	int i;
        	for (i=0; i<10; i++)
        		if (execLoop.results.isEmpty())
        			Thread.sleep(100);
        	if (i==10)
        		assertFalse("Waited one second but no result have been submitted", execLoop.results.isEmpty());
        	execLoop.handleResults();
        }
        assertTrue(execLoop.activities.isEmpty());
        assertEquals(Status.ERROR, process.state);
        
		assertTrue(process.isClosed());
        
        assertEquals(process.activityContaining("cat-1").state, Status.OK);
        assertEquals(process.activityContaining("failure-1").state, Status.ERROR);
        assertEquals(process.activityContaining("cat-2").state, Status.ERROR);
        assertEquals(process.activityContaining("sleep-1").state, Status.ERROR);
        Result result = process.getResult();
        assertEquals(Status.ERROR, result.status);
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-1"));
        assertEquals(ProgramStatus.ERROR, result.getExecStatus("failure-1"));
        assertEquals(ProgramStatus.NOT_EXECUTED, result.getExecStatus("sleep-1"));
        assertEquals(ProgramStatus.NOT_EXECUTED, result.getExecStatus("cat-2"));        
	}
		
	/**
	 * Test du wf fail_exitStatus_2: 1 prg. cat, 1 prg 'failure' qui produit correctement ses sorties mais dont le
	 * code de retour est 1, chaîné avec: cat-2 sur la sortie présente, sleep-1 sur la sortie absente.
	 */
	@Test
	public void fail_exitStatus_2() throws Exception
	{
		Process process = initTestProcess("fail_exit-status_2");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();
        for (int nb=0; nb<2; nb++)
        {
        	execLoop.handleActivities(); // should trigger the execution of the workflow
        	// now wait for the result to come back
        	int i;
        	for (i=0; i<10; i++)
        		if (execLoop.results.isEmpty())
        			Thread.sleep(100);
        	if (i==10)
        		assertFalse("Waited one second but no result have been submitted", execLoop.results.isEmpty());
        	execLoop.handleResults();
        }
        assertTrue(execLoop.activities.isEmpty());
        assertEquals(Status.ERROR, process.state);
        
		assertTrue(process.isClosed());
        
        assertEquals(process.activityContaining("cat-1").state, Status.OK);
        assertEquals(process.activityContaining("failure-1").state, Status.ERROR);
        assertEquals(process.activityContaining("cat-2").state, Status.ERROR);
        assertEquals(process.activityContaining("sleep-1").state, Status.ERROR);
        Result result = process.getResult();
        assertEquals(Status.ERROR, result.status);
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-1"));
        assertEquals(ProgramStatus.ERROR, result.getExecStatus("failure-1"));
        assertEquals(ProgramStatus.NOT_EXECUTED, result.getExecStatus("sleep-1"));
        assertEquals(ProgramStatus.NOT_EXECUTED, result.getExecStatus("cat-2"));
	}
	@Test
	public void ok_multiple_branches_1() throws Exception
	{
		Process process = initTestProcess("ok_multiple-branches_1");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();
        for (int nb=0; nb<3; nb++) // 3 steps: sleep-2 and cat-3 are executed in parallel
        {
        	execLoop.handleActivities(); // should trigger the execution of the workflow
        	// now wait for the result to come back
        	int final_result_count = waitForFinalResults(execLoop);
        	assertFalse("Waited one second but no result have been submitted", 0==final_result_count);
        	execLoop.handleResults();
        }
        assertTrue(execLoop.activities.isEmpty());
        assertEquals(Status.OK, process.state);
        
		assertTrue(process.isClosed());
        
        assertEquals(process.activityContaining("cat-1").state, Status.OK);
        assertEquals(process.activityContaining("sleep-2").state, Status.OK);
        assertEquals(process.activityContaining("cat-2").state, Status.OK);
        assertEquals(process.activityContaining("cat-3").state, Status.OK);
        Result result = process.getResult();
        assertEquals(Status.OK, result.status);
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-1"));
        assertEquals(ProgramStatus.OK, result.getExecStatus("sleep-2"));
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-2"));
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-3"));
	}

	/**
	 * Test d'un worfklow en 2 branches qui se rejoignent: un cat-1 et un sleep-1 en parallèle, un cat-2 avec deux
	 * entrées à la fin
	 * @throws Exception
	 */
	@Test
	public void ok_multiple_branches_2() throws Exception
	{
		Process process = initTestProcess("ok_multiple-branches_2");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();

        // cat-1 and sleep-1 are executed in parallel
        execLoop.handleActivities(); // should trigger the execution of the workflow
        // now wait for the result to come back
        int final_result_count = waitForFinalResults(execLoop);
        assertFalse("Waited one second but no result have been submitted", 0==final_result_count);
        execLoop.handleResults();

        execLoop.handleActivities();
        assertEquals(0, execLoop.activities.size());
        assertEquals(Status.RUNNING, process.state);
        
        assertEquals(Status.OK, process.activityContaining("cat-1").state);
        assertEquals(Status.OK, process.activityContaining("sleep-1").state);
        assertEquals(Status.RUNNING, process.activityContaining("cat-2").state);
        // execute the last one: cat-2
    	final_result_count = waitForFinalResults(execLoop);
    	assertFalse("Waited one second but no result have been submitted", 0==final_result_count);
    	execLoop.handleResults();

    	assertEquals(Status.OK, process.activityContaining("cat-2").state);
        Result result = process.getResult();
        assertEquals(Status.OK, result.status);
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-1"));
        assertEquals(ProgramStatus.OK, result.getExecStatus("sleep-1"));
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-2"));
	}
	

	/**
	 * tests that a single file can be redirected to multiple inputs of the same program
	 * @throws Exception
	 */
	@Test
	public void ok_multiple_branches_3() throws Exception
	{
		Process process = initTestProcess("ok_multiple-branches_3");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();

        execLoop.handleActivities(); // should trigger the execution of the workflow
        // now wait for the result to come back
        int i;
        for (i=0; i<10; i++)
        	if (execLoop.results.isEmpty())
        		Thread.sleep(100);
        if (i==10)
        	assertFalse("Waited one second but no result have been submitted", execLoop.results.isEmpty());
        execLoop.handleResults();

        execLoop.handleActivities();
        assertEquals(0, execLoop.activities.size());
        assertEquals(Status.OK, process.state);
        
        assertEquals(Status.OK, process.activityContaining("cat-1").state);
    	execLoop.handleResults();

        Result result = process.getResult();
        assertEquals(Status.OK, result.status);
        assertEquals(ProgramStatus.OK, result.getExecStatus("cat-1"));
	}

	/**
	 * Submitting an empty workflow should end with an ERROR status
	 */
	@Test
	public void empty_workflow() throws Exception
	{
		Process process = initTestProcess("empty_workflow");
		execLoop.add(process);
        execLoop.handleSubmittedProcesses();
        assertEquals(0, execLoop.activities.size());
        assertEquals(0, execLoop.results.size());
        assertEquals(Status.ERROR, process.getState());
        Result result = process.getResult();
        assertEquals(Status.ERROR, result.status);
        assertNotNull(result.zipFile); // not null, but empty
        try (ZipFile zipFile = new ZipFile(result.zipFile)) {
        	assertEquals(0, zipFile.size());
        }
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		org.junit.runner.JUnitCore.runClasses(ExecutionLoopTest.class);
	}
	
}
