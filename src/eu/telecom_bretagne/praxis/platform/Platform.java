/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.platform;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.jdom.Document;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.ReleaseInfo;
import eu.telecom_bretagne.praxis.common.Utile;
import eu.telecom_bretagne.praxis.common.events.CommunicationFacade;
import eu.telecom_bretagne.praxis.common.events.Event;
import eu.telecom_bretagne.praxis.platform.execution.PlatformExecution;
import eu.telecom_bretagne.praxis.server.execution.ExecutionEngine;

/**
 */
public class Platform extends AbstractPlatform
{
	/**
	 * Stores the registered executions. This is a {@link ConcurrentHashMap}
	 * since it may be read & updated by two threads at a time (platform & "strategie")
	 */
	private ConcurrentHashMap <File, PlatformExecution> executions = new ConcurrentHashMap<File, PlatformExecution>();
	
	/**
	 * Creates a new Platform connected to a server using the supplied connection.
	 * @param cnx
	 *            an established connection to the server
	 * @param configurationFile
	 *            the configuration file for this platform
	 * @throws RuntimeException
	 *             if the underlying OS is not supported, or if the configuration file is invalid. When an exception
	 *             is raised, the supplied connection is automatically {@link CommunicationFacade#disconnect()
	 *             disconnected}.
	 */
	public Platform(CommunicationFacade cnx, String configurationFile)
	{
		super(cnx, initConfiguration(configurationFile));
	}
	
	/**
	 * @param cnx
	 *            an established connection to the server
	 * @param configuration
	 *            the configuration for this platform
	 * @throws RuntimeException
	 *             if the underlying OS is not supported, or if the configuration is invalid. When an exception is
	 *             raised, the supplied connection is automatically {@link CommunicationFacade#disconnect()
	 *             disconnected}.
	 */
	public Platform(CommunicationFacade cnx, Document configuration)
	{
		super(cnx, configuration);
	}

	/**
	 * Ajouter une nouvelle execution dans la plateform
	 * 
	 * @param key L'execution est stockée sous une clé qui represent son repertoire d'execution
	 * @param execution Le modelé d'execution
	 */
	public void registerExecution(File key, PlatformExecution execution)
	{
		this.executions.put(key, execution);
	}
	
	/**
	 * Obtenir une execution depuis son repertoire d'execution
	 * 
	 * @param key Le repertoire d'execution pour la strategie d'execution
	 * @return L'instance d'execution stockée sous la plateforme
	 */
	public PlatformExecution getExecution(File key)
	{
		return executions.get(key);
	}
	
	/**
	 * Removes an execution from the list of currently running executions.
	 * 
	 * @param key le repertoire d'execution de l'execution
	 * @return true if the 
	 */
	public synchronized boolean removeExecution(File key)
	{
		PlatformExecution execution = executions.remove(key);
		if (execution==null)
			return false;
		
		execution.cancel();
		return true;
	}
	
	/**
	 * Verifier si il y a une execution dans le repertoire cle d'une execution
	 * 
	 * @param cle -
	 *          le repertoire clé d'une execution
	 * @return - true si l'execution est en train de executer, sinon false
	 */
	public synchronized boolean executesWorkflow(File cle)
	{
		return executions.containsKey(cle);
	}
		
	/* **** ServerToPlatformEventListener **** */
	public void serverCancelsExecution(Event event) {
		eu.telecom_bretagne.praxis.common.Utile.unimplemented();
		/*
		File executionRepertoire = null;
		if (!removeExecution(executionRepertoire))
			Log.log.warning("No execution exists for: "+(String)event.data);
		else
			Log.log.info("Execution stopped: "+executionRepertoire.getAbsolutePath());
		*/
	}
	
	public void serverRequestsExecution(Event event) {
		ExecutionEngine executionEngine = (ExecutionEngine) event.data; 
		File execDir = executionEngine.getExecutionDirectory(); // TODO check null value
		
		File zipFile = new File(execDir, "script.zip");
		if (event.file_conveyor!=null)
		{
			Utile.renameFile(event.file_conveyor, zipFile);
		}
		PlatformExecution exec = new PlatformExecution(this, executionEngine, zipFile);
		registerExecution(execDir, exec);
		exec.start();    
	}
	
	public static Platform launchPlatform(String platform_confID, String[] connection) throws IOException
	{
		String configurationFile = Configuration.get("platform."+platform_confID);
		Document conf = initConfiguration(configurationFile);
		return launchPlatform(conf, connection);
	}
	
	public static Platform launchPlatform(Document configuration, String[] connection) throws IOException
	{
		CommunicationFacade cnx = CommunicationFacade.buildConnection("Platform", connection);
		cnx.start();
		return new Platform(cnx, configuration);
	}
	
	public static void launch() throws IOException
	{
		// Get the platforms to launch...
		String[] platforms = Configuration.getArray("platforms");

		// ...and the appropriate connection
		String[] conn = Utile.serverForString(Configuration.get("platform.connection"));

		for (String p:platforms)
			if (!"".equals(p))
				launchPlatform(p, conn);
	}

	/**
	 * Launches a new platform and connects it to a given server.
	 */
	public static void main(String[] args) throws IOException
	{
		//String usage = "Usage:\n       --version   output version information and exit";
		if (args.length>0 && "--version".equals(args[0]))
		{
			System.out.printf("%s", (Object[])ReleaseInfo.getVersionInfo());
			System.exit(0);
		}

		launch();
	}
	
}
