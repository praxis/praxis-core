/* License: please refer to the file README.license located at the root directory of the project */
package eu.telecom_bretagne.praxis.common.events;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMISocketFactory;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import eu.telecom_bretagne.praxis.common.Configuration;
import eu.telecom_bretagne.praxis.common.Log;
import eu.telecom_bretagne.praxis.core.resource.ResourceRepository;
import eu.telecom_bretagne.praxis.server.Server;


/**
 * A implementation of {@link CommunicationFacade} using RMI.
 * @author Sébastien Bigaret
 */
public class RMICommunicationFacade
    extends CommunicationFacade
{
	/**
	 * This is the interface on which the RMICommunicationFacade is based to internally handle the events sent between
	 * the server and the clients. Classes implementing this interface must order events FIFO (First-In-First-Out).
	 * @author Sébastien Bigaret
	 */
	public static interface RMIConnection extends Remote, Serializable
	{
		/**
		 * Sends an event to the other side.  Events are ordered in a FIFO (first-in-first-out) manner, they are retrieved
		 * in the same order as sent.
		 * @param event the event to be sent
		 * @throws RemoteException if the connection has been interrupted for any reason.
		 * @see #retrieveEvent()
		 */
		public void sendEvent(byte[] serialized_event) throws RemoteException;
		
		/**
		 * Retrieves an event sent on the other side. If no event is available, the method waits at most
		 * {@link #RETRIEVE_EVENT_DELAY} seconds before returning; when the delay is over, if no event is available, a
		 * <code>null</code> value is returned. Events are ordered in a FIFO (first-in-first-out) manner, they are
		 * retrieved in the same order as sent.
		 * @return the serialized event, as returned by {@link Event#writeTo(java.io.OutputStream)}
		 * @throws RemoteException
		 * @see {@link #sendEvent(Event)}
		 */
		public byte[] retrieveEvent() throws RemoteException;		
	}
	/**
	 * A simple class representing the connection to the server. When a client/platform {@link RMIServer#connect()
	 * connects} to the server, it gets a remote reference (<i>stub</i>) of a newly created instance of this class
	 * that holds a reference to the corresponding RMICommunicationFacade object on the server-side.
	 * @author Sébastien Bigaret
	 */
	protected static class CNX
	    extends UnicastRemoteObject
	    implements RMIConnection, Unreferenced
	{
		private static final long serialVersionUID = -4924686983466819101L;
		/**
		 * The communication facade on the server-side.
		 */
		protected transient RMICommunicationFacade serverSide_commFacade;
		
		public static CNX buildCNX(RMICommunicationFacade cnx, boolean useSSL) throws RemoteException
		{
			if (useSSL)
				return new CNX(cnx, "value ignored, just use SSL");
			return new CNX(cnx);
		}
		protected CNX(RMICommunicationFacade cnx) throws RemoteException
		{
			super(Configuration.RMI_REGISTRY_PORT);
			serverSide_commFacade = cnx;
		}
		
		protected CNX(RMICommunicationFacade cnx, String unused_this_is_just_the_constructor_for_ssl) throws RemoteException
		{
			super(Configuration.RMI_REGISTRY_PORT, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
			serverSide_commFacade = cnx;
		}
		
		/**
		 * Simply forwards the message to the {@link #serverSide_commFacade}
		 */
		@Override
		public byte[] retrieveEvent() throws RemoteException
		{
			return serverSide_commFacade.retrieveEvent();
		}
		
		/**
		 * Simply forwards the message to the underlying comm
		 */
		@Override
		public void sendEvent(byte[] serialized_event) throws RemoteException
		{
			Event event = null;
			try
			{
				event = Event.readFrom(new ByteArrayInputStream(serialized_event));
			}
			catch (IOException | ClassNotFoundException e)
			{
				throw new RemoteException("Could not reconstruct the sent event", e);
			}
			serverSide_commFacade.handle_event(event);
		}
		
		@Override
		public void unreferenced()
		{
			Log.log.finer("Unreferenced");
			serverSide_commFacade.disconnect();
		}
		
	}
	
	/**
	 * Interface for the object acting as the "RMI Server" for the communication between clients/platforms and the
	 * server.
	 * @author Sébastien Bigaret
	 *
	 */
	public static interface RMIServerInterface
	    extends Remote
	{
		/**
		 * Creates and returns a remote object used by clients' RMICommunicationFacade to communicate with the server.
		 * @return a remote object exported through RMI
		 * @throws RemoteException
		 */
		public RMIConnection connect() throws RemoteException;
	}
	
	/**
	 * 
	 * @author Sébastien Bigaret
	 *
	 */
	protected static class RMIServer
	    extends UnicastRemoteObject
	    implements RMIServerInterface
	{
		private static final long serialVersionUID = 8305600074094392467L;

		public static RMIServer buildRMIServer(boolean useSSL) throws RemoteException
		{
			if ( useSSL )
				return new RMIServer("value ignored, just use SSL");
			return new RMIServer();
		}
		protected RMIServer() throws RemoteException
		{
			super(Configuration.RMI_REGISTRY_PORT);
		}
		protected RMIServer(String unused_this_is_just_the_constructor_for_ssl) throws RemoteException
		{
			super(Configuration.RMI_REGISTRY_PORT, new SslRMIClientSocketFactory(), new SslRMIServerSocketFactory());
		}

		public CNX connect() throws RemoteException
		{
			String clientIP;
			try
			{
				clientIP = java.rmi.server.RemoteServer.getClientHost();
			}
			catch (ServerNotActiveException e)
			{
				clientIP = null;
			}
			RMICommunicationFacade c = new RMICommunicationFacade(clientIP);
			new Server(c);
			return CNX.buildCNX(c, Configuration.getBoolean("rmi.useSSL"));
		}
	}
	
	/**
	 * Defines the maximum delay (in seconds) that passes before the method {@link #retrieveEvent()} returns.
	 */
	public static final int RETRIEVE_EVENT_DELAY = 10;

	/**
	 * Name under which the RMIServer is registered into the RMI registry
	 * @see RMICommunicationFacade#launchServer()
	 */
	public static final String       RMI_CNX_SERVER_NAME = "PraxisRMICnxServer";
	
	/**
	 * On the client/platform side, this is the server as returned by {@link RMIServer#connect()}. On the server side,
	 * the object is null.
	 * @see #onTheServerSide()
	 */
	protected final RMIConnection    clientCnxToServer;
	
	/**
	 * On the server-side, events are stored in this list, accessed through {@link #sendEvent(Event)}
	 * {@link #retrieveEvent()} in FIFO order.
	 */
	protected final ArrayList<Event> events              = new ArrayList<Event>();
	
	protected final Semaphore        events_semaphore    = new Semaphore(0);
	
	/**
	 * The client's IP on the server side. On the client side, it equals to null.
	 */
	protected final String           clientIP;
	
	
	/**
	 * Controls whether the RMI connection created by {@link #RMICommunicationFacade(String, int)} should be tunnelled
	 * in a HTTP tunnel or not. It only applies to the client-side.
	 */
	public final boolean             useHTTP;
	
	/**
	 * Registers a RMIServer into the rmi registry under the name {@value #RMI_CNX_SERVER_NAME}.
	 */
	public static void launchServer()
	{
		try
		{
			RMIServer server = RMIServer.buildRMIServer(Configuration.getBoolean("rmi.useSSL"));
			
			ResourceRepository.rmiRegistry.rebind(RMI_CNX_SERVER_NAME, server);
			Log.log.info("RMIServer registered");
		}
		catch (Exception e)
		{
			e.printStackTrace(); // TODO
		}
	}
	
	/**
	 * Constructor to be called on the server-side
	 * @param clientIP
	 */
	public RMICommunicationFacade(String clientIP)
	{
		super("RMI client " + clientIP);
		this.clientCnxToServer = null;
		this.clientIP = clientIP;
		this.useHTTP = false; // not used on the server-side
	}
	
	/**
	 * Constructor to be called on the client/platform side:
	 * @param hostname
	 * @param portNumber
	 * @throws IOException if the connection to the server fails
	 */
	public RMICommunicationFacade(String name, String hostname, int portNumber, boolean useHTTP) throws IOException
	{
		super(name);
		this.clientIP = null;
		this.useHTTP = useHTTP; // cf. rmi.useHTTP in run_configuration.xml ?
		final boolean useSSL = Configuration.getBoolean("rmi.useSSL");
		try
		{
			// The following directly enables HTTP-to-CGI.
			// One use this to bypass the standard mechanism which can take some time to realize that some factories
			// fail (waiting for timeouts for example).
			// Note that this is the latest behaviour tried by the standard RMI mechanism; previous one is
			// HTTP-to-PORT
			// (sun.rmi.transport.proxy.RMIHttpToPortSocketFactory)
			if (useHTTP)
			{
				// TODO: test that the java property proxyHost IS set
				if (RMISocketFactory.getSocketFactory() != null)
					Log.log.warning("Overriding a non-null RMISocketFactory: " + RMISocketFactory.getSocketFactory());
				/*FIX sun.rmi*/RMISocketFactory.setSocketFactory(new sun.rmi.transport.proxy.RMIHttpToCGISocketFactory());
			}
			Registry registry;
			if (useSSL)
				registry = LocateRegistry.getRegistry(hostname, portNumber, new SslRMIClientSocketFactory());
			else
				registry = LocateRegistry.getRegistry(hostname, portNumber);
			RMIServerInterface rmiServer = (RMIServerInterface) registry.lookup(RMI_CNX_SERVER_NAME);// (RMIServerInterface)Naming.lookup(rmiServerURL);
			// String ipClient = InetAddress.getLocalHost().getHostAddress();
			
			this.clientCnxToServer = rmiServer.connect();
		}
		catch (Exception e)
		{
			final StringBuilder err = new StringBuilder();
			err.append("Connection to RMI Registry ").append(hostname).append(":").append(portNumber).append(" failed");
			err.append(useSSL ? " (ssl)" : "");
			Log.log.log(Level.SEVERE, err.toString(), e);
			throw new IOException(err.toString(), e);
		} 
	}
	
	/**
	 * Tells whether we're on the server-side
	 * @return true if it is called on the server-side, false otherwise
	 */
	protected boolean onTheServerSide()
	{
		return clientCnxToServer == null;
	}
	
	@Override
	public boolean isConnected()
	{
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public void send(Event evt)
	{
        Log.log.finest(()->"Sending "+evt+" on RMI");
		/*
		 * The situation is not symmetrical
		 */
		if (onTheServerSide())
			sendFromServer(evt);
		else
			sendToServer(evt);
	}

	protected void sendFromServer(Event event)
	{
		synchronized(events)
		{
			if (event instanceof ServerToClientEvent)
			{
				/*
				 * If this is a update for the execution status of a workflow, invalidate all previous ones in the
				 * queue: the last one contains the latest information for that workflow.
				 * Invalidating means replacing it by null: we do not want to remove the element, because the
				 * number of permits of the semaphore should be equal to the number of events available.
				 */
				final ServerToClientEvent evt = (ServerToClientEvent) event;
				if ( evt.type == ServerToClientEvent.Type.EXECUTION_STATUS )
				{
					for (int i=0; i<events.size(); i++)
					{
						final ServerToClientEvent e = (ServerToClientEvent) events.get(i);
						if ( e == null )
							continue;
						if (e.type==ServerToClientEvent.Type.EXECUTION_STATUS && e.workflowID.equals(evt.workflowID))
							events.set(i, null);
					}
				}
			}
			events.add(event);
		}
		events_semaphore.release();
	}
	
	protected void sendToServer(Event event)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
        {
	        event.writeTo(baos);
	        // baos.close(); // useless: ByteArrayOutputStream.close() does nothing
        }
        catch (IOException e)
        {
        	Log.log.log(Level.SEVERE, "Unable to serialized a retrieved event", e);
        	return; // TODO
        }
		try
		{
			clientCnxToServer.sendEvent(baos.toByteArray());
		}
		catch (RemoteException e)
		{
			Log.log.log(java.util.logging.Level.SEVERE, "distant call failed", e);
		}
        eventSent(event);
	}
	
	/**
	 * Returns the next event to be processed. If no event is available, the method waits at most
	 * {@link #RETRIEVE_EVENT_DELAY} seconds before returning; when the delay is over, if no event is available, a
	 * <code>null</code> value is returned.
	 * @return the next event to be processed, or <code>null</code> if there is no such event.
	 * @throws RemoteException
	 */
	public byte[] retrieveEvent() throws RemoteException
	{
		Event event = null;
		while (event == null)
		{ /* events in the queue may be null because they were invalidated by sendFromServer */
			try
			{
				Log.log.finest(()->"Waiting events "+this);
				// Wait a reasonable time, and return if nothing is available.
				/*
				 * IMPORTANT: using acquireUninterruptibly() here is a very bad idea. The method here is called remotely
				 * by RMI; hence, when the client disconnects or disappears, the corresponding thread waits forever and
				 * never exits. Not only this adds a thread that never exits each time a client connects, but in
				 * configurations where RMI makes use of HTTP tunnelling through apache+tomcat, it blocks a worker
				 * forever: as a result, when tomcat runs out of threads, the server becomes unresponsive.
				 */
				if (!events_semaphore.tryAcquire(RETRIEVE_EVENT_DELAY, TimeUnit.SECONDS))
					return null;
			}
			catch (InterruptedException e)
			{
				return null;
			}
			synchronized(events)
			{
				// IndexOutOfBounds will not happen: events are added before the semaphore is released
				event = events.remove(0);
			}
		}
		Log.log.info("event: "+event.toString()+" w/ file size: "+(event.file_conveyor!=null?event.file_conveyor.length():-1));
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
        {
	        event.writeTo(baos);
	        // baos.close(); // useless: ByteArrayOutputStream.close() does nothing
        }
        catch (IOException e)
        {
        	Log.log.log(Level.SEVERE, "Unable to serialized an event: "+event, e);
        	throw new RemoteException("Couldn't serialize an event");
        }
		
		// This is the reason why we serialize the event by hand, instead of letting RMI do it automatically: we
		// need to mark the event as sent... If the attached callback does some cleanup, such as deleting the attached
		// file, the event could not be sent with its file!

		eventSent(event);
		
		return baos.toByteArray();
	}
	
	/**
	 * The main method of the thread running on the client side. It waits for the client to connect, then it polls the
	 * server for incoming event. The thread exits when the client disconnects.
	 */
	@Override
	public void run()
	{
		while (!isConnected())
			synchronized(this)
			{
				try
				{
					wait(1);
				}
				catch (InterruptedException e)
				{ Log.exception.log(Level.INFO, "Interrupted", e); }
			}
		Log.log.info("Client is connected: thread starts polling the server for events");
		while (isConnected())
		{
			Event event;
			byte[] serialized_event = null;
			try
			{
				serialized_event = clientCnxToServer.retrieveEvent();
				/* wait a little, if and only if we did not receive anything */
				try { if (serialized_event==null) Thread.sleep(500); }
				catch (InterruptedException e) { Log.exception.log(Level.INFO, "Interrupted", e); }
			}
			catch (Exception e)
			{
				/* Not RemoteException only: e.g. OutOfMemoryException may happen on the client-side */
				Log.log.log(Level.WARNING, "Could not retrieve an event", e);
				continue;
			}
			
			if ( serialized_event==null )
				// retrieveEvent() returns null when there is no event waiting in the queue
				continue;
			try
            {
	            event = Event.readFrom(new ByteArrayInputStream(serialized_event));
				handle_event(event);
            }
            catch (IOException | ClassNotFoundException e)
            {
                Log.log.log(Level.SEVERE, "", e);
            }
		}
		Log.log.info("Client is disconnected: thread stops polling the server for events and exits");
	}
	
}
